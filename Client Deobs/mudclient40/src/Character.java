// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 


public class Character {

    public long hash;
    public String name;
    public int serverIndex;
    public int br;
    public int currentX;
    public int currentY;
    public int npcId;
    public int stepCount;
    public int animationCurrent;
    public int animationNext;
    public int movingStep;
    public int waypointCurrent;
    public int waypointsX[];
    public int waypointsY[];
    public int mr[];
    public String message;
    public int messageTimeout;
    public int pr;
    public int bubbleTimeout;
    public int rr;
    public int healthCurrent;
    public int healthMax;
    public int combatTimer;
    public int attackable;
    public int level;
    public int xr;
    public int yr;
    public int colourBottom;
    public int as;
    public int incomingProjectileSprite;
    public int attackingPlayerServerIndex;
    public int attackingNpcServerIndex;
    public int projectileRange;
    public boolean fs;
    public int gs;
    public int hs;
    public Character() {
        waypointsX = new int[10];
        waypointsY = new int[10];
        mr = new int[12];
        level = -1;
        fs = false;
        gs = -1;
    }
}
