// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 

package jagex.client;

import java.awt.*;

// Referenced classes of package jagex.client:
//            k

public class GameFrame extends Frame {

    int width;
    int height;
    int translate;
    int yoff;
    GameApplet gameApplet;
    Graphics graphics;

    public GameFrame(GameApplet gameApplet, int width, int height, String title, boolean resizable, boolean appletMode) {
        yoff = 28;
        this.width = width;
        this.height = height;
        this.gameApplet = gameApplet;
        if (appletMode)
            yoff = 48;
        else
            yoff = 28;
        setTitle(title);
        setResizable(resizable);
        show();
        toFront();
        resize(this.width, this.height);
        graphics = getGraphics();
    }

    public Graphics getGraphics() {
        Graphics g = super.getGraphics();
        if (translate == 0)
            g.translate(0, 24);
        else
            g.translate(-5, 0);
        return g;
    }

    public void resize(int i, int j) {
        super.resize(i, j + yoff);
    }

    public int getWidth() {
        return size().width;
    }

    public int getHeight() {
        return size().height - yoff;
    }

    public boolean handleEvent(Event event) {
        if (event.id == 401)
            gameApplet.keyDown(event, event.key);
        else if (event.id == 402)
            gameApplet.keyUp(event, event.key);
        else if (event.id == 501)
            gameApplet.mouseDown(event, event.x, event.y - 24);
        else if (event.id == 506)
            gameApplet.mouseDrag(event, event.x, event.y - 24);
        else if (event.id == 502)
            gameApplet.mouseUp(event, event.x, event.y - 24);
        else if (event.id == 503)
            gameApplet.mouseMove(event, event.x, event.y - 24);
        else if (event.id == 201)
            gameApplet.destroy();
        else if (event.id == 1001)
            gameApplet.action(event, event.target);
        else if (event.id == 403)
            gameApplet.keyDown(event, event.key);
        else if (event.id == 404)
            gameApplet.keyUp(event, event.key);
        return true;
    }

    public final void paint(Graphics g) {
        gameApplet.paint(g);
    }
}
