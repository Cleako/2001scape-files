import jagex.client.*;
import jagex.o;
import java.awt.*;
import java.io.IOException;

public class Client extends LoginHandler {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6802374422422671179L;

	public static void main(String[] as) {
		Client mudclient1 = new Client();
		mudclient1.yt = false;
		mudclient1.ui(mudclient1.bu, mudclient1.cu + 22, "Runescape by Andrew Gower", false);
		mudclient1.lq = 10;
	}

	public void yi() {
		if (yt) {
			String address = getDocumentBase().getHost().toLowerCase();
			if (!address.endsWith("jagex.com") && !address.endsWith("jagex.co.uk") && !address.endsWith("jagex.superb.net")
					&& !address.endsWith("207.228.231.226") && !address.endsWith("runescape.com") && !address.endsWith("runescape.co.uk")
					&& !address.endsWith("64.23.60.47") && !address.endsWith("penguin.local")
					&& !address.endsWith("jagex.dnsalias.com")) {
				nt = true;
				return;
			}
		}
		super.port = 43594;
		super.rp = -11;
		LoginHandler.vc = 500;
		LoginHandler.uc = false;
		LoginHandler.xc = 6;
		loadConfigs();
		az = 2000;
		zy = az + 100;
		nw = zy + 50;
		vz = nw + 300;
		ot = getGraphics();
		lj(50);
		qt = new m(bu, cu + 12, 2600, this);
		qt.is = this;
		qt.kf(0, 0, bu, cu + 12);
		g.dg = false;
		g.eg = zy;
		ecb = new g(qt, 5);
		int i1 = ((GameImageProducer) (qt)).sj - 199;
		byte byte0 = 36;
		fcb = ecb.nc(i1, byte0 + 24, 196, 90, 1, 500, true);
		hcb = new g(qt, 5);
		icb = hcb.nc(i1, byte0 + 40, 196, 126, 1, 500, true);
		loadMedia();
		loadEntities(true);
		pt = new j(qt, 15000, 15000, 1000);
		pt.yh(bu / 2, cu / 2, bu / 2, cu / 2, bu, du);
		pt.dm = 2400;
		pt.em = 2400;
		pt.fm = 1;
		pt.gm = 2300;
		pt.xh(-50, -10, -50);
		cv = new p(pt, qt);
		cv.ydb = az;
		loadTextures();
		loadModels();
		loadMaps();
		gj(100, "Starting game...");
		hl();
		sendRegisterInterface();
		createCharacter();
		vl();
		sj();
	}

	public void loadConfigs() {
		if (qj()) {
			byte[] abyte0 = null;
			try {
				abyte0 = wi("config" + Versions.ConfigsVersion + ".jag", "Configuration", 10);
			} catch (IOException ioexception) {
				System.out.println("Load error:" + ioexception);
			}
			Definitions.oo(abyte0);
		} else {
			hj(10, "Loading configuration");
			Definitions.uo();
		}
	}

	public void loadMedia() {
		if (qj()) {
			byte[] abyte0 = null;
			try {
				abyte0 = wi("media" + Versions.MediaVersion + ".jag", "2d graphics", 20);
			} catch (IOException ioexception) {
				System.out.println("Load error:" + ioexception);
			}
			qt.eg(abyte0, o.mm("inv1.tga", abyte0), az, true, false);
			qt.cg(abyte0, o.mm("inv2.tga", abyte0), az + 1, true, 1, 6, false);
			qt.eg(abyte0, o.mm("bubble.tga", abyte0), az + 9, true, false);
			qt.eg(abyte0, o.mm("runescape.tga", abyte0), az + 10, true, false);
			qt.jg(abyte0, o.mm("splat.tga", abyte0), az + 11, true, 3, false);
			qt.cg(abyte0, o.mm("icon.tga", abyte0), az + 14, true, 4, 2, false);
			qt.eg(abyte0, o.mm("hbar.tga", abyte0), az + 22, false, false);
			qt.eg(abyte0, o.mm("hbar2.tga", abyte0), az + 23, true, false);
			qt.eg(abyte0, o.mm("compass.tga", abyte0), az + 24, true, false);
			qt.jg(abyte0, o.mm("scrollbar.tga", abyte0), zy, true, 2, false);
			qt.jg(abyte0, o.mm("corners.tga", abyte0), zy + 2, true, 4, false);
			qt.jg(abyte0, o.mm("arrows.tga", abyte0), zy + 6, true, 2, false);
			rt = bj(o.lm("hbar.tga", 0, abyte0));
			int i1 = Definitions.ufb;
			for (int k1 = 1; i1 > 0; k1++) {
				int i2 = i1;
				i1 -= 30;
				if (i2 > 30)
					i2 = 30;
				qt.cg(abyte0, o.mm("objects" + k1 + ".tga", abyte0), nw + (k1 - 1) * 30, true, 10, (i2 + 9) / 10,
						false);
			}

			qt.jg(abyte0, o.mm("projectile.tga", abyte0), vz, true, Definitions.hjb, false);
			return;
		}
		byte[] abyte1 = new byte[0x186a0];
		gj(20, "Loading 2d graphics");
		try {
			o.jm("../gamedata/media/inv1.tga", abyte1, 0x186a0);
			qt.eg(abyte1, 0, az, true, false);
			o.jm("../gamedata/media/inv2.tga", abyte1, 0x186a0);
			qt.cg(abyte1, 0, az + 1, true, 1, 6, false);
			o.jm("../gamedata/media/bubble.tga", abyte1, 0x186a0);
			qt.eg(abyte1, 0, az + 9, true, false);
			o.jm("../gamedata/media/runescape.tga", abyte1, 0x186a0);
			qt.eg(abyte1, 0, az + 10, true, false);
			o.jm("../gamedata/media/splat.tga", abyte1, 0x186a0);
			qt.jg(abyte1, 0, az + 11, true, 3, false);
			o.jm("../gamedata/media/icon.tga", abyte1, 0x186a0);
			qt.cg(abyte1, 0, az + 14, true, 4, 2, false);
			o.jm("../gamedata/media/hbar.tga", abyte1, 0x186a0);
			qt.eg(abyte1, 0, az + 22, false, false);
			rt = bj(abyte1);
			o.jm("../gamedata/media/hbar2.tga", abyte1, 0x186a0);
			qt.eg(abyte1, 0, az + 23, true, false);
			o.jm("../gamedata/media/compass.tga", abyte1, 0x186a0);
			qt.eg(abyte1, 0, az + 24, true, false);
			o.jm("../gamedata/media/scrollbar.tga", abyte1, 0x186a0);
			qt.jg(abyte1, 0, zy, true, 2, false);
			o.jm("../gamedata/media/corners.tga", abyte1, 0x186a0);
			qt.jg(abyte1, 0, zy + 2, true, 4, false);
			o.jm("../gamedata/media/arrows.tga", abyte1, 0x186a0);
			qt.jg(abyte1, 0, zy + 6, true, 2, false);
			int j1 = Definitions.ufb;
			for (int l1 = 1; j1 > 0; l1++) {
				int j2 = j1;
				j1 -= 30;
				if (j2 > 30)
					j2 = 30;
				o.jm("../gamedata/media/objects" + l1 + ".tga", abyte1, 0x186a0);
				qt.cg(abyte1, 0, nw + (l1 - 1) * 30, true, 10, (j2 + 9) / 10, false);
			}

			o.jm("../gamedata/media/projectile.tga", abyte1, 0x186a0);
			qt.jg(abyte1, 0, vz, true, Definitions.hjb, false);
		} catch (IOException _ex) {
			System.out.println("ERROR: in raw media loader");
		}
	}

	public void loadEntities(boolean flag) {
		iw = 0;
		jw = iw;
		byte[] abyte0 = null;
		if (qj() && flag) {
			String s = "entity" + Versions.EntitiesVersion + ".jag";
			try {
				abyte0 = wi(s, "people and monsters", 30);
			} catch (IOException ioexception) {
				System.out.println("Load error:" + ioexception);
			}
		} else {
			gj(30, "Loading people and monsters");
		}
		int i1 = 0;
		label0: for (int j1 = 0; j1 < Definitions.shb; j1++) {
			String s1 = Definitions.uhb[j1];
			for (int k1 = 0; k1 < j1; k1++) {
				if (!Definitions.uhb[k1].equalsIgnoreCase(s1))
					continue;
				Definitions.zhb[j1] = Definitions.zhb[k1];
				continue label0;
			}

			if (flag)
				if (qj()) {
					boolean flag1 = true;
					if (Definitions.whb[j1] != 0)
						flag1 = false;
					qt.jg(o.lm(s1 + ".tga", 0, abyte0), 0, jw, true, 15, flag1);
					i1 += 15;
					if (Definitions.xhb[j1] == 1) {
						qt.jg(o.lm(s1 + "a.tga", 0, abyte0), 0, jw + 15, true, 3, true);
						i1 += 3;
					}
					if (Definitions.yhb[j1] == 1) {
						qt.jg(o.lm(s1 + "f.tga", 0, abyte0), 0, jw + 18, true, 9, true);
						i1 += 9;
					}
				} else {
					try {
						byte[] abyte1 = new byte[0x493e0];
						o.jm("../gamedata/entity/" + s1 + ".tga", abyte1, 0x493e0);
						i1 += 15;
						boolean flag2 = true;
						if (Definitions.whb[j1] != 0)
							flag2 = false;
						qt.jg(abyte1, 0, jw, true, 15, flag2);
						if (Definitions.xhb[j1] == 1) {
							o.jm("../gamedata/entity/" + s1 + "a.tga", abyte1, 0x493e0);
							i1 += 3;
							qt.jg(abyte1, 0, jw + 15, true, 3, true);
						}
						if (Definitions.yhb[j1] == 1) {
							o.jm("../gamedata/entity/" + s1 + "f.tga", abyte1, 0x493e0);
							i1 += 9;
							qt.jg(abyte1, 0, jw + 18, true, 9, true);
						}
					} catch (IOException _ex) {
						System.out.println("ERROR: in raw entity loader - no:" + j1 + " " + s1);
					}
				}
			Definitions.zhb[j1] = jw;
			jw += 27;
		}

		System.out.println("Loaded: " + i1 + " frames of animation");
	}

	public void loadTextures() {
		if (qj()) {
			pt.yg("textures" + Versions.TexturesVersion + ".jag", 7, 11, 50, this);
		} else {
			gj(50, "Loading textures");
			pt.gi("../gamedata/textures");
		}
	}

	public void loadModels() {
		Definitions.mo("torcha2");
		Definitions.mo("torcha3");
		Definitions.mo("torcha4");
		Definitions.mo("skulltorcha2");
		Definitions.mo("skulltorcha3");
		Definitions.mo("skulltorcha4");
		Definitions.mo("firea2");
		Definitions.mo("firea3");
		if (qj()) {
			byte[] abyte0 = null;
			try {
				abyte0 = wi("models" + Versions.ModelsVersion + ".jag", "3d models", 70);
			} catch (IOException ioexception) {
				System.out.println("Load error:" + ioexception);
			}
			for (int j1 = 0; j1 < Definitions.lkb; j1++) {
				int k1 = o.mm(Definitions.mkb[j1] + ".ob2", abyte0);
				if (k1 != 0)
					zw[j1] = new h(abyte0, k1);
				else
					zw[j1] = new h(1, 1);
			}

			return;
		}
		gj(70, "Loading 3d models");
		for (int i1 = 0; i1 < Definitions.lkb; i1++)
			zw[i1] = new h("../gamedata/models/" + Definitions.mkb[i1] + ".ob2");

	}

	public void loadMaps() {
		if (qj()) {
			cv.geb = null;
			try {
				cv.geb = wi("maps" + Versions.MapsVersion + ".jag", "map", 90);
			} catch (IOException ioexception) {
				System.out.println("Load error:" + ioexception);
			}
		} else {
			cv.udb = false;
		}
	}

	public void hl() {
		kz = new g(qt, 10);
		lz = kz.bc(5, 269, 502, 56, 1, 20, true);
		mz = kz.cc(7, 324, 498, 14, 1, 80, false, true);
		nz = kz.bc(5, 269, 502, 56, 1, 20, true);
		oz = kz.bc(5, 269, 502, 56, 1, 20, true);
		kz.lc(mz);
	}

	public void dj() {
		if (nt)
			return;
		eu++;
		if (ut == 0) {
			super.sp = 0;
			registerAccount();
		}
		if (ut == 1) {
			jt++;
			super.sp++;
			hm();
		}
		if (at > 0)
			at--;
		if (zs && at == 0) {
			zs = false;
			super.dd.m();
			at = 24;
		}
		super.pq = 0;
		super.rq = 0;
		if (gz > 0)
			gz--;
		if (hz > 0)
			hz--;
		if (iz > 0)
			iz--;
		if (jz > 0)
			jz--;
	}

	public void jj() {
		if (nt) {
			Graphics g1 = getGraphics();
			g1.setColor(Color.black);
			g1.fillRect(0, 0, 512, 356);
			g1.setFont(new Font("Helvetica", 1, 20));
			g1.setColor(Color.white);
			g1.drawString("Error - unable to load game!", 50, 50);
			g1.drawString("To play RuneScape make sure you play from", 50, 100);
			g1.drawString("http://www.runescape.com", 50, 150);
			lj(1);
			return;
		}
		if (ut == 0) {
			qt.vk = false;
			dl();
		}
		if (ut == 1) {
			qt.vk = true;
			sendDeathInterface();
		}
	}

	public void kj() {
		mb();
		if (qt != null) {
			qt.dg();
			qt.yj = null;
			qt = null;
		}
		if (pt != null) {
			pt.ni();
			pt = null;
		}
		zw = null;
		uw = null;
		dx = null;
		yv = null;
		zv = null;
		gw = null;
		hw = null;
		player = null;
		if (cv != null) {
			cv.xeb = null;
			cv.yeb = null;
			cv.zeb = null;
			cv.afb = null;
			cv = null;
		}
		System.gc();
	}

	public void sj() {
		ot.drawImage(rt, 0, 0, this);
	}

	public void rj(int i1) {
		if (ut == 0) {
			if (bz == 0)
				wz.ld(i1);
			if (bz == 1)
				xz.ld(i1);
			if (bz == 2)
				yz.ld(i1);
		}
		if (sbb)
			zz.ld(i1);
		if (ut == 1) {
			if (gt == 0 && ht == 0)
				kz.ld(i1);
			if (gt == 3 || gt == 4)
				gt = 0;
		}
	}

	public void ll() {
		super.dd.g();
		zs = true;
		ib();
	}

	public void vl() {
		ut = 0;
		bz = 0;
		cz = "";
		dz = "";
		ez = "Please enter a username:";
		fz = "*" + cz + "*";
		wv = 0;
		fw = 0;
	}

	public void bl() {
		super.vq = "";
		super.wq = "";
	}

	public void createCharacter() {
		zz = new g(qt, 100);
		zz.sendText(256, 10, "Design Your Character", 4, true);
		char c1 = '\214';
		int i1 = 34;
		zz.yc(c1, i1, 200, 25);
		zz.sendText(c1, i1, "Appearance", 4, false);
		i1 += 15;
		zz.sendText(c1 - 55, i1 + 110, "Front", 3, true);
		zz.sendText(c1, i1 + 110, "Side", 3, true);
		zz.sendText(c1 + 55, i1 + 110, "Back", 3, true);
		byte byte0 = 54;
		i1 += 145;
		zz.ic(c1 - byte0, i1, 53, 41);
		zz.sendText(c1 - byte0, i1 - 8, "Head", 1, true);
		zz.sendText(c1 - byte0, i1 + 8, "Type", 1, true);
		zz.jc(c1 - byte0 - 40, i1, g.eg + 7);
		yab = zz.id(c1 - byte0 - 40, i1, 20, 20);
		zz.jc((c1 - byte0) + 40, i1, g.eg + 6);
		zab = zz.id((c1 - byte0) + 40, i1, 20, 20);
		zz.ic(c1 + byte0, i1, 53, 41);
		zz.sendText(c1 + byte0, i1 - 8, "Hair", 1, true);
		zz.sendText(c1 + byte0, i1 + 8, "Color", 1, true);
		zz.jc((c1 + byte0) - 40, i1, g.eg + 7);
		abb = zz.id((c1 + byte0) - 40, i1, 20, 20);
		zz.jc(c1 + byte0 + 40, i1, g.eg + 6);
		bbb = zz.id(c1 + byte0 + 40, i1, 20, 20);
		i1 += 50;
		zz.ic(c1 - byte0, i1, 53, 41);
		zz.sendText(c1 - byte0, i1, "Gender", 1, true);
		zz.jc(c1 - byte0 - 40, i1, g.eg + 7);
		cbb = zz.id(c1 - byte0 - 40, i1, 20, 20);
		zz.jc((c1 - byte0) + 40, i1, g.eg + 6);
		dbb = zz.id((c1 - byte0) + 40, i1, 20, 20);
		zz.ic(c1 + byte0, i1, 53, 41);
		zz.sendText(c1 + byte0, i1 - 8, "Top", 1, true);
		zz.sendText(c1 + byte0, i1 + 8, "Color", 1, true);
		zz.jc((c1 + byte0) - 40, i1, g.eg + 7);
		ebb = zz.id((c1 + byte0) - 40, i1, 20, 20);
		zz.jc(c1 + byte0 + 40, i1, g.eg + 6);
		fbb = zz.id(c1 + byte0 + 40, i1, 20, 20);
		i1 += 50;
		zz.ic(c1 - byte0, i1, 53, 41);
		zz.sendText(c1 - byte0, i1 - 8, "Skin", 1, true);
		zz.sendText(c1 - byte0, i1 + 8, "Color", 1, true);
		zz.jc(c1 - byte0 - 40, i1, g.eg + 7);
		gbb = zz.id(c1 - byte0 - 40, i1, 20, 20);
		zz.jc((c1 - byte0) + 40, i1, g.eg + 6);
		hbb = zz.id((c1 - byte0) + 40, i1, 20, 20);
		zz.ic(c1 + byte0, i1, 53, 41);
		zz.sendText(c1 + byte0, i1 - 8, "Bottom", 1, true);
		zz.sendText(c1 + byte0, i1 + 8, "Color", 1, true);
		zz.jc((c1 + byte0) - 40, i1, g.eg + 7);
		ibb = zz.id((c1 + byte0) - 40, i1, 20, 20);
		zz.jc(c1 + byte0 + 40, i1, g.eg + 6);
		jbb = zz.id(c1 + byte0 + 40, i1, 20, 20);
		c1 = '\u0174';
		i1 = 35;
		zz.yc(c1, i1, 200, 25);
		zz.sendText(c1, i1, "Character Type", 4, false);
		i1 += 22;
		zz.sendText(c1, i1, "Each character type has different starting", 0, true);
		i1 += 13;
		zz.sendText(c1, i1, "bonuses. But the choice you make here", 0, true);
		i1 += 13;
		zz.sendText(c1, i1, "isn't permanent, and will change depending", 0, true);
		i1 += 13;
		zz.sendText(c1, i1, "on how you play the game.", 0, true);
		i1 += 73;
		zz.ic(c1, i1, 215, 125);
		String[] classes = { "Adventurer", "Warrior", "Wizard", "Ranger", "Miner" };
		lbb = zz.ac(c1, i1 + 2, classes, 3, true);
		i1 += 75;
		zz.ic(c1, i1 + 21, 215, 60);
		zz.sendText(c1, i1, "Do you wish to be able to fight with other", 0, true);
		i1 += 13;
		zz.sendText(c1, i1, "players? Warning! If you choose 'yes' then", 0, true);
		i1 += 13;
		zz.sendText(c1, i1, "other players will be able to attack you too!", 0, true);
		i1 += 13;
		String[] as1 = { "No thanks", "Yes I'll fight" };
		mbb = zz.qc(c1, i1, as1, 1, true);
		i1 += 32;
		zz.yc(c1, i1, 200, 30);
		zz.sendText(c1, i1, "Start Game", 4, false);
		kbb = zz.id(c1, i1, 200, 30);
	}

	public void ek() {
		qt.rk = false;
		qt.df();
		zz.tc(0, 0, bu, cu);
		zz.fc();
		char c1 = '\214';
		byte byte0 = 50;
		qt.xf(c1 - 32 - 55, byte0, 64, 102, Definitions.zhb[vbb], bcb[ybb]);
		qt.pf(c1 - 32 - 55, byte0, 64, 102, Definitions.zhb[ubb], bcb[xbb], dcb[zbb], 0, false);
		qt.pf(c1 - 32 - 55, byte0, 64, 102, Definitions.zhb[tbb], ccb[wbb], dcb[zbb], 0, false);
		qt.xf(c1 - 32, byte0, 64, 102, Definitions.zhb[vbb] + 6, bcb[ybb]);
		qt.pf(c1 - 32, byte0, 64, 102, Definitions.zhb[ubb] + 6, bcb[xbb], dcb[zbb], 0, false);
		qt.pf(c1 - 32, byte0, 64, 102, Definitions.zhb[tbb] + 6, ccb[wbb], dcb[zbb], 0, false);
		qt.xf((c1 - 32) + 55, byte0, 64, 102, Definitions.zhb[vbb] + 12, bcb[ybb]);
		qt.pf((c1 - 32) + 55, byte0, 64, 102, Definitions.zhb[ubb] + 12, bcb[xbb], dcb[zbb], 0, false);
		qt.pf((c1 - 32) + 55, byte0, 64, 102, Definitions.zhb[tbb] + 12, ccb[wbb], dcb[zbb], 0, false);
		qt.rg(0, cu, az + 22);
		qt.cf(ot, 0, 11);
	}

	public void im() {
		zz.md(super.mq, super.nq, super.pq, super.oq);
		if (zz.od(yab))
			do
				tbb = ((tbb - 1) + Definitions.shb) % Definitions.shb;
			while ((Definitions.whb[tbb] & 3) != 1 || (Definitions.whb[tbb] & 4 * acb) == 0);
		if (zz.od(zab))
			do
				tbb = (tbb + 1) % Definitions.shb;
			while ((Definitions.whb[tbb] & 3) != 1 || (Definitions.whb[tbb] & 4 * acb) == 0);
		if (zz.od(abb))
			wbb = ((wbb - 1) + ccb.length) % ccb.length;
		if (zz.od(bbb))
			wbb = (wbb + 1) % ccb.length;
		if (zz.od(cbb) || zz.od(dbb)) {
			for (acb = 3 - acb; (Definitions.whb[tbb] & 3) != 1 || (Definitions.whb[tbb] & 4 * acb) == 0; tbb = (tbb + 1) % Definitions.shb)
				;
			for (; (Definitions.whb[ubb] & 3) != 2 || (Definitions.whb[ubb] & 4 * acb) == 0; ubb = (ubb + 1) % Definitions.shb)
				;
		}
		if (zz.od(ebb))
			xbb = ((xbb - 1) + bcb.length) % bcb.length;
		if (zz.od(fbb))
			xbb = (xbb + 1) % bcb.length;
		if (zz.od(gbb))
			zbb = ((zbb - 1) + dcb.length) % dcb.length;
		if (zz.od(hbb))
			zbb = (zbb + 1) % dcb.length;
		if (zz.od(ibb))
			ybb = ((ybb - 1) + bcb.length) % bcb.length;
		if (zz.od(jbb))
			ybb = (ybb + 1) % bcb.length;
		if (zz.od(kbb)) {
			super.dd.i(236);
			super.dd.n(acb);
			super.dd.n(tbb);
			super.dd.n(ubb);
			super.dd.n(vbb);
			super.dd.n(wbb);
			super.dd.n(xbb);
			super.dd.n(ybb);
			super.dd.n(zbb);
			super.dd.n(zz.rc(lbb));
			super.dd.n(zz.rc(mbb));
			super.dd.e();
			sbb = false;
		}
	}

	public void sendRegisterInterface() {
		wz = new g(qt, 50);
		int i1 = 35;
		wz.sendText(250, 200 + i1, "Click on an option", 5, true);
		wz.yc(150, 240 + i1, 120, 35);
		wz.yc(350, 240 + i1, 120, 35);
		wz.sendText(150, 240 + i1, "New User", 5, false);
		wz.sendText(350, 240 + i1, "Existing User", 5, false);
		bab = wz.id(150, 240 + i1, 120, 35);
		cab = wz.id(350, 240 + i1, 120, 35);
		i1 = 60;
		byte byte0 = 110;
		boolean flag = false;
		xz = new g(qt, 50);
		xz.yc(250, i1 + 17, 420, 34);
		xz.sendText(250, i1 + 8, "Choose a Username (This is the name other users will see)", 4, flag);
		mab = xz.vc(250, i1 + 25, 200, 40, 4, 12, false, flag);
		i1 += 40;
		xz.yc(250, i1 + 17, 420, 34);
		xz.sendText(250, i1 + 8, "Choose a Password (You will require this to login)", 4, flag);
		oab = xz.vc(250, i1 + 25, 200, 40, 4, 12, false, flag);
		i1 += 40;
		xz.yc(250, i1 + 17, 420, 34);
		xz.sendText(250, i1 + 8, "E-mail address", 4, flag);
		nab = xz.vc(250, i1 + 25, 200, 40, 4, 40, false, flag);
		i1 += 40;
		xz.yc(250, i1 + 22, 420, 44);
		xz.sendText(250, i1 + 7, "Do you want to receive our free weekly newsletter? Get news of the latest", 1, flag);
		xz.sendText(250, i1 + 21, "improvements, new-quests!, hints+tips, hiscores, special-events! etc...", 1, flag);
		String[] as = { "Yes sounds great!", "No-thanks" };
		iab = xz.qc(250, i1 + 35, as, 1, flag);
		i1 += 50;
		xz.yc(((250 - byte0) + 50) - 15, i1 + 17, 270, 34);
		dab = xz.sendText(((250 - byte0) + 50) - 15, i1 + 8, "To create an account please enter", 4, true);
		eab = xz.sendText(((250 - byte0) + 50) - 15, i1 + 25, "all the requested details", 4, true);
		xz.yc(350, i1 + 17, 70, 34);
		xz.sendText(350, i1 + 17, "Submit", 5, flag);
		lab = xz.id(335, i1 + 17, 100, 34);
		xz.yc(425, i1 + 17, 70, 34);
		xz.sendText(425, i1 + 17, "Cancel", 5, flag);
		kab = xz.id(425, i1 + 17, 100, 34);
		xz.lc(mab);
		yz = new g(qt, 50);
		i1 = 83;
		yz.yc(250, i1, 300, 40);
		pab = yz.sendText(250, i1 - 10, "Please enter your", 5, true);
		qab = yz.sendText(250, i1 + 10, "username and password", 5, true);
		i1 += 60;
		yz.yc(250, i1, 200, 40);
		yz.sendText(250, i1 - 10, "Username:", 4, flag);
		rab = yz.vc(250, i1 + 10, 200, 40, 4, 12, false, flag);
		i1 += 60;
		yz.yc(250, i1, 200, 40);
		yz.sendText(250, i1 - 10, "Password:", 4, flag);
		sab = yz.vc(250, i1 + 10, 200, 40, 4, 20, true, flag);
		i1 += 60;
		byte0 = 70;
		yz.yc(250 - byte0, i1, 110, 40);
		yz.sendText(250 - byte0, i1, "Ok", 4, flag);
		tab = yz.id(250 - byte0, i1, 110, 40);
		yz.yc(250 + byte0, i1, 110, 40);
		yz.sendText(250 + byte0, i1, "Cancel", 4, flag);
		uab = yz.id(250 + byte0, i1, 110, 40);
		yz.lc(rab);
		aab = new g(qt, 50);
		i1 = 20;
		aab.sendText(250, i1, "Runescape-Rules / Terms+Conditions", 5, true);
		i1 += 30;
		aab.gc(40, i1 - 10, 420, 220);
		pbb = aab.bc(50, i1, 400, 200, 1, 1000, true);
		sendRulesInterface(aab, pbb);
		i1 += 240;
		aab.yc(120, i1, 170, 50);
		aab.sendText(120, i1 - 10, "I have read the terms", 1, false);
		aab.sendText(120, i1, "and conditions above", 1, false);
		aab.sendText(120, i1 + 10, "And I Agree", 1, false);
		nbb = aab.id(120, i1, 170, 50);
		aab.yc(380, i1, 170, 50);
		aab.sendText(380, i1, "I do not agree", 1, false);
		obb = aab.id(380, i1, 170, 50);
	}

	public void dl() {
		qt.rk = false;
		qt.df();
		if (bz == 0)
			qt._mthif(256, 95, az + 10);
		if (bz == 0) {
			wz.tc(0, 0, bu, cu);
			wz.fc();
		}
		if (bz == 1) {
			xz.tc(0, 0, bu, cu);
			xz.fc();
		}
		if (bz == 2) {
			yz.tc(0, 0, bu, cu);
			yz.fc();
		}
		if (bz == 3) {
			aab.tc(0, 0, bu, cu);
			aab.fc();
		}
		qt.rg(0, cu, az + 22);
		qt.cf(ot, 0, 11);
	}

	public void registerAccount() {
		if (bz == 0) {
			wz.md(super.mq, super.nq, super.pq, super.oq);
			if (wz.od(bab))
				bz = 3;
			if (wz.od(cab)) {
				bz = 2;
				yz.hd(pab, "Please enter your");
				yz.hd(qab, "username and password");
				yz.hd(rab, "");
				yz.hd(sab, "");
				yz.lc(rab);
			}
		} else if (bz == 1) {
			xz.md(super.mq, super.nq, super.pq, super.oq);
			if (xz.od(mab))
				xz.lc(oab);
			if (xz.od(oab))
				xz.lc(nab);
			if (xz.od(nab))
				xz.lc(mab);
			if (xz.od(kab))
				bz = 0;
			if (xz.od(lab)) {
				if (xz.oc(mab) != null && xz.oc(mab).length() == 0 || xz.oc(nab) != null && xz.oc(nab).length() == 0
						|| xz.oc(oab) != null && xz.oc(oab).length() == 0) {
					xz.hd(dab, "Please fill in ALL requested");
					xz.hd(eab, "information to continue!");
					return;
				}
				xz.hd(dab, "Please wait...");
				xz.hd(eab, "Creating new account");
				dl();
				pj();
				xz.oc(fab);
				xz.oc(gab);
				String s = xz.oc(mab);
				String s1 = xz.oc(oab);
				String s2 = xz.oc(nab);
				int i1 = xz.rc(jab);
				int j1 = xz.rc(iab);
				int k1 = 0;
				String s3 = xz.oc(hab);
				try {
					k1 = Integer.parseInt(s3);
				} catch (Exception _ex) {
				}
				db(s, s1, s2, i1, k1, j1);
			}
		} else {
			if (bz == 2) {
				yz.md(super.mq, super.nq, super.pq, super.oq);
				if (yz.od(uab))
					bz = 0;
				if (yz.od(rab))
					yz.lc(sab);
				if (yz.od(sab) || yz.od(tab)) {
					cz = yz.oc(rab);
					dz = yz.oc(sab);
					fb(cz, dz);
				}
				return;
			}
			if (bz == 3) {
				aab.md(super.mq, super.nq, super.pq, super.oq);
				if (aab.od(nbb)) {
					bz = 1;
					xz.hd(dab, "To create an account please enter");
					xz.hd(eab, "all the requested details");
					xz.hd(mab, "");
					xz.hd(nab, "");
					xz.hd(oab, "");
					xz.lc(mab);
				}
				if (aab.od(obb))
					bz = 0;
			}
		}
	}

	public void ob(String s, String s1) {
		if (bz == 1) {
			xz.hd(dab, s);
			xz.hd(eab, s1);
		}
		if (bz == 2) {
			yz.hd(pab, s);
			yz.hd(qab, s1);
		}
		fz = s1;
		dl();
		pj();
	}

	public void v() {
		bz = 0;
		ut = 0;
	}

	public void q() {
		u();
	}

	public void u() {
		tz = 0;
		bz = 0;
		ut = 1;
		bl();
		qt.df();
		qt.cf(ot, 0, 11);
		for (int i1 = 0; i1 < tw; i1++) {
			pt.sh(uw[i1]);
			cv.zn(vw[i1], ww[i1], xw[i1]);
		}

		for (int j1 = 0; j1 < cx; j1++) {
			pt.sh(dx[j1]);
			cv.an(ex[j1], fx[j1], gx[j1], hx[j1]);
		}

		tw = 0;
		cx = 0;
		mw = 0;
		wv = 0;
		for (int k1 = 0; k1 < uv; k1++)
			yv[k1] = null;

		for (int l1 = 0; l1 < vv; l1++)
			zv[l1] = null;

		fw = 0;
		for (int i2 = 0; i2 < dw; i2++)
			gw[i2] = null;

		for (int j2 = 0; j2 < ew; j2++)
			hw[j2] = null;

		mx = 0;
		super.pq = 0;
		super.oq = 0;
	}

	public void sendLoginInterface() {
		String s = xz.oc(mab);
		String s1 = xz.oc(oab);
		bz = 2;
		yz.hd(pab, "Please enter your");
		yz.hd(qab, "username and password");
		yz.hd(rab, s);
		yz.hd(sab, s1);
		dl();
		pj();
		fb(s, s1);
	}

	public void sendRulesInterface(g g1, int i1) {
		g1.ec(i1, "Runescape rules of use", false);
		g1.ec(i1, "", false);
		g1.ec(i1, "In order to keep runescape enjoyable for everyone there are a few", false);
		g1.ec(i1, "rules you must observe. You must agree to these rules to play", false);
		g1.ec(i1, "", false);
		g1.ec(i1, "When using the built in chat facility you must not use any language", false);
		g1.ec(i1, "which may be considered by others to be offensive, racist or", false);
		g1.ec(i1, "obscene. You must not use the chat facility to harass, threaten or", false);
		g1.ec(i1, "deceive other players.", false);
		g1.ec(i1, "", false);
		g1.ec(i1, "You must not exploit any cheats or errors which you find in the", false);
		g1.ec(i1, "game, to give yourself an unfair advantage. Any exploits which you", false);
		g1.ec(i1, "find must be immediately reported to Jagex Software.", false);
		g1.ec(i1, "", false);
		g1.ec(i1, "You must not attempt to use other programs in conjunction with", false);
		g1.ec(i1, "RuneScape to give yourself an unfair advantage at the game. You", false);
		g1.ec(i1, "may not use any bots or macros to control your character for you.", false);
		g1.ec(i1, "When you are not playing the game you must log-out. You may not", false);
		g1.ec(i1, "circumvent any of our mechanisms designed to log out inactive", false);
		g1.ec(i1, "players automatically.", false);
		g1.ec(i1, "", false);
		g1.ec(i1, "You must not create multiple characters and use them to help each", false);
		g1.ec(i1, "other. You may create more than one character, but if you do, you", false);
		g1.ec(i1, "may not log in more than one at any time, and they must not interact", false);
		g1.ec(i1, "with each other in any way. If you wish to form an adventuring", false);
		g1.ec(i1, "party you should do so by cooperating with other players in the game", false);
		g1.ec(i1, "", false);
		g1.ec(i1, "Terms and conditions", false);
		g1.ec(i1, "", false);
		g1.ec(i1, "You agree that your character and account in runescape, is the", false);
		g1.ec(i1, "property of, and remains the property of Jagex Software. You may", false);
		g1.ec(i1, "not sell, transfer, or lend your character to anyone else. We may", false);
		g1.ec(i1, "delete or modify your character at any time for any reason.", false);
		g1.ec(i1, "For instance failing to follow the rules above may be cause for", false);
		g1.ec(i1, "IMMEDIATE DELETION of all your characters.", false);
		g1.ec(i1, "", false);
		g1.ec(i1, "You agree that for purposes such as preventing offensive language", false);
		g1.ec(i1, "we may automatically or manually censor the chat as we see fit,", false);
		g1.ec(i1, "and that we may record the chat to help us identify offenders.", false);
		g1.ec(i1, "", false);
		g1.ec(i1, "No Warranty is supplied with this Software. All implied warranties", false);
		g1.ec(i1, "conditions or terms are excluded to the fullest extent permitted by", false);
		g1.ec(i1, "law. We do not warrant that the operation of the Software will be", false);
		g1.ec(i1, "uninterrupted or error free. We accept no responsibility for any", false);
		g1.ec(i1, "consequential or indirect loss or damages. You use this software at", false);
		g1.ec(i1, "your own risk, and assume full responsibility for any and all real,", false);
		g1.ec(i1, "claimed, or supposed damages that may occur as a result of running", false);
		g1.ec(i1, "this software.", false);
		g1.ec(i1, "", false);
		g1.ec(i1, "We reserve all rights related to the runescape name, logo, web site,", false);
		g1.ec(i1, "and game. All materials associated with runescape are protected", false);
		g1.ec(i1, "by UK copyright laws and all other applicable national laws, and", false);
		g1.ec(i1, "may not be copied, reproduced, republished, uploaded, posted,", false);
		g1.ec(i1, "transmitted, or distributed in any way without our prior written", false);
		g1.ec(i1, "consent. We reserve the right to modify or remove this game at any", false);
		g1.ec(i1, "time. You agree that we may change this service, and these terms", false);
		g1.ec(i1, "and conditions, as and when we deem necessary.", false);
		g1.ec(i1, "", false);
		g1.ec(i1, "We accept no responsibility for the actions of other users of our", false);
		g1.ec(i1, "website. You acknowledge that it is inpractical for us to control", false);
		g1.ec(i1, "and monitor everything that users do in our game or post on our", false);
		g1.ec(i1, "message boards, and that we therefore cannot be held responsible", false);
		g1.ec(i1, "for any abusive or inappropriate content which appears on our site", false);
		g1.ec(i1, "as a result.", false);
		g1.ec(i1, "", false);
		g1.ec(i1, "Occasionally we may accept ideas and game additions from the", false);
		g1.ec(i1, "players. You agree that by submitting material for inclusion in", false);
		g1.ec(i1, "runescape you are giving us a non-exclusive, perpetual, worldwide,", false);
		g1.ec(i1, "royalty-free license to use or modify the submission as we see", false);
		g1.ec(i1, "fit. You agree that you will not withdraw the submission or attempt", false);
		g1.ec(i1, "to make a charge for its use. Furthermore you warrant that you", false);
		g1.ec(i1, "are the exclusive copyright holder of the submission, and that the", false);
		g1.ec(i1, "submission in no way violates any other person or entity's rights", false);
		g1.ec(i1, "", false);
		g1.ec(i1, "These Terms shall be governed by the laws of England, and the", false);
		g1.ec(i1, "courts of England shall have exclusive jurisdiction in all matters", false);
		g1.ec(i1, "arising.", false);
	}

	public void hm() {
		z();
		if (super.sp > 4500 && lt == 0) {
			mb();
			return;
		}
		if (player.gr == 8 || player.gr == 9)
			lt = 500;
		if (lt > 0)
			lt--;
		for (int i1 = 0; i1 < wv; i1++) {
			Player l1 = zv[i1];
			int k1 = (l1.jr + 1) % 10;
			if (l1.ir != k1) {
				int j2 = -1;
				int k4 = l1.ir;
				int k5;
				if (k4 < k1)
					k5 = k1 - k4;
				else
					k5 = (10 + k1) - k4;
				int j6 = 4;
				if (k5 > 2)
					j6 = (k5 - 1) * 4;
				if (l1.kr[k4] - l1.cr > tt * 3 || l1.lr[k4] - l1.dr > tt * 3 || l1.kr[k4] - l1.cr < -tt * 3
						|| l1.lr[k4] - l1.dr < -tt * 3 || k5 > 8) {
					l1.cr = l1.kr[k4];
					l1.dr = l1.lr[k4];
				} else {
					if (l1.cr < l1.kr[k4]) {
						l1.cr += j6;
						l1.fr++;
						j2 = 2;
					} else if (l1.cr > l1.kr[k4]) {
						l1.cr -= j6;
						l1.fr++;
						j2 = 6;
					}
					if (l1.cr - l1.kr[k4] < j6 && l1.cr - l1.kr[k4] > -j6)
						l1.cr = l1.kr[k4];
					if (l1.dr < l1.lr[k4]) {
						l1.dr += j6;
						l1.fr++;
						if (j2 == -1)
							j2 = 4;
						else if (j2 == 2)
							j2 = 3;
						else
							j2 = 5;
					} else if (l1.dr > l1.lr[k4]) {
						l1.dr -= j6;
						l1.fr++;
						if (j2 == -1)
							j2 = 0;
						else if (j2 == 2)
							j2 = 1;
						else
							j2 = 7;
					}
					if (l1.dr - l1.lr[k4] < j6 && l1.dr - l1.lr[k4] > -j6)
						l1.dr = l1.lr[k4];
				}
				if (j2 != -1)
					l1.gr = j2;
				if (l1.cr == l1.kr[k4] && l1.dr == l1.lr[k4])
					l1.ir = (k4 + 1) % 10;
			} else {
				l1.gr = l1.hr;
			}
			if (l1.or > 0)
				l1.or--;
			if (l1.qr > 0)
				l1.qr--;
			if (l1.ur > 0)
				l1.ur--;
			if (fu > 0) {
				fu--;
				if (fu == 0)
					gk("You have been granted another life. Be more careful this time!", 3);
				if (fu == 0)
					gk("You retain your skills. Unless you attacked another player recently,", 3);
				if (fu == 0)
					gk("you also keep your best 3 items. Everything else lands where you died.", 3);
			}
		}

		for (int j1 = 0; j1 < fw; j1++) {
			Player l2 = hw[j1];
			int k2 = (l2.jr + 1) % 10;
			if (l2.ir != k2) {
				int l4 = -1;
				int l5 = l2.ir;
				int k6;
				if (l5 < k2)
					k6 = k2 - l5;
				else
					k6 = (10 + k2) - l5;
				int l6 = 4;
				if (k6 > 2)
					l6 = (k6 - 1) * 4;
				if (l2.kr[l5] - l2.cr > tt * 3 || l2.lr[l5] - l2.dr > tt * 3 || l2.kr[l5] - l2.cr < -tt * 3
						|| l2.lr[l5] - l2.dr < -tt * 3 || k6 > 8) {
					l2.cr = l2.kr[l5];
					l2.dr = l2.lr[l5];
				} else {
					if (l2.cr < l2.kr[l5]) {
						l2.cr += l6;
						l2.fr++;
						l4 = 2;
					} else if (l2.cr > l2.kr[l5]) {
						l2.cr -= l6;
						l2.fr++;
						l4 = 6;
					}
					if (l2.cr - l2.kr[l5] < l6 && l2.cr - l2.kr[l5] > -l6)
						l2.cr = l2.kr[l5];
					if (l2.dr < l2.lr[l5]) {
						l2.dr += l6;
						l2.fr++;
						if (l4 == -1)
							l4 = 4;
						else if (l4 == 2)
							l4 = 3;
						else
							l4 = 5;
					} else if (l2.dr > l2.lr[l5]) {
						l2.dr -= l6;
						l2.fr++;
						if (l4 == -1)
							l4 = 0;
						else if (l4 == 2)
							l4 = 1;
						else
							l4 = 7;
					}
					if (l2.dr - l2.lr[l5] < l6 && l2.dr - l2.lr[l5] > -l6)
						l2.dr = l2.lr[l5];
				}
				if (l4 != -1)
					l2.gr = l4;
				if (l2.cr == l2.kr[l5] && l2.dr == l2.lr[l5])
					l2.ir = (l5 + 1) % 10;
			} else {
				l2.gr = l2.hr;
			}
			if (l2.or > 0)
				l2.or--;
			if (l2.qr > 0)
				l2.qr--;
			if (l2.ur > 0)
				l2.ur--;
		}

		for (int i2 = 0; i2 < wv; i2++) {
			Player l3 = zv[i2];
			if (l3.es > 0)
				l3.es--;
		}

		if (bt) {
			if (nu - player.cr < -500 || nu - player.cr > 500 || ou - player.dr < -500 || ou - player.dr > 500) {
				nu = player.cr;
				ou = player.dr;
			}
		} else {
			if (nu - player.cr < -500 || nu - player.cr > 500 || ou - player.dr < -500 || ou - player.dr > 500) {
				nu = player.cr;
				ou = player.dr;
			}
			if (nu != player.cr)
				nu += (player.cr - nu) / (16 + (lu - 500) / 15);
			if (ou != player.dr)
				ou += (player.dr - ou) / (16 + (lu - 500) / 15);
			if (ct) {
				int i3 = pu * 32;
				int i5 = i3 - zt;
				byte byte0 = 1;
				if (i5 != 0) {
					qu++;
					if (i5 > 128) {
						byte0 = -1;
						i5 = 256 - i5;
					} else if (i5 > 0)
						byte0 = 1;
					else if (i5 < -128) {
						byte0 = 1;
						i5 = 256 + i5;
					} else if (i5 < 0) {
						byte0 = -1;
						i5 = -i5;
					}
					zt += ((qu * i5 + 255) / 256) * byte0;
					zt &= 0xff;
				} else {
					qu = 0;
				}
			}
		}
		if (sbb) {
			im();
			return;
		}
		if (super.nq > cu - 4) {
			if (super.mq > 15 && super.mq < 96 && super.pq == 1)
				pz = 0;
			if (super.mq > 110 && super.mq < 194 && super.pq == 1) {
				pz = 1;
				kz.ue[lz] = 0xf423f;
			}
			if (super.mq > 215 && super.mq < 295 && super.pq == 1) {
				pz = 2;
				kz.ue[nz] = 0xf423f;
			}
			if (super.mq > 315 && super.mq < 395 && super.pq == 1) {
				pz = 3;
				kz.ue[oz] = 0xf423f;
			}
			super.pq = 0;
			super.oq = 0;
		}
		kz.md(super.mq, super.nq, super.pq, super.oq);
		if (pz > 0 && super.mq >= 494 && super.nq >= cu - 66)
			super.pq = 0;
		if (kz.od(mz)) {
			String s = kz.oc(mz);
			kz.hd(mz, "");
			if (s.equalsIgnoreCase("simlostcon99"))
				super.dd.xb();
			else if (!x(s)) {
				player.or = 150;
				player.nr = s;
				gk(player.playerName + ": " + s, 2);
			}
		}
		if (pz == 0) {
			for (int j3 = 0; j3 < qz; j3++)
				if (sz[j3] > 0)
					sz[j3]--;

		}
		if (fu != 0)
			super.pq = 0;
		if (ux) {
			if (super.oq != 0)
				ey++;
			else
				ey = 0;
			if (ey > 300)
				fy += 50;
			else if (ey > 150)
				fy += 5;
			else if (ey > 50)
				fy++;
			else if (ey > 20 && (ey & 5) == 0)
				fy++;
		} else {
			ey = 0;
			fy = 0;
		}
		if (super.pq == 1)
			mx = 1;
		else if (super.pq == 2)
			mx = 2;
		pt.ph(super.mq, super.nq);
		super.pq = 0;
		if (ct) {
			if (qu == 0 || bt) {
				if (super.fq) {
					pu = pu + 1 & 7;
					super.fq = false;
					if (!mu) {
						if ((pu & 1) == 0)
							pu = pu + 1 & 7;
						for (int k3 = 0; k3 < 8; k3++) {
							if (ol(pu))
								break;
							pu = pu + 1 & 7;
						}

					}
				}
				if (super.gq) {
					pu = pu + 7 & 7;
					super.gq = false;
					if (!mu) {
						if ((pu & 1) == 0)
							pu = pu + 7 & 7;
						for (int i4 = 0; i4 < 8; i4++) {
							if (ol(pu))
								break;
							pu = pu + 7 & 7;
						}

					}
				}
			}
		} else if (super.fq)
			zt = zt + 2 & 0xff;
		else if (super.gq)
			zt = zt - 2 & 0xff;
		if (mu && lu > 550)
			lu -= 4;
		else if (!mu && lu < 750)
			lu += 4;
		if (scb > 0)
			scb--;
		else if (scb < 0)
			scb++;
		pt.ai(17);
		ncb++;
		if (ncb > 5) {
			ncb = 0;
			ocb = ocb + 1 & 3;
			pcb = (pcb + 1) % 3;
		}
		for (int j4 = 0; j4 < tw; j4++) {
			int j5 = vw[j4];
			int i6 = ww[j4];
			if (j5 >= 0 && i6 >= 0 && j5 < 96 && i6 < 96 && xw[j4] == 74)
				uw[j4].pe(1, 0, 0);
		}

	}

	public void gk(String s, int i1) {
		if (i1 == 2 || i1 == 4 || i1 == 6) {
			for (; s.length() > 5 && s.charAt(0) == '@' && s.charAt(4) == '@'; s = s.substring(5))
				;
			int j1 = s.indexOf(":");
			if (j1 != -1) {
				String s1 = s.substring(0, j1);
				long l1 = o.rm(s1);
				for (int i2 = 0; i2 < super.md; i2++)
					if (super.nd[i2] == l1)
						return;

			}
		}
		if (i1 == 2)
			s = "@yel@" + s;
		if (i1 == 3 || i1 == 4)
			s = "@whi@" + s;
		if (i1 == 6)
			s = "@cya@" + s;
		if (pz != 0) {
			if (i1 == 4 || i1 == 3)
				gz = 200;
			if (i1 == 2 && pz != 1)
				hz = 200;
			if (i1 == 5 && pz != 2)
				iz = 200;
			if (i1 == 6 && pz != 3)
				jz = 200;
			if (i1 == 3 && pz != 0)
				pz = 0;
			if (i1 == 6 && pz != 3 && pz != 0)
				pz = 3;
		}
		for (int k1 = qz - 1; k1 > 0; k1--) {
			rz[k1] = rz[k1 - 1];
			sz[k1] = sz[k1 - 1];
		}

		rz[0] = s;
		sz[0] = 300;
		if (i1 == 2)
			if (kz.ue[lz] == kz.ve[lz] - 4)
				kz.ec(lz, s, true);
			else
				kz.ec(lz, s, false);
		if (i1 == 5)
			if (kz.ue[nz] == kz.ve[nz] - 4)
				kz.ec(nz, s, true);
			else
				kz.ec(nz, s, false);
		if (i1 == 6) {
			if (kz.ue[oz] == kz.ve[oz] - 4) {
				kz.ec(oz, s, true);
				return;
			}
			kz.ec(oz, s, false);
		}
	}

	public void w(String s) {
		if (s.equals("@sys@k")) {
			mb();
			return;
		}
		if (s.startsWith("@cha@")) {
			gk(s, 2);
			return;
		}
		if (s.startsWith("@bor@")) {
			gk(s, 4);
			return;
		}
		if (s.startsWith("@que@")) {
			gk("@whi@" + s, 5);
			return;
		}
		if (s.startsWith("@pri@")) {
			gk(s, 6);
		} else {
			gk(s, 3);
		}
	}

	public void sendUpdate(int i1, int j1, byte[] abyte0) {
		try {
			if (i1 == 230) {
				int k1 = abyte0[1] & 0xff;
				int j6 = abyte0[2] & 0xff;
				int i11 = abyte0[3] & 0xff;
				int k15 = abyte0[4] & 0xff;
				int l19 = abyte0[5] & 0xff;
				int j23 = abyte0[6] & 0xff;
				if (k1 > Versions.ConfigsVersion || j6 > Versions.MapsVersion || i11 > Versions.MediaVersion
						|| k15 > Versions.ModelsVersion || l19 > Versions.TexturesVersion
						|| j23 > Versions.EntitiesVersion) {
					mb();
					super.up = "Runescape has been updated. Getting latest files";
					super.tp = 2;
					gj(0, "Loading");
					if (k1 > Versions.ConfigsVersion) {
						Versions.ConfigsVersion = k1;
						loadConfigs();
					}
					if (i11 > Versions.MediaVersion) {
						Versions.MediaVersion = i11;
						loadMedia();
					}
					if (j23 > Versions.EntitiesVersion) {
						Versions.EntitiesVersion = j23;
						loadEntities(true);
					} else {
						loadEntities(false);
					}
					if (l19 > Versions.TexturesVersion) {
						Versions.TexturesVersion = l19;
						loadTextures();
					}
					if (k15 > Versions.ModelsVersion) {
						Versions.ModelsVersion = k15;
						loadModels();
					}
					if (j6 > Versions.MapsVersion) {
						Versions.MapsVersion = j6;
						loadMaps();
					}
					cv.web = false;
					uu = -1;
					super.tp = 0;
					fb(cz, dz);
					return;
				}
			}
			if (i1 == 232) {
				int l1 = (j1 - 1) / 2;
				for (int k6 = 0; k6 < l1; k6++) {
					int j11 = o.vm(abyte0, 1 + k6 * 2);
					for (int l15 = 0; l15 < wv; l15++) {
						Player l20 = zv[l15];
						if (l20 == null || l20.ar == j11) {
							wv--;
							for (int k23 = l15; k23 < wv; k23++)
								zv[k23] = zv[k23 + 1];

							l15--;
						}
					}

				}

			}
			if (i1 == 231) {
				int i2 = (j1 - 1) / 2;
				for (int l6 = 0; l6 < i2; l6++) {
					int k11 = o.vm(abyte0, 1 + l6 * 2);
					for (int i16 = 0; i16 < fw; i16++) {
						Player l21 = hw[i16];
						if (l21 == null || l21.ar == k11) {
							fw--;
							for (int i24 = i16; i24 < fw; i24++)
								hw[i24] = hw[i24 + 1];

							i16--;
						}
					}

				}

			}
			if (i1 == 255) {
				int j2 = 1;
				int i7 = 0;
				int l31 = 0;
				hu = true;
				while (j2 < j1) {
					int l11;
					int j16;
					int i20;
					int j24;
					boolean flag;
					if (i7 == 0) {
						l11 = o.vm(abyte0, 1);
						bw = o.vm(abyte0, 3);
						cw = o.vm(abyte0, 5);
						j24 = o.sm(abyte0[7]);
						j2 += 7;
						yk(bw, cw);
						bw -= vu;
						cw -= wu;
						j16 = bw * tt + 64;
						i20 = cw * tt + 64;
						if (j24 >= 128) {
							j24 -= 128;
							flag = true;
						} else {
							flag = false;
						}
					} else {
						int k38 = o.vm(abyte0, j2);
						int k34;
						int i37;
						if ((k38 & 0xfc00) == 64512) {
							int k39 = o.vm(abyte0, j2 + 2);
							j2 += 4;
							k34 = k38 >> 5 & 0x1f;
							if (k34 > 15)
								k34 -= 32;
							i37 = k38 & 0x1f;
							if (i37 > 15)
								i37 -= 32;
							l11 = k39 >> 4 & 0xfff;
							j24 = k39 & 0xf;
							if (j24 == 15) {
								j24 = 0;
								flag = true;
							} else {
								flag = false;
							}
						} else {
							int l39 = o.sm(abyte0[j2 + 2]);
							j2 += 3;
							l11 = k38 >> 6 & 0x3ff;
							k34 = k38 >> 1 & 0x1f;
							if (k34 > 15)
								k34 -= 32;
							i37 = (k38 << 4 & 0x10) + (l39 >> 4 & 0xf);
							if (i37 > 15)
								i37 -= 32;
							j24 = l39 & 0xf;
							if (j24 == 15) {
								j24 = 0;
								flag = true;
							} else {
								flag = false;
							}
						}
						j16 = (bw + k34) * tt + 64;
						i20 = (cw + i37) * tt + 64;
					}
					if (yv[l11] == null) {
						yv[l11] = new Player();
						yv[l11].ar = l11;
						yv[l11].br = 0;
					}
					Player l34 = yv[l11];
					if (i7 == 0)
						player = l34;
					boolean flag3 = false;
					for (int l38 = 0; l38 < wv; l38++) {
						if (zv[l38] != l34)
							continue;
						flag3 = true;
						break;
					}

					if (!flag3)
						zv[wv++] = yv[l11];
					if (flag3 && hu) {
						l34.hr = j24;
						int i40 = l34.jr;
						if (j16 != l34.kr[i40] || i20 != l34.lr[i40]) {
							l34.jr = i40 = (i40 + 1) % 10;
							l34.kr[i40] = j16;
							l34.lr[i40] = i20;
						}
					} else {
						l34.ar = l11;
						l34.ir = 0;
						l34.jr = 0;
						l34.kr[0] = l34.cr = j16;
						l34.lr[0] = l34.dr = i20;
						l34.hr = l34.gr = j24;
						l34.fr = 0;
						if (!flag && !flag3)
							vy[l31++] = l11;
					}
					i7++;
				}
				if (l31 > 0) {
					super.dd.i(254);
					super.dd.k(l31);
					for (int i35 = 0; i35 < l31; i35++) {
						Player l37 = yv[vy[i35]];
						super.dd.k(l37.ar);
						super.dd.k(l37.br);
					}

					super.dd.e();
				}
			} else {
				if (i1 == 254) {
					for (int k2 = 1; k2 < j1;)
						if (o.sm(abyte0[k2]) == 255) {
							int j7 = 0;
							int i12 = bw + abyte0[k2 + 1] >> 3;
							int k16 = cw + abyte0[k2 + 2] >> 3;
							k2 += 3;
							for (int j20 = 0; j20 < mw; j20++) {
								int k24 = (ow[j20] >> 3) - i12;
								int l27 = (pw[j20] >> 3) - k16;
								if (k24 != 0 || l27 != 0) {
									if (j20 != j7) {
										ow[j7] = ow[j20];
										pw[j7] = pw[j20];
										qw[j7] = qw[j20];
										rw[j7] = rw[j20];
									}
									j7++;
								}
							}

							mw = j7;
						} else {
							int k7 = o.vm(abyte0, k2);
							k2 += 2;
							int j12 = bw + abyte0[k2++];
							int l16 = cw + abyte0[k2++];
							if ((k7 & 0x8000) == 0) {
								ow[mw] = j12;
								pw[mw] = l16;
								qw[mw] = k7;
								rw[mw] = 0;
								for (int k20 = 0; k20 < tw; k20++) {
									if (vw[k20] != j12 || ww[k20] != l16)
										continue;
									rw[mw] = Definitions.kib[xw[k20]];
									break;
								}

								mw++;
							} else {
								k7 &= 0x7fff;
								int i21 = 0;
								for (int i25 = 0; i25 < mw; i25++)
									if (ow[i25] != j12 || pw[i25] != l16 || qw[i25] != k7) {
										if (i25 != i21) {
											ow[i21] = ow[i25];
											pw[i21] = pw[i25];
											qw[i21] = qw[i25];
											rw[i21] = rw[i25];
										}
										i21++;
									} else {
										k7 = -123;
									}

								mw = i21;
							}
						}

					return;
				}
				if (i1 == 253) {
					for (int l2 = 1; l2 < j1;)
						if (o.sm(abyte0[l2]) == 255) {
							int l7 = 0;
							int k12 = bw + abyte0[l2 + 1] >> 3;
							int i17 = cw + abyte0[l2 + 2] >> 3;
							l2 += 3;
							for (int j21 = 0; j21 < tw; j21++) {
								int j25 = (vw[j21] >> 3) - k12;
								int i28 = (ww[j21] >> 3) - i17;
								if (j25 != 0 || i28 != 0) {
									if (j21 != l7) {
										uw[l7] = uw[j21];
										uw[l7].ph = l7;
										vw[l7] = vw[j21];
										ww[l7] = ww[j21];
										xw[l7] = xw[j21];
										yw[l7] = yw[j21];
									}
									l7++;
								} else {
									pt.sh(uw[j21]);
									cv.zn(vw[j21], ww[j21], xw[j21]);
								}
							}

							tw = l7;
						} else {
							int i8 = o.vm(abyte0, l2);
							l2 += 2;
							int l12 = bw + abyte0[l2++];
							int j17 = cw + abyte0[l2++];
							int k21 = 0;
							for (int k25 = 0; k25 < tw; k25++)
								if (vw[k25] != l12 || ww[k25] != j17) {
									if (k25 != k21) {
										uw[k21] = uw[k25];
										uw[k21].ph = k21;
										vw[k21] = vw[k25];
										ww[k21] = ww[k25];
										xw[k21] = xw[k25];
										yw[k21] = yw[k25];
									}
									k21++;
								} else {
									pt.sh(uw[k25]);
									cv.zn(vw[k25], ww[k25], xw[k25]);
								}

							tw = k21;
							if (i8 != 60000) {
								int j28 = cv.dn(l12, j17);
								int i32;
								int j35;
								if (j28 == 0 || j28 == 4) {
									i32 = Definitions.gib[i8];
									j35 = Definitions.hib[i8];
								} else {
									j35 = Definitions.gib[i8];
									i32 = Definitions.hib[i8];
								}
								int j37 = ((l12 + l12 + i32) * tt) / 2;
								int i39 = ((j17 + j17 + j35) * tt) / 2;
								int j40 = Definitions.fib[i8];
								h h2 = zw[j40].le();
								pt.oh(h2);
								h2.ph = tw;
								h2.pe(0, j28 * 32, 0);
								h2.wd(j37, -cv.jn(j37, i39), i39);
								h2.ne(true, 48, 48, -50, -10, -50);
								cv.pn(l12, j17, i8);
								if (i8 == 74)
									h2.wd(0, -480, 0);
								vw[tw] = l12;
								ww[tw] = j17;
								xw[tw] = i8;
								yw[tw] = j28;
								uw[tw++] = h2;
							}
						}

					return;
				}
				if (i1 == 252) {
					ox = (j1 - 1) / 4;
					for (int i3 = 0; i3 < ox; i3++) {
						px[i3] = o.vm(abyte0, i3 * 4 + 1);
						if (px[i3] >= 32768) {
							px[i3] -= 32768;
							rx[i3] = 1;
						} else {
							rx[i3] = 0;
						}
						qx[i3] = o.vm(abyte0, i3 * 4 + 3);
					}

					return;
				}
				if (i1 == 251) {
					int j3 = o.vm(abyte0, 1);
					int j8 = 3;
					for (int i13 = 0; i13 < j3; i13++) {
						int k17 = o.vm(abyte0, j8);
						j8 += 2;
						Player l22 = yv[k17];
						if (l22 != null) {
							l22.br = o.vm(abyte0, j8);
							j8 += 2;
							l22.yq = o.qm(abyte0, j8);
							j8 += 8;
							l22.playerName = o.tm(l22.yq);
							int l25 = o.sm(abyte0[j8]);
							j8++;
							for (int k28 = 0; k28 < l25; k28++) {
								l22.mr[k28] = o.sm(abyte0[j8]);
								j8++;
							}

							for (int j32 = l25; j32 < 12; j32++)
								l22.mr[j32] = 0;

							l22.xr = abyte0[j8++] & 0xff;
							l22.yr = abyte0[j8++] & 0xff;
							l22.zr = abyte0[j8++] & 0xff;
							l22.as = abyte0[j8++] & 0xff;
							l22.vr = abyte0[j8++] & 0xff;
							l22.combatLevel = abyte0[j8++] & 0xff;
							l22.hs = abyte0[j8++] & 0xff;
						} else {
							j8 += 14;
							int i26 = o.sm(abyte0[j8]);
							j8 += i26 + 1;
						}
					}

					return;
				}
				if (i1 == 250) {
					int k3 = o.vm(abyte0, 1);
					int k8 = 3;
					for (int j13 = 0; j13 < k3; j13++) {
						int l17 = o.vm(abyte0, k8);
						k8 += 2;
						Player l23 = yv[l17];
						byte byte4 = abyte0[k8];
						k8++;
						if (byte4 == 0) {
							int l28 = o.vm(abyte0, k8);
							k8 += 2;
							if (l23 != null) {
								l23.qr = 150;
								l23.pr = l28;
							}
						} else if (byte4 == 1) {
							byte byte5 = abyte0[k8];
							k8++;
							if (l23 != null) {
								String s1 = new String(abyte0, k8, byte5);
								if (s1.startsWith("@que@")) {
									l23.or = 150;
									l23.nr = s1;
									if (l23 == player)
										gk("@yel@" + l23.playerName + ": " + l23.nr, 5);
								} else if (l23 != player) {
									boolean flag2 = false;
									for (int k37 = 0; k37 < super.md; k37++)
										if (super.nd[k37] == l23.yq)
											flag2 = true;

									if (!flag2) {
										s1 = o.nm(s1, true);
										l23.or = 150;
										l23.nr = s1;
										gk(l23.playerName + ": " + l23.nr, 2);
									}
								}
							}
							k8 += byte5;
						} else if (byte4 == 2) {
							int i29 = o.sm(abyte0[k8]);
							k8++;
							int k32 = o.sm(abyte0[k8]);
							k8++;
							int k35 = o.sm(abyte0[k8]);
							k8++;
							if (l23 != null) {
								l23.rr = i29;
								l23.sr = k32;
								l23.tr = k35;
								l23.ur = 200;
								if (l23 == player) {
									level[3] = k32;
									realLevel[3] = k35;
								}
							}
						} else if (byte4 == 3) {
							int j29 = o.vm(abyte0, k8);
							k8 += 2;
							int l32 = o.vm(abyte0, k8);
							k8 += 2;
							if (l23 != null) {
								l23.bs = j29;
								l23.ds = l32;
								l23.cs = -1;
								l23.es = uz;
							}
						} else if (byte4 == 4) {
							int k29 = o.vm(abyte0, k8);
							k8 += 2;
							int i33 = o.vm(abyte0, k8);
							k8 += 2;
							if (l23 != null) {
								l23.bs = k29;
								l23.cs = i33;
								l23.ds = -1;
								l23.es = uz;
							}
						}
					}

					return;
				}
				if (i1 == 249) {
					for (int l3 = 1; l3 < j1;)
						if (o.sm(abyte0[l3]) == 255) {
							int l8 = 0;
							int k13 = bw + abyte0[l3 + 1] >> 3;
							int i18 = cw + abyte0[l3 + 2] >> 3;
							l3 += 3;
							for (int i22 = 0; i22 < cx; i22++) {
								int j26 = (ex[i22] >> 3) - k13;
								int l29 = (fx[i22] >> 3) - i18;
								if (j26 != 0 || l29 != 0) {
									if (i22 != l8) {
										dx[l8] = dx[i22];
										dx[l8].ph = l8 + 10000;
										ex[l8] = ex[i22];
										fx[l8] = fx[i22];
										gx[l8] = gx[i22];
										hx[l8] = hx[i22];
									}
									l8++;
								} else {
									pt.sh(dx[i22]);
									cv.an(ex[i22], fx[i22], gx[i22], hx[i22]);
								}
							}

							cx = l8;
						} else {
							int i9 = o.vm(abyte0, l3);
							l3 += 2;
							int l13 = bw + abyte0[l3++];
							int j18 = cw + abyte0[l3++];
							byte byte3 = abyte0[l3++];
							int k26 = 0;
							for (int i30 = 0; i30 < cx; i30++)
								if (ex[i30] != l13 || fx[i30] != j18 || gx[i30] != byte3) {
									if (i30 != k26) {
										dx[k26] = dx[i30];
										dx[k26].ph = k26 + 10000;
										ex[k26] = ex[i30];
										fx[k26] = fx[i30];
										gx[k26] = gx[i30];
										hx[k26] = hx[i30];
									}
									k26++;
								} else {
									pt.sh(dx[i30]);
									cv.an(ex[i30], fx[i30], gx[i30], hx[i30]);
								}

							cx = k26;
							if (i9 != 65535) {
								cv.bo(l13, j18, byte3, i9);
								h h1 = qk(l13, j18, byte3, i9, cx);
								dx[cx] = h1;
								ex[cx] = l13;
								fx[cx] = j18;
								hx[cx] = i9;
								gx[cx++] = byte3;
							}
						}

					return;
				}
				if (i1 == 248) {
					int i4 = (j1 - 1) / 4;
					int j9 = 1;
					for (int i14 = 0; i14 < i4; i14++) {
						int k18 = o.vm(abyte0, j9);
						int j22 = o.sm(abyte0[j9 + 2]);
						int l26 = k18 >> 6 & 0x3ff;
						int j30 = k18 >> 1 & 0x1f;
						if (j30 > 15)
							j30 -= 32;
						int j33 = (k18 << 4 & 0x10) + (j22 >> 4 & 0xf);
						if (j33 > 15)
							j33 -= 32;
						int l35 = j22 & 0xf;
						int i38 = (bw + j30) * tt + 64;
						int j39 = (cw + j33) * tt + 64;
						int k40 = o.sm(abyte0[j9 + 3]);
						j9 += 4;
						if (k40 >= Definitions.ogb)
							k40 = 24;
						if (gw[l26] == null) {
							gw[l26] = new Player();
							gw[l26].ar = l26;
						}
						Player l40 = gw[l26];
						boolean flag4 = false;
						for (int i41 = 0; i41 < fw; i41++) {
							if (hw[i41] != l40)
								continue;
							flag4 = true;
							break;
						}

						if (!flag4)
							hw[fw++] = gw[l26];
						if (flag4) {
							l40.er = k40;
							l40.hr = l35;
							int j41 = l40.jr;
							if (i38 != l40.kr[j41] || j39 != l40.lr[j41]) {
								l40.jr = j41 = (j41 + 1) % 10;
								l40.kr[j41] = i38;
								l40.lr[j41] = j39;
							}
						} else {
							l40.ar = l26;
							l40.ir = 0;
							l40.jr = 0;
							l40.kr[0] = l40.cr = i38;
							l40.lr[0] = l40.dr = j39;
							l40.er = k40;
							l40.hr = l40.gr = l35;
							l40.fr = 0;
						}
					}

					return;
				}
				if (i1 == 247) {
					int j4 = o.vm(abyte0, 1);
					int k9 = 3;
					for (int j14 = 0; j14 < j4; j14++) {
						int l18 = o.vm(abyte0, k9);
						k9 += 2;
						Player l24 = gw[l18];
						int i27 = o.sm(abyte0[k9]);
						k9++;
						if (i27 == 1) {
							int k30 = o.vm(abyte0, k9);
							k9 += 2;
							byte byte6 = abyte0[k9];
							k9++;
							if (l24 != null) {
								String s2 = new String(abyte0, k9, byte6);
								l24.or = 150;
								l24.nr = s2;
								if (k30 == player.ar)
									gk("@yel@" + Definitions.npcName[l24.er][0] + ": " + l24.nr, 5);
							}
							k9 += byte6;
						} else if (i27 == 2) {
							int l30 = o.sm(abyte0[k9]);
							k9++;
							int k33 = o.sm(abyte0[k9]);
							k9++;
							int i36 = o.sm(abyte0[k9]);
							k9++;
							if (l24 != null) {
								l24.rr = l30;
								l24.sr = k33;
								l24.tr = i36;
								l24.ur = 200;
							}
						}
					}

					return;
				}
				if (i1 == 246) {
					wy = true;
					int k4 = o.sm(abyte0[1]);
					xy = k4;
					int l9 = 2;
					for (int k14 = 0; k14 < k4; k14++) {
						int i19 = o.sm(abyte0[l9]);
						l9++;
						yy[k14] = new String(abyte0, l9, i19);
						l9 += i19;
					}

					return;
				}
				if (i1 == 245) {
					wy = false;
					return;
				}
				if (i1 == 244) {
					ru = o.vm(abyte0, 1);
					su = o.vm(abyte0, 3);
					xu = o.vm(abyte0, 5);
					tu = o.vm(abyte0, 7);
					su -= xu * tu;
					return;
				}
				if (i1 == 243) {
					int l4 = 1;
					for (int i10 = 0; i10 < 16; i10++)
						level[i10] = o.sm(abyte0[l4++]);

					for (int l14 = 0; l14 < 16; l14++)
						realLevel[l14] = o.sm(abyte0[l4++]);

					questPoints = o.sm(abyte0[l4++]);
					return;
				}
				if (i1 == 242) {
					for (int i5 = 0; i5 < 5; i5++)
						qy[i5] = o.sm(abyte0[1 + i5]);

					return;
				}
				if (i1 == 241) {
					fu = 250;
					uy += 10;
					return;
				}
				if (i1 == 240) {
					int j5 = (j1 - 1) / 4;
					for (int j10 = 0; j10 < j5; j10++) {
						int i15 = bw + o.wm(abyte0, 1 + j10 * 4) >> 3;
						int j19 = cw + o.wm(abyte0, 3 + j10 * 4) >> 3;
						int k22 = 0;
						for (int j27 = 0; j27 < mw; j27++) {
							int i31 = (ow[j27] >> 3) - i15;
							int l33 = (pw[j27] >> 3) - j19;
							if (i31 != 0 || l33 != 0) {
								if (j27 != k22) {
									ow[k22] = ow[j27];
									pw[k22] = pw[j27];
									qw[k22] = qw[j27];
									rw[k22] = rw[j27];
								}
								k22++;
							}
						}

						mw = k22;
						k22 = 0;
						for (int j31 = 0; j31 < tw; j31++) {
							int i34 = (vw[j31] >> 3) - i15;
							int j36 = (ww[j31] >> 3) - j19;
							if (i34 != 0 || j36 != 0) {
								if (j31 != k22) {
									uw[k22] = uw[j31];
									uw[k22].ph = k22;
									vw[k22] = vw[j31];
									ww[k22] = ww[j31];
									xw[k22] = xw[j31];
									yw[k22] = yw[j31];
								}
								k22++;
							} else {
								pt.sh(uw[j31]);
								cv.zn(vw[j31], ww[j31], xw[j31]);
							}
						}

						tw = k22;
						k22 = 0;
						for (int j34 = 0; j34 < cx; j34++) {
							int k36 = (ex[j34] >> 3) - i15;
							int j38 = (fx[j34] >> 3) - j19;
							if (k36 != 0 || j38 != 0) {
								if (j34 != k22) {
									dx[k22] = dx[j34];
									dx[k22].ph = k22 + 10000;
									ex[k22] = ex[j34];
									fx[k22] = fx[j34];
									gx[k22] = gx[j34];
									hx[k22] = hx[j34];
								}
								k22++;
							} else {
								pt.sh(dx[j34]);
								cv.an(ex[j34], fx[j34], gx[j34], hx[j34]);
							}
						}

						cx = k22;
					}

					return;
				}
				if (i1 == 239) {
					sbb = true;
					return;
				}
				if (i1 == 238) {
					int k5 = o.vm(abyte0, 1);
					if (yv[k5] != null)
						vx = yv[k5].playerName;
					ux = true;
					cy = false;
					dy = false;
					wx = 0;
					zx = 0;
					return;
				}
				if (i1 == 237) {
					ux = false;
					return;
				}
				if (i1 == 236) {
					zx = abyte0[1] & 0xff;
					int l5 = 2;
					for (int k10 = 0; k10 < zx; k10++) {
						ay[k10] = o.vm(abyte0, l5);
						l5 += 2;
						by[k10] = o.vm(abyte0, l5);
						l5 += 2;
					}

					cy = false;
					dy = false;
					return;
				}
				if (i1 == 235) {
					byte byte0 = abyte0[1];
					if (byte0 == 1) {
						cy = true;
						return;
					} else {
						cy = false;
						return;
					}
				}
				if (i1 == 234) {
					gy = true;
					int i6 = 1;
					int l10 = abyte0[i6++] & 0xff;
					byte byte2 = abyte0[i6++];
					hy = abyte0[i6++] & 0xff;
					iy = abyte0[i6++] & 0xff;
					for (int k19 = 0; k19 < 40; k19++)
						jy[k19] = -1;

					for (int i23 = 0; i23 < l10; i23++) {
						jy[i23] = o.vm(abyte0, i6);
						i6 += 2;
						ky[i23] = o.vm(abyte0, i6);
						i6 += 2;
						ly[i23] = abyte0[i6++];
					}

					if (byte2 == 1) {
						int k27 = 39;
						for (int k31 = 0; k31 < ox; k31++) {
							if (k27 < l10)
								break;
							boolean flag1 = false;
							for (int l36 = 0; l36 < 40; l36++) {
								if (jy[l36] != px[k31])
									continue;
								flag1 = true;
								break;
							}

							if (px[k31] == 10)
								flag1 = true;
							if (!flag1) {
								jy[k27] = px[k31] & 0x7fff;
								ky[k27] = 0;
								ly[k27] = 0;
								k27--;
							}
						}

					}
					if (my >= 0 && my < 40 && jy[my] != ny) {
						my = -1;
						ny = -2;
					}
				} else {
					if (i1 == 233) {
						gy = false;
						return;
					}
					if (i1 == 229) {
						byte byte1 = abyte0[1];
						if (byte1 == 1) {
							dy = true;
							return;
						} else {
							dy = false;
							return;
						}
					}
					if (i1 == 228) {
						System.out.println("Got config");
						dt = o.sm(abyte0[1]) == 1;
						ct = o.sm(abyte0[2]) == 1;
						et = o.sm(abyte0[3]);
						kv = o.sm(abyte0[4]) == 1;
					}
				}
			}
		} catch (RuntimeException runtimeexception) {
			if (mt < 3) {
				super.dd.i(17);
				super.dd.d(runtimeexception.toString());
				ll();
				super.dd.i(17);
				super.dd.d("ptype:" + i1 + " psize:" + j1);
				ll();
				super.dd.i(17);
				super.dd.d("rx:" + bw + " ry:" + cw + " num3l:" + tw);
				ll();
				String s = "";
				for (int j15 = 0; j15 < 80 && j15 < j1; j15++)
					s = s + abyte0[j15] + " ";

				super.dd.i(17);
				super.dd.d(s);
				ll();
				mt++;
			}
		}
	}

	public boolean ol(int i1) {
		int j1 = player.cr / 128;
		int k1 = player.dr / 128;
		for (int l1 = 2; l1 >= 1; l1--) {
			if (i1 == 1 && ((cv.ueb[j1][k1 - l1] & 0x80) == 128 || (cv.ueb[j1 - l1][k1] & 0x80) == 128
					|| (cv.ueb[j1 - l1][k1 - l1] & 0x80) == 128))
				return false;
			if (i1 == 3 && ((cv.ueb[j1][k1 + l1] & 0x80) == 128 || (cv.ueb[j1 - l1][k1] & 0x80) == 128
					|| (cv.ueb[j1 - l1][k1 + l1] & 0x80) == 128))
				return false;
			if (i1 == 5 && ((cv.ueb[j1][k1 + l1] & 0x80) == 128 || (cv.ueb[j1 + l1][k1] & 0x80) == 128
					|| (cv.ueb[j1 + l1][k1 + l1] & 0x80) == 128))
				return false;
			if (i1 == 7 && ((cv.ueb[j1][k1 - l1] & 0x80) == 128 || (cv.ueb[j1 + l1][k1] & 0x80) == 128
					|| (cv.ueb[j1 + l1][k1 - l1] & 0x80) == 128))
				return false;
			if (i1 == 0 && (cv.ueb[j1][k1 - l1] & 0x80) == 128)
				return false;
			if (i1 == 2 && (cv.ueb[j1 - l1][k1] & 0x80) == 128)
				return false;
			if (i1 == 4 && (cv.ueb[j1][k1 + l1] & 0x80) == 128)
				return false;
			if (i1 == 6 && (cv.ueb[j1 + l1][k1] & 0x80) == 128)
				return false;
		}

		return true;
	}

	public void xk() {
		if ((pu & 1) == 1 && ol(pu))
			return;
		if ((pu & 1) == 0 && ol(pu)) {
			if (ol(pu + 1 & 7)) {
				pu = pu + 1 & 7;
				return;
			}
			if (ol(pu + 7 & 7))
				pu = pu + 7 & 7;
			return;
		}
		int[] ai = { 1, -1, 2, -2, 3, -3, 4 };
		for (int i1 = 0; i1 < 7; i1++) {
			if (!ol(pu + ai[i1] + 8 & 7))
				continue;
			pu = pu + ai[i1] + 8 & 7;
			break;
		}

		if ((pu & 1) == 0 && ol(pu)) {
			if (ol(pu + 1 & 7)) {
				pu = pu + 1 & 7;
				return;
			}
			if (ol(pu + 7 & 7))
				pu = pu + 7 & 7;
		} else {
		}
	}

	public void sendDeathInterface() {
		if (fu != 0) {
			qt.ze();
			qt.mg("Oh dear! You are dead...", bu / 2, cu / 2, 7, 0xff0000);
			sendChatInterface();
			qt.cf(ot, 0, 11);
			return;
		}
		if (!cv.web)
			return;
		if (sbb) {
			ek();
			return;
		}
		for (int i1 = 0; i1 < 64; i1++) {
			pt.sh(cv.zeb[uu][i1]);
			if (uu == 0) {
				pt.sh(cv.yeb[1][i1]);
				pt.sh(cv.zeb[1][i1]);
				pt.sh(cv.yeb[2][i1]);
				pt.sh(cv.zeb[2][i1]);
			}
			mu = true;
			if (uu == 0 && (cv.ueb[player.cr / 128][player.dr / 128] & 0x80) == 0) {
				pt.oh(cv.zeb[uu][i1]);
				if (uu == 0) {
					pt.oh(cv.yeb[1][i1]);
					pt.oh(cv.zeb[1][i1]);
					pt.oh(cv.yeb[2][i1]);
					pt.oh(cv.zeb[2][i1]);
				}
				mu = false;
			}
		}

		if (ocb != qcb) {
			qcb = ocb;
			for (int j1 = 0; j1 < tw; j1++) {
				if (xw[j1] == 51) {
					int i2 = vw[j1];
					int j3 = ww[j1];
					int k4 = i2 - player.cr / 128;
					int j6 = j3 - player.dr / 128;
					byte byte0 = 7;
					if (i2 >= 0 && j3 >= 0 && i2 < 96 && j3 < 96 && k4 > -byte0 && k4 < byte0 && j6 > -byte0
							&& j6 < byte0) {
						pt.sh(uw[j1]);
						String s1 = "torcha" + (ocb + 1);
						int l10 = Definitions.mo(s1);
						h h1 = zw[l10].le();
						pt.oh(h1);
						h1.ne(true, 48, 48, -50, -10, -50);
						h1.vd(uw[j1]);
						h1.ph = j1;
						uw[j1] = h1;
					}
				}
				if (xw[j1] == 143) {
					int j2 = vw[j1];
					int k3 = ww[j1];
					int i5 = j2 - player.cr / 128;
					int k6 = k3 - player.dr / 128;
					byte byte1 = 7;
					if (j2 >= 0 && k3 >= 0 && j2 < 96 && k3 < 96 && i5 > -byte1 && i5 < byte1 && k6 > -byte1
							&& k6 < byte1) {
						pt.sh(uw[j1]);
						String s2 = "skulltorcha" + (ocb + 1);
						int i11 = Definitions.mo(s2);
						h h2 = zw[i11].le();
						pt.oh(h2);
						h2.ne(true, 48, 48, -50, -10, -50);
						h2.vd(uw[j1]);
						h2.ph = j1;
						uw[j1] = h2;
					}
				}
			}

		}
		if (pcb != rcb) {
			rcb = pcb;
			for (int k1 = 0; k1 < tw; k1++)
				if (xw[k1] == 97) {
					int k2 = vw[k1];
					int l3 = ww[k1];
					int j5 = k2 - player.cr / 128;
					int i7 = l3 - player.dr / 128;
					byte byte2 = 9;
					if (k2 >= 0 && l3 >= 0 && k2 < 96 && l3 < 96 && j5 > -byte2 && j5 < byte2 && i7 > -byte2
							&& i7 < byte2) {
						pt.sh(uw[k1]);
						String s3 = "firea" + (pcb + 1);
						int j11 = Definitions.mo(s3);
						h h3 = zw[j11].le();
						pt.oh(h3);
						h3.ne(true, 48, 48, -50, -10, -50);
						h3.vd(uw[k1]);
						h3.ph = k1;
						uw[k1] = h3;
					}
				}

		}
		pt.ei(xv);
		xv = 0;
		for (int l1 = 0; l1 < wv; l1++) {
			Player l2 = zv[l1];
			if (l2.zr != 255) {
				int i4 = l2.cr;
				int k5 = l2.dr;
				int j7 = -cv.jn(i4, k5);
				int i9 = pt.fh(5000 + l1, i4, j7, k5, 145, 220, l1 + 10000);
				xv++;
				if (l2 == player)
					pt.gh(i9);
				if (l2.gr == 8)
					pt.ii(i9, -30);
				if (l2.gr == 9)
					pt.ii(i9, 30);
			}
		}

		for (int i3 = 0; i3 < wv; i3++) {
			Player l4 = zv[i3];
			if (l4.es > 0) {
				Player l5 = null;
				if (l4.ds != -1)
					l5 = gw[l4.ds];
				else if (l4.cs != -1)
					l5 = yv[l4.cs];
				if (l5 != null) {
					int k7 = l4.cr;
					int j9 = l4.dr;
					int j10 = -cv.jn(k7, j9) - 110;
					int k11 = l5.cr;
					int i12 = l5.dr;
					int j12 = -cv.jn(k11, i12) - Definitions.mhb[l5.er] / 2;
					int k12 = (k7 * l4.es + k11 * (uz - l4.es)) / uz;
					int l12 = (j10 * l4.es + j12 * (uz - l4.es)) / uz;
					int i13 = (j9 * l4.es + i12 * (uz - l4.es)) / uz;
					pt.fh(vz + l4.bs, k12, l12, i13, 32, 32, 0);
					xv++;
				}
			}
		}

		for (int j4 = 0; j4 < fw; j4++) {
			Player l6 = hw[j4];
			int l7 = l6.cr;
			int k9 = l6.dr;
			int k10 = -cv.jn(l7, k9);
			int l11 = pt.fh(20000 + j4, l7, k10, k9, Definitions.lhb[l6.er], Definitions.mhb[l6.er], j4 + 30000);
			xv++;
			if (l6.gr == 8)
				pt.ii(l11, -30);
			if (l6.gr == 9)
				pt.ii(l11, 30);
		}

		for (int i6 = 0; i6 < mw; i6++) {
			int i8 = ow[i6] * tt + 64;
			int l9 = pw[i6] * tt + 64;
			pt.fh(40000 + qw[i6], i8, -cv.jn(i8, l9) - rw[i6], l9, 96, 64, i6 + 20000);
			xv++;
		}

		qt.rk = false;
		qt.df();
		qt.rk = super.sq;
		if (uu == 3) {
			int j8 = 40 + (int) (Math.random() * 3D);
			int i10 = 40 + (int) (Math.random() * 7D);
			pt.th(true, j8, i10, -50, -10, -50);
		}
		bdb = 0;
		vcb = 0;
		gdb = 0;
		if (bt) {
			if (ct && !mu) {
				int k8 = pu;
				xk();
				if (pu != k8) {
					nu = player.cr;
					ou = player.dr;
				}
			}
			pt.dm = 3000;
			pt.em = 3000;
			pt.fm = 1;
			pt.gm = 2800;
			zt = pu * 32;
			pt.uh(nu, -cv.jn(nu, ou), ou, 912, zt * 4, 0, 2000);
		} else {
			if (ct && !mu)
				xk();
			if (!super.sq) {
				pt.dm = 2400;
				pt.em = 2400;
				pt.fm = 1;
				pt.gm = 2300;
			} else {
				pt.dm = 2200;
				pt.em = 2200;
				pt.fm = 1;
				pt.gm = 2100;
			}
			pt.uh(nu, -cv.jn(nu, ou), ou, 912, zt * 4, 0, lu * 2);
		}
		pt.qi();
		nk();
		if (scb > 0)
			qt.rg(tcb - 8, ucb - 8, az + 14 + (24 - scb) / 6);
		if (scb < 0)
			qt.rg(tcb - 8, ucb - 8, az + 18 + (24 + scb) / 6);
		qt.ef("Fps: " + super.xq, 450, cu - 10, 1, 0xffff00);
		if (pz == 0) {
			for (int l8 = 0; l8 < qz; l8++)
				if (sz[l8] > 0) {
					String s = rz[l8];
					qt.ef(s, 7, cu - 18 - l8 * 12, 1, 0xffff00);
				}

		}
		kz.nd(lz);
		kz.nd(nz);
		kz.nd(oz);
		if (pz == 1)
			kz.bd(lz);
		else if (pz == 2)
			kz.bd(nz);
		else if (pz == 3)
			kz.bd(oz);
		g.ig = 2;
		kz.fc();
		g.ig = 0;
		qt.vf(((GameImageProducer) (qt)).sj - 3 - 197, 3, az);
		jk();
		qt.vk = false;
		sendChatInterface();
		qt.cf(ot, 0, 11);
	}

	public void sendChatInterface() {
		qt.rg(0, cu - 4, az + 23);
		int i1 = GameImageProducer.fg(200, 200, 255);
		if (pz == 0)
			i1 = GameImageProducer.fg(255, 200, 50);
		if (gz % 30 > 15)
			i1 = GameImageProducer.fg(255, 50, 50);
		qt.mg("All messages", 54, cu + 6, 0, i1);
		i1 = GameImageProducer.fg(200, 200, 255);
		if (pz == 1)
			i1 = GameImageProducer.fg(255, 200, 50);
		if (hz % 30 > 15)
			i1 = GameImageProducer.fg(255, 50, 50);
		qt.mg("Chat history", 155, cu + 6, 0, i1);
		i1 = GameImageProducer.fg(200, 200, 255);
		if (pz == 2)
			i1 = GameImageProducer.fg(255, 200, 50);
		if (iz % 30 > 15)
			i1 = GameImageProducer.fg(255, 50, 50);
		qt.mg("Quest history", 255, cu + 6, 0, i1);
		i1 = GameImageProducer.fg(200, 200, 255);
		if (pz == 3)
			i1 = GameImageProducer.fg(255, 200, 50);
		if (jz % 30 > 15)
			i1 = GameImageProducer.fg(255, 50, 50);
		qt.mg("Private history", 355, cu + 6, 0, i1);
	}

	public void hk(int i1, int j1, int k1, int l1, int i2, int j2, int k2) {
		int l2 = Definitions.zfb[i2] + nw;
		int i3 = Definitions.lgb[i2];
		qt.pf(i1, j1, k1, l1, l2, i3, 0, 0, false);
	}

	public void uk(int i1, int j1, int k1, int l1, int i2, int j2, int k2) {
		Player l2 = hw[i2];
		int i3 = l2.gr + (zt + 16) / 32 & 7;
		boolean flag = false;
		int j3 = i3;
		if (j3 == 5) {
			j3 = 3;
			flag = true;
		} else if (j3 == 6) {
			j3 = 2;
			flag = true;
		} else if (j3 == 7) {
			j3 = 1;
			flag = true;
		}
		int k3 = j3 * 3 + vt[(l2.fr / Definitions.nhb[l2.er]) % 4];
		if (l2.gr == 8) {
			j3 = 5;
			flag = false;
			i1 -= (Definitions.phb[l2.er] * k2) / 100;
			k3 = j3 * 3 + wt[(eu / (Definitions.ohb[l2.er] - 1)) % 8];
		} else if (l2.gr == 9) {
			j3 = 5;
			flag = true;
			i1 += (Definitions.phb[l2.er] * k2) / 100;
			k3 = j3 * 3 + xt[(eu / Definitions.ohb[l2.er]) % 8];
		}
		for (int l3 = 0; l3 < 12; l3++) {
			int i4 = animDirLayer_To_CharLayer[j3][l3];
			int l4 = Definitions.ghb[l2.er][i4];
			if (l4 >= 0) {
				int j5 = 0;
				int k5 = 0;
				int l5 = k3;
				if (flag && j3 >= 1 && j3 <= 3 && Definitions.yhb[l4] == 1)
					l5 += 15;
				if (j3 != 5 || Definitions.xhb[l4] == 1) {
					int i6 = l5 + Definitions.zhb[l4];
					j5 = (j5 * k1) / ((GameImageProducer) (qt)).jk[i6];
					k5 = (k5 * l1) / ((GameImageProducer) (qt)).kk[i6];
					int j6 = (k1 * ((GameImageProducer) (qt)).jk[i6]) / ((GameImageProducer) (qt)).jk[Definitions.zhb[l4]];
					j5 -= (j6 - k1) / 2;
					int k6 = Definitions.vhb[l4];
					int l6 = 0;
					if (k6 == 1) {
						k6 = Definitions.hhb[l2.er];
						l6 = Definitions.khb[l2.er];
					} else if (k6 == 2) {
						k6 = Definitions.ihb[l2.er];
						l6 = Definitions.khb[l2.er];
					} else if (k6 == 3) {
						k6 = Definitions.jhb[l2.er];
						l6 = Definitions.khb[l2.er];
					}
					qt.pf(i1 + j5, j1 + k5, j6, l1, i6, k6, l6, j2, flag);
				}
			}
		}

		if (l2.or > 0) {
			zcb[vcb] = qt.xe(l2.nr, 1) / 2;
			adb[vcb] = qt.ig(1);
			if (zcb[vcb] > 300) {
				zcb[vcb] = 300;
				adb[vcb] *= 2;
			}
			xcb[vcb] = i1 + k1 / 2;
			ycb[vcb] = j1;
			wcb[vcb++] = l2.nr;
		}
		if (l2.gr == 8 || l2.gr == 9 || l2.ur != 0) {
			if (l2.ur > 0) {
				int j4 = i1;
				if (l2.gr == 8)
					j4 -= (20 * k2) / 100;
				else if (l2.gr == 9)
					j4 += (20 * k2) / 100;
				int i5 = (l2.sr * 30) / l2.tr;
				hdb[gdb] = j4 + k1 / 2;
				idb[gdb] = j1;
				jdb[gdb++] = i5;
			}
			if (l2.ur > 150) {
				int k4 = i1;
				if (l2.gr == 8)
					k4 -= (10 * k2) / 100;
				else if (l2.gr == 9)
					k4 += (10 * k2) / 100;
				qt._mthif(k4 + k1 / 2, j1 + l1 / 2, az + 12);
				qt.mg(String.valueOf(l2.rr), (k4 + k1 / 2) - 1, j1 + l1 / 2 + 5, 3, 0xffffff);
			}
		}
	}

	public void il(int i1, int j1, int k1, int l1, int i2, int j2, int k2) {
		Player l2 = zv[i2];
		if (l2.zr == 255)
			return;
		int i3 = l2.gr + (zt + 16) / 32 & 7;
		boolean flag = false;
		int j3 = i3;
		if (j3 == 5) {
			j3 = 3;
			flag = true;
		} else if (j3 == 6) {
			j3 = 2;
			flag = true;
		} else if (j3 == 7) {
			j3 = 1;
			flag = true;
		}
		int k3 = j3 * 3 + vt[(l2.fr / 6) % 4];
		if (l2.gr == 8) {
			j3 = 5;
			i3 = 2;
			flag = false;
			i1 -= (5 * k2) / 100;
			k3 = j3 * 3 + wt[(eu / 5) % 8];
		} else if (l2.gr == 9) {
			j3 = 5;
			i3 = 2;
			flag = true;
			i1 += (5 * k2) / 100;
			k3 = j3 * 3 + xt[(eu / 6) % 8];
		}
		for (int l3 = 0; l3 < 12; l3++) {
			int i4 = animDirLayer_To_CharLayer[i3][l3];
			int i5 = l2.mr[i4] - 1;
			if (i5 >= 0) {
				int l5 = 0;
				int j6 = 0;
				int k6 = k3;
				if (flag && j3 >= 1 && j3 <= 3)
					if (Definitions.yhb[i5] == 1)
						k6 += 15;
					else if (i4 == 4 && j3 == 1) {
						l5 = -22;
						j6 = -3;
						k6 = j3 * 3 + vt[(2 + l2.fr / 6) % 4];
					} else if (i4 == 4 && j3 == 2) {
						l5 = 0;
						j6 = -8;
						k6 = j3 * 3 + vt[(2 + l2.fr / 6) % 4];
					} else if (i4 == 4 && j3 == 3) {
						l5 = 26;
						j6 = -5;
						k6 = j3 * 3 + vt[(2 + l2.fr / 6) % 4];
					} else if (i4 == 3 && j3 == 1) {
						l5 = 22;
						j6 = 3;
						k6 = j3 * 3 + vt[(2 + l2.fr / 6) % 4];
					} else if (i4 == 3 && j3 == 2) {
						l5 = 0;
						j6 = 8;
						k6 = j3 * 3 + vt[(2 + l2.fr / 6) % 4];
					} else if (i4 == 3 && j3 == 3) {
						l5 = -26;
						j6 = 5;
						k6 = j3 * 3 + vt[(2 + l2.fr / 6) % 4];
					}
				if (j3 != 5 || Definitions.xhb[i5] == 1) {
					int l6 = k6 + Definitions.zhb[i5];
					l5 = (l5 * k1) / ((GameImageProducer) (qt)).jk[l6];
					j6 = (j6 * l1) / ((GameImageProducer) (qt)).kk[l6];
					int i7 = (k1 * ((GameImageProducer) (qt)).jk[l6]) / ((GameImageProducer) (qt)).jk[Definitions.zhb[i5]];
					l5 -= (i7 - k1) / 2;
					int j7 = Definitions.vhb[i5];
					int k7 = 0;
					if (j7 == 1) {
						j7 = ccb[l2.xr];
						k7 = dcb[l2.as];
					} else if (j7 == 2) {
						j7 = bcb[l2.yr];
						k7 = dcb[l2.as];
					} else if (j7 == 3) {
						j7 = bcb[l2.zr];
						k7 = dcb[l2.as];
					}
					qt.pf(i1 + l5, j1 + j6, i7, l1, l6, j7, k7, j2, flag);
				}
			}
		}

		if (l2.or > 0) {
			zcb[vcb] = qt.xe(l2.nr, 1) / 2;
			adb[vcb] = qt.ig(1);
			if (zcb[vcb] > 300) {
				zcb[vcb] = 300;
				adb[vcb] *= 2;
			}
			xcb[vcb] = i1 + k1 / 2;
			ycb[vcb] = j1;
			wcb[vcb++] = l2.nr;
		}
		if (l2.qr > 0) {
			cdb[bdb] = i1 + k1 / 2;
			ddb[bdb] = j1;
			edb[bdb] = k2;
			fdb[bdb++] = l2.pr;
		}
		if (l2.gr == 8 || l2.gr == 9 || l2.ur != 0) {
			if (l2.ur > 0) {
				int j4 = i1;
				if (l2.gr == 8)
					j4 -= (20 * k2) / 100;
				else if (l2.gr == 9)
					j4 += (20 * k2) / 100;
				int j5 = (l2.sr * 30) / l2.tr;
				hdb[gdb] = j4 + k1 / 2;
				idb[gdb] = j1;
				jdb[gdb++] = j5;
			}
			if (l2.ur > 150) {
				int k4 = i1;
				if (l2.gr == 8)
					k4 -= (10 * k2) / 100;
				else if (l2.gr == 9)
					k4 += (10 * k2) / 100;
				qt._mthif(k4 + k1 / 2, j1 + l1 / 2, az + 11);
				qt.mg(String.valueOf(l2.rr), (k4 + k1 / 2) - 1, j1 + l1 / 2 + 5, 3, 0xffffff);
			}
		}
		if (l2.hs == 1 && l2.qr == 0) {
			int l4 = j2 + i1 + k1 / 2;
			if (l2.gr == 8)
				l4 -= (20 * k2) / 100;
			else if (l2.gr == 9)
				l4 += (20 * k2) / 100;
			int k5 = (16 * k2) / 100;
			int i6 = (16 * k2) / 100;
			qt.lf(l4 - k5 / 2, j1 - i6 / 2 - (10 * k2) / 100, k5, i6, az + 13);
		}
	}

	public void nk() {
		for (int i1 = 0; i1 < vcb; i1++) {
			int j1 = xcb[i1];
			int l1 = ycb[i1];
			int k2 = zcb[i1];
			int j3 = adb[i1];
			boolean flag = true;
			while (flag) {
				flag = false;
				for (int k4 = 0; k4 < i1; k4++)
					if (l1 > ycb[k4] - adb[k4] && l1 - j3 < ycb[k4] && j1 - k2 < xcb[k4] + zcb[k4]
							&& j1 + k2 > xcb[k4] - zcb[k4]) {
						l1 = ycb[k4] - j3;
						flag = true;
					}

			}
			ycb[i1] = l1;
			qt.ug(wcb[i1], j1, l1, 1, 0xffff00, 300);
		}

		for (int k1 = 0; k1 < bdb; k1++) {
			int i2 = cdb[k1];
			int l2 = ddb[k1];
			int k3 = edb[k1];
			int i4 = fdb[k1];
			int l4 = (39 * k3) / 100;
			int i5 = (27 * k3) / 100;
			int j5 = l2 - i5;
			qt.wg(i2 - l4 / 2, j5, l4, i5, az + 9, 85);
			int k5 = (36 * k3) / 100;
			int l5 = (24 * k3) / 100;
			qt.pf(i2 - k5 / 2, (j5 + i5 / 2) - l5 / 2, k5, l5, Definitions.zfb[i4] + nw, Definitions.lgb[i4], 0, 0, false);
		}

		for (int j2 = 0; j2 < gdb; j2++) {
			int i3 = hdb[j2];
			int l3 = idb[j2];
			int j4 = jdb[j2];
			qt.nf(i3 - 15, l3 - 3, j4, 5, 65280, 192);
			qt.nf((i3 - 15) + j4, l3 - 3, 30 - j4, 5, 0xff0000, 192);
		}

	}

	public int getItemAmount(int itemId) {
		int j1 = 0;
		for (int k1 = 0; k1 < ox; k1++)
			if (px[k1] == itemId)
				if (Definitions.bgb[itemId] == 1)
					j1++;
				else
					j1 += qx[k1];

		return j1;
	}

	public boolean mk(int i1, int j1) {
		if (i1 == 31 && ok(197))
			return true;
		if (i1 == 32 && ok(102))
			return true;
		if (i1 == 33 && ok(101))
			return true;
		if (i1 == 34 && ok(103))
			return true;
		return getItemAmount(i1) >= j1;
	}

	public boolean ok(int i1) {
		for (int j1 = 0; j1 < ox; j1++)
			if (px[j1] == i1 && rx[j1] == 1)
				return true;

		return false;
	}

	public void fk(int i1, int j1, int k1) {
		qt.gg(i1, j1, k1);
		qt.gg(i1 - 1, j1, k1);
		qt.gg(i1 + 1, j1, k1);
		qt.gg(i1, j1 - 1, k1);
		qt.gg(i1, j1 + 1, k1);
	}

	public void ul(int i1, int j1, int k1, int l1, boolean flag) {
		fl(i1, j1, k1, l1, k1, l1, false, flag);
	}

	public void gl(int i1, int j1, int k1, int l1, boolean flag) {
		if (fl(i1, j1, k1, l1, k1, l1, false, flag)) {
		} else {
			fl(i1, j1, k1, l1, k1, l1, true, flag);
		}
	}

	public void zk(int i1, int j1, int k1, int l1) {
		int i2;
		int j2;
		if (k1 == 0 || k1 == 4) {
			i2 = Definitions.gib[l1];
			j2 = Definitions.hib[l1];
		} else {
			j2 = Definitions.gib[l1];
			i2 = Definitions.hib[l1];
		}
		if (Definitions.iib[l1] == 2 || Definitions.iib[l1] == 3) {
			if (k1 == 0) {
				i1--;
				i2++;
			}
			if (k1 == 2)
				j2++;
			if (k1 == 4)
				i2++;
			if (k1 == 6) {
				j1--;
				j2++;
			}
			fl(bw, cw, i1, j1, (i1 + i2) - 1, (j1 + j2) - 1, false, true);
		} else {
			fl(bw, cw, i1, j1, (i1 + i2) - 1, (j1 + j2) - 1, true, true);
		}
	}

	public void dm(int i1, int j1, int k1) {
		if (k1 == 0) {
			fl(bw, cw, i1, j1 - 1, i1, j1, false, true);
			return;
		}
		if (k1 == 1) {
			fl(bw, cw, i1 - 1, j1, i1, j1, false, true);
		} else {
			fl(bw, cw, i1, j1, i1, j1, true, true);
		}
	}

	public boolean fl(int i1, int j1, int k1, int l1, int i2, int j2, boolean flag, boolean flag1) {
		int k2 = cv.cn(i1, j1, k1, l1, i2, j2, kx, lx, flag);
		if (k2 == -1)
			return false;
		k2--;
		i1 = kx[k2];
		j1 = lx[k2];
		k2--;
		if (flag1)
			super.dd.i(215);
		else
			super.dd.i(255);
		super.dd.k(i1 + vu);
		super.dd.k(j1 + wu);
		for (int l2 = k2; l2 >= 0 && l2 > k2 - 25; l2--) {
			super.dd.n(kx[l2] - i1);
			super.dd.n(lx[l2] - j1);
		}

		ll();
		scb = -24;
		tcb = super.mq;
		ucb = super.nq;
		return true;
	}

	public void yk(int i1, int j1) {
		if (fu != 0) {
			cv.web = false;
			return;
		}
		i1 += ru;
		j1 += su;
		if (uu == xu && i1 > yu && i1 < av && j1 > zu && j1 < bv) {
			cv.web = true;
			return;
		}
		qt.mg("Loading... Please wait", 256, 192, 1, 0xffffff);
		sendChatInterface();
		qt.cf(ot, 0, 11);
		int k1 = vu;
		int l1 = wu;
		int i2 = (i1 + 24) / 48;
		int j2 = (j1 + 24) / 48;
		uu = xu;
		vu = i2 * 48 - 48;
		wu = j2 * 48 - 48;
		yu = i2 * 48 - 32;
		zu = j2 * 48 - 32;
		av = i2 * 48 + 32;
		bv = j2 * 48 + 32;
		cv.rn(i1, j1, uu);
		vu -= ru;
		wu -= su;
		int k2 = vu - k1;
		int l2 = wu - l1;
		for (int i3 = 0; i3 < tw; i3++) {
			vw[i3] -= k2;
			ww[i3] -= l2;
			int j3 = vw[i3];
			int l3 = ww[i3];
			int k4 = xw[i3];
			h h1 = uw[i3];
			try {
				int i6 = yw[i3];
				int i7;
				int k7;
				if (i6 == 0 || i6 == 4) {
					i7 = Definitions.gib[k4];
					k7 = Definitions.hib[k4];
				} else {
					k7 = Definitions.gib[k4];
					i7 = Definitions.hib[k4];
				}
				int l7 = ((j3 + j3 + i7) * tt) / 2;
				int i8 = ((l3 + l3 + k7) * tt) / 2;
				if (j3 >= 0 && l3 >= 0 && j3 < 96 && l3 < 96) {
					pt.oh(h1);
					h1.ce(l7, -cv.jn(l7, i8), i8);
					cv.pn(j3, l3, k4);
					if (k4 == 74)
						h1.wd(0, -480, 0);
				}
			} catch (RuntimeException runtimeexception) {
				System.out.println("Loc Error: " + runtimeexception.getMessage());
				System.out.println("i:" + i3 + " obj:" + h1);
				runtimeexception.printStackTrace();
			}
		}

		for (int k3 = 0; k3 < cx; k3++) {
			ex[k3] -= k2;
			fx[k3] -= l2;
			int i4 = ex[k3];
			int l4 = fx[k3];
			int j5 = hx[k3];
			int j6 = gx[k3];
			try {
				cv.bo(i4, l4, j6, j5);
				h h2 = qk(i4, l4, j6, j5, k3);
				dx[k3] = h2;
			} catch (RuntimeException runtimeexception1) {
				System.out.println("Bound Error: " + runtimeexception1.getMessage());
				runtimeexception1.printStackTrace();
			}
		}

		for (int j4 = 0; j4 < mw; j4++) {
			ow[j4] -= k2;
			pw[j4] -= l2;
		}

		for (int i5 = 0; i5 < wv; i5++) {
			Player l5 = zv[i5];
			l5.cr -= k2 * tt;
			l5.dr -= l2 * tt;
			for (int k6 = 0; k6 < l5.jr; k6++) {
				l5.kr[k6] -= k2 * tt;
				l5.lr[k6] -= l2 * tt;
			}

		}

		for (int k5 = 0; k5 < fw; k5++) {
			Player l6 = hw[k5];
			l6.cr -= k2 * tt;
			l6.dr -= l2 * tt;
			for (int j7 = 0; j7 < l6.jr; j7++) {
				l6.kr[j7] -= k2 * tt;
				l6.lr[j7] -= l2 * tt;
			}

		}

		cv.web = true;
		hu = false;
	}

	public h qk(int i1, int j1, int k1, int l1, int i2) {
		int j2 = i1;
		int k2 = j1;
		int l2 = i1;
		int i3 = j1;
		int j3 = Definitions.rib[l1];
		int k3 = Definitions.sib[l1];
		int l3 = Definitions.qib[l1];
		h h1 = new h(4, 1);
		if (k1 == 0)
			l2 = i1 + 1;
		if (k1 == 1)
			i3 = j1 + 1;
		if (k1 == 2) {
			j2 = i1 + 1;
			i3 = j1 + 1;
		}
		if (k1 == 3) {
			l2 = i1 + 1;
			i3 = j1 + 1;
		}
		j2 *= tt;
		k2 *= tt;
		l2 *= tt;
		i3 *= tt;
		int i4 = h1.je(j2, -cv.jn(j2, k2), k2);
		int j4 = h1.je(j2, -cv.jn(j2, k2) - l3, k2);
		int k4 = h1.je(l2, -cv.jn(l2, i3) - l3, i3);
		int l4 = h1.je(l2, -cv.jn(l2, i3), i3);
		int[] ai = { i4, j4, k4, l4 };
		h1.ie(4, ai, j3, k3);
		h1.ne(false, 60, 24, -50, -10, -50);
		if (i1 >= 0 && j1 >= 0 && i1 < 96 && j1 < 96)
			pt.oh(h1);
		h1.ph = i2 + 10000;
		return h1;
	}

	public void jk() {
		if (gy && lt == 0)
			sendShopInterface();
		else if (ux)
			sendTradeInterface();
		else if (gt != 0)
			sendLogin();
		else if (ht != 0)
			handleFriendsList();
		else if (ft == 1)
			sendPvpSwitchInterface();
		else if (!kt && jt > 0x2bf20 && lt == 0) {
			sendAdvertisementInterface();
		} else {
			if (wy)
				nl();
			if (player.ur > 0)
				sendCombatStyleInterface();
			pk();
			boolean flag = !wy && !dv;
			if (flag)
				iv = 0;
			if (nx == 0 && flag)
				sendPlayerOptions();
			if (nx == 1)
				sendInventoryInterface(flag);
			if (nx == 2)
				sendMapInterface(flag);
			if (nx == 3)
				sendSkillsInterface(flag);
			if (nx == 4)
				sendMagicInterface(flag);
			if (nx == 5)
				sendFriendsInterface(flag);
			if (nx == 6)
				sendSettingsInterface(flag);
			if (!dv && !wy)
				fm();
			if (dv && !wy)
				am();
		}
		mx = 0;
	}

	public void nl() {
		if (mx != 0) {
			for (int i1 = 0; i1 < xy; i1++) {
				if (super.mq >= qt.xe(yy[i1], 1) || super.nq <= i1 * 12 || super.nq >= 12 + i1 * 12)
					continue;
				super.dd.i(237);
				super.dd.n(i1);
				ll();
				break;
			}

			mx = 0;
			wy = false;
			return;
		}
		for (int j1 = 0; j1 < xy; j1++) {
			int k1 = 65535;
			if (super.mq < qt.xe(yy[j1], 1) && super.nq > j1 * 12 && super.nq < 12 + j1 * 12)
				k1 = 0xff0000;
			qt.ef(yy[j1], 6, 12 + j1 * 12, 1, k1);
		}

	}

	public void sendCombatStyleInterface() {
		byte byte0 = 7;
		byte byte1 = 15;
		char c1 = '\257';
		if (mx != 0) {
			for (int i1 = 0; i1 < 5; i1++) {
				if (i1 <= 0 || super.mq <= byte0 || super.mq >= byte0 + c1 || super.nq <= byte1 + i1 * 20
						|| super.nq >= byte1 + i1 * 20 + 20)
					continue;
				tz = i1 - 1;
				mx = 0;
				super.dd.i(231);
				super.dd.n(tz);
				ll();
				break;
			}

		}
		for (int j1 = 0; j1 < 5; j1++) {
			if (j1 == tz + 1)
				qt.nf(byte0, byte1 + j1 * 20, c1, 20, GameImageProducer.fg(255, 0, 0), 128);
			else
				qt.nf(byte0, byte1 + j1 * 20, c1, 20, GameImageProducer.fg(190, 190, 190), 128);
			qt.kg(byte0, byte1 + j1 * 20, c1, 0);
			qt.kg(byte0, byte1 + j1 * 20 + 20, c1, 0);
		}

		qt.mg("Select combat style", byte0 + c1 / 2, byte1 + 16, 3, 0xffffff);
		qt.mg("Controlled (+1 of each)", byte0 + c1 / 2, byte1 + 36, 3, 0);
		qt.mg("Aggressive (+3 strength)", byte0 + c1 / 2, byte1 + 56, 3, 0);
		qt.mg("Accurate   (+3 attack)", byte0 + c1 / 2, byte1 + 76, 3, 0);
		qt.mg("Defensive  (+3 defense)", byte0 + c1 / 2, byte1 + 96, 3, 0);
	}

	public void sendAdvertisementInterface() {
		if (mx != 0) {
			mx = 0;
			if (super.mq > 200 && super.mq < 300 && super.nq > 230 && super.nq < 253) {
				kt = true;
				return;
			}
		}
		int i1 = 90;
		qt.qf(106, 70, 300, 190, 0);
		qt.hf(106, 70, 300, 190, 0xffffff);
		qt.mg("You have been playing for", 256, i1, 4, 0xffffff);
		i1 += 20;
		qt.mg("over 1 hour. Please consider", 256, i1, 4, 0xffffff);
		i1 += 20;
		qt.mg("visiting our advertiser if you", 256, i1, 4, 0xffffff);
		i1 += 20;
		qt.mg("see an advert which interests you.", 256, i1, 4, 0xffffff);
		i1 += 40;
		qt.mg("Doing so will help ensure", 256, i1, 4, 0xffffff);
		i1 += 20;
		qt.mg("Runescape remains free.", 256, i1, 4, 0xffffff);
		i1 += 40;
		int j1 = 0xffffff;
		if (super.mq > 200 && super.mq < 300 && super.nq > i1 - 20 && super.nq < i1 + 3)
			j1 = 0xffff00;
		qt.mg("Close", 256, i1, 4, j1);
	}

	public void sendPvpSwitchInterface() {
		if (mx != 0) {
			mx = 0;
			char c1 = '\372';
			if (super.mq < 56 || super.nq < 70 || super.mq > 456 || super.nq > 260) {
				ft = 0;
				return;
			}
			if (super.mq > 250 && super.mq < 350 && super.nq > c1 - 20 && super.nq < c1 + 3) {
				ft = 0;
				return;
			}
			if (super.mq > 150 && super.mq < 250 && super.nq > c1 - 20 && super.nq < c1 + 3) {
				dt = !dt;
				super.dd.i(213);
				super.dd.n(1);
				super.dd.n(dt ? 1 : 0);
				super.dd.e();
				ft = 0;
				return;
			}
		}
		qt.qf(56, 70, 400, 190, 0);
		qt.hf(56, 70, 400, 190, 0xffffff);
		int i1 = 90;
		if (!dt) {
			qt.mg("Are you sure you want to change", 256, i1, 4, 0xffffff);
			i1 += 20;
			qt.mg("to being able to fight other players?", 256, i1, 4, 0xffffff);
			i1 += 40;
			qt.mg("If you do other players will be able to", 256, i1, 4, 0xffffff);
			i1 += 20;
			qt.mg("attack you, and you will probably die", 256, i1, 4, 0xffffff);
			i1 += 20;
			qt.mg("much more often.", 256, i1, 4, 0xffffff);
			i1 += 40;
		}
		if (dt) {
			qt.mg("Are you sure you want to change", 256, i1, 4, 0xffffff);
			i1 += 20;
			qt.mg("to not fighting other players?", 256, i1, 4, 0xffffff);
			i1 += 40;
			qt.mg("This will make you safe from attack,", 256, i1, 4, 0xffffff);
			i1 += 20;
			qt.mg("but will also preventing you from attacking", 256, i1, 4, 0xffffff);
			i1 += 20;
			qt.mg("others (except in the arena - coming soon)", 256, i1, 4, 0xffffff);
			i1 += 40;
		}
		if (et == 2) {
			qt.mg("You can only change a total of 2 times", 256, i1, 4, 0xffffff);
			i1 += 20;
		}
		if (et == 1) {
			qt.mg("You will not be allowed to change back again", 256, i1, 4, 0xffffff);
			i1 += 20;
		}
		i1 = 250;
		int j1 = 0xffffff;
		if (super.mq > 150 && super.mq < 250 && super.nq > i1 - 20 && super.nq < i1 + 3)
			j1 = 0xffff00;
		qt.mg("Yes I'm sure", 200, i1, 4, j1);
		j1 = 0xffffff;
		if (super.mq > 250 && super.mq < 350 && super.nq > i1 - 20 && super.nq < i1 + 3)
			j1 = 0xffff00;
		qt.mg("No thanks", 300, i1, 4, j1);
	}

	public void sendLogin() {
		if (mx != 0) {
			mx = 0;
			if (super.mq < 106 || super.nq < 150 || super.mq > 406 || super.nq > 210) {
				gt = 0;
				return;
			}
		}
		int i1 = 150;
		qt.qf(106, i1, 300, 60, 0);
		qt.hf(106, i1, 300, 60, 0xffffff);
		i1 += 22;
		if (gt == 1) {
			qt.mg("Please enter your new password", 256, i1, 4, 0xffffff);
			i1 += 25;
			String s = "*";
			for (int j1 = 0; j1 < super.tq.length(); j1++)
				s = "X" + s;

			qt.mg(s, 256, i1, 4, 0xffffff);
			if (super.uq.length() > 0) {
				it = super.uq;
				super.tq = "";
				super.uq = "";
				gt = 2;
			}
		} else if (gt == 2) {
			qt.mg("Enter password again to confirm", 256, i1, 4, 0xffffff);
			i1 += 25;
			String s1 = "*";
			for (int k1 = 0; k1 < super.tq.length(); k1++)
				s1 = "X" + s1;

			qt.mg(s1, 256, i1, 4, 0xffffff);
			if (super.uq.length() > 0)
				if (super.uq.equalsIgnoreCase(it)) {
					gt = 4;
					ab(it);
				} else {
					gt = 3;
				}
		} else {
			if (gt == 3) {
				qt.mg("Passwords do not match!", 256, i1, 4, 0xffffff);
				i1 += 25;
				qt.mg("Press any key to close", 256, i1, 4, 0xffffff);
				return;
			}
			if (gt == 4) {
				qt.mg("Ok, your request has been sent", 256, i1, 4, 0xffffff);
				i1 += 25;
				qt.mg("Press any key to close", 256, i1, 4, 0xffffff);
			}
		}
	}

	public void handleFriendsList() {
		if (mx != 0) {
			mx = 0;
			if (ht == 1 && (super.mq < 106 || super.nq < 145 || super.mq > 406 || super.nq > 215)) {
				ht = 0;
				return;
			}
			if (ht == 2 && (super.mq < 6 || super.nq < 145 || super.mq > 506 || super.nq > 215)) {
				ht = 0;
				return;
			}
			if (ht == 3 && (super.mq < 106 || super.nq < 145 || super.mq > 406 || super.nq > 215)) {
				ht = 0;
				return;
			}
			if (super.mq > 236 && super.mq < 276 && super.nq > 193 && super.nq < 213) {
				ht = 0;
				return;
			}
		}
		int i1 = 145;
		if (ht == 1) {
			qt.qf(106, i1, 300, 70, 0);
			qt.hf(106, i1, 300, 70, 0xffffff);
			i1 += 20;
			qt.mg("Enter name to add to friends list", 256, i1, 4, 0xffffff);
			i1 += 20;
			qt.mg(super.tq + "*", 256, i1, 4, 0xffffff);
			if (super.uq.length() > 0) {
				String s = super.uq.trim();
				super.tq = "";
				super.uq = "";
				ht = 0;
				if (s.length() > 0 && o.rm(s) != player.yq)
					gb(s);
			}
		}
		if (ht == 2) {
			qt.qf(6, i1, 500, 70, 0);
			qt.hf(6, i1, 500, 70, 0xffffff);
			i1 += 20;
			qt.mg("Enter message to send to " + o.tm(kcb), 256, i1, 4, 0xffffff);
			i1 += 20;
			qt.mg(super.vq + "*", 256, i1, 4, 0xffffff);
			if (super.wq.length() > 0) {
				String s1 = super.wq;
				super.vq = "";
				super.wq = "";
				ht = 0;
				t(kcb, s1);
			}
		}
		if (ht == 3) {
			qt.qf(106, i1, 300, 70, 0);
			qt.hf(106, i1, 300, 70, 0xffffff);
			i1 += 20;
			qt.mg("Enter name to add to ignore list", 256, i1, 4, 0xffffff);
			i1 += 20;
			qt.mg(super.tq + "*", 256, i1, 4, 0xffffff);
			if (super.uq.length() > 0) {
				String s2 = super.uq.trim();
				super.tq = "";
				super.uq = "";
				ht = 0;
				if (s2.length() > 0 && o.rm(s2) != player.yq)
					lb(s2);
			}
		}
		int j1 = 0xffffff;
		if (super.mq > 236 && super.mq < 276 && super.nq > 193 && super.nq < 213)
			j1 = 0xffff00;
		qt.mg("Cancel", 256, 208, 1, j1);
	}

	public void sendShopInterface() {
		if (mx != 0) {
			mx = 0;
			int i1 = super.mq - 52;
			int j1 = super.nq - 44;
			if (i1 >= 0 && j1 >= 12 && i1 < 408 && j1 < 246) {
				int k1 = 0;
				for (int i2 = 0; i2 < 5; i2++) {
					for (int i3 = 0; i3 < 8; i3++) {
						int l3 = 7 + i3 * 49;
						int l4 = 28 + i2 * 34;
						if (i1 > l3 && i1 < l3 + 49 && j1 > l4 && j1 < l4 + 34 && jy[k1] != -1) {
							my = k1;
							ny = jy[k1];
						}
						k1++;
					}

				}

				if (my >= 0) {
					int j3 = jy[my];
					if (j3 != -1) {
						if (ky[my] > 0 && i1 > 298 && j1 >= 204 && i1 < 408 && j1 <= 215) {
							int i4 = iy + ly[my];
							if (i4 < 10)
								i4 = 10;
							int i5 = (i4 * Definitions.agb[j3]) / 100;
							super.dd.i(217);
							super.dd.k(jy[my]);
							super.dd.k(i5);
							ll();
						}
						if (getItemAmount(j3) > 0 && i1 > 2 && j1 >= 229 && i1 < 112 && j1 <= 240) {
							int j4 = hy + ly[my];
							if (j4 < 10)
								j4 = 10;
							int j5 = (j4 * Definitions.agb[j3]) / 100;
							super.dd.i(216);
							super.dd.k(jy[my]);
							super.dd.k(j5);
							ll();
						}
					}
				}
			} else {
				super.dd.i(218);
				ll();
				gy = false;
				return;
			}
		}
		byte byte0 = 52;
		byte byte1 = 44;
		qt.qf(byte0, byte1, 408, 12, 192);
		int l1 = 0x989898;
		qt.nf(byte0, byte1 + 12, 408, 17, l1, 160);
		qt.nf(byte0, byte1 + 29, 8, 170, l1, 160);
		qt.nf(byte0 + 399, byte1 + 29, 9, 170, l1, 160);
		qt.nf(byte0, byte1 + 199, 408, 47, l1, 160);
		qt.ef("Buying and selling items", byte0 + 1, byte1 + 10, 1, 0xffffff);
		int j2 = 0xffffff;
		if (super.mq > byte0 + 320 && super.nq >= byte1 && super.mq < byte0 + 408 && super.nq < byte1 + 12)
			j2 = 0xff0000;
		qt.sg("Close window", byte0 + 406, byte1 + 10, 1, j2);
		qt.ef("Shops stock in green", byte0 + 2, byte1 + 24, 1, 65280);
		qt.ef("Number you own in blue", byte0 + 135, byte1 + 24, 1, 65535);
		qt.ef("Your money: " + getItemAmount(10) + "gp", byte0 + 280, byte1 + 24, 1, 0xffff00);
		int k3 = 0xd0d0d0;
		int k4 = 0;
		for (int k5 = 0; k5 < 5; k5++) {
			for (int l5 = 0; l5 < 8; l5++) {
				int j6 = byte0 + 7 + l5 * 49;
				int i7 = byte1 + 28 + k5 * 34;
				if (my == k4)
					qt.nf(j6, i7, 49, 34, 0xff0000, 160);
				else
					qt.nf(j6, i7, 49, 34, k3, 160);
				qt.hf(j6, i7, 50, 35, 0);
				if (jy[k4] != -1) {
					qt.pf(j6, i7, 48, 32, nw + Definitions.zfb[jy[k4]], Definitions.lgb[jy[k4]], 0, 0, false);
					qt.ef(String.valueOf(ky[k4]), j6 + 1, i7 + 10, 1, 65280);
					qt.sg(String.valueOf(getItemAmount(jy[k4])), j6 + 47, i7 + 10, 1, 65535);
				}
				k4++;
			}

		}

		qt.kg(byte0 + 5, byte1 + 222, 398, 0);
		if (my == -1) {
			qt.mg("Select an object to buy or sell", byte0 + 204, byte1 + 214, 3, 0xffff00);
			return;
		}
		int i6 = jy[my];
		if (i6 != -1) {
			if (ky[my] > 0) {
				int k6 = iy + ly[my];
				if (k6 < 10)
					k6 = 10;
				int j7 = (k6 * Definitions.agb[i6]) / 100;
				qt.ef("Buy a new " + Definitions.itemName[i6][0] + " for " + j7 + "gp", byte0 + 2, byte1 + 214, 1, 0xffff00);
				int k2 = 0xffffff;
				if (super.mq > byte0 + 298 && super.nq >= byte1 + 204 && super.mq < byte0 + 408
						&& super.nq <= byte1 + 215)
					k2 = 0xff0000;
				qt.sg("Click here to buy", byte0 + 405, byte1 + 214, 3, k2);
			} else {
				qt.mg("This item is not currently available to buy", byte0 + 204, byte1 + 214, 3, 0xffff00);
			}
			if (getItemAmount(i6) > 0) {
				int l6 = hy + ly[my];
				if (l6 < 10)
					l6 = 10;
				int k7 = (l6 * Definitions.agb[i6]) / 100;
				qt.sg("Sell your " + Definitions.itemName[i6][0] + " for " + k7 + "gp", byte0 + 405, byte1 + 239, 1, 0xffff00);
				int l2 = 0xffffff;
				if (super.mq > byte0 + 2 && super.nq >= byte1 + 229 && super.mq < byte0 + 112
						&& super.nq <= byte1 + 240)
					l2 = 0xff0000;
				qt.ef("Click here to sell", byte0 + 2, byte1 + 239, 3, l2);
				return;
			}
			qt.mg("You do not have any of this item to sell", byte0 + 204, byte1 + 239, 3, 0xffff00);
		}
	}

	public void sendTradeInterface() {
		if (mx != 0 && fy == 0)
			fy = 1;
		if (fy > 0) {
			int i1 = super.mq - 22;
			int j1 = super.nq - 36;
			if (i1 >= 0 && j1 >= 0 && i1 < 468 && j1 < 262) {
				if (i1 > 216 && j1 > 30 && i1 < 462 && j1 < 235) {
					int k1 = (i1 - 217) / 49 + ((j1 - 31) / 34) * 5;
					if (k1 >= 0 && k1 < ox) {
						boolean flag = false;
						int l2 = 0;
						int k3 = px[k1];
						for (int k4 = 0; k4 < wx; k4++)
							if (xx[k4] == k3)
								if (Definitions.bgb[k3] == 0) {
									for (int i5 = 0; i5 < fy; i5++) {
										if (yx[k4] < qx[k1])
											yx[k4]++;
										flag = true;
									}

								} else {
									l2++;
								}

						if (getItemAmount(k3) <= l2)
							flag = true;
						if (!flag && wx < 12) {
							xx[wx] = k3;
							yx[wx] = 1;
							wx++;
							flag = true;
						}
						if (flag) {
							super.dd.i(234);
							super.dd.n(wx);
							for (int j5 = 0; j5 < wx; j5++) {
								super.dd.k(xx[j5]);
								super.dd.k(yx[j5]);
							}

							ll();
							cy = false;
							dy = false;
						}
					}
				}
				if (i1 > 8 && j1 > 30 && i1 < 205 && j1 < 133) {
					int l1 = (i1 - 9) / 49 + ((j1 - 31) / 34) * 4;
					if (l1 >= 0 && l1 < wx) {
						int j2 = xx[l1];
						for (int i3 = 0; i3 < fy; i3++) {
							if (Definitions.bgb[j2] == 0 && yx[l1] > 1) {
								yx[l1]--;
								continue;
							}
							wx--;
							ey = 0;
							for (int l3 = l1; l3 < wx; l3++) {
								xx[l3] = xx[l3 + 1];
								yx[l3] = yx[l3 + 1];
							}

							break;
						}

						super.dd.i(234);
						super.dd.n(wx);
						for (int i4 = 0; i4 < wx; i4++) {
							super.dd.k(xx[i4]);
							super.dd.k(yx[i4]);
						}

						ll();
						cy = false;
						dy = false;
					}
				}
				if (i1 > 143 && j1 > 141 && i1 < 154 && j1 < 152) {
					dy = !dy;
					super.dd.i(232);
					super.dd.n(dy ? 1 : 0);
					ll();
				}
				if (i1 > 413 && j1 > 237 && i1 < 462 && j1 < 258) {
					ux = false;
					super.dd.i(233);
					ll();
				}
			} else if (mx != 0) {
				ux = false;
				super.dd.i(233);
				ll();
			}
			mx = 0;
			fy = 0;
		}
		if (!ux)
			return;
		byte byte0 = 22;
		byte byte1 = 36;
		qt.qf(byte0, byte1, 468, 12, 192);
		int i2 = 0x989898;
		qt.nf(byte0, byte1 + 12, 468, 18, i2, 160);
		qt.nf(byte0, byte1 + 30, 8, 248, i2, 160);
		qt.nf(byte0 + 205, byte1 + 30, 11, 248, i2, 160);
		qt.nf(byte0 + 462, byte1 + 30, 6, 248, i2, 160);
		qt.nf(byte0 + 8, byte1 + 133, 197, 22, i2, 160);
		qt.nf(byte0 + 8, byte1 + 258, 197, 20, i2, 160);
		qt.nf(byte0 + 216, byte1 + 235, 246, 43, i2, 160);
		int k2 = 0xd0d0d0;
		qt.nf(byte0 + 8, byte1 + 30, 197, 103, k2, 160);
		qt.nf(byte0 + 8, byte1 + 155, 197, 103, k2, 160);
		qt.nf(byte0 + 216, byte1 + 30, 246, 205, k2, 160);
		for (int j3 = 0; j3 < 4; j3++)
			qt.kg(byte0 + 8, byte1 + 30 + j3 * 34, 197, 0);

		for (int j4 = 0; j4 < 4; j4++)
			qt.kg(byte0 + 8, byte1 + 155 + j4 * 34, 197, 0);

		for (int l4 = 0; l4 < 7; l4++)
			qt.kg(byte0 + 216, byte1 + 30 + l4 * 34, 246, 0);

		for (int k5 = 0; k5 < 6; k5++) {
			if (k5 < 5)
				qt.og(byte0 + 8 + k5 * 49, byte1 + 30, 103, 0);
			if (k5 < 5)
				qt.og(byte0 + 8 + k5 * 49, byte1 + 155, 103, 0);
			qt.og(byte0 + 216 + k5 * 49, byte1 + 30, 205, 0);
		}

		qt.ef("Trading with: " + vx, byte0 + 1, byte1 + 10, 1, 0xffffff);
		qt.ef("Your Offer", byte0 + 9, byte1 + 27, 4, 0xffffff);
		qt.ef("Opponent's Offer", byte0 + 9, byte1 + 152, 4, 0xffffff);
		qt.ef("Your Inventory", byte0 + 216, byte1 + 27, 4, 0xffffff);
		qt.sg("Accepted", byte0 + 204, byte1 + 27, 4, 65280);
		qt.hf(byte0 + 125, byte1 + 16, 11, 11, 65280);
		if (cy)
			qt.qf(byte0 + 127, byte1 + 18, 7, 7, 65280);
		qt.sg("Accept", byte0 + 204, byte1 + 152, 4, 65280);
		qt.hf(byte0 + 143, byte1 + 141, 11, 11, 65280);
		if (dy)
			qt.qf(byte0 + 145, byte1 + 143, 7, 7, 65280);
		qt.sg("Close", byte0 + 408 + 49, byte1 + 251, 4, 0xc00000);
		qt.hf(byte0 + 364 + 49, byte1 + 237, 49, 21, 0xc00000);
		for (int l5 = 0; l5 < ox; l5++) {
			int i6 = 217 + byte0 + (l5 % 5) * 49;
			int k6 = 31 + byte1 + (l5 / 5) * 34;
			qt.pf(i6, k6, 48, 32, nw + Definitions.zfb[px[l5]], Definitions.lgb[px[l5]], 0, 0, false);
			if (Definitions.bgb[px[l5]] == 0)
				qt.ef(String.valueOf(qx[l5]), i6 + 1, k6 + 10, 1, 0xffff00);
		}

		for (int j6 = 0; j6 < wx; j6++) {
			int l6 = 9 + byte0 + (j6 % 4) * 49;
			int j7 = 31 + byte1 + (j6 / 4) * 34;
			qt.pf(l6, j7, 48, 32, nw + Definitions.zfb[xx[j6]], Definitions.lgb[xx[j6]], 0, 0, false);
			if (Definitions.bgb[xx[j6]] == 0)
				qt.ef(String.valueOf(yx[j6]), l6 + 1, j7 + 10, 1, 0xffff00);
			if (super.mq > l6 && super.mq < l6 + 48 && super.nq > j7 && super.nq < j7 + 32)
				qt.ef(Definitions.itemName[xx[j6]][0] + ": @whi@" + Definitions.wfb[xx[j6]], byte0 + 8, byte1 + 273, 1, 0xffff00);
		}

		for (int i7 = 0; i7 < zx; i7++) {
			int k7 = 9 + byte0 + (i7 % 4) * 49;
			int l7 = 156 + byte1 + (i7 / 4) * 34;
			qt.pf(k7, l7, 48, 32, nw + Definitions.zfb[ay[i7]], Definitions.lgb[ay[i7]], 0, 0, false);
			if (Definitions.bgb[ay[i7]] == 0)
				qt.ef(String.valueOf(by[i7]), k7 + 1, l7 + 10, 1, 0xffff00);
			if (super.mq > k7 && super.mq < k7 + 48 && super.nq > l7 && super.nq < l7 + 32)
				qt.ef(Definitions.itemName[ay[i7]][0] + ": @whi@" + Definitions.wfb[ay[i7]], byte0 + 8, byte1 + 273, 1, 0xffff00);
		}

	}

	public void pk() {
		if (nx == 0 && super.mq >= ((GameImageProducer) (qt)).sj - 35 && super.nq >= 3 && super.mq < ((GameImageProducer) (qt)).sj - 3 && super.nq < 35)
			nx = 1;
		if (nx == 0 && super.mq >= ((GameImageProducer) (qt)).sj - 35 - 33 && super.nq >= 3 && super.mq < ((GameImageProducer) (qt)).sj - 3 - 33
				&& super.nq < 35)
			nx = 2;
		if (nx == 0 && super.mq >= ((GameImageProducer) (qt)).sj - 35 - 66 && super.nq >= 3 && super.mq < ((GameImageProducer) (qt)).sj - 3 - 66
				&& super.nq < 35)
			nx = 3;
		if (nx == 0 && super.mq >= ((GameImageProducer) (qt)).sj - 35 - 99 && super.nq >= 3 && super.mq < ((GameImageProducer) (qt)).sj - 3 - 99
				&& super.nq < 35)
			nx = 4;
		if (nx == 0 && super.mq >= ((GameImageProducer) (qt)).sj - 35 - 132 && super.nq >= 3 && super.mq < ((GameImageProducer) (qt)).sj - 3 - 132
				&& super.nq < 35)
			nx = 5;
		if (nx == 0 && super.mq >= ((GameImageProducer) (qt)).sj - 35 - 165 && super.nq >= 3 && super.mq < ((GameImageProducer) (qt)).sj - 3 - 165
				&& super.nq < 35)
			nx = 6;
		if (nx != 0 && super.mq >= ((GameImageProducer) (qt)).sj - 35 && super.nq >= 3 && super.mq < ((GameImageProducer) (qt)).sj - 3 && super.nq < 26)
			nx = 1;
		if (nx != 0 && super.mq >= ((GameImageProducer) (qt)).sj - 35 - 33 && super.nq >= 3 && super.mq < ((GameImageProducer) (qt)).sj - 3 - 33
				&& super.nq < 26)
			nx = 2;
		if (nx != 0 && super.mq >= ((GameImageProducer) (qt)).sj - 35 - 66 && super.nq >= 3 && super.mq < ((GameImageProducer) (qt)).sj - 3 - 66
				&& super.nq < 26)
			nx = 3;
		if (nx != 0 && super.mq >= ((GameImageProducer) (qt)).sj - 35 - 99 && super.nq >= 3 && super.mq < ((GameImageProducer) (qt)).sj - 3 - 99
				&& super.nq < 26)
			nx = 4;
		if (nx != 0 && super.mq >= ((GameImageProducer) (qt)).sj - 35 - 132 && super.nq >= 3 && super.mq < ((GameImageProducer) (qt)).sj - 3 - 132
				&& super.nq < 26)
			nx = 5;
		if (nx != 0 && super.mq >= ((GameImageProducer) (qt)).sj - 35 - 165 && super.nq >= 3 && super.mq < ((GameImageProducer) (qt)).sj - 3 - 165
				&& super.nq < 26)
			nx = 6;
		if (nx == 1 && (super.mq < ((GameImageProducer) (qt)).sj - 248 || super.nq > 240))
			nx = 0;
		if (nx >= 2 && nx <= 5 && (super.mq < ((GameImageProducer) (qt)).sj - 199 || super.nq > 240))
			nx = 0;
		if (nx == 6 && (super.mq < ((GameImageProducer) (qt)).sj - 199 || super.nq > 267))
			nx = 0;
	}

	public void sendInventoryInterface(boolean flag) {
		int i1 = ((GameImageProducer) (qt)).sj - 248;
		qt.rg(i1, 3, az + 1);
		for (int j1 = 0; j1 < 30; j1++) {
			int k1 = i1 + (j1 % 5) * 49;
			int i2 = 36 + (j1 / 5) * 34;
			if (j1 < ox && rx[j1] == 1)
				qt.nf(k1, i2, 49, 34, 0xff0000, 128);
			else
				qt.nf(k1, i2, 49, 34, GameImageProducer.fg(181, 181, 181), 128);
			if (j1 < ox) {
				qt.pf(k1, i2, 48, 32, nw + Definitions.zfb[px[j1]], Definitions.lgb[px[j1]], 0, 0, false);
				if (Definitions.bgb[px[j1]] == 0)
					qt.ef(String.valueOf(qx[j1]), k1 + 1, i2 + 10, 1, 0xffff00);
			}
		}

		for (int l1 = 1; l1 <= 4; l1++)
			qt.og(i1 + l1 * 49, 36, 204, 0);

		for (int j2 = 1; j2 <= 5; j2++)
			qt.kg(i1, 36 + j2 * 34, 245, 0);

		if (!flag)
			return;
		i1 = super.mq - (((GameImageProducer) (qt)).sj - 248);
		int k2 = super.nq - 36;
		if (i1 >= 0 && k2 >= 0 && i1 < 248 && k2 < 204) {
			int l2 = i1 / 49 + (k2 / 34) * 5;
			if (l2 < ox) {
				int i3 = px[l2];
				if (lcb >= 0) {
					if (Definitions.vjb[lcb] == 3) {
						mv[iv] = mcb + Definitions.rjb[lcb] + " on";
						lv[iv] = "@lre@" + Definitions.itemName[i3][0];
						nv[iv] = 600;
						qv[iv] = l2;
						rv[iv] = lcb;
						iv++;
					}
				} else {
					if (sx >= 0) {
						mv[iv] = "Use " + tx + " with";
						lv[iv] = "@lre@" + Definitions.itemName[i3][0];
						nv[iv] = 610;
						qv[iv] = l2;
						rv[iv] = sx;
						iv++;
						return;
					}
					if (rx[l2] == 1) {
						mv[iv] = "Remove";
						lv[iv] = "@lre@" + Definitions.itemName[i3][0];
						nv[iv] = 620;
						qv[iv] = l2;
						iv++;
					} else if (Definitions.jgb[i3] != 0) {
						if ((Definitions.jgb[i3] & 0x18) != 0)
							mv[iv] = "Wield";
						else
							mv[iv] = "Wear";
						lv[iv] = "@lre@" + Definitions.itemName[i3][0];
						nv[iv] = 630;
						qv[iv] = l2;
						iv++;
					}
					if (!Definitions.yfb[i3].equals("_")) {
						mv[iv] = Definitions.yfb[i3];
						lv[iv] = "@lre@" + Definitions.itemName[i3][0];
						nv[iv] = 640;
						qv[iv] = l2;
						iv++;
					}
					mv[iv] = "Use";
					lv[iv] = "@lre@" + Definitions.itemName[i3][0];
					nv[iv] = 650;
					qv[iv] = l2;
					iv++;
					mv[iv] = "Drop";
					lv[iv] = "@lre@" + Definitions.itemName[i3][0];
					nv[iv] = 660;
					qv[iv] = l2;
					iv++;
					mv[iv] = "Examine";
					lv[iv] = "@lre@" + Definitions.itemName[i3][0];
					nv[iv] = 3600;
					qv[iv] = i3;
					iv++;
				}
			}
		}
	}

	public void sendMapInterface(boolean flag) {
		int i1 = ((GameImageProducer) (qt)).sj - 199;
		char c1 = '\234';
		char c3 = '\230';
		qt.rg(i1 - 49, 3, az + 2);
		i1 += 40;
		qt.qf(i1, 36, c1, c3, 0);
		qt.kf(i1, 36, i1 + c1, 36 + c3);
		char c5 = '\300';
		int k1 = ((player.cr - 6040) * 3 * c5) / 2048;
		int i3 = ((player.dr - 6040) * 3 * c5) / 2048;
		int k4 = j.hm[1024 - zt * 4 & 0x3ff];
		int i5 = j.hm[(1024 - zt * 4 & 0x3ff) + 1024];
		int k5 = i3 * k4 + k1 * i5 >> 18;
		i3 = i3 * i5 - k1 * k4 >> 18;
		k1 = k5;
		qt.gf((i1 + c1 / 2) - k1, 36 + c3 / 2 + i3, az - 1, zt + 64 & 0xff, c5);
		for (int i7 = 0; i7 < tw; i7++) {
			int l1 = (((vw[i7] * tt + 64) - player.cr) * 3 * c5) / 2048;
			int j3 = (((ww[i7] * tt + 64) - player.dr) * 3 * c5) / 2048;
			int l5 = j3 * k4 + l1 * i5 >> 18;
			j3 = j3 * i5 - l1 * k4 >> 18;
			l1 = l5;
			fk(i1 + c1 / 2 + l1, (36 + c3 / 2) - j3, 65535);
		}

		for (int j7 = 0; j7 < mw; j7++) {
			int i2 = (((ow[j7] * tt + 64) - player.cr) * 3 * c5) / 2048;
			int k3 = (((pw[j7] * tt + 64) - player.dr) * 3 * c5) / 2048;
			int i6 = k3 * k4 + i2 * i5 >> 18;
			k3 = k3 * i5 - i2 * k4 >> 18;
			i2 = i6;
			fk(i1 + c1 / 2 + i2, (36 + c3 / 2) - k3, 0xff0000);
		}

		for (int k7 = 0; k7 < fw; k7++) {
			Player l7 = hw[k7];
			int j2 = ((l7.cr - player.cr) * 3 * c5) / 2048;
			int l3 = ((l7.dr - player.dr) * 3 * c5) / 2048;
			int j6 = l3 * k4 + j2 * i5 >> 18;
			l3 = l3 * i5 - j2 * k4 >> 18;
			j2 = j6;
			fk(i1 + c1 / 2 + j2, (36 + c3 / 2) - l3, 0xffff00);
		}

		for (int i8 = 0; i8 < wv; i8++) {
			Player l8 = zv[i8];
			int k2 = ((l8.cr - player.cr) * 3 * c5) / 2048;
			int i4 = ((l8.dr - player.dr) * 3 * c5) / 2048;
			int k6 = i4 * k4 + k2 * i5 >> 18;
			i4 = i4 * i5 - k2 * k4 >> 18;
			k2 = k6;
			fk(i1 + c1 / 2 + k2, (36 + c3 / 2) - i4, 0xffffff);
		}

		qt.rf(i1 + c1 / 2, 36 + c3 / 2, 2, 0xffffff, 255);
		qt.gf(i1 + 19, 55, az + 24, zt + 128 & 0xff, 128);
		qt.kf(0, 0, bu, cu + 12);
		if (!flag)
			return;
		i1 = super.mq - (((GameImageProducer) (qt)).sj - 199);
		int j8 = super.nq - 36;
		if (i1 >= 40 && j8 >= 0 && i1 < 196 && j8 < 152) {
			char c2 = '\234';
			char c4 = '\230';
			char c6 = '\300';
			int j1 = ((GameImageProducer) (qt)).sj - 199;
			j1 += 40;
			int l2 = ((super.mq - (j1 + c2 / 2)) * 16384) / (3 * c6);
			int j4 = ((super.nq - (36 + c4 / 2)) * 16384) / (3 * c6);
			int l4 = j.hm[1024 - zt * 4 & 0x3ff];
			int j5 = j.hm[(1024 - zt * 4 & 0x3ff) + 1024];
			int l6 = j4 * l4 + l2 * j5 >> 15;
			j4 = j4 * j5 - l2 * l4 >> 15;
			l2 = l6;
			l2 += player.cr;
			j4 = player.dr - j4;
			if (mx == 1)
				ul(bw, cw, l2 / 128, j4 / 128, false);
			mx = 0;
		}
	}

	public void sendSkillsInterface(boolean flag) {
		int i1 = ((GameImageProducer) (qt)).sj - 199;
		qt.rg(i1 - 49, 3, az + 3);
		char c1 = '\304';
		char c2 = '\266';
		qt.nf(i1, 36, c1, c2, GameImageProducer.fg(181, 181, 181), 160);
		int j1 = 48;
		qt.ef("Skills", i1 + 5, j1, 3, 0xffff00);
		j1 += 13;
		for (int k1 = 0; k1 < 8; k1++) {
			qt.ef(skills[k1] + ":@yel@" + level[k1] + "/" + realLevel[k1], i1 + 5, j1, 1, 0xffffff);
			qt.ef(skills[k1 + 8] + ":@yel@" + level[k1 + 8] + "/" + realLevel[k1 + 8], (i1 + c1 / 2) - 8, j1 - 13, 1, 0xffffff);
			j1 += 13;
		}

		qt.ef("Quest Points:@yel@" + questPoints, (i1 + c1 / 2) - 8, j1 - 13, 1, 0xffffff);
		j1 += 8;
		qt.ef("Equipment Status", i1 + 5, j1, 3, 0xffff00);
		j1 += 12;
		for (int l1 = 0; l1 < 3; l1++) {
			qt.ef(attributes[l1] + ":@yel@" + qy[l1], i1 + 5, j1, 1, 0xffffff);
			if (l1 < 2)
				qt.ef(attributes[l1 + 3] + ":@yel@" + qy[l1 + 3], i1 + c1 / 2 + 25, j1, 1, 0xffffff);
			j1 += 13;
		}

	}

	public void sendMagicInterface(boolean flag) {
		int i1 = ((GameImageProducer) (qt)).sj - 199;
		int j1 = 36;
		qt.rg(i1 - 49, 3, az + 4);
		char c1 = '\304';
		char c2 = '\266';
		int l1;
		int k1 = l1 = GameImageProducer.fg(160, 160, 160);
		if (gcb == 0)
			k1 = GameImageProducer.fg(220, 220, 220);
		else
			l1 = GameImageProducer.fg(220, 220, 220);
		qt.nf(i1, j1, c1 / 2, 24, k1, 128);
		qt.nf(i1 + c1 / 2, j1, c1 / 2, 24, l1, 128);
		qt.nf(i1, j1 + 24, c1, 90, GameImageProducer.fg(220, 220, 220), 128);
		qt.nf(i1, j1 + 24 + 90, c1, c2 - 90 - 24, GameImageProducer.fg(160, 160, 160), 128);
		qt.kg(i1, j1 + 24, c1, 0);
		qt.og(i1 + c1 / 2, j1, 24, 0);
		qt.kg(i1, j1 + 113, c1, 0);
		qt.mg("Magic", i1 + c1 / 4, j1 + 16, 4, 0);
		qt.mg("Prayers", i1 + c1 / 4 + c1 / 2, j1 + 16, 4, 0);
		if (gcb == 0) {
			ecb.kc(fcb);
			int i2 = 0;
			for (int k2 = 0; k2 < Definitions.qjb; k2++) {
				String s = "@yel@";
				for (int k3 = 0; k3 < Definitions.ujb[k2]; k3++) {
					int j4 = Definitions.xjb[k2][k3];
					if (mk(j4, Definitions.yjb[k2][k3]))
						continue;
					s = "@whi@";
					break;
				}

				int k4 = level[6];
				if (Definitions.spellLevel[k2] > k4)
					s = "@bla@";
				ecb.ad(fcb, i2++, s + "Level " + Definitions.spellLevel[k2] + ": " + Definitions.rjb[k2]);
			}

			ecb.fc();
			int i3 = ecb.hc(fcb);
			if (i3 != -1) {
				qt.ef("Level " + Definitions.spellLevel[i3] + ": " + Definitions.rjb[i3], i1 + 2, j1 + 124, 1, 0);
				qt.ef(Definitions.sjb[i3], i1 + 2, j1 + 136, 0, 0);
				for (int l3 = 0; l3 < Definitions.ujb[i3]; l3++) {
					int l4 = Definitions.xjb[i3][l3];
					qt.rg(i1 + 2 + l3 * 44, j1 + 150, nw + Definitions.zfb[l4]);
					int i5 = getItemAmount(l4);
					int j5 = Definitions.yjb[i3][l3];
					String s1 = "@red@";
					if (mk(l4, j5))
						s1 = "@gre@";
					qt.ef(s1 + i5 + "/" + j5, i1 + 2 + l3 * 44, j1 + 150, 1, 0xffffff);
				}

			} else {
				qt.ef("Point at a spell for a description", i1 + 2, j1 + 124, 1, 0);
			}
		}
		if (!flag)
			return;
		i1 = super.mq - (((GameImageProducer) (qt)).sj - 199);
		j1 = super.nq - 36;
		if (i1 >= 0 && j1 >= 0 && i1 < 196 && j1 < 182) {
			ecb.md(i1 + (((GameImageProducer) (qt)).sj - 199), j1 + 36, super.pq, super.oq);
			if (j1 <= 24 && mx == 1)
				if (i1 < 98 && gcb == 1) {
					gcb = 0;
					ecb.xc(fcb);
				} else if (i1 > 98 && gcb == 0) {
					gcb = 1;
					ecb.xc(fcb);
				}
			if (mx == 1 && gcb == 0) {
				int j2 = ecb.hc(fcb);
				if (j2 != -1) {
					int magicLevel = level[6];
					if (Definitions.spellLevel[j2] > magicLevel) {
						gk("Your magic ability is not high enough for this spell", 3);
					} else {
						int j3;
						for (j3 = 0; j3 < Definitions.ujb[j2]; j3++) {
							int i4 = Definitions.xjb[j2][j3];
							if (mk(i4, Definitions.yjb[j2][j3]))
								continue;
							gk("You don't have all the reagents you need for this spell", 3);
							j3 = -1;
							break;
						}

						if (j3 == Definitions.ujb[j2]) {
							lcb = j2;
							sx = -1;
							mcb = "Cast ";
						}
					}
				}
			}
			mx = 0;
		}
	}

	public void sendFriendsInterface(boolean flag) {
		int i1 = ((GameImageProducer) (qt)).sj - 199;
		int j1 = 36;
		qt.rg(i1 - 49, 3, az + 5);
		char c1 = '\304';
		char c2 = '\266';
		int l1;
		int k1 = l1 = GameImageProducer.fg(160, 160, 160);
		if (jcb == 0)
			k1 = GameImageProducer.fg(220, 220, 220);
		else
			l1 = GameImageProducer.fg(220, 220, 220);
		qt.nf(i1, j1, c1 / 2, 24, k1, 128);
		qt.nf(i1 + c1 / 2, j1, c1 / 2, 24, l1, 128);
		qt.nf(i1, j1 + 24, c1, c2 - 24, GameImageProducer.fg(220, 220, 220), 128);
		qt.kg(i1, j1 + 24, c1, 0);
		qt.og(i1 + c1 / 2, j1, 24, 0);
		qt.kg(i1, (j1 + c2) - 16, c1, 0);
		qt.mg("Friends", i1 + c1 / 4, j1 + 16, 4, 0);
		qt.mg("Ignore", i1 + c1 / 4 + c1 / 2, j1 + 16, 4, 0);
		hcb.kc(icb);
		if (jcb == 0) {
			for (int i2 = 0; i2 < super.jd; i2++) {
				String s;
				if (super.ld[i2] == 2)
					s = "@gre@";
				else if (super.ld[i2] == 1)
					s = "@yel@";
				else
					s = "@red@";
				hcb.ad(icb, i2, s + o.tm(super.kd[i2]) + "~439~@whi@Remove         WWWWWWWWWW");
			}

		}
		if (jcb == 1) {
			for (int j2 = 0; j2 < super.md; j2++)
				hcb.ad(icb, j2, "@yel@" + o.tm(super.nd[j2]) + "~439~@whi@Remove         WWWWWWWWWW");

		}
		hcb.fc();
		if (jcb == 0) {
			int k2 = hcb.hc(icb);
			if (k2 >= 0 && super.mq < 489) {
				if (super.mq > 429)
					qt.mg("Click to remove " + o.tm(super.kd[k2]), i1 + c1 / 2, j1 + 35, 1, 0xffffff);
				else if (super.ld[k2] == 2)
					qt.mg("Click to message " + o.tm(super.kd[k2]), i1 + c1 / 2, j1 + 35, 1, 0xffffff);
				else if (super.ld[k2] == 1)
					qt.mg(o.tm(super.kd[k2]) + " is on a different server", i1 + c1 / 2, j1 + 35, 1, 0xffffff);
				else
					qt.mg(o.tm(super.kd[k2]) + " is offline", i1 + c1 / 2, j1 + 35, 1, 0xffffff);
			} else {
				qt.mg("Click a name to send a message", i1 + c1 / 2, j1 + 35, 1, 0xffffff);
			}
			int k3;
			if (super.mq > i1 && super.mq < i1 + c1 && super.nq > (j1 + c2) - 16 && super.nq < j1 + c2)
				k3 = 0xffff00;
			else
				k3 = 0xffffff;
			qt.mg("Click here to add a friend", i1 + c1 / 2, (j1 + c2) - 3, 1, k3);
		}
		if (jcb == 1) {
			int l2 = hcb.hc(icb);
			if (l2 >= 0 && super.mq < 489 && super.mq > 429) {
				if (super.mq > 429)
					qt.mg("Click to remove " + o.tm(super.nd[l2]), i1 + c1 / 2, j1 + 35, 1, 0xffffff);
			} else {
				qt.mg("Blocking messages from:", i1 + c1 / 2, j1 + 35, 1, 0xffffff);
			}
			int l3;
			if (super.mq > i1 && super.mq < i1 + c1 && super.nq > (j1 + c2) - 16 && super.nq < j1 + c2)
				l3 = 0xffff00;
			else
				l3 = 0xffffff;
			qt.mg("Click here to add a name", i1 + c1 / 2, (j1 + c2) - 3, 1, l3);
		}
		if (!flag)
			return;
		i1 = super.mq - (((GameImageProducer) (qt)).sj - 199);
		j1 = super.nq - 36;
		if (i1 >= 0 && j1 >= 0 && i1 < 196 && j1 < 182) {
			hcb.md(i1 + (((GameImageProducer) (qt)).sj - 199), j1 + 36, super.pq, super.oq);
			if (j1 <= 24 && mx == 1)
				if (i1 < 98 && jcb == 1) {
					jcb = 0;
					hcb.xc(icb);
				} else if (i1 > 98 && jcb == 0) {
					jcb = 1;
					hcb.xc(icb);
				}
			if (mx == 1 && jcb == 0) {
				int i3 = hcb.hc(icb);
				if (i3 >= 0 && super.mq < 489)
					if (super.mq > 429)
						y(super.kd[i3]);
					else if (super.ld[i3] != 0) {
						ht = 2;
						kcb = super.kd[i3];
						super.vq = "";
						super.wq = "";
					}
			}
			if (mx == 1 && jcb == 1) {
				int j3 = hcb.hc(icb);
				if (j3 >= 0 && super.mq < 489 && super.mq > 429)
					cb(super.nd[j3]);
			}
			if (j1 > 166 && mx == 1 && jcb == 0) {
				ht = 1;
				super.tq = "";
				super.uq = "";
			}
			if (j1 > 166 && mx == 1 && jcb == 1) {
				ht = 3;
				super.tq = "";
				super.uq = "";
			}
			mx = 0;
		}
	}

	public void sendSettingsInterface(boolean flag) {
		int i1 = ((GameImageProducer) (qt)).sj - 199;
		int j1 = 36;
		qt.rg(i1 - 49, 3, az + 6);
		char c1 = '\304';
		qt.nf(i1, 36, c1, 90, GameImageProducer.fg(181, 181, 181), 160);
		qt.nf(i1, 126, c1, 105, GameImageProducer.fg(201, 2011, 201), 160);
		qt.nf(i1, 231, c1, 30, GameImageProducer.fg(181, 181, 181), 160);
		int k1 = i1 + 3;
		int i2 = j1 + 15;
		qt.ef("Game options - click to toggle", k1, i2, 1, 0);
		i2 += 15;
		if (ct)
			qt.ef("Camera angle mode - @gre@Auto", k1, i2, 1, 0xffffff);
		else
			qt.ef("Camera angle mode - @red@Manual", k1, i2, 1, 0xffffff);
		i2 += 15;
		if (kv)
			qt.ef("Mouse buttons - @red@One", k1, i2, 1, 0xffffff);
		else
			qt.ef("Mouse buttons - @gre@Two", k1, i2, 1, 0xffffff);
		i2 += 15;
		if (dt)
			qt.ef("Player type: @red@Player-Killer", k1, i2, 1, 0xffffff);
		else
			qt.ef("Player type: @gre@Non Player-Killer", k1, i2, 1, 0xffffff);
		i2 += 15;
		int k2 = 0xffffff;
		if (super.mq > k1 && super.mq < k1 + c1 && super.nq > i2 - 12 && super.nq < i2 + 4)
			k2 = 0xffff00;
		qt.ef("Change password", k1, i2, 1, k2);
		i2 += 15;
		i2 += 15;
		qt.ef("Privacy settings. Will be applied to", i1 + 3, i2, 1, 0);
		i2 += 15;
		qt.ef("all people not on your friends list", i1 + 3, i2, 1, 0);
		i2 += 15;
		if (super.od == 0)
			qt.ef("Hide online-status: @red@<off>", i1 + 3, i2, 1, 0xffffff);
		else
			qt.ef("Hide online-status: @gre@<on>", i1 + 3, i2, 1, 0xffffff);
		i2 += 15;
		if (super.pd == 0)
			qt.ef("Block chat messages: @red@<off>", i1 + 3, i2, 1, 0xffffff);
		else
			qt.ef("Block chat messages: @gre@<on>", i1 + 3, i2, 1, 0xffffff);
		i2 += 15;
		if (super.qd == 0)
			qt.ef("Block private messages: @red@<off>", i1 + 3, i2, 1, 0xffffff);
		else
			qt.ef("Block private messages: @gre@<on>", i1 + 3, i2, 1, 0xffffff);
		i2 += 15;
		if (super.rd == 0)
			qt.ef("Block trade requests: @red@<off>", i1 + 3, i2, 1, 0xffffff);
		else
			qt.ef("Block trade requests: @gre@<on>", i1 + 3, i2, 1, 0xffffff);
		i2 += 15;
		i2 += 15;
		k2 = 0xffffff;
		if (super.mq > k1 && super.mq < k1 + c1 && super.nq > i2 - 12 && super.nq < i2 + 4)
			k2 = 0xffff00;
		qt.ef("Click here to logout", i1 + 3, i2, 1, k2);
		if (!flag)
			return;
		i1 = super.mq - (((GameImageProducer) (qt)).sj - 199);
		j1 = super.nq - 36;
		if (i1 >= 0 && j1 >= 0 && i1 < 196 && j1 < 231) {
			int l2 = ((GameImageProducer) (qt)).sj - 199;
			byte byte0 = 36;
			char c2 = '\304';
			int l1 = l2 + 3;
			int j2 = byte0 + 30;
			if (super.mq > l1 && super.mq < l1 + c2 && super.nq > j2 - 12 && super.nq < j2 + 4 && mx == 1) {
				ct = !ct;
				super.dd.i(213);
				super.dd.n(0);
				super.dd.n(ct ? 1 : 0);
				super.dd.e();
			}
			j2 += 15;
			if (super.mq > l1 && super.mq < l1 + c2 && super.nq > j2 - 12 && super.nq < j2 + 4 && mx == 1) {
				kv = !kv;
				super.dd.i(213);
				super.dd.n(2);
				super.dd.n(kv ? 1 : 0);
				super.dd.e();
			}
			j2 += 15;
			if (super.mq > l1 && super.mq < l1 + c2 && super.nq > j2 - 12 && super.nq < j2 + 4 && mx == 1 && et > 0)
				ft = 1;
			j2 += 15;
			if (super.mq > l1 && super.mq < l1 + c2 && super.nq > j2 - 12 && super.nq < j2 + 4 && mx == 1) {
				gt = 1;
				super.tq = "";
				super.uq = "";
			}
			boolean flag1 = false;
			j2 += 60;
			if (super.mq > l1 && super.mq < l1 + c2 && super.nq > j2 - 12 && super.nq < j2 + 4 && mx == 1) {
				super.od = 1 - super.od;
				flag1 = true;
			}
			j2 += 15;
			if (super.mq > l1 && super.mq < l1 + c2 && super.nq > j2 - 12 && super.nq < j2 + 4 && mx == 1) {
				super.pd = 1 - super.pd;
				flag1 = true;
			}
			j2 += 15;
			if (super.mq > l1 && super.mq < l1 + c2 && super.nq > j2 - 12 && super.nq < j2 + 4 && mx == 1) {
				super.qd = 1 - super.qd;
				flag1 = true;
			}
			j2 += 15;
			if (super.mq > l1 && super.mq < l1 + c2 && super.nq > j2 - 12 && super.nq < j2 + 4 && mx == 1) {
				super.rd = 1 - super.rd;
				flag1 = true;
			}
			if (flag1)
				nb(super.od, super.pd, super.qd, super.rd, 0);
			j2 += 30;
			if (super.mq > l1 && super.mq < l1 + c2 && super.nq > j2 - 12 && super.nq < j2 + 4 && mx == 1)
				if (lt > 450)
					gk("@cya@You can't logout during combat!", 3);
				else if (lt > 0) {
					gk("@cya@You can't logout for 10 seconds after combat", 3);
				} else {
					mb();
					return;
				}
			mx = 0;
		}
	}

	public void sendPlayerOptions() {
		int i1 = -1;
		for (int j1 = 0; j1 < tw; j1++)
			ax[j1] = false;

		for (int k1 = 0; k1 < cx; k1++)
			ix[k1] = false;

		int l1 = pt.mi();
		h[] ah = pt.ih();
		int[] ai = pt.hi();
		for (int i2 = 0; i2 < l1; i2++) {
			int j2 = ai[i2];
			h h1 = ah[i2];
			if (h1.qh[j2] <= 65535 || h1.qh[j2] >= 0x30d40 && h1.qh[j2] <= 0x493e0)
				if (h1 == pt.vn) {
					int l2 = h1.qh[j2] % 10000;
					int k3 = h1.qh[j2] / 10000;
					if (k3 == 1) {
						String message = "";
						int combatLevelDifference = -1;
						int l4 = zv[l2].vr;
						if (l4 == 1) {
							combatLevelDifference = 0;
							if (player.combatLevel > 0 && zv[l2].combatLevel > 0)
								combatLevelDifference = player.combatLevel - zv[l2].combatLevel;
							if (combatLevelDifference < 0)
								message = "@or1@";
							if (combatLevelDifference < -3)
								message = "@or2@";
							if (combatLevelDifference < -6)
								message = "@or3@";
							if (combatLevelDifference < -9)
								message = "@red@";
							if (combatLevelDifference > 0)
								message = "@gr1@";
							if (combatLevelDifference > 3)
								message = "@gr2@";
							if (combatLevelDifference > 6)
								message = "@gr3@";
							if (combatLevelDifference > 9)
								message = "@gre@";
							message = " " + message + "(level-" + zv[l2].combatLevel + ")";
						}
						if (lcb >= 0) {
							if (Definitions.vjb[lcb] == 1 || Definitions.vjb[lcb] == 2 && l4 == 1 && player.vr == 1) {
								mv[iv] = mcb + Definitions.rjb[lcb] + " on";
								lv[iv] = "@whi@" + zv[l2].playerName;
								nv[iv] = 800;
								ov[iv] = zv[l2].cr;
								pv[iv] = zv[l2].dr;
								qv[iv] = zv[l2].ar;
								rv[iv] = lcb;
								iv++;
							}
						} else if (sx >= 0) {
							mv[iv] = "Use " + tx + " with";
							lv[iv] = "@whi@" + zv[l2].playerName;
							nv[iv] = 810;
							ov[iv] = zv[l2].cr;
							pv[iv] = zv[l2].dr;
							qv[iv] = zv[l2].ar;
							rv[iv] = sx;
							iv++;
						} else {
							if (l4 == 1 && player.vr == 1) {
								mv[iv] = "Attack";
								lv[iv] = "@whi@" + zv[l2].playerName + message;
								if (combatLevelDifference >= 0 && combatLevelDifference < 5)
									nv[iv] = 805;
								else
									nv[iv] = 2805;
								ov[iv] = zv[l2].cr;
								pv[iv] = zv[l2].dr;
								qv[iv] = zv[l2].ar;
								iv++;
							}
							mv[iv] = "Trade with";
							lv[iv] = "@whi@" + zv[l2].playerName;
							nv[iv] = 2810;
							qv[iv] = zv[l2].ar;
							iv++;
							mv[iv] = "Follow";
							lv[iv] = "@whi@" + zv[l2].playerName;
							nv[iv] = 2820;
							qv[iv] = zv[l2].ar;
							iv++;
						}
					} else if (k3 == 2) {
						if (lcb >= 0) {
							if (Definitions.vjb[lcb] == 3) {
								mv[iv] = mcb + Definitions.rjb[lcb] + " on";
								lv[iv] = "@lre@" + Definitions.itemName[qw[l2]][0];
								nv[iv] = 200;
								ov[iv] = ow[l2];
								pv[iv] = pw[l2];
								qv[iv] = qw[l2];
								rv[iv] = lcb;
								iv++;
							}
						} else if (sx >= 0) {
							mv[iv] = "Use " + tx + " with";
							lv[iv] = "@lre@" + Definitions.itemName[qw[l2]][0];
							nv[iv] = 210;
							ov[iv] = ow[l2];
							pv[iv] = pw[l2];
							qv[iv] = qw[l2];
							rv[iv] = sx;
							iv++;
						} else {
							mv[iv] = "Take";
							lv[iv] = "@lre@" + Definitions.itemName[qw[l2]][0];
							nv[iv] = 220;
							ov[iv] = ow[l2];
							pv[iv] = pw[l2];
							qv[iv] = qw[l2];
							iv++;
							mv[iv] = "Examine";
							lv[iv] = "@lre@" + Definitions.itemName[qw[l2]][0];
							nv[iv] = 3200;
							qv[iv] = qw[l2];
							iv++;
						}
					} else if (k3 == 3) {
						String message = "";
						int combatLevelDifference = -1;
						int npc = hw[l2].er;
						if (Definitions.vgb[npc] > 0) {
							int npcCombat = (Definitions.rgb[npc] + Definitions.ugb[npc] + Definitions.sgb[npc] + Definitions.tgb[npc]) / 4;
							int playerCombat = (realLevel[0] + realLevel[1] + realLevel[2] + realLevel[3] + 27) / 4;
							combatLevelDifference = playerCombat - npcCombat;
							message = "@yel@";
							if (combatLevelDifference < 0)
								message = "@or1@";
							if (combatLevelDifference < -3)
								message = "@or2@";
							if (combatLevelDifference < -6)
								message = "@or3@";
							if (combatLevelDifference < -9)
								message = "@red@";
							if (combatLevelDifference > 0)
								message = "@gr1@";
							if (combatLevelDifference > 3)
								message = "@gr2@";
							if (combatLevelDifference > 6)
								message = "@gr3@";
							if (combatLevelDifference > 9)
								message = "@gre@";
							message = " " + message + "(level-" + npcCombat + ")";
						}
						if (lcb >= 0) {
							if (Definitions.vjb[lcb] == 2) {
								mv[iv] = mcb + Definitions.rjb[lcb] + " on";
								lv[iv] = "@yel@" + Definitions.npcName[hw[l2].er][0];
								nv[iv] = 700;
								ov[iv] = hw[l2].cr;
								pv[iv] = hw[l2].dr;
								qv[iv] = hw[l2].ar;
								rv[iv] = lcb;
								iv++;
							}
						} else if (sx >= 0) {
							mv[iv] = "Use " + tx + " with";
							lv[iv] = "@yel@" + Definitions.npcName[hw[l2].er][0];
							nv[iv] = 710;
							ov[iv] = hw[l2].cr;
							pv[iv] = hw[l2].dr;
							qv[iv] = hw[l2].ar;
							rv[iv] = sx;
							iv++;
						} else {
							if (Definitions.vgb[npc] > 0) {
								mv[iv] = "Attack";
								lv[iv] = "@yel@" + Definitions.npcName[hw[l2].er][0] + message;
								if (combatLevelDifference >= 0)
									nv[iv] = 715;
								else
									nv[iv] = 2715;
								ov[iv] = hw[l2].cr;
								pv[iv] = hw[l2].dr;
								qv[iv] = hw[l2].ar;
								iv++;
							}
							mv[iv] = "Talk-to";
							lv[iv] = "@yel@" + Definitions.npcName[hw[l2].er][0];
							nv[iv] = 720;
							ov[iv] = hw[l2].cr;
							pv[iv] = hw[l2].dr;
							qv[iv] = hw[l2].ar;
							iv++;
							mv[iv] = "Examine";
							lv[iv] = "@yel@" + Definitions.npcName[hw[l2].er][0];
							nv[iv] = 3700;
							qv[iv] = hw[l2].er;
							iv++;
						}
					}
				} else if (h1 != null && h1.ph >= 10000) {
					int i3 = h1.ph - 10000;
					int l3 = hx[i3];
					if (!ix[i3]) {
						if (lcb >= 0) {
							if (Definitions.vjb[lcb] == 4) {
								mv[iv] = mcb + Definitions.rjb[lcb] + " on";
								lv[iv] = "@cya@" + Definitions.mib[l3][0];
								nv[iv] = 300;
								ov[iv] = ex[i3];
								pv[iv] = fx[i3];
								qv[iv] = gx[i3];
								rv[iv] = lcb;
								iv++;
							}
						} else if (sx >= 0) {
							mv[iv] = "Use " + tx + " with";
							lv[iv] = "@cya@" + Definitions.mib[l3][0];
							nv[iv] = 310;
							ov[iv] = ex[i3];
							pv[iv] = fx[i3];
							qv[iv] = gx[i3];
							rv[iv] = sx;
							iv++;
						} else {
							if (!Definitions.oib[l3].equalsIgnoreCase("WalkTo")) {
								mv[iv] = Definitions.oib[l3];
								lv[iv] = "@cya@" + Definitions.mib[l3][0];
								nv[iv] = 320;
								ov[iv] = ex[i3];
								pv[iv] = fx[i3];
								qv[iv] = gx[i3];
								iv++;
							}
							if (!Definitions.pib[l3].equalsIgnoreCase("Examine")) {
								mv[iv] = Definitions.pib[l3];
								lv[iv] = "@cya@" + Definitions.mib[l3][0];
								nv[iv] = 2300;
								ov[iv] = ex[i3];
								pv[iv] = fx[i3];
								qv[iv] = gx[i3];
								iv++;
							}
							mv[iv] = "Examine";
							lv[iv] = "@cya@" + Definitions.mib[l3][0];
							nv[iv] = 3300;
							qv[iv] = l3;
							iv++;
						}
						ix[i3] = true;
					}
				} else if (h1 != null && h1.ph >= 0) {
					int j3 = h1.ph;
					int i4 = xw[j3];
					if (!ax[j3]) {
						if (lcb >= 0) {
							if (Definitions.vjb[lcb] == 5) {
								mv[iv] = mcb + Definitions.rjb[lcb] + " on";
								lv[iv] = "@cya@" + Definitions.bib[i4][0];
								nv[iv] = 400;
								ov[iv] = vw[j3];
								pv[iv] = ww[j3];
								qv[iv] = yw[j3];
								rv[iv] = xw[j3];
								sv[iv] = lcb;
								iv++;
							}
						} else if (sx >= 0) {
							mv[iv] = "Use " + tx + " with";
							lv[iv] = "@cya@" + Definitions.bib[i4][0];
							nv[iv] = 410;
							ov[iv] = vw[j3];
							pv[iv] = ww[j3];
							qv[iv] = yw[j3];
							rv[iv] = xw[j3];
							sv[iv] = sx;
							iv++;
						} else {
							if (!Definitions.dib[i4].equalsIgnoreCase("WalkTo")) {
								mv[iv] = Definitions.dib[i4];
								lv[iv] = "@cya@" + Definitions.bib[i4][0];
								nv[iv] = 420;
								ov[iv] = vw[j3];
								pv[iv] = ww[j3];
								qv[iv] = yw[j3];
								rv[iv] = xw[j3];
								iv++;
							}
							if (!Definitions.eib[i4].equalsIgnoreCase("Examine")) {
								mv[iv] = Definitions.eib[i4];
								lv[iv] = "@cya@" + Definitions.bib[i4][0];
								nv[iv] = 2400;
								ov[iv] = vw[j3];
								pv[iv] = ww[j3];
								qv[iv] = yw[j3];
								rv[iv] = xw[j3];
								iv++;
							}
							mv[iv] = "Examine";
							lv[iv] = "@cya@" + Definitions.bib[i4][0];
							nv[iv] = 3400;
							qv[iv] = i4;
							iv++;
						}
						ax[j3] = true;
					}
				} else {
					if (j2 >= 0)
						j2 = h1.qh[j2] - 0x30d40;
					if (j2 >= 0)
						i1 = j2;
				}
		}

		if (lcb >= 0 && Definitions.vjb[lcb] <= 1) {
			mv[iv] = mcb + "on self";
			lv[iv] = "";
			nv[iv] = 1000;
			qv[iv] = lcb;
			iv++;
		}
		if (i1 != -1) {
			int k2 = i1;
			if (lcb >= 0) {
				if (Definitions.vjb[lcb] == 6) {
					mv[iv] = mcb + "on ground";
					lv[iv] = "";
					nv[iv] = 900;
					ov[iv] = cv.reb[k2];
					pv[iv] = cv.seb[k2];
					qv[iv] = lcb;
					iv++;
				}
			} else if (sx < 0) {
				mv[iv] = "Walk here";
				lv[iv] = "";
				nv[iv] = 920;
				ov[iv] = cv.reb[k2];
				pv[iv] = cv.seb[k2];
				iv++;
			}
		}
	}

	public void am() {
		if (mx != 0) {
			for (int i1 = 0; i1 < iv; i1++) {
				int k1 = ev + 2;
				int i2 = fv + 27 + i1 * 15;
				if (super.mq <= k1 - 2 || super.nq <= i2 - 12 || super.nq >= i2 + 4 || super.mq >= (k1 - 3) + gv)
					continue;
				jl(tv[i1]);
				break;
			}

			mx = 0;
			dv = false;
			return;
		}
		if (super.mq < ev - 10 || super.nq < fv - 10 || super.mq > ev + gv + 10 || super.nq > fv + hv + 10) {
			dv = false;
			return;
		}
		qt.nf(ev, fv, gv, hv, 0xd0d0d0, 160);
		qt.ef("Choose option", ev + 2, fv + 12, 1, 65535);
		for (int j1 = 0; j1 < iv; j1++) {
			int l1 = ev + 2;
			int j2 = fv + 27 + j1 * 15;
			int k2 = 0xffffff;
			if (super.mq > l1 - 2 && super.nq > j2 - 12 && super.nq < j2 + 4 && super.mq < (l1 - 3) + gv)
				k2 = 0xffff00;
			qt.ef(mv[tv[j1]] + " " + lv[tv[j1]], l1, j2, 1, k2);
		}

	}

	public void fm() {
		if (lcb >= 0 || sx >= 0) {
			mv[iv] = "Cancel";
			lv[iv] = "";
			nv[iv] = 4000;
			iv++;
		}
		for (int i1 = 0; i1 < iv; i1++)
			tv[i1] = i1;

		for (boolean flag = false; !flag;) {
			flag = true;
			for (int j1 = 0; j1 < iv - 1; j1++) {
				int l1 = tv[j1];
				int j2 = tv[j1 + 1];
				if (nv[l1] > nv[j2]) {
					tv[j1] = j2;
					tv[j1 + 1] = l1;
					flag = false;
				}
			}

		}

		if (iv > 20)
			iv = 20;
		if (iv > 0) {
			int k1 = -1;
			for (int i2 = 0; i2 < iv; i2++) {
				if (lv[tv[i2]] == null || lv[tv[i2]].length() <= 0)
					continue;
				k1 = i2;
				break;
			}

			String text = null;
			if ((sx >= 0 || lcb >= 0) && iv == 1)
				text = "Choose a target";
			else if ((sx >= 0 || lcb >= 0) && iv > 1)
				text = "@whi@" + mv[tv[0]] + " " + lv[tv[0]];
			else if (k1 != -1)
				text = lv[tv[k1]] + ": @whi@" + mv[tv[0]];
			if (iv == 2 && text != null)
				text = text + "@whi@ / 1 more option";
			if (iv > 2 && text != null)
				text = text + "@whi@ / " + (iv - 1) + " more options";
			if (text != null)
				qt.ef(text, 6, 14, 1, 0xffff00);
			if (!kv && mx == 1 || kv && mx == 1 && iv == 1) {
				jl(tv[0]);
				mx = 0;
				return;
			}
			if (!kv && mx == 2 || kv && mx == 1) {
				hv = (iv + 1) * 15;
				gv = qt.xe("Choose option", 1) + 5;
				for (int k2 = 0; k2 < iv; k2++) {
					int l2 = qt.xe(mv[k2] + " " + lv[k2], 1) + 5;
					if (l2 > gv)
						gv = l2;
				}

				ev = super.mq - gv / 2;
				fv = super.nq - 7;
				dv = true;
				if (ev < 0)
					ev = 0;
				if (fv < 0)
					fv = 0;
				if (ev + gv > 510)
					ev = 510 - gv;
				if (fv + hv > 315)
					fv = 315 - hv;
				mx = 0;
			}
		}
	}

	public void jl(int i1) {
		int j1 = ov[i1];
		int k1 = pv[i1];
		int l1 = qv[i1];
		int i2 = rv[i1];
		int j2 = sv[i1];
		int k2 = nv[i1];
		if (k2 == 200) {
			gl(bw, cw, j1, k1, true);
			super.dd.i(224);
			super.dd.k(j1 + vu);
			super.dd.k(k1 + wu);
			super.dd.k(l1);
			super.dd.k(i2);
			ll();
			lcb = -1;
		}
		if (k2 == 210) {
			gl(bw, cw, j1, k1, true);
			super.dd.i(250);
			super.dd.k(j1 + vu);
			super.dd.k(k1 + wu);
			super.dd.k(l1);
			super.dd.k(i2);
			ll();
			sx = -1;
		}
		if (k2 == 220) {
			gl(bw, cw, j1, k1, true);
			super.dd.i(252);
			super.dd.k(j1 + vu);
			super.dd.k(k1 + wu);
			super.dd.k(l1);
			ll();
		}
		if (k2 == 3200)
			gk(Definitions.wfb[l1], 3);
		if (k2 == 300) {
			dm(j1, k1, l1);
			super.dd.i(223);
			super.dd.k(j1 + vu);
			super.dd.k(k1 + wu);
			super.dd.n(l1);
			super.dd.k(i2);
			ll();
			lcb = -1;
		}
		if (k2 == 310) {
			dm(j1, k1, l1);
			super.dd.i(239);
			super.dd.k(j1 + vu);
			super.dd.k(k1 + wu);
			super.dd.n(l1);
			super.dd.k(i2);
			ll();
			sx = -1;
		}
		if (k2 == 320) {
			dm(j1, k1, l1);
			super.dd.i(238);
			super.dd.k(j1 + vu);
			super.dd.k(k1 + wu);
			super.dd.n(l1);
			ll();
		}
		if (k2 == 2300) {
			dm(j1, k1, l1);
			super.dd.i(229);
			super.dd.k(j1 + vu);
			super.dd.k(k1 + wu);
			super.dd.n(l1);
			ll();
		}
		if (k2 == 3300)
			gk(Definitions.nib[l1], 3);
		if (k2 == 400) {
			zk(j1, k1, l1, i2);
			super.dd.i(222);
			super.dd.k(j1 + vu);
			super.dd.k(k1 + wu);
			super.dd.k(j2);
			ll();
			lcb = -1;
		}
		if (k2 == 410) {
			zk(j1, k1, l1, i2);
			super.dd.i(241);
			super.dd.k(j1 + vu);
			super.dd.k(k1 + wu);
			super.dd.k(j2);
			ll();
			sx = -1;
		}
		if (k2 == 420) {
			zk(j1, k1, l1, i2);
			super.dd.i(242);
			super.dd.k(j1 + vu);
			super.dd.k(k1 + wu);
			ll();
		}
		if (k2 == 2400) {
			zk(j1, k1, l1, i2);
			super.dd.i(230);
			super.dd.k(j1 + vu);
			super.dd.k(k1 + wu);
			ll();
		}
		if (k2 == 3400)
			gk(Definitions.cib[l1], 3);
		if (k2 == 600) {
			super.dd.i(220);
			super.dd.k(l1);
			super.dd.k(i2);
			ll();
			lcb = -1;
		}
		if (k2 == 610) {
			super.dd.i(240);
			super.dd.k(l1);
			super.dd.k(i2);
			ll();
			sx = -1;
		}
		if (k2 == 620) {
			super.dd.i(248);
			super.dd.k(l1);
			ll();
		}
		if (k2 == 630) {
			super.dd.i(249);
			super.dd.k(l1);
			ll();
		}
		if (k2 == 640) {
			super.dd.i(246);
			super.dd.k(l1);
			ll();
		}
		if (k2 == 650) {
			sx = l1;
			nx = 0;
			tx = Definitions.itemName[px[sx]][0];
		}
		if (k2 == 660) {
			super.dd.i(251);
			super.dd.k(l1);
			ll();
			sx = -1;
			nx = 0;
			gk("Dropping " + Definitions.itemName[px[l1]][0], 4);
		}
		if (k2 == 3600)
			gk(Definitions.wfb[l1], 3);
		if (k2 == 700) {
			int l2 = (j1 - 64) / tt;
			int k4 = (k1 - 64) / tt;
			ul(bw, cw, l2, k4, true);
			super.dd.i(225);
			super.dd.k(l1);
			super.dd.k(i2);
			ll();
			lcb = -1;
		}
		if (k2 == 710) {
			int i3 = (j1 - 64) / tt;
			int l4 = (k1 - 64) / tt;
			ul(bw, cw, i3, l4, true);
			super.dd.i(243);
			super.dd.k(l1);
			super.dd.k(i2);
			ll();
			sx = -1;
		}
		if (k2 == 720) {
			int j3 = (j1 - 64) / tt;
			int i5 = (k1 - 64) / tt;
			ul(bw, cw, j3, i5, true);
			super.dd.i(245);
			super.dd.k(l1);
			ll();
		}
		if (k2 == 715 || k2 == 2715) {
			int k3 = (j1 - 64) / tt;
			int j5 = (k1 - 64) / tt;
			ul(bw, cw, k3, j5, true);
			super.dd.i(244);
			super.dd.k(l1);
			ll();
		}
		if (k2 == 3700)
			gk(Definitions.qgb[l1], 3);
		if (k2 == 800) {
			int l3 = (j1 - 64) / tt;
			int k5 = (k1 - 64) / tt;
			ul(bw, cw, l3, k5, true);
			super.dd.i(226);
			super.dd.k(l1);
			super.dd.k(i2);
			ll();
			lcb = -1;
		}
		if (k2 == 810) {
			int i4 = (j1 - 64) / tt;
			int l5 = (k1 - 64) / tt;
			ul(bw, cw, i4, l5, true);
			super.dd.i(219);
			super.dd.k(l1);
			super.dd.k(i2);
			ll();
			sx = -1;
		}
		if (k2 == 805 || k2 == 2805) {
			int j4 = (j1 - 64) / tt;
			int i6 = (k1 - 64) / tt;
			ul(bw, cw, j4, i6, true);
			super.dd.i(228);
			super.dd.k(l1);
			ll();
		}
		if (k2 == 2810) {
			super.dd.i(235);
			super.dd.k(l1);
			ll();
		}
		if (k2 == 2820) {
			super.dd.i(214);
			super.dd.k(l1);
			ll();
		}
		if (k2 == 900) {
			ul(bw, cw, j1, k1, true);
			super.dd.i(221);
			super.dd.k(j1 + vu);
			super.dd.k(k1 + wu);
			super.dd.k(l1);
			ll();
			lcb = -1;
		}
		if (k2 == 920) {
			ul(bw, cw, j1, k1, false);
			if (scb == -24)
				scb = 24;
		}
		if (k2 == 1000) {
			super.dd.i(227);
			super.dd.k(l1);
			ll();
			lcb = -1;
		}
		if (k2 == 4000) {
			sx = -1;
			lcb = -1;
		}
	}

	public Client() {
		zs = false;
		bt = false;
		ct = true;
		dt = false;
		it = "";
		kt = false;
		nt = false;
		tt = 128;
		yt = true;
		zt = 128;
		bu = 512;
		cu = 334;
		du = 9;
		gu = 0xbc614e;
		hu = false;
		lu = 550;
		mu = false;
		pu = 1;
		uu = -1;
		xu = -1;
		dv = false;
		jv = 200;
		kv = false;
		lv = new String[jv];
		mv = new String[jv];
		nv = new int[jv];
		ov = new int[jv];
		pv = new int[jv];
		qv = new int[jv];
		rv = new int[jv];
		sv = new int[jv];
		tv = new int[jv];
		uv = 4000;
		vv = 500;
		yv = new Player[uv];
		zv = new Player[vv];
		player = new Player();
		dw = 1000;
		ew = 500;
		gw = new Player[dw];
		hw = new Player[ew];
		lw = 500;
		ow = new int[lw];
		pw = new int[lw];
		qw = new int[lw];
		rw = new int[lw];
		sw = 1500;
		uw = new h[sw];
		vw = new int[sw];
		ww = new int[sw];
		xw = new int[sw];
		yw = new int[sw];
		zw = new h[200];
		ax = new boolean[sw];
		bx = 500;
		dx = new h[bx];
		ex = new int[bx];
		fx = new int[bx];
		gx = new int[bx];
		hx = new int[bx];
		ix = new boolean[bx];
		jx = 8000;
		kx = new int[jx];
		lx = new int[jx];
		px = new int[30];
		qx = new int[30];
		rx = new int[30];
		sx = -1;
		tx = "";
		ux = false;
		vx = "";
		xx = new int[14];
		yx = new int[14];
		ay = new int[14];
		by = new int[14];
		cy = false;
		dy = false;
		gy = false;
		jy = new int[256];
		ky = new int[256];
		ly = new int[256];
		my = -1;
		ny = -2;
		level = new int[16];
		realLevel = new int[16];
		qy = new int[5];
		vy = new int[500];
		wy = false;
		yy = new String[5];
		cz = "";
		dz = "";
		ez = "";
		fz = "";
		qz = 5;
		rz = new String[qz];
		sz = new int[qz];
		uz = 40;
		sbb = false;
		ubb = 1;
		vbb = 2;
		wbb = 2;
		xbb = 8;
		ybb = 14;
		acb = 1;
		lcb = -1;
		qcb = -1;
		rcb = -1;
		wcb = new String[50];
		xcb = new int[50];
		ycb = new int[50];
		zcb = new int[50];
		adb = new int[50];
		cdb = new int[50];
		ddb = new int[50];
		edb = new int[50];
		fdb = new int[50];
		hdb = new int[50];
		idb = new int[50];
		jdb = new int[50];
	}

	boolean zs;
	int at;
	boolean bt;
	boolean ct;
	boolean dt;
	int et;
	int ft;
	int gt;
	int ht;
	String it;
	int jt;
	boolean kt;
	int lt;
	int mt;
	boolean nt;
	Graphics ot;
	j pt;
	m qt;
	Image rt;
	int st;
	int tt;
	int ut;
	int[] vt = { 0, 1, 2, 1 };
	int[] wt = { 0, 1, 2, 1, 0, 0, 0, 0 };
	int[] xt = { 0, 0, 0, 0, 0, 1, 2, 1 };
	public boolean yt;
	public int zt;
	public int au;
	int bu;
	int cu;
	int du;
	int eu;
	int fu;
	int gu;
	boolean hu;
	int iu;
	int ju;
	int ku;
	int lu;
	boolean mu;
	int nu;
	int ou;
	int pu;
	int qu;
	int ru;
	int su;
	int tu;
	int uu;
	int vu;
	int wu;
	int xu;
	int yu;
	int zu;
	int av;
	int bv;
	p cv;
	boolean dv;
	int ev;
	int fv;
	int gv;
	int hv;
	int iv;
	int jv;
	boolean kv;
	String[] lv;
	String[] mv;
	int[] nv;
	int[] ov;
	int[] pv;
	int[] qv;
	int[] rv;
	int[] sv;
	int[] tv;
	int uv;
	int vv;
	int wv;
	int xv;
	Player[] yv;
	Player[] zv;
	Player player;
	int bw;
	int cw;
	int dw;
	int ew;
	int fw;
	Player[] gw;
	Player[] hw;
	int iw;
	int jw;
	int[][] animDirLayer_To_CharLayer = { { 11, 2, 9, 7, 1, 6, 10, 0, 5, 8, 3, 4 }, { 11, 2, 9, 7, 1, 6, 10, 0, 5, 8, 3, 4 },
			{ 11, 3, 2, 9, 7, 1, 6, 10, 0, 5, 8, 4 }, { 3, 4, 2, 9, 7, 1, 6, 10, 8, 11, 0, 5 },
			{ 3, 4, 2, 9, 7, 1, 6, 10, 8, 11, 0, 5 }, { 4, 3, 2, 9, 7, 1, 6, 10, 8, 11, 0, 5 },
			{ 11, 4, 2, 9, 7, 1, 6, 10, 0, 5, 8, 3 }, { 11, 2, 9, 7, 1, 6, 10, 0, 5, 8, 4, 3 } };
	int lw;
	int mw;
	int nw;
	int[] ow;
	int[] pw;
	int[] qw;
	int[] rw;
	int sw;
	int tw;
	h[] uw;
	int[] vw;
	int[] ww;
	int[] xw;
	int[] yw;
	h[] zw;
	boolean[] ax;
	int bx;
	int cx;
	h[] dx;
	int[] ex;
	int[] fx;
	int[] gx;
	int[] hx;
	boolean[] ix;
	int jx;
	int[] kx;
	int[] lx;
	int mx;
	int nx;
	int ox;
	int[] px;
	int[] qx;
	int[] rx;
	int sx;
	String tx;
	boolean ux;
	String vx;
	int wx;
	int[] xx;
	int[] yx;
	int zx;
	int[] ay;
	int[] by;
	boolean cy;
	boolean dy;
	int ey;
	int fy;
	boolean gy;
	int hy;
	int iy;
	int[] jy;
	int[] ky;
	int[] ly;
	int my;
	int ny;
	int[] level;
	int[] realLevel;
	int[] qy;
	int questPoints;
	String[] skills = { "Attack", "Defense", "Strength", "Hits", "Ranged", "Prayer", "Magic", "Cooking", "Woodcutting",
			"Fletching", "Fishing", "Firemaking", "Crafting", "Smithing", "Mining", "Herblaw" };
	String[] attributes = { "Armour", "WeaponAim", "WeaponPower", "Magic", "Prayer" };
	int uy;
	int[] vy;
	boolean wy;
	int xy;
	String[] yy;
	int zy;
	int az;
	int bz;
	String cz;
	String dz;
	String ez;
	String fz;
	int gz;
	int hz;
	int iz;
	int jz;
	g kz;
	int lz;
	int mz;
	int nz;
	int oz;
	int pz;
	int qz;
	String[] rz;
	int[] sz;
	int tz;
	int uz;
	int vz;
	g wz;
	g xz;
	g yz;
	g zz;
	g aab;
	int bab;
	int cab;
	int dab;
	int eab;
	int fab;
	int gab;
	int hab;
	int iab;
	int jab;
	int kab;
	int lab;
	int mab;
	int nab;
	int oab;
	int pab;
	int qab;
	int rab;
	int sab;
	int tab;
	int uab;
	int vab;
	int wab;
	int xab;
	int yab;
	int zab;
	int abb;
	int bbb;
	int cbb;
	int dbb;
	int ebb;
	int fbb;
	int gbb;
	int hbb;
	int ibb;
	int jbb;
	int kbb;
	int lbb;
	int mbb;
	int nbb;
	int obb;
	int pbb;
	int qbb;
	int rbb;
	boolean sbb;
	int tbb;
	int ubb;
	int vbb;
	int wbb;
	int xbb;
	int ybb;
	int zbb;
	int acb;
	public int[] bcb = { 0xff0000, 0xff8000, 0xffe000, 0xa0e000, 57344, 32768, 41088, 45311, 33023, 12528, 0xe000e0,
			0x303030, 0x604000, 0x805000, 0xffffff };
	public int[] ccb = { 0xffc030, 0xffa040, 0x805030, 0x604020, 0x303030, 0xff6020, 0xff4000, 0xffffff, 65280, 65535 };
	public int[] dcb = { 0xecded0, 0xccb366, 0xb38c40, 0x997326, 0x906020 };
	g ecb;
	int fcb;
	int gcb;
	g hcb;
	int icb;
	int jcb;
	long kcb;
	int lcb;
	String mcb;
	int ncb;
	int ocb;
	int pcb;
	int qcb;
	int rcb;
	int scb;
	int tcb;
	int ucb;
	int vcb;
	String[] wcb;
	int[] xcb;
	int[] ycb;
	int[] zcb;
	int[] adb;
	int bdb;
	int[] cdb;
	int[] ddb;
	int[] edb;
	int[] fdb;
	int gdb;
	int[] hdb;
	int[] idb;
	int[] jdb;
}
