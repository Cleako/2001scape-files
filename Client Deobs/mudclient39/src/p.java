import jagex.client.*;
import jagex.o;
import java.io.IOException;

public class p {

	public p(j j1, GameImageProducer k) {
		udb = true;
		vdb = false;
		ydb = 750;
		beb = new int[256];
		heb = new byte[4][2304];
		ieb = new byte[4][2304];
		jeb = new byte[4][2304];
		keb = new byte[4][2304];
		leb = new byte[4][2304];
		meb = new byte[4][2304];
		neb = new byte[4][2304];
		oeb = new int[4][2304];
		peb = 96;
		qeb = 96;
		reb = new int[peb * qeb * 2];
		seb = new int[peb * qeb * 2];
		teb = new int[peb][qeb];
		ueb = new int[peb][qeb];
		veb = new int[peb][qeb];
		web = false;
		xeb = new h[64];
		yeb = new h[4][64];
		zeb = new h[4][64];
		xdb = j1;
		wdb = k;
		for (int l = 0; l < 64; l++)
			beb[l] = j.zh(255 - l * 4, 255 - (int) ((double) l * 1.75D), 255 - l * 4);

		for (int i1 = 0; i1 < 64; i1++)
			beb[i1 + 64] = j.zh(i1 * 3, 144, 0);

		for (int k1 = 0; k1 < 64; k1++)
			beb[k1 + 128] = j.zh(192 - (int) ((double) k1 * 1.5D), 144 - (int) ((double) k1 * 1.5D), 0);

		for (int l1 = 0; l1 < 64; l1++)
			beb[l1 + 192] = j.zh(96 - (int) ((double) l1 * 1.5D), 48 + (int) ((double) l1 * 1.5D), 0);

	}

	public int cn(int k, int l, int i1, int j1, int k1, int l1, int ai[], int ai1[], boolean flag) {
		for (int i2 = 0; i2 < peb; i2++) {
			for (int j2 = 0; j2 < qeb; j2++)
				teb[i2][j2] = 0;

		}

		int k2 = 0;
		int l2 = 0;
		int i3 = k;
		int j3 = l;
		teb[k][l] = 99;
		ai[k2] = k;
		ai1[k2++] = l;
		int k3 = ai.length;
		boolean flag1 = false;
		while (l2 != k2) {
			i3 = ai[l2];
			j3 = ai1[l2];
			l2 = (l2 + 1) % k3;
			if (i3 >= i1 && i3 <= k1 && j3 >= j1 && j3 <= l1) {
				flag1 = true;
				break;
			}
			if (flag) {
				if (i3 > 0 && i3 - 1 >= i1 && i3 - 1 <= k1 && j3 >= j1 && j3 <= l1 && (ueb[i3 - 1][j3] & 8) == 0) {
					flag1 = true;
					break;
				}
				if (i3 < peb - 1 && i3 + 1 >= i1 && i3 + 1 <= k1 && j3 >= j1 && j3 <= l1
						&& (ueb[i3 + 1][j3] & 2) == 0) {
					flag1 = true;
					break;
				}
				if (j3 > 0 && i3 >= i1 && i3 <= k1 && j3 - 1 >= j1 && j3 - 1 <= l1 && (ueb[i3][j3 - 1] & 4) == 0) {
					flag1 = true;
					break;
				}
				if (j3 < qeb - 1 && i3 >= i1 && i3 <= k1 && j3 + 1 >= j1 && j3 + 1 <= l1
						&& (ueb[i3][j3 + 1] & 1) == 0) {
					flag1 = true;
					break;
				}
			}
			if (i3 > 0 && teb[i3 - 1][j3] == 0 && (ueb[i3 - 1][j3] & 0x78) == 0) {
				ai[k2] = i3 - 1;
				ai1[k2] = j3;
				k2 = (k2 + 1) % k3;
				teb[i3 - 1][j3] = 2;
			}
			if (i3 < peb - 1 && teb[i3 + 1][j3] == 0 && (ueb[i3 + 1][j3] & 0x72) == 0) {
				ai[k2] = i3 + 1;
				ai1[k2] = j3;
				k2 = (k2 + 1) % k3;
				teb[i3 + 1][j3] = 8;
			}
			if (j3 > 0 && teb[i3][j3 - 1] == 0 && (ueb[i3][j3 - 1] & 0x74) == 0) {
				ai[k2] = i3;
				ai1[k2] = j3 - 1;
				k2 = (k2 + 1) % k3;
				teb[i3][j3 - 1] = 1;
			}
			if (j3 < qeb - 1 && teb[i3][j3 + 1] == 0 && (ueb[i3][j3 + 1] & 0x71) == 0) {
				ai[k2] = i3;
				ai1[k2] = j3 + 1;
				k2 = (k2 + 1) % k3;
				teb[i3][j3 + 1] = 4;
			}
			if (i3 > 0 && j3 > 0 && (ueb[i3][j3 - 1] & 0x74) == 0 && (ueb[i3 - 1][j3] & 0x78) == 0
					&& (ueb[i3 - 1][j3 - 1] & 0x7c) == 0 && teb[i3 - 1][j3 - 1] == 0) {
				ai[k2] = i3 - 1;
				ai1[k2] = j3 - 1;
				k2 = (k2 + 1) % k3;
				teb[i3 - 1][j3 - 1] = 3;
			}
			if (i3 < peb - 1 && j3 > 0 && (ueb[i3][j3 - 1] & 0x74) == 0 && (ueb[i3 + 1][j3] & 0x72) == 0
					&& (ueb[i3 + 1][j3 - 1] & 0x76) == 0 && teb[i3 + 1][j3 - 1] == 0) {
				ai[k2] = i3 + 1;
				ai1[k2] = j3 - 1;
				k2 = (k2 + 1) % k3;
				teb[i3 + 1][j3 - 1] = 9;
			}
			if (i3 > 0 && j3 < qeb - 1 && (ueb[i3][j3 + 1] & 0x71) == 0 && (ueb[i3 - 1][j3] & 0x78) == 0
					&& (ueb[i3 - 1][j3 + 1] & 0x79) == 0 && teb[i3 - 1][j3 + 1] == 0) {
				ai[k2] = i3 - 1;
				ai1[k2] = j3 + 1;
				k2 = (k2 + 1) % k3;
				teb[i3 - 1][j3 + 1] = 6;
			}
			if (i3 < peb - 1 && j3 < qeb - 1 && (ueb[i3][j3 + 1] & 0x71) == 0 && (ueb[i3 + 1][j3] & 0x72) == 0
					&& (ueb[i3 + 1][j3 + 1] & 0x73) == 0 && teb[i3 + 1][j3 + 1] == 0) {
				ai[k2] = i3 + 1;
				ai1[k2] = j3 + 1;
				k2 = (k2 + 1) % k3;
				teb[i3 + 1][j3 + 1] = 12;
			}
		}
		if (!flag1)
			return -1;
		l2 = 0;
		ai[l2] = i3;
		ai1[l2++] = j3;
		int i4;
		for (int l3 = i4 = teb[i3][j3]; i3 != k || j3 != l; l3 = teb[i3][j3]) {
			if (l3 != i4) {
				i4 = l3;
				ai[l2] = i3;
				ai1[l2++] = j3;
			}
			if ((l3 & 2) != 0)
				i3++;
			else if ((l3 & 8) != 0)
				i3--;
			if ((l3 & 1) != 0)
				j3++;
			else if ((l3 & 4) != 0)
				j3--;
		}

		return l2;
	}

	public void ao(int k, int l, int i1) {
		ueb[k][l] |= i1;
	}

	public void tn(int k, int l, int i1) {
		ueb[k][l] &= 65535 - i1;
	}

	public void bo(int k, int l, int i1, int j1) {
		if (k < 0 || l < 0 || k >= peb - 1 || l >= qeb - 1)
			return;
		if (Definitions.tib[j1] == 1) {
			if (i1 == 0) {
				ueb[k][l] |= 1;
				if (l > 0)
					ao(k, l - 1, 4);
			} else if (i1 == 1) {
				ueb[k][l] |= 2;
				if (k > 0)
					ao(k - 1, l, 8);
			} else if (i1 == 2)
				ueb[k][l] |= 0x10;
			else if (i1 == 3)
				ueb[k][l] |= 0x20;
			in(k, l, 1, 1);
		}
	}

	public void an(int k, int l, int i1, int j1) {
		if (k < 0 || l < 0 || k >= peb - 1 || l >= qeb - 1)
			return;
		if (Definitions.tib[j1] == 1) {
			if (i1 == 0) {
				ueb[k][l] &= 0xfffe;
				if (l > 0)
					tn(k, l - 1, 4);
			} else if (i1 == 1) {
				ueb[k][l] &= 0xfffd;
				if (k > 0)
					tn(k - 1, l, 8);
			} else if (i1 == 2)
				ueb[k][l] &= 0xffef;
			else if (i1 == 3)
				ueb[k][l] &= 0xffdf;
			in(k, l, 1, 1);
		}
	}

	public void pn(int k, int l, int i1) {
		if (k < 0 || l < 0 || k >= peb - 1 || l >= qeb - 1)
			return;
		if (Definitions.iib[i1] == 1 || Definitions.iib[i1] == 2) {
			int j1 = dn(k, l);
			int k1;
			int l1;
			if (j1 == 0 || j1 == 4) {
				k1 = Definitions.gib[i1];
				l1 = Definitions.hib[i1];
			} else {
				l1 = Definitions.gib[i1];
				k1 = Definitions.hib[i1];
			}
			for (int i2 = k; i2 < k + k1; i2++) {
				for (int j2 = l; j2 < l + l1; j2++)
					if (Definitions.iib[i1] == 1)
						ueb[i2][j2] |= 0x40;
					else if (j1 == 0) {
						ueb[i2][j2] |= 2;
						if (i2 > 0)
							ao(i2 - 1, j2, 8);
					} else if (j1 == 2) {
						ueb[i2][j2] |= 4;
						if (j2 < qeb - 1)
							ao(i2, j2 + 1, 1);
					} else if (j1 == 4) {
						ueb[i2][j2] |= 8;
						if (i2 < peb - 1)
							ao(i2 + 1, j2, 2);
					} else if (j1 == 6) {
						ueb[i2][j2] |= 1;
						if (j2 > 0)
							ao(i2, j2 - 1, 4);
					}

			}

			in(k, l, k1, l1);
		}
	}

	public void zn(int k, int l, int i1) {
		if (k < 0 || l < 0 || k >= peb - 1 || l >= qeb - 1)
			return;
		if (Definitions.iib[i1] == 1 || Definitions.iib[i1] == 2) {
			int j1 = dn(k, l);
			int k1;
			int l1;
			if (j1 == 0 || j1 == 4) {
				k1 = Definitions.gib[i1];
				l1 = Definitions.hib[i1];
			} else {
				l1 = Definitions.gib[i1];
				k1 = Definitions.hib[i1];
			}
			for (int i2 = k; i2 < k + k1; i2++) {
				for (int j2 = l; j2 < l + l1; j2++)
					if (Definitions.iib[i1] == 1)
						ueb[i2][j2] &= 0xffbf;
					else if (j1 == 0) {
						ueb[i2][j2] &= 0xfffd;
						if (i2 > 0)
							tn(i2 - 1, j2, 8);
					} else if (j1 == 2) {
						ueb[i2][j2] &= 0xfffb;
						if (j2 < qeb - 1)
							tn(i2, j2 + 1, 1);
					} else if (j1 == 4) {
						ueb[i2][j2] &= 0xfff7;
						if (i2 < peb - 1)
							tn(i2 + 1, j2, 2);
					} else if (j1 == 6) {
						ueb[i2][j2] &= 0xfffe;
						if (j2 > 0)
							tn(i2, j2 - 1, 4);
					}

			}

			in(k, l, k1, l1);
		}
	}

	public void in(int k, int l, int i1, int j1) {
		if (k < 1 || l < 1 || k + i1 >= peb || l + j1 >= qeb)
			return;
		for (int k1 = k; k1 <= k + i1; k1++) {
			for (int l1 = l; l1 <= l + j1; l1++)
				if ((fo(k1, l1) & 0x63) != 0 || (fo(k1 - 1, l1) & 0x59) != 0 || (fo(k1, l1 - 1) & 0x56) != 0
						|| (fo(k1 - 1, l1 - 1) & 0x6c) != 0)
					nn(k1, l1, 35);
				else
					nn(k1, l1, 0);

		}

	}

	public void nn(int k, int l, int i1) {
		int j1 = k / 12;
		int k1 = l / 12;
		int l1 = (k - 1) / 12;
		int i2 = (l - 1) / 12;
		co(j1, k1, k, l, i1);
		if (j1 != l1)
			co(l1, k1, k, l, i1);
		if (k1 != i2)
			co(j1, i2, k, l, i1);
		if (j1 != l1 && k1 != i2)
			co(l1, i2, k, l, i1);
	}

	public void co(int k, int l, int i1, int j1, int k1) {
		h h1 = xeb[k + l * 8];
		for (int l1 = 0; l1 < h1.jg; l1++)
			if (h1.ei[l1] == i1 * 128 && h1.gi[l1] == j1 * 128) {
				h1.ud(l1, k1);
				return;
			}

	}

	public int fo(int k, int l) {
		if (k < 0 || l < 0 || k >= peb || l >= qeb)
			return 0;
		else
			return ueb[k][l];
	}

	public int jn(int k, int l) {
		int i1 = k >> 7;
		int j1 = l >> 7;
		int k1 = k & 0x7f;
		int l1 = l & 0x7f;
		if (i1 < 0 || j1 < 0 || i1 >= peb - 1 || j1 >= qeb - 1)
			return 0;
		int i2;
		int j2;
		int k2;
		if (k1 <= 128 - l1) {
			i2 = on(i1, j1);
			j2 = on(i1 + 1, j1) - i2;
			k2 = on(i1, j1 + 1) - i2;
		} else {
			i2 = on(i1 + 1, j1 + 1);
			j2 = on(i1, j1 + 1) - i2;
			k2 = on(i1 + 1, j1) - i2;
			k1 = 128 - k1;
			l1 = 128 - l1;
		}
		int l2 = i2 + (j2 * k1) / 128 + (k2 * l1) / 128;
		return l2;
	}

	public int on(int k, int l) {
		if (k < 0 || k >= 96 || l < 0 || l >= 96)
			return 0;
		byte byte0 = 0;
		if (k >= 48 && l < 48) {
			byte0 = 1;
			k -= 48;
		} else if (k < 48 && l >= 48) {
			byte0 = 2;
			l -= 48;
		} else if (k >= 48 && l >= 48) {
			byte0 = 3;
			k -= 48;
			l -= 48;
		}
		return (heb[byte0][k * 48 + l] & 0xff) * 3;
	}

	public int bn(int k, int l) {
		if (k < 0 || k >= 96 || l < 0 || l >= 96)
			return 0;
		byte byte0 = 0;
		if (k >= 48 && l < 48) {
			byte0 = 1;
			k -= 48;
		} else if (k < 48 && l >= 48) {
			byte0 = 2;
			l -= 48;
		} else if (k >= 48 && l >= 48) {
			byte0 = 3;
			k -= 48;
			l -= 48;
		}
		return ieb[byte0][k * 48 + l] & 0xff;
	}

	public int qn(int k, int l, int i1) {
		if (k < 0 || k >= 96 || l < 0 || l >= 96)
			return 0;
		byte byte0 = 0;
		if (k >= 48 && l < 48) {
			byte0 = 1;
			k -= 48;
		} else if (k < 48 && l >= 48) {
			byte0 = 2;
			l -= 48;
		} else if (k >= 48 && l >= 48) {
			byte0 = 3;
			k -= 48;
			l -= 48;
		}
		return meb[byte0][k * 48 + l] & 0xff;
	}

	public void mn(int k, int l, int i1) {
		if (k < 0 || k >= 96 || l < 0 || l >= 96)
			return;
		byte byte0 = 0;
		if (k >= 48 && l < 48) {
			byte0 = 1;
			k -= 48;
		} else if (k < 48 && l >= 48) {
			byte0 = 2;
			l -= 48;
		} else if (k >= 48 && l >= 48) {
			byte0 = 3;
			k -= 48;
			l -= 48;
		}
		meb[byte0][k * 48 + l] = (byte) i1;
	}

	public int wn(int k, int l, int i1) {
		int j1 = qn(k, l, i1);
		if (j1 == 0)
			return -1;
		int k1 = Definitions.ejb[j1 - 1];
		return k1 != 2 ? 0 : 1;
	}

	public int hn(int k, int l, int i1, int j1) {
		int k1 = qn(k, l, i1);
		if (k1 == 0)
			return j1;
		else
			return Definitions.djb[k1 - 1];
	}

	public int gn(int k, int l) {
		if (k < 0 || k >= 96 || l < 0 || l >= 96)
			return 0;
		byte byte0 = 0;
		if (k >= 48 && l < 48) {
			byte0 = 1;
			k -= 48;
		} else if (k < 48 && l >= 48) {
			byte0 = 2;
			l -= 48;
		} else if (k >= 48 && l >= 48) {
			byte0 = 3;
			k -= 48;
			l -= 48;
		}
		return oeb[byte0][k * 48 + l];
	}

	public int vn(int k, int l) {
		if (k < 0 || k >= 96 || l < 0 || l >= 96)
			return 0;
		byte byte0 = 0;
		if (k >= 48 && l < 48) {
			byte0 = 1;
			k -= 48;
		} else if (k < 48 && l >= 48) {
			byte0 = 2;
			l -= 48;
		} else if (k >= 48 && l >= 48) {
			byte0 = 3;
			k -= 48;
			l -= 48;
		}
		return leb[byte0][k * 48 + l];
	}

	public int dn(int k, int l) {
		if (k < 0 || k >= 96 || l < 0 || l >= 96)
			return 0;
		byte byte0 = 0;
		if (k >= 48 && l < 48) {
			byte0 = 1;
			k -= 48;
		} else if (k < 48 && l >= 48) {
			byte0 = 2;
			l -= 48;
		} else if (k >= 48 && l >= 48) {
			byte0 = 3;
			k -= 48;
			l -= 48;
		}
		return neb[byte0][k * 48 + l];
	}

	public boolean yn(int k, int l) {
		return vn(k, l) > 0 || vn(k - 1, l) > 0 || vn(k - 1, l - 1) > 0 || vn(k, l - 1) > 0;
	}

	public boolean xn(int k, int l) {
		return vn(k, l) > 0 && vn(k - 1, l) > 0 && vn(k - 1, l - 1) > 0 && vn(k, l - 1) > 0;
	}

	public int kn(int k, int l) {
		if (k < 0 || k >= 96 || l < 0 || l >= 96)
			return 0;
		byte byte0 = 0;
		if (k >= 48 && l < 48) {
			byte0 = 1;
			k -= 48;
		} else if (k < 48 && l >= 48) {
			byte0 = 2;
			l -= 48;
		} else if (k >= 48 && l >= 48) {
			byte0 = 3;
			k -= 48;
			l -= 48;
		}
		return keb[byte0][k * 48 + l] & 0xff;
	}

	public int sn(int k, int l) {
		if (k < 0 || k >= 96 || l < 0 || l >= 96)
			return 0;
		byte byte0 = 0;
		if (k >= 48 && l < 48) {
			byte0 = 1;
			k -= 48;
		} else if (k < 48 && l >= 48) {
			byte0 = 2;
			l -= 48;
		} else if (k >= 48 && l >= 48) {
			byte0 = 3;
			k -= 48;
			l -= 48;
		}
		return jeb[byte0][k * 48 + l] & 0xff;
	}

	public void zm(int k, int l, int i1, int j1) {
		String s = "m" + i1 + k / 10 + k % 10 + l / 10 + l % 10;
		int k1;
		try {
			byte abyte0[];
			if (udb) {
				abyte0 = o.lm(s + ".jm", 0, geb);
				if (abyte0 == null || abyte0.length == 0)
					throw new IOException("Map not defined");
			} else {
				abyte0 = new byte[20736];
				o.jm("../gamedata/maps/" + s + ".jm", abyte0, 20736);
			}
			int l1 = 0;
			int i2 = 0;
			for (int j2 = 0; j2 < 2304; j2++) {
				l1 = l1 + abyte0[i2++] & 0xff;
				heb[j1][j2] = (byte) l1;
			}

			l1 = 0;
			for (int k2 = 0; k2 < 2304; k2++) {
				l1 = l1 + abyte0[i2++] & 0xff;
				ieb[j1][k2] = (byte) l1;
			}

			for (int l2 = 0; l2 < 2304; l2++)
				jeb[j1][l2] = abyte0[i2++];

			for (int i3 = 0; i3 < 2304; i3++)
				keb[j1][i3] = abyte0[i2++];

			for (int j3 = 0; j3 < 2304; j3++) {
				oeb[j1][j3] = (abyte0[i2] & 0xff) * 256 + (abyte0[i2 + 1] & 0xff);
				i2 += 2;
			}

			for (int k3 = 0; k3 < 2304; k3++)
				leb[j1][k3] = abyte0[i2++];

			for (int l3 = 0; l3 < 2304; l3++)
				meb[j1][l3] = abyte0[i2++];

			for (int i4 = 0; i4 < 2304; i4++)
				neb[j1][i4] = abyte0[i2++];

			return;
		} catch (IOException _ex) {
			k1 = 0;
		}
		for (; k1 < 2304; k1++) {
			heb[j1][k1] = 0;
			ieb[j1][k1] = 0;
			jeb[j1][k1] = 0;
			keb[j1][k1] = 0;
			oeb[j1][k1] = 0;
			leb[j1][k1] = 0;
			meb[j1][k1] = 0;
			if (i1 == 0)
				meb[j1][k1] = -6;
			if (i1 == 3)
				meb[j1][k1] = 8;
			neb[j1][k1] = 0;
		}

	}

	public void eo() {
		xdb.ni();
		for (int k = 0; k < 64; k++) {
			xeb[k] = null;
			for (int l = 0; l < 4; l++)
				yeb[l][k] = null;

			for (int i1 = 0; i1 < 4; i1++)
				zeb[i1][k] = null;

		}

		System.gc();
	}

	public void rn(int k, int l, int i1) {
		eo();
		int j1 = (k + 24) / 48;
		int k1 = (l + 24) / 48;
		fn(k, l, i1, true);
		if (i1 == 0) {
			fn(k, l, 1, false);
			fn(k, l, 2, false);
			zm(j1 - 1, k1 - 1, i1, 0);
			zm(j1, k1 - 1, i1, 1);
			zm(j1 - 1, k1, i1, 2);
			zm(j1, k1, i1, 3);
			ln();
		}
	}

	public void ln() {
		for (int k = 0; k < 96; k++) {
			for (int l = 0; l < 96; l++)
				if (qn(k, l, 0) == 250)
					if (k == 47 && qn(k + 1, l, 0) != 250 && qn(k + 1, l, 0) != 2)
						mn(k, l, 9);
					else if (l == 47 && qn(k, l + 1, 0) != 250 && qn(k, l + 1, 0) != 2)
						mn(k, l, 9);
					else
						mn(k, l, 2);

		}

	}

	public void en(int k, int l, int i1, int j1, int k1) {
		int l1 = k * 3;
		int i2 = l * 3;
		int j2 = xdb.oi(j1);
		int k2 = xdb.oi(k1);
		j2 = j2 >> 1 & 0x7f7f7f;
		k2 = k2 >> 1 & 0x7f7f7f;
		if (i1 == 0) {
			wdb.kg(l1, i2, 3, j2);
			wdb.kg(l1, i2 + 1, 2, j2);
			wdb.kg(l1, i2 + 2, 1, j2);
			wdb.kg(l1 + 2, i2 + 1, 1, k2);
			wdb.kg(l1 + 1, i2 + 2, 2, k2);
			return;
		}
		if (i1 == 1) {
			wdb.kg(l1, i2, 3, k2);
			wdb.kg(l1 + 1, i2 + 1, 2, k2);
			wdb.kg(l1 + 2, i2 + 2, 1, k2);
			wdb.kg(l1, i2 + 1, 1, j2);
			wdb.kg(l1, i2 + 2, 2, j2);
		}
	}

	public void fn(int k, int l, int i1, boolean flag) {
		int j1 = (k + 24) / 48;
		int k1 = (l + 24) / 48;
		zm(j1 - 1, k1 - 1, i1, 0);
		zm(j1, k1 - 1, i1, 1);
		zm(j1 - 1, k1, i1, 2);
		zm(j1, k1, i1, 3);
		ln();
		if (afb == null)
			afb = new h(peb * qeb * 2 + 256, peb * qeb * 2 + 256, true, true, false, false, true);
		if (flag) {
			wdb.df();
			for (int l1 = 0; l1 < 96; l1++) {
				for (int j2 = 0; j2 < 96; j2++)
					ueb[l1][j2] = 0;

			}

			h h1 = afb;
			h1.re();
			for (int l2 = 0; l2 < 96; l2++) {
				for (int k3 = 0; k3 < 96; k3++) {
					int k4 = -on(l2, k3);
					if (qn(l2, k3, i1) > 0 && Definitions.ejb[qn(l2, k3, i1) - 1] == 4)
						k4 = 0;
					if (qn(l2 - 1, k3, i1) > 0 && Definitions.ejb[qn(l2 - 1, k3, i1) - 1] == 4)
						k4 = 0;
					if (qn(l2, k3 - 1, i1) > 0 && Definitions.ejb[qn(l2, k3 - 1, i1) - 1] == 4)
						k4 = 0;
					if (qn(l2 - 1, k3 - 1, i1) > 0 && Definitions.ejb[qn(l2 - 1, k3 - 1, i1) - 1] == 4)
						k4 = 0;
					int l5 = h1.je(l2 * 128, k4, k3 * 128);
					int l7 = (int) (Math.random() * 10D) - 5;
					h1.ud(l5, l7);
				}

			}

			for (int l3 = 0; l3 < 95; l3++) {
				for (int l4 = 0; l4 < 95; l4++) {
					int i6 = bn(l3, l4);
					int i8 = beb[i6];
					int k10 = i8;
					int i13 = i8;
					int j15 = 0;
					if (i1 == 1 || i1 == 2) {
						i8 = 0xbc614e;
						k10 = 0xbc614e;
						i13 = 0xbc614e;
					}
					if (qn(l3, l4, i1) > 0) {
						int j17 = qn(l3, l4, i1);
						int j6 = Definitions.ejb[j17 - 1];
						int k19 = wn(l3, l4, i1);
						i8 = k10 = Definitions.djb[j17 - 1];
						if (j6 == 4) {
							i8 = 1;
							k10 = 1;
						}
						if (j6 == 5) {
							if (gn(l3, l4) > 0 && gn(l3, l4) < 24000)
								if (hn(l3 - 1, l4, i1, i13) != 0xbc614e && hn(l3, l4 - 1, i1, i13) != 0xbc614e) {
									i8 = hn(l3 - 1, l4, i1, i13);
									j15 = 0;
								} else if (hn(l3 + 1, l4, i1, i13) != 0xbc614e && hn(l3, l4 + 1, i1, i13) != 0xbc614e) {
									k10 = hn(l3 + 1, l4, i1, i13);
									j15 = 0;
								} else if (hn(l3 + 1, l4, i1, i13) != 0xbc614e && hn(l3, l4 - 1, i1, i13) != 0xbc614e) {
									k10 = hn(l3 + 1, l4, i1, i13);
									j15 = 1;
								} else if (hn(l3 - 1, l4, i1, i13) != 0xbc614e && hn(l3, l4 + 1, i1, i13) != 0xbc614e) {
									i8 = hn(l3 - 1, l4, i1, i13);
									j15 = 1;
								}
						} else if (j6 != 2 || gn(l3, l4) > 0 && gn(l3, l4) < 24000)
							if (wn(l3 - 1, l4, i1) != k19 && wn(l3, l4 - 1, i1) != k19) {
								i8 = i13;
								j15 = 0;
							} else if (wn(l3 + 1, l4, i1) != k19 && wn(l3, l4 + 1, i1) != k19) {
								k10 = i13;
								j15 = 0;
							} else if (wn(l3 + 1, l4, i1) != k19 && wn(l3, l4 - 1, i1) != k19) {
								k10 = i13;
								j15 = 1;
							} else if (wn(l3 - 1, l4, i1) != k19 && wn(l3, l4 + 1, i1) != k19) {
								i8 = i13;
								j15 = 1;
							}
						if (Definitions.fjb[j17 - 1] != 0)
							ueb[l3][l4] |= 0x40;
						if (Definitions.ejb[j17 - 1] == 2)
							ueb[l3][l4] |= 0x80;
					}
					en(l3, l4, j15, i8, k10);
					int k17 = ((on(l3 + 1, l4 + 1) - on(l3 + 1, l4)) + on(l3, l4 + 1)) - on(l3, l4);
					if (i8 != k10 || k17 != 0) {
						int ai[] = new int[3];
						int ai7[] = new int[3];
						if (j15 == 0) {
							if (i8 != 0xbc614e) {
								ai[0] = l4 + l3 * peb + peb;
								ai[1] = l4 + l3 * peb;
								ai[2] = l4 + l3 * peb + 1;
								int j22 = h1.ie(3, ai, 0xbc614e, i8);
								reb[j22] = l3;
								seb[j22] = l4;
								h1.qh[j22] = 0x30d40 + j22;
							}
							if (k10 != 0xbc614e) {
								ai7[0] = l4 + l3 * peb + 1;
								ai7[1] = l4 + l3 * peb + peb + 1;
								ai7[2] = l4 + l3 * peb + peb;
								int k22 = h1.ie(3, ai7, 0xbc614e, k10);
								reb[k22] = l3;
								seb[k22] = l4;
								h1.qh[k22] = 0x30d40 + k22;
							}
						} else {
							if (i8 != 0xbc614e) {
								ai[0] = l4 + l3 * peb + 1;
								ai[1] = l4 + l3 * peb + peb + 1;
								ai[2] = l4 + l3 * peb;
								int l22 = h1.ie(3, ai, 0xbc614e, i8);
								reb[l22] = l3;
								seb[l22] = l4;
								h1.qh[l22] = 0x30d40 + l22;
							}
							if (k10 != 0xbc614e) {
								ai7[0] = l4 + l3 * peb + peb;
								ai7[1] = l4 + l3 * peb;
								ai7[2] = l4 + l3 * peb + peb + 1;
								int i23 = h1.ie(3, ai7, 0xbc614e, k10);
								reb[i23] = l3;
								seb[i23] = l4;
								h1.qh[i23] = 0x30d40 + i23;
							}
						}
					} else if (i8 != 0xbc614e) {
						int ai1[] = new int[4];
						ai1[0] = l4 + l3 * peb + peb;
						ai1[1] = l4 + l3 * peb;
						ai1[2] = l4 + l3 * peb + 1;
						ai1[3] = l4 + l3 * peb + peb + 1;
						int j20 = h1.ie(4, ai1, 0xbc614e, i8);
						reb[j20] = l3;
						seb[j20] = l4;
						h1.qh[j20] = 0x30d40 + j20;
					}
				}

			}

			for (int i5 = 1; i5 < 95; i5++) {
				for (int k6 = 1; k6 < 95; k6++)
					if (qn(i5, k6, i1) > 0 && Definitions.ejb[qn(i5, k6, i1) - 1] == 4) {
						int j8 = Definitions.djb[qn(i5, k6, i1) - 1];
						int l10 = h1.je(i5 * 128, -on(i5, k6), k6 * 128);
						int j13 = h1.je((i5 + 1) * 128, -on(i5 + 1, k6), k6 * 128);
						int k15 = h1.je((i5 + 1) * 128, -on(i5 + 1, k6 + 1), (k6 + 1) * 128);
						int l17 = h1.je(i5 * 128, -on(i5, k6 + 1), (k6 + 1) * 128);
						int ai2[] = { l10, j13, k15, l17 };
						int k20 = h1.ie(4, ai2, j8, 0xbc614e);
						reb[k20] = i5;
						seb[k20] = k6;
						h1.qh[k20] = 0x30d40 + k20;
						en(i5, k6, 0, j8, j8);
					} else if (qn(i5, k6, i1) == 0 || Definitions.ejb[qn(i5, k6, i1) - 1] != 3) {
						if (qn(i5, k6 + 1, i1) > 0 && Definitions.ejb[qn(i5, k6 + 1, i1) - 1] == 4) {
							int k8 = Definitions.djb[qn(i5, k6 + 1, i1) - 1];
							int i11 = h1.je(i5 * 128, -on(i5, k6), k6 * 128);
							int k13 = h1.je((i5 + 1) * 128, -on(i5 + 1, k6), k6 * 128);
							int l15 = h1.je((i5 + 1) * 128, -on(i5 + 1, k6 + 1), (k6 + 1) * 128);
							int i18 = h1.je(i5 * 128, -on(i5, k6 + 1), (k6 + 1) * 128);
							int ai3[] = { i11, k13, l15, i18 };
							int l20 = h1.ie(4, ai3, k8, 0xbc614e);
							reb[l20] = i5;
							seb[l20] = k6;
							h1.qh[l20] = 0x30d40 + l20;
							en(i5, k6, 0, k8, k8);
						}
						if (qn(i5, k6 - 1, i1) > 0 && Definitions.ejb[qn(i5, k6 - 1, i1) - 1] == 4) {
							int l8 = Definitions.djb[qn(i5, k6 - 1, i1) - 1];
							int j11 = h1.je(i5 * 128, -on(i5, k6), k6 * 128);
							int l13 = h1.je((i5 + 1) * 128, -on(i5 + 1, k6), k6 * 128);
							int i16 = h1.je((i5 + 1) * 128, -on(i5 + 1, k6 + 1), (k6 + 1) * 128);
							int j18 = h1.je(i5 * 128, -on(i5, k6 + 1), (k6 + 1) * 128);
							int ai4[] = { j11, l13, i16, j18 };
							int i21 = h1.ie(4, ai4, l8, 0xbc614e);
							reb[i21] = i5;
							seb[i21] = k6;
							h1.qh[i21] = 0x30d40 + i21;
							en(i5, k6, 0, l8, l8);
						}
						if (qn(i5 + 1, k6, i1) > 0 && Definitions.ejb[qn(i5 + 1, k6, i1) - 1] == 4) {
							int i9 = Definitions.djb[qn(i5 + 1, k6, i1) - 1];
							int k11 = h1.je(i5 * 128, -on(i5, k6), k6 * 128);
							int i14 = h1.je((i5 + 1) * 128, -on(i5 + 1, k6), k6 * 128);
							int j16 = h1.je((i5 + 1) * 128, -on(i5 + 1, k6 + 1), (k6 + 1) * 128);
							int k18 = h1.je(i5 * 128, -on(i5, k6 + 1), (k6 + 1) * 128);
							int ai5[] = { k11, i14, j16, k18 };
							int j21 = h1.ie(4, ai5, i9, 0xbc614e);
							reb[j21] = i5;
							seb[j21] = k6;
							h1.qh[j21] = 0x30d40 + j21;
							en(i5, k6, 0, i9, i9);
						}
						if (qn(i5 - 1, k6, i1) > 0 && Definitions.ejb[qn(i5 - 1, k6, i1) - 1] == 4) {
							int j9 = Definitions.djb[qn(i5 - 1, k6, i1) - 1];
							int l11 = h1.je(i5 * 128, -on(i5, k6), k6 * 128);
							int j14 = h1.je((i5 + 1) * 128, -on(i5 + 1, k6), k6 * 128);
							int k16 = h1.je((i5 + 1) * 128, -on(i5 + 1, k6 + 1), (k6 + 1) * 128);
							int l18 = h1.je(i5 * 128, -on(i5, k6 + 1), (k6 + 1) * 128);
							int ai6[] = { l11, j14, k16, l18 };
							int k21 = h1.ie(4, ai6, j9, 0xbc614e);
							reb[k21] = i5;
							seb[k21] = k6;
							h1.qh[k21] = 0x30d40 + k21;
							en(i5, k6, 0, j9, j9);
						}
					}

			}

			h1.ne(true, 40, 48, -50, -10, -50);
			xeb = afb.rd(0, 0, 1536, 1536, 8, 64, 233, false);
			for (int l6 = 0; l6 < 64; l6++)
				xdb.oh(xeb[l6]);

			for (int k9 = 0; k9 < 96; k9++) {
				for (int i12 = 0; i12 < 96; i12++)
					veb[k9][i12] = on(k9, i12);

			}

		}
		afb.re();
		int i2 = 0x606060;
		for (int k2 = 0; k2 < 95; k2++) {
			for (int i3 = 0; i3 < 95; i3++) {
				int i4 = kn(k2, i3);
				if (i4 > 0 && (Definitions.vib[i4 - 1] == 0 || vdb)) {
					_mthdo(afb, i4 - 1, k2, i3, k2 + 1, i3);
					if (flag && Definitions.tib[i4 - 1] != 0) {
						ueb[k2][i3] |= 1;
						if (i3 > 0)
							ao(k2, i3 - 1, 4);
					}
					if (flag)
						wdb.kg(k2 * 3, i3 * 3, 3, i2);
				}
				i4 = sn(k2, i3);
				if (i4 > 0 && (Definitions.vib[i4 - 1] == 0 || vdb)) {
					_mthdo(afb, i4 - 1, k2, i3, k2, i3 + 1);
					if (flag && Definitions.tib[i4 - 1] != 0) {
						ueb[k2][i3] |= 2;
						if (k2 > 0)
							ao(k2 - 1, i3, 8);
					}
					if (flag)
						wdb.og(k2 * 3, i3 * 3, 3, i2);
				}
				i4 = gn(k2, i3);
				if (i4 > 0 && i4 < 12000 && (Definitions.vib[i4 - 1] == 0 || vdb)) {
					_mthdo(afb, i4 - 1, k2, i3, k2 + 1, i3 + 1);
					if (flag && Definitions.tib[i4 - 1] != 0)
						ueb[k2][i3] |= 0x20;
					if (flag) {
						wdb.gg(k2 * 3, i3 * 3, i2);
						wdb.gg(k2 * 3 + 1, i3 * 3 + 1, i2);
						wdb.gg(k2 * 3 + 2, i3 * 3 + 2, i2);
					}
				}
				if (i4 > 12000 && i4 < 24000 && (Definitions.vib[i4 - 12001] == 0 || vdb)) {
					_mthdo(afb, i4 - 12001, k2 + 1, i3, k2, i3 + 1);
					if (flag && Definitions.tib[i4 - 12001] != 0)
						ueb[k2][i3] |= 0x10;
					if (flag) {
						wdb.gg(k2 * 3 + 2, i3 * 3, i2);
						wdb.gg(k2 * 3 + 1, i3 * 3 + 1, i2);
						wdb.gg(k2 * 3, i3 * 3 + 2, i2);
					}
				}
			}

		}

		if (flag)
			wdb.jf(ydb - 1, 0, 0, 285, 285);
		afb.ne(false, 60, 24, -50, -10, -50);
		yeb[i1] = afb.rd(0, 0, 1536, 1536, 8, 64, 338, true);
		for (int j3 = 0; j3 < 64; j3++)
			xdb.oh(yeb[i1][j3]);

		for (int j4 = 0; j4 < 95; j4++) {
			for (int j5 = 0; j5 < 95; j5++) {
				int i7 = kn(j4, j5);
				if (i7 > 0)
					un(i7 - 1, j4, j5, j4 + 1, j5);
				i7 = sn(j4, j5);
				if (i7 > 0)
					un(i7 - 1, j4, j5, j4, j5 + 1);
				i7 = gn(j4, j5);
				if (i7 > 0 && i7 < 12000)
					un(i7 - 1, j4, j5, j4 + 1, j5 + 1);
				if (i7 > 12000 && i7 < 24000)
					un(i7 - 12001, j4 + 1, j5, j4, j5 + 1);
			}

		}

		for (int k5 = 1; k5 < 95; k5++) {
			for (int j7 = 1; j7 < 95; j7++) {
				int l9 = vn(k5, j7);
				if (l9 > 0) {
					int j12 = k5;
					int k14 = j7;
					int l16 = k5 + 1;
					int i19 = j7;
					int l19 = k5 + 1;
					int l21 = j7 + 1;
					int j23 = k5;
					int l23 = j7 + 1;
					int j24 = 0;
					int l24 = veb[j12][k14];
					int j25 = veb[l16][i19];
					int l25 = veb[l19][l21];
					int j26 = veb[j23][l23];
					if (l24 > 0x13880)
						l24 -= 0x13880;
					if (j25 > 0x13880)
						j25 -= 0x13880;
					if (l25 > 0x13880)
						l25 -= 0x13880;
					if (j26 > 0x13880)
						j26 -= 0x13880;
					if (l24 > j24)
						j24 = l24;
					if (j25 > j24)
						j24 = j25;
					if (l25 > j24)
						j24 = l25;
					if (j26 > j24)
						j24 = j26;
					if (j24 >= 0x13880)
						j24 -= 0x13880;
					if (l24 < 0x13880)
						veb[j12][k14] = j24;
					else
						veb[j12][k14] -= 0x13880;
					if (j25 < 0x13880)
						veb[l16][i19] = j24;
					else
						veb[l16][i19] -= 0x13880;
					if (l25 < 0x13880)
						veb[l19][l21] = j24;
					else
						veb[l19][l21] -= 0x13880;
					if (j26 < 0x13880)
						veb[j23][l23] = j24;
					else
						veb[j23][l23] -= 0x13880;
				}
			}

		}

		afb.re();
		for (int k7 = 1; k7 < 95; k7++) {
			for (int i10 = 1; i10 < 95; i10++) {
				int k12 = vn(k7, i10);
				if (k12 > 0) {
					int l14 = k7;
					int i17 = i10;
					int j19 = k7 + 1;
					int i20 = i10;
					int i22 = k7 + 1;
					int k23 = i10 + 1;
					int i24 = k7;
					int k24 = i10 + 1;
					int i25 = k7 * 128;
					int k25 = i10 * 128;
					int i26 = i25 + 128;
					int k26 = k25 + 128;
					int l26 = i25;
					int i27 = k25;
					int j27 = i26;
					int k27 = k26;
					int l27 = veb[l14][i17];
					int i28 = veb[j19][i20];
					int j28 = veb[i22][k23];
					int k28 = veb[i24][k24];
					int l28 = Definitions.yib[k12 - 1];
					if (xn(l14, i17) && l27 < 0x13880) {
						l27 += l28 + 0x13880;
						veb[l14][i17] = l27;
					}
					if (xn(j19, i20) && i28 < 0x13880) {
						i28 += l28 + 0x13880;
						veb[j19][i20] = i28;
					}
					if (xn(i22, k23) && j28 < 0x13880) {
						j28 += l28 + 0x13880;
						veb[i22][k23] = j28;
					}
					if (xn(i24, k24) && k28 < 0x13880) {
						k28 += l28 + 0x13880;
						veb[i24][k24] = k28;
					}
					if (l27 >= 0x13880)
						l27 -= 0x13880;
					if (i28 >= 0x13880)
						i28 -= 0x13880;
					if (j28 >= 0x13880)
						j28 -= 0x13880;
					if (k28 >= 0x13880)
						k28 -= 0x13880;
					byte byte0 = 16;
					if (!yn(l14 - 1, i17))
						i25 -= byte0;
					if (!yn(l14 + 1, i17))
						i25 += byte0;
					if (!yn(l14, i17 - 1))
						k25 -= byte0;
					if (!yn(l14, i17 + 1))
						k25 += byte0;
					if (!yn(j19 - 1, i20))
						i26 -= byte0;
					if (!yn(j19 + 1, i20))
						i26 += byte0;
					if (!yn(j19, i20 - 1))
						i27 -= byte0;
					if (!yn(j19, i20 + 1))
						i27 += byte0;
					if (!yn(i22 - 1, k23))
						j27 -= byte0;
					if (!yn(i22 + 1, k23))
						j27 += byte0;
					if (!yn(i22, k23 - 1))
						k26 -= byte0;
					if (!yn(i22, k23 + 1))
						k26 += byte0;
					if (!yn(i24 - 1, k24))
						l26 -= byte0;
					if (!yn(i24 + 1, k24))
						l26 += byte0;
					if (!yn(i24, k24 - 1))
						k27 -= byte0;
					if (!yn(i24, k24 + 1))
						k27 += byte0;
					k12 = Definitions.ajb[k12 - 1];
					l27 = -l27;
					i28 = -i28;
					j28 = -j28;
					k28 = -k28;
					if (gn(k7, i10) > 12000 && gn(k7, i10) < 24000 && vn(k7 - 1, i10 - 1) == 0) {
						int ai8[] = new int[3];
						ai8[0] = afb.je(j27, j28, k26);
						ai8[1] = afb.je(l26, k28, k27);
						ai8[2] = afb.je(i26, i28, i27);
						afb.ie(3, ai8, k12, 0xbc614e);
					} else if (gn(k7, i10) > 12000 && gn(k7, i10) < 24000 && vn(k7 + 1, i10 + 1) == 0) {
						int ai9[] = new int[3];
						ai9[0] = afb.je(i25, l27, k25);
						ai9[1] = afb.je(i26, i28, i27);
						ai9[2] = afb.je(l26, k28, k27);
						afb.ie(3, ai9, k12, 0xbc614e);
					} else if (gn(k7, i10) > 0 && gn(k7, i10) < 12000 && vn(k7 + 1, i10 - 1) == 0) {
						int ai10[] = new int[3];
						ai10[0] = afb.je(l26, k28, k27);
						ai10[1] = afb.je(i25, l27, k25);
						ai10[2] = afb.je(j27, j28, k26);
						afb.ie(3, ai10, k12, 0xbc614e);
					} else if (gn(k7, i10) > 0 && gn(k7, i10) < 12000 && vn(k7 - 1, i10 + 1) == 0) {
						int ai11[] = new int[3];
						ai11[0] = afb.je(i26, i28, i27);
						ai11[1] = afb.je(j27, j28, k26);
						ai11[2] = afb.je(i25, l27, k25);
						afb.ie(3, ai11, k12, 0xbc614e);
					} else if (l27 == i28 && j28 == k28) {
						int ai12[] = new int[4];
						ai12[0] = afb.je(i25, l27, k25);
						ai12[1] = afb.je(i26, i28, i27);
						ai12[2] = afb.je(j27, j28, k26);
						ai12[3] = afb.je(l26, k28, k27);
						afb.ie(4, ai12, k12, 0xbc614e);
					} else if (l27 == k28 && i28 == j28) {
						int ai13[] = new int[4];
						ai13[0] = afb.je(l26, k28, k27);
						ai13[1] = afb.je(i25, l27, k25);
						ai13[2] = afb.je(i26, i28, i27);
						ai13[3] = afb.je(j27, j28, k26);
						afb.ie(4, ai13, k12, 0xbc614e);
					} else {
						boolean flag1 = true;
						if (vn(k7 - 1, i10 - 1) > 0)
							flag1 = false;
						if (vn(k7 + 1, i10 + 1) > 0)
							flag1 = false;
						if (!flag1) {
							int ai14[] = new int[3];
							ai14[0] = afb.je(i26, i28, i27);
							ai14[1] = afb.je(j27, j28, k26);
							ai14[2] = afb.je(i25, l27, k25);
							afb.ie(3, ai14, k12, 0xbc614e);
							int ai16[] = new int[3];
							ai16[0] = afb.je(l26, k28, k27);
							ai16[1] = afb.je(i25, l27, k25);
							ai16[2] = afb.je(j27, j28, k26);
							afb.ie(3, ai16, k12, 0xbc614e);
						} else {
							int ai15[] = new int[3];
							ai15[0] = afb.je(i25, l27, k25);
							ai15[1] = afb.je(i26, i28, i27);
							ai15[2] = afb.je(l26, k28, k27);
							afb.ie(3, ai15, k12, 0xbc614e);
							int ai17[] = new int[3];
							ai17[0] = afb.je(j27, j28, k26);
							ai17[1] = afb.je(l26, k28, k27);
							ai17[2] = afb.je(i26, i28, i27);
							afb.ie(3, ai17, k12, 0xbc614e);
						}
					}
				}
			}

		}

		afb.ne(true, 50, 50, -50, -10, -50);
		zeb[i1] = afb.rd(0, 0, 1536, 1536, 8, 64, 169, true);
		for (int j10 = 0; j10 < 64; j10++)
			xdb.oh(zeb[i1][j10]);

		for (int l12 = 0; l12 < 96; l12++) {
			for (int i15 = 0; i15 < 96; i15++)
				if (veb[l12][i15] >= 0x13880)
					veb[l12][i15] -= 0x13880;

		}

	}

	public void _mthdo(h h1, int k, int l, int i1, int j1, int k1) {
		nn(l, i1, 40);
		nn(j1, k1, 40);
		int l1 = Definitions.qib[k];
		int i2 = Definitions.rib[k];
		int j2 = Definitions.sib[k];
		int k2 = l * 128;
		int l2 = i1 * 128;
		int i3 = j1 * 128;
		int j3 = k1 * 128;
		int k3 = h1.je(k2, -veb[l][i1], l2);
		int l3 = h1.je(k2, -veb[l][i1] - l1, l2);
		int i4 = h1.je(i3, -veb[j1][k1] - l1, j3);
		int j4 = h1.je(i3, -veb[j1][k1], j3);
		int ai[] = { k3, l3, i4, j4 };
		int k4 = h1.ie(4, ai, i2, j2);
		if (Definitions.vib[k] == 5) {
			h1.qh[k4] = 30000 + k;
			return;
		} else {
			h1.qh[k4] = 0;
			return;
		}
	}

	public void un(int k, int l, int i1, int j1, int k1) {
		int l1 = Definitions.qib[k];
		if (veb[l][i1] < 0x13880)
			veb[l][i1] += 0x13880 + l1;
		if (veb[j1][k1] < 0x13880)
			veb[j1][k1] += 0x13880 + l1;
	}

	boolean udb;
	boolean vdb;
	GameImageProducer wdb;
	j xdb;
	int ydb;
	final int zdb = 0xbc614e;
	final int aeb = 128;
	int beb[];
	int ceb;
	int deb[];
	int eeb[];
	int feb[];
	byte geb[];
	byte heb[][];
	byte ieb[][];
	byte jeb[][];
	byte keb[][];
	byte leb[][];
	byte meb[][];
	byte neb[][];
	int oeb[][];
	int peb;
	int qeb;
	int reb[];
	int seb[];
	int teb[][];
	int ueb[][];
	int veb[][];
	boolean web;
	h xeb[];
	h yeb[][];
	h zeb[][];
	h afb;
}
