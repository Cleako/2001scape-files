public class Versions {

	public Versions() {
	}

	public static int ClientRevision = 39;
	public static int ConfigsVersion = 18;
	public static int MapsVersion = 14;
	public static int MediaVersion = 12;
	public static int ModelsVersion = 6;
	public static int TexturesVersion = 5;
	public static int EntitiesVersion = 3;

}
