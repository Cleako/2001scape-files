import jagex.Stream;
import jagex.o;

import java.io.IOException;

public class Definitions {

    public static void uo() {
        try {
            unpackProjectiles(new Stream("../gamedata/config/projectile.txt"));
            unpackEntities(new Stream("../gamedata/config/entity.txt"));
            unpackObjects(new Stream("../gamedata/config/objects.txt"));
            unpackNpcs(new Stream("../gamedata/config/npc.txt"));
            unpackLocations(new Stream("../gamedata/config/location.txt"));
            unpackBoundaries(new Stream("../gamedata/config/boundary.txt"));
            unpackRoofs(new Stream("../gamedata/config/roof.txt"));
            unpackFloors(new Stream("../gamedata/config/floor.txt"));
            unpackSkills(new Stream("../gamedata/config/spells.txt"));
            unpackShops(new Stream("../gamedata/config/shop.txt"));
            vo();
        } catch (IOException ioexception) {
            System.out.println("Error loading config files");
            ioexception.printStackTrace();
        }
    }

    public static void oo(byte[] abyte0) {
        try {
            unpackProjectiles(new Stream(abyte0, o.mm("projectile.txt", abyte0)));
            unpackEntities(new Stream(abyte0, o.mm("entity.txt", abyte0)));
            unpackObjects(new Stream(abyte0, o.mm("objects.txt", abyte0)));
            unpackNpcs(new Stream(abyte0, o.mm("npc.txt", abyte0)));
            unpackLocations(new Stream(abyte0, o.mm("location.txt", abyte0)));
            unpackBoundaries(new Stream(abyte0, o.mm("boundary.txt", abyte0)));
            unpackRoofs(new Stream(abyte0, o.mm("roof.txt", abyte0)));
            unpackFloors(new Stream(abyte0, o.mm("floor.txt", abyte0)));
            unpackSkills(new Stream(abyte0, o.mm("spells.txt", abyte0)));
            unpackShops(new Stream(abyte0, o.mm("shop.txt", abyte0)));
            vo();
        } catch (IOException ioexception) {
            System.out.println("Error loading config files");
            ioexception.printStackTrace();
        }
    }

    public static void unpackShops(Stream f1) throws IOException {
        f1.qb();
        int i = f1.vb();
        zjb = i;
        System.out.println("Found " + i + " shops");
        akb = new String[i];
        bkb = new int[i];
        fkb = new int[i];
        ckb = new int[i];
        dkb = new int[i];
        ekb = new int[i];
        gkb = new int[i][40];
        hkb = new int[i][40];
        ikb = new int[i][40];
        for (int j = 0; j < i; j++) {
            f1.qb();
            akb[j] = f1.wb();
            int k = bkb[j] = f1.vb();
            ckb[j] = f1.vb();
            dkb[j] = f1.vb();
            ekb[j] = f1.vb();
            fkb[j] = f1.vb();
            for (int l = 0; l < k; l++) {
                f1.qb();
                gkb[j][l] = go(f1.wb());
                hkb[j][l] = f1.vb();
                ikb[j][l] = f1.vb();
            }

        }

    }

    public static void unpackSkills(Stream f1) throws IOException {
        f1.qb();
        int i = f1.vb();
        qjb = i;
        System.out.println("Found " + i + " skills");
        rjb = new String[i];
        sjb = new String[i];
        spellLevel = new int[i];
        vjb = new int[i];
        wjb = new int[i];
        ujb = new int[i];
        xjb = new int[i][];
        yjb = new int[i][];
        for (int j = 0; j < i; j++) {
            f1.qb();
            rjb[j] = f1.wb();
            spellLevel[j] = f1.vb();
            sjb[j] = f1.wb();
            vjb[j] = f1.vb();
            wjb[j] = f1.vb();
            f1.qb();
            int k = ujb[j] = f1.vb();
            xjb[j] = new int[k];
            yjb[j] = new int[k];
            for (int l = 0; l < k; l++) {
                xjb[j][l] = go(f1.wb());
                yjb[j][l] = f1.vb();
            }

        }

        f1.xb();
    }

    public static void unpackProjectiles(Stream f1) throws IOException {
        f1.qb();
        int i = f1.vb();
        gjb = i;
        System.out.println("Found " + i + " projectiles");
        ijb = new String[i];
        jjb = new String[i];
        kjb = new int[i];
        ljb = new int[i];
        mjb = new int[i];
        njb = new int[i];
        ojb = new int[i];
        pjb = new int[i];
        for (int j = 0; j < i; j++) {
            f1.qb();
            ijb[j] = f1.wb();
            kjb[j] = f1.vb();
            ljb[j] = f1.vb();
            mjb[j] = f1.vb();
            njb[j] = f1.vb();
            jjb[j] = f1.wb();
            pjb[j] = f1.vb();
            if (kjb[j] + 1 > hjb)
                hjb = kjb[j] + 1;
        }

        f1.xb();
    }

    public static void vo() {
        for (int i = 0; i < gjb; i++)
            ojb[i] = go(jjb[i]);

    }

    public static void unpackEntities(Stream f1) throws IOException {
        f1.qb();
        int i = f1.vb();
        shb = i;
        System.out.println("Found " + i + " entities");
        thb = new String[i];
        uhb = new String[i];
        whb = new int[i];
        vhb = new int[i];
        xhb = new int[i];
        yhb = new int[i];
        zhb = new int[i];
        for (int j = 0; j < i; j++) {
            f1.qb();
            thb[j] = f1.wb();
            uhb[j] = f1.wb();
            vhb[j] = f1.rb();
            whb[j] = f1.vb();
            xhb[j] = f1.vb();
            yhb[j] = f1.vb();
        }

        f1.xb();
    }

    public static void unpackNpcs(Stream f1) throws IOException {
        f1.qb();
        int i = f1.vb();
        ogb = i;
        System.out.println("Found " + i + " npcs");
        npcName = new String[i][];
        qgb = new String[i];
        rgb = new int[i];
        sgb = new int[i];
        tgb = new int[i];
        ugb = new int[i];
        vgb = new int[i];
        wgb = new int[i];
        xgb = new int[i];
        ygb = new int[i];
        zgb = new int[i];
        ahb = new int[i];
        bhb = new int[i];
        chb = new int[i];
        dhb = new int[i];
        ehb = new int[i];
        fhb = new int[i];
        ghb = new int[i][12];
        hhb = new int[i];
        ihb = new int[i];
        jhb = new int[i];
        khb = new int[i];
        lhb = new int[i];
        mhb = new int[i];
        nhb = new int[i];
        ohb = new int[i];
        phb = new int[i];
        qhb = new int[i][];
        rhb = new int[i][];
        for (int j = 0; j < i; j++) {
            f1.qb();
            int k = f1.vb();
            npcName[j] = new String[k];
            for (int l = 0; l < k; l++)
                npcName[j][l] = f1.wb();

            qgb[j] = f1.wb();
            f1.qb();
            rgb[j] = f1.vb();
            sgb[j] = f1.vb();
            tgb[j] = f1.vb();
            ugb[j] = f1.vb();
            vgb[j] = f1.vb();
            wgb[j] = f1.vb();
            xgb[j] = f1.vb();
            ygb[j] = f1.vb();
            zgb[j] = f1.vb();
            ahb[j] = f1.vb();
            bhb[j] = f1.vb();
            chb[j] = f1.vb();
            dhb[j] = f1.vb();
            ehb[j] = f1.vb();
            fhb[j] = f1.vb();
            f1.qb();
            for (int i1 = 0; i1 < 12; i1++)
                ghb[j][i1] = no(f1.wb());

            hhb[j] = f1.rb();
            ihb[j] = f1.rb();
            jhb[j] = f1.rb();
            khb[j] = f1.rb();
            f1.qb();
            lhb[j] = f1.vb();
            mhb[j] = f1.vb();
            nhb[j] = f1.vb();
            ohb[j] = f1.vb();
            phb[j] = f1.vb();
            f1.qb();
            int j1 = f1.vb();
            qhb[j] = new int[j1];
            rhb[j] = new int[j1];
            for (int k1 = 0; k1 < j1; k1++) {
                qhb[j][k1] = go(f1.wb());
                rhb[j][k1] = f1.vb();
            }

        }

        f1.xb();
    }

    public static void unpackObjects(Stream f1) throws IOException {
        f1.qb();
        int i = f1.vb();
        tfb = i;
        System.out.println("Found " + i + " objects");
        itemName = new String[i][];
        wfb = new String[i];
        yfb = new String[i];
        zfb = new int[i];
        agb = new int[i];
        bgb = new int[i];
        cgb = new int[i];
        dgb = new int[i];
        egb = new int[i];
        fgb = new int[i];
        ggb = new int[i];
        hgb = new int[i];
        igb = new int[i];
        jgb = new int[i];
        kgb = new int[i];
        lgb = new int[i];
        mgb = new int[i];
        ngb = new int[i];
        for (int j = 0; j < i; j++) {
            f1.qb();
            int k = f1.vb();
            itemName[j] = new String[k];
            for (int l = 0; l < k; l++)
                itemName[j][l] = f1.wb();

            wfb[j] = f1.wb();
            f1.qb();
            zfb[j] = f1.vb();
            if (zfb[j] >= ufb)
                ufb = zfb[j] + 1;
            agb[j] = f1.vb();
            bgb[j] = f1.vb();
            yfb[j] = f1.wb();
            f1.qb();
            cgb[j] = f1.vb();
            dgb[j] = f1.vb();
            egb[j] = f1.vb();
            fgb[j] = f1.vb();
            ggb[j] = f1.vb();
            hgb[j] = f1.vb();
            igb[j] = jo(f1.wb()) + 1;
            f1.qb();
            jgb[j] = f1.vb();
            kgb[j] = no(f1.wb());
            lgb[j] = f1.rb();
            mgb[j] = f1.vb();
            ngb[j] = f1.vb();
        }

        f1.xb();
    }

    public static void unpackLocations(Stream f1) throws IOException {
        f1.qb();
        int i = f1.vb();
        aib = i;
        System.out.println("Found " + i + " locations");
        bib = new String[i][];
        cib = new String[i];
        dib = new String[i];
        eib = new String[i];
        fib = new int[i];
        gib = new int[i];
        hib = new int[i];
        iib = new int[i];
        jib = new int[i];
        kib = new int[i];
        for (int j = 0; j < i; j++) {
            f1.qb();
            int k = f1.vb();
            bib[j] = new String[k];
            for (int l = 0; l < k; l++)
                bib[j][l] = f1.wb();

            cib[j] = f1.wb();
            f1.qb();
            fib[j] = mo(f1.wb());
            gib[j] = f1.vb();
            hib[j] = f1.vb();
            iib[j] = f1.vb();
            jib[j] = f1.vb();
            dib[j] = f1.wb();
            if (dib[j].equals("_"))
                dib[j] = "WalkTo";
            eib[j] = f1.wb();
            if (eib[j].equals("_"))
                eib[j] = "Examine";
            kib[j] = f1.vb();
        }

        f1.xb();
    }

    public static void unpackBoundaries(Stream f1) throws IOException {
        f1.qb();
        int i = f1.vb();
        lib = i;
        System.out.println("Found " + i + " boundaries");
        mib = new String[i][];
        nib = new String[i];
        oib = new String[i];
        pib = new String[i];
        qib = new int[i];
        rib = new int[i];
        sib = new int[i];
        tib = new int[i];
        uib = new int[i];
        vib = new int[i];
        for (int j = 0; j < i; j++) {
            f1.qb();
            int k = f1.vb();
            mib[j] = new String[k];
            for (int l = 0; l < k; l++)
                mib[j][l] = f1.wb();

            nib[j] = f1.wb();
            f1.qb();
            qib[j] = f1.vb();
            rib[j] = f1.vb();
            sib[j] = f1.vb();
            tib[j] = f1.vb();
            uib[j] = f1.vb();
            vib[j] = f1.vb();
            oib[j] = f1.wb();
            if (oib[j].equals("_"))
                oib[j] = "WalkTo";
            pib[j] = f1.wb();
            if (pib[j].equals("_"))
                pib[j] = "Examine";
        }

        f1.xb();
    }

    public static void unpackRoofs(Stream f1) throws IOException {
        f1.qb();
        int i = f1.vb();
        wib = i;
        System.out.println("Found " + i + " roofs");
        xib = new String[i];
        yib = new int[i];
        zib = new int[i];
        ajb = new int[i];
        for (int j = 0; j < i; j++) {
            f1.qb();
            xib[j] = f1.wb();
            yib[j] = f1.vb();
            zib[j] = f1.vb();
            ajb[j] = f1.vb();
        }

        f1.xb();
    }

    public static void unpackFloors(Stream f1) throws IOException {
        f1.qb();
        int i = f1.vb();
        bjb = i;
        System.out.println("Found " + i + " floors");
        cjb = new String[i];
        djb = new int[i];
        ejb = new int[i];
        fjb = new int[i];
        for (int j = 0; j < i; j++) {
            f1.qb();
            cjb[j] = f1.wb();
            djb[j] = f1.vb();
            ejb[j] = f1.vb();
            fjb[j] = f1.vb();
        }

        f1.xb();
    }

    public static int no(String s) {
        if (s.equalsIgnoreCase("na"))
            return -1;
        for (int i = 0; i < shb; i++)
            if (s.equalsIgnoreCase(thb[i]))
                return i;

        System.out.println("WARNING: unable to match entity " + s);
        return 0;
    }

    public static int mo(String s) {
        if (s.equalsIgnoreCase("na"))
            return 0;
        for (int i = 0; i < lkb; i++)
            if (mkb[i].equalsIgnoreCase(s))
                return i;

        mkb[lkb++] = s;
        return lkb - 1;
    }

    public static int go(String s) {
        if (s.equalsIgnoreCase("na"))
            return 0;
        for (int i = 0; i < tfb; i++) {
            for (int j = 0; j < itemName[i].length; j++)
                if (itemName[i][j].equalsIgnoreCase(s))
                    return i;

        }

        System.out.println("WARNING: unable to match object: " + s);
        return 0;
    }

    public static int jo(String s) {
        if (s.equals("_"))
            return -1;
        for (int i = 0; i < gjb; i++)
            if (ijb[i].equalsIgnoreCase(s))
                return i;

        System.out.println("WARNING: unable to match projectile: " + s);
        return -1;
    }

    public Definitions() {
    }

    public final int sfb = 0xbc614e;
    public static int tfb;
    public static int ufb;
    public static String[][] itemName;
    public static String[] wfb;
    public static String[] xfb;
    public static String[] yfb;
    public static int[] zfb;
    public static int[] agb;
    public static int[] bgb;
    public static int[] cgb;
    public static int[] dgb;
    public static int[] egb;
    public static int[] fgb;
    public static int[] ggb;
    public static int[] hgb;
    public static int[] igb;
    public static int[] jgb;
    public static int[] kgb;
    public static int[] lgb;
    public static int[] mgb;
    public static int[] ngb;
    public static int ogb;
    public static String[][] npcName;
    public static String[] qgb;
    public static int[] rgb;
    public static int[] sgb;
    public static int[] tgb;
    public static int[] ugb;
    public static int[] vgb;
    public static int[] wgb;
    public static int[] xgb;
    public static int[] ygb;
    public static int[] zgb;
    public static int[] ahb;
    public static int[] bhb;
    public static int[] chb;
    public static int[] dhb;
    public static int[] ehb;
    public static int[] fhb;
    public static int[][] ghb;
    public static int[] hhb;
    public static int[] ihb;
    public static int[] jhb;
    public static int[] khb;
    public static int[] lhb;
    public static int[] mhb;
    public static int[] nhb;
    public static int[] ohb;
    public static int[] phb;
    public static int[][] qhb;
    public static int[][] rhb;
    public static int shb;
    public static String[] thb;
    public static String[] uhb;
    public static int[] vhb;
    public static int[] whb;
    public static int[] xhb;
    public static int[] yhb;
    public static int[] zhb;
    public static int aib;
    public static String[][] bib;
    public static String[] cib;
    public static String[] dib;
    public static String[] eib;
    public static int[] fib;
    public static int[] gib;
    public static int[] hib;
    public static int[] iib;
    public static int[] jib;
    public static int[] kib;
    public static int lib;
    public static String[][] mib;
    public static String[] nib;
    public static String[] oib;
    public static String[] pib;
    public static int[] qib;
    public static int[] rib;
    public static int[] sib;
    public static int[] tib;
    public static int[] uib;
    public static int[] vib;
    public static int wib;
    public static String[] xib;
    public static int[] yib;
    public static int[] zib;
    public static int[] ajb;
    public static int bjb;
    public static String[] cjb;
    public static int[] djb;
    public static int[] ejb;
    public static int[] fjb;
    public static int gjb;
    public static int hjb;
    public static String[] ijb;
    public static String[] jjb;
    public static int[] kjb;
    public static int[] ljb;
    public static int[] mjb;
    public static int[] njb;
    public static int[] ojb;
    public static int[] pjb;
    public static int qjb;
    public static String[] rjb;
    public static String[] sjb;
    public static int[] spellLevel;
    public static int[] ujb;
    public static int[] vjb;
    public static int[] wjb;
    public static int[][] xjb;
    public static int[][] yjb;
    public static int zjb;
    public static String[] akb;
    public static int[] bkb;
    public static int[] ckb;
    public static int[] dkb;
    public static int[] ekb;
    public static int[] fkb;
    public static int[][] gkb;
    public static int[][] hkb;
    public static int[][] ikb;
    public static int lkb;
    public static String[] mkb = new String[200];

}
