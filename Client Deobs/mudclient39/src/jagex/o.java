package jagex;

import java.io.*;
import java.net.URL;

public class o {

	public static InputStream km(String s) throws IOException {
		InputStream obj;
		if (kdb == null) {
			obj = new FileInputStream(s);
		} else {
			URL url = new URL(kdb, s);
			obj = url.openStream();
		}
		return obj;
	}

	public static void jm(String s, byte[] abyte0, int i) throws IOException {
		InputStream inputstream = km(s);
		DataInputStream datainputstream = new DataInputStream(inputstream);
		try {
			datainputstream.readFully(abyte0, 0, i);
		} catch (EOFException ignored) {
		}
		datainputstream.close();
	}

	public static int sm(byte byte0) {
		return byte0 & 0xff;
	}

	public static int vm(byte[] abyte0, int i) {
		return ((abyte0[i] & 0xff) << 8) + (abyte0[i + 1] & 0xff);
	}

	public static int um(byte[] abyte0, int i) {
		return ((abyte0[i] & 0xff) << 24) + ((abyte0[i + 1] & 0xff) << 16) + ((abyte0[i + 2] & 0xff) << 8)
				+ (abyte0[i + 3] & 0xff);
	}

	public static long qm(byte[] abyte0, int i) {
		return (((long) um(abyte0, i) & 0xffffffffL) << 32) + ((long) um(abyte0, i + 4) & 0xffffffffL);
	}

	public static int wm(byte[] abyte0, int i) {
		int j = sm(abyte0[i]) * 256 + sm(abyte0[i + 1]);
		if (j > 32767)
			j -= 0x10000;
		return j;
	}

	public static String xm(String s, int i) {
		StringBuilder s1 = new StringBuilder();
		for (int j = 0; j < i; j++)
			if (j >= s.length()) {
				s1.append(" ");
			} else {
				char c = s.charAt(j);
				if (c >= 'a' && c <= 'z')
					s1.append(c);
				else if (c >= 'A' && c <= 'Z')
					s1.append(c);
				else if (c >= '0' && c <= '9')
					s1.append(c);
				else
					s1.append('_');
			}

		return s1.toString();
	}

	public static long rm(String s) {
		StringBuilder s1 = new StringBuilder();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c >= 'a' && c <= 'z')
				s1.append(c);
			else if (c >= 'A' && c <= 'Z')
				s1.append((char) ((c + 97) - 65));
			else if (c >= '0' && c <= '9')
				s1.append(c);
			else
				s1.append(' ');
		}

		s1 = new StringBuilder(s1.toString().trim());
		if (s1.length() > 12)
			s1 = new StringBuilder(s1.substring(0, 12));
		long l = 0L;
		for (int j = 0; j < s1.length(); j++) {
			char c1 = s1.charAt(j);
			l *= 37L;
			if (c1 >= 'a' && c1 <= 'z')
				l += (1 + c1) - 97;
			else if (c1 >= '0' && c1 <= '9')
				l += (27 + c1) - 48;
		}

		return l;
	}

	public static String tm(long l) {
		StringBuilder s = new StringBuilder();
		while (l != 0L) {
			int i = (int) (l % 37L);
			l /= 37L;
			if (i == 0)
				s.insert(0, " ");
			else if (i < 27) {
				if (l % 37L == 0L)
					s.insert(0, (char) ((i + 65) - 1));
				else
					s.insert(0, (char) ((i + 97) - 1));
			} else {
				s.insert(0, (char) ((i + 48) - 27));
			}
		}
		return s.toString();
	}

	public static byte[] pm(String s) throws IOException {
		int i = 0;
		int j = 0;
		int k = 0;
		byte[] abyte0 = null;
		while (i < 2)
			try {
				if (i == 1)
					s = s.toUpperCase();
				InputStream inputstream = km(s);
				DataInputStream datainputstream = new DataInputStream(inputstream);
				byte[] abyte2 = new byte[6];
				datainputstream.readFully(abyte2, 0, 6);
				j = ((abyte2[0] & 0xff) << 16) + ((abyte2[1] & 0xff) << 8) + (abyte2[2] & 0xff);
				k = ((abyte2[3] & 0xff) << 16) + ((abyte2[4] & 0xff) << 8) + (abyte2[5] & 0xff);
				int l = 0;
				abyte0 = new byte[k];
				int i1;
				for (; l < k; l += i1) {
					i1 = k - l;
					if (i1 > 1000)
						i1 = 1000;
					datainputstream.readFully(abyte0, l, i1);
				}

				i = 2;
				datainputstream.close();
			} catch (IOException _ex) {
				i++;
			}
		if (k != j) {
			byte[] abyte1 = new byte[j];
			n.uj(abyte1, j, abyte0, k, 0);
			return abyte1;
		} else {
			return abyte0;
		}
	}

	public static int mm(String s, byte[] abyte0) {
		int i = abyte0[0] * 256 + abyte0[1];
		int j = 0;
		s = s.toUpperCase();
		for (int k = 0; k < s.length(); k++)
			j = (j * 61 + s.charAt(k)) - 32;

		int l = 2 + i * 10;
		for (int i1 = 0; i1 < i; i1++) {
			int j1 = (abyte0[i1 * 10 + 2] & 0xff) * 0x1000000 + (abyte0[i1 * 10 + 3] & 0xff) * 0x10000
					+ (abyte0[i1 * 10 + 4] & 0xff) * 256 + (abyte0[i1 * 10 + 5] & 0xff);
			int k1 = (abyte0[i1 * 10 + 9] & 0xff) * 0x10000 + (abyte0[i1 * 10 + 10] & 0xff) * 256
					+ (abyte0[i1 * 10 + 11] & 0xff);
			if (j1 == j)
				return l;
			l += k1;
		}

		return 0;
	}

	public static byte[] ym(String s, int i, byte[] abyte0, byte[] abyte1) {
		int j = abyte0[0] * 256 + abyte0[1];
		int k = 0;
		s = s.toUpperCase();
		for (int l = 0; l < s.length(); l++)
			k = (k * 61 + s.charAt(l)) - 32;

		int i1 = 2 + j * 10;
		for (int j1 = 0; j1 < j; j1++) {
			int k1 = (abyte0[j1 * 10 + 2] & 0xff) * 0x1000000 + (abyte0[j1 * 10 + 3] & 0xff) * 0x10000
					+ (abyte0[j1 * 10 + 4] & 0xff) * 256 + (abyte0[j1 * 10 + 5] & 0xff);
			int l1 = (abyte0[j1 * 10 + 6] & 0xff) * 0x10000 + (abyte0[j1 * 10 + 7] & 0xff) * 256
					+ (abyte0[j1 * 10 + 8] & 0xff);
			int i2 = (abyte0[j1 * 10 + 9] & 0xff) * 0x10000 + (abyte0[j1 * 10 + 10] & 0xff) * 256
					+ (abyte0[j1 * 10 + 11] & 0xff);
			if (k1 == k) {
				if (l1 != i2) {
					n.uj(abyte1, l1, abyte0, i2, i1);
				} else {
					if (l1 >= 0) System.arraycopy(abyte0, i1, abyte1, 0, l1);

				}
				return abyte1;
			}
			i1 += i2;
		}

		return null;
	}

	public static byte[] lm(String s, int i, byte[] abyte0) {
		int j = abyte0[0] * 256 + abyte0[1];
		int k = 0;
		s = s.toUpperCase();
		for (int l = 0; l < s.length(); l++)
			k = (k * 61 + s.charAt(l)) - 32;

		int i1 = 2 + j * 10;
		for (int j1 = 0; j1 < j; j1++) {
			int k1 = (abyte0[j1 * 10 + 2] & 0xff) * 0x1000000 + (abyte0[j1 * 10 + 3] & 0xff) * 0x10000
					+ (abyte0[j1 * 10 + 4] & 0xff) * 256 + (abyte0[j1 * 10 + 5] & 0xff);
			int l1 = (abyte0[j1 * 10 + 6] & 0xff) * 0x10000 + (abyte0[j1 * 10 + 7] & 0xff) * 256
					+ (abyte0[j1 * 10 + 8] & 0xff);
			int i2 = (abyte0[j1 * 10 + 9] & 0xff) * 0x10000 + (abyte0[j1 * 10 + 10] & 0xff) * 256
					+ (abyte0[j1 * 10 + 11] & 0xff);
			if (k1 == k) {
				byte[] abyte1 = new byte[l1 + i];
				if (l1 != i2) {
					n.uj(abyte1, l1, abyte0, i2, i1);
				} else {
					if (l1 >= 0) System.arraycopy(abyte0, i1, abyte1, 0, l1);

				}
				return abyte1;
			}
			i1 += i2;
		}

		return null;
	}

	public static String nm(String s, boolean flag) {
		for (int i = 0; i < 2; i++) {
			String s1 = s;
			odb = 0;
			int j = 0;
			for (int k = 0; k < s.length(); k++) {
				char c = s.charAt(k);
				if (c >= 'A' && c <= 'Z')
					c = (char) ((c + 97) - 65);
				if (flag && c == '@' && k + 4 < s.length() && s.charAt(k + 4) == '@') {
					k += 4;
				} else {
					byte byte0;
					if (c >= 'a' && c <= 'z' || c >= '0' && c <= '9')
						byte0 = 0;
					else if (c == '\'')
						byte0 = 1;
					else if (c == '\r' || c == ' ' || c == '.' || c == ',' || c == '-' || c == '(' || c == ')'
							|| c == '?' || c == '!')
						byte0 = 2;
					else
						byte0 = 3;
					int l = odb;
					for (int i1 = 0; i1 < l; i1++)
						if (byte0 == 3) {
							if (tdb[i1] > 0 && tdb[i1] < pdb[i1] + rdb[i1].length() / 2) {
								tdb[odb] = tdb[i1] + 1;
								sdb[odb] = sdb[i1];
								qdb[odb] = qdb[i1] + 1;
								pdb[odb] = pdb[i1];
								rdb[odb++] = rdb[i1];
								tdb[i1] = -tdb[i1];
							}
						} else {
							char c1 = rdb[i1].charAt(qdb[i1]);
							if (om(c, c1)) {
								qdb[i1]++;
								if (tdb[i1] < 0)
									tdb[i1] = -tdb[i1];
							} else if ((c == ' ' || c == '\r') && pdb[i1] == 0) {
								qdb[i1] = 0x1869f;
							} else {
								char c2 = rdb[i1].charAt(qdb[i1] - 1);
								if (byte0 == 0 && !om(c, c2))
									qdb[i1] = 0x1869f;
							}
						}

					if (byte0 >= 2)
						j = 1;
					if (byte0 <= 2) {
						for (String element : ldb)
							if (om(c, element.charAt(0))) {
								tdb[odb] = 1;
								sdb[odb] = k;
								qdb[odb] = 1;
								pdb[odb] = 1;
								rdb[odb++] = element;
							}

						for (String item : mdb)
							if (om(c, item.charAt(0))) {
								tdb[odb] = 1;
								sdb[odb] = k;
								qdb[odb] = 1;
								pdb[odb] = j;
								rdb[odb++] = item;
							}

						if (j == 1) {
							for (String value : ndb)
								if (om(c, value.charAt(0))) {
									tdb[odb] = 1;
									sdb[odb] = k;
									qdb[odb] = 1;
									pdb[odb] = 1;
									rdb[odb++] = value;
								}

						}
						if (byte0 == 0)
							j = 0;
					}
					for (int k1 = 0; k1 < odb; k1++)
						if (qdb[k1] >= rdb[k1].length()) {
							if (qdb[k1] < 0x1869f) {
								StringBuilder s2 = new StringBuilder();
								for (int k2 = 0; k2 < s.length(); k2++)
									if (k2 < sdb[k1] || k2 > k)
										s2.append(s.charAt(k2));
									else
										s2.append("*");

								s = s2.toString();
							}
							odb--;
							for (int i2 = k1; i2 < odb; i2++) {
								pdb[i2] = pdb[i2 + 1];
								qdb[i2] = qdb[i2 + 1];
								rdb[i2] = rdb[i2 + 1];
								sdb[i2] = sdb[i2 + 1];
								tdb[i2] = tdb[i2 + 1];
							}

							k1--;
						}

				}
			}

			if (s.equalsIgnoreCase(s1))
				break;
		}

		return s;
	}

	static boolean om(char c, char c1) {
		if (c == c1)
			return true;
		if (c1 == 'i' && (c == 'l' || c == '1' || c == '!' || c == '|' || c == ':' || c == '\246' || c == ';'))
			return true;
		if (c1 == 's' && (c == '5' || c == '$'))
			return true;
		if (c1 == 'a' && (c == '4' || c == '@'))
			return true;
		if (c1 == 'c' && (c == '(' || c == '<' || c == '['))
			return true;
		if (c1 == 'o' && c == '0')
			return true;
		return c1 == 'u' && c == 'v';
	}

	public o() {
	}

	public static URL kdb = null;
	static String[] ldb = { "fuck", "bastard", "lesbian", "prostitut", "spastic", "vagina", "retard", "arsehole",
			"asshole", "tosser", "homosex", "hetrosex", "hitler", "urinate" };
	static String[] mdb = { "shit", "lesbo", "phuck", "bitch", "penis", "whore", "bisex", "sperm", "rapist", "shag",
			"slag", "slut", "clit", "cunt", "piss", "nazi", "urine" };
	static String[] ndb = { "wank", "naked", "fag", "niga", "nige", "gay", "rape", "cock", "homo", "twat", "arse",
			"crap", "poo" };
	static int odb;
	static int[] pdb = new int[1000];
	static int[] qdb = new int[1000];
	static String[] rdb = new String[1000];
	static int[] sdb = new int[1000];
	static int[] tdb = new int[1000];

}
