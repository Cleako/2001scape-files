package jagex.client;

import jagex.Stream;
import java.applet.Applet;
import java.io.*;
import java.net.*;

public class GameSocket extends Stream implements Runnable {

	public GameSocket(InputStream inputstream) {
		super(inputstream);
		a = false;
		b = "error in twriter";
		g = true;
		h = 3;
	}

	public GameSocket(Socket socket) throws IOException {
		super(socket);
		a = false;
		b = "error in twriter";
		g = true;
		h = 3;
	}

	public GameSocket(String s) throws IOException {
		super(s);
		a = false;
		b = "error in twriter";
		g = true;
		h = 3;
	}

	public GameSocket(byte abyte0[]) {
		super(abyte0);
		a = false;
		b = "error in twriter";
		g = true;
		h = 3;
	}

	public static GameSocket a(String s, Applet applet, int i1) throws IOException {
		Socket socket;
		if (applet != null)
			socket = new Socket(InetAddress.getByName(applet.getCodeBase().getHost()), i1);
		else
			socket = new Socket(InetAddress.getByName(s), i1);
		socket.setSoTimeout(30000);
		return new GameSocket(socket);
	}

	public void xb() {
		if (super.wd)
			return;
		try {
			if (super.vd != null)
				super.vd.close();
			if (f != null) {
				g = true;
				synchronized (this) {
					notify();
				}
				f = null;
			}
			if (super.td != null)
				super.td.close();
			if (super.ud != null)
				super.ud.close();
			c = null;
			i = null;
			return;
		} catch (IOException _ex) {
			System.out.println("Error closing stream");
		}
	}

	public void h(byte abyte0[], int i1, int j1) throws IOException {
		if (super.wd) {
			return;
		} else {
			super.ud.write(abyte0, i1, j1);
			return;
		}
	}

	public void l(byte abyte0[], int i1, int j1, boolean flag) throws IOException {
		if (super.wd)
			return;
		if (c == null)
			c = new byte[5000];
		synchronized (this) {
			for (int k1 = 0; k1 < j1; k1++) {
				c[e] = abyte0[k1 + i1];
				e = (e + 1) % 5000;
				if (e != (d + 4900) % 5000)
					continue;
				a = true;
				b = "Write buffer full! " + j1;
				k1 = j1 + 1;
				g = true;
				super.td.close();
				super.ud.close();
				super.wd = true;
				break;
			}

			if (flag) {
				if (f == null) {
					g = false;
					f = new Thread(this);
					f.setDaemon(true);
					f.setPriority(4);
					f.start();
				}
				notify();
			}
		}
		if (a)
			throw new IOException(b);
		else
			return;
	}

	public void m() {
		synchronized (this) {
			if (e == d || c == null)
				return;
			if (f == null) {
				g = false;
				f = new Thread(this);
				f.setDaemon(true);
				f.setPriority(4);
				f.start();
			}
			notify();
		}
	}

	public void run() {
		while (f != null && !g) {
			int i1;
			int j1;
			synchronized (this) {
				if (e == d)
					try {
						wait();
					} catch (InterruptedException _ex) {
					}
				if (f == null || g)
					return;
				j1 = d;
				if (e >= d)
					i1 = e - d;
				else
					i1 = 5000 - d;
			}
			if (i1 > 0) {
				try {
					super.ud.write(c, j1, i1);
				} catch (IOException ioexception) {
					a = true;
					b = "Twriter IOEx:" + ioexception;
				}
				d = (d + i1) % 5000;
				try {
					if (e == d)
						super.ud.flush();
				} catch (IOException ioexception1) {
					a = true;
					b = "Twriter IOEx:" + ioexception1;
				}
			}
		}
	}

	public void i(int i1) {
		if (i == null)
			i = new byte[4000];
		i[2] = (byte) i1;
		h = 3;
	}

	public void n(int i1) {
		i[h++] = (byte) i1;
	}

	public void k(int i1) {
		i[h++] = (byte) (i1 >> 8);
		i[h++] = (byte) i1;
	}

	public void j(int i1) {
		i[h++] = (byte) (i1 >> 24);
		i[h++] = (byte) (i1 >> 16);
		i[h++] = (byte) (i1 >> 8);
		i[h++] = (byte) i1;
	}

	public void f(long l1) {
		j((int) (l1 >> 32));
		j((int) (l1 & -1L));
	}

	@SuppressWarnings("deprecation")
	public void d(String s) {
		s.getBytes(0, s.length(), i, h);
		h += s.length();
	}

	public void b(int i1, int j1) {
		i[j1++] = (byte) (i1 >> 8);
		i[j1++] = (byte) i1;
	}

	public void c() throws IOException {
		i[0] = (byte) ((h - 2) / 256);
		i[1] = (byte) (h - 2 & 0xff);
		l(i, 0, h, true);
	}

	public void e() {
		i[0] = (byte) ((h - 2) / 256);
		i[1] = (byte) (h - 2 & 0xff);
		try {
			l(i, 0, h, true);
			return;
		} catch (IOException _ex) {
			return;
		}
	}

	public void g() {
		i[0] = (byte) ((h - 2) / 256);
		i[1] = (byte) (h - 2 & 0xff);
		try {
			l(i, 0, h, false);
			return;
		} catch (IOException _ex) {
			return;
		}
	}

	private boolean a;
	private String b;
	private byte c[];
	private int d;
	private int e;
	private Thread f;
	private boolean g;
	public int h;
	public byte i[];
}
