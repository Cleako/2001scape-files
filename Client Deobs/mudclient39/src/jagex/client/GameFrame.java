package jagex.client;

import java.awt.*;

public class GameFrame extends Frame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8225819526527346552L;

	@SuppressWarnings("deprecation")
	public GameFrame(GameApplet k1, int i, int j, String title, boolean flag, boolean flag1) {
		qc = 28;
		nc = i;
		oc = j;
		rc = k1;
		if (flag1)
			qc = 48;
		else
			qc = 28;
		setTitle(title);
		setResizable(flag);
		show();
		toFront();
		resize(nc, oc);
		sc = getGraphics();
	}

	public Graphics getGraphics() {
		Graphics g = super.getGraphics();
		if (pc == 0)
			g.translate(0, 24);
		else
			g.translate(-5, 0);
		return g;
	}

	@SuppressWarnings("deprecation")
	public void resize(int i, int j) {
		super.resize(i, j + qc);
	}

	@SuppressWarnings("deprecation")
	public int o() {
		return size().width;
	}

	@SuppressWarnings("deprecation")
	public int p() {
		return size().height - qc;
	}

	@SuppressWarnings("deprecation")
	public boolean handleEvent(Event event) {
		if (event.id == 401)
			rc.keyDown(event, event.key);
		else if (event.id == 402)
			rc.keyUp(event, event.key);
		else if (event.id == 501)
			rc.mouseDown(event, event.x, event.y - 24);
		else if (event.id == 506)
			rc.mouseDrag(event, event.x, event.y - 24);
		else if (event.id == 502)
			rc.mouseUp(event, event.x, event.y - 24);
		else if (event.id == 503)
			rc.mouseMove(event, event.x, event.y - 24);
		else if (event.id == 201)
			rc.destroy();
		else if (event.id == 1001)
			rc.action(event, event.target);
		else if (event.id == 403)
			rc.keyDown(event, event.key);
		else if (event.id == 404)
			rc.keyUp(event, event.key);
		return true;
	}

	public final void paint(Graphics g) {
		rc.paint(g);
	}

	int nc;
	int oc;
	int pc;
	int qc;
	GameApplet rc;
	Graphics sc;
}
