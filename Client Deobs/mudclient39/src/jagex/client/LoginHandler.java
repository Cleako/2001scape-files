package jagex.client;

import jagex.o;

import java.awt.*;
import java.io.IOException;

@SuppressWarnings("serial")
public class LoginHandler extends GameApplet {

	public void fb(String s1, String s2) {
		try {
			bd = s1;
			s1 = o.xm(s1, 20);
			cd = s2;
			s2 = o.xm(s2, 20);
			if (s1.trim().length() == 0 || s2.trim().length() == 0) {
				return;
			}
			if (qj())
				dd = GameSocket.a(host1, this, port);
			else
				dd = GameSocket.a(host1, null, port);
			dd.i(0);
			dd.f(o.rm(s1));
			dd.d(s2);
			dd.k(xc);
			dd.c();
			dd.tb();
			int loginResponseId = dd.yb();
			System.out.println("Login response: " + loginResponseId);
			if (loginResponseId == 0) {
				kb();
				return;
			}
		} catch (Exception exception) {
			host1 = host2;
			System.out.println(String.valueOf(exception));
			return;
		}
	}

	public void db(String s1, String s2, String s3, int i, int j, int l) {
		try {
			if (qj())
				dd = GameSocket.a(host1, this, port);
			else
				dd = GameSocket.a(host1, null, port);
			dd.i(2);
			s1 = o.xm(s1, 20);
			s2 = o.xm(s2, 20);
			dd.f(o.rm(s1));
			dd.d(s2);
			for (; s3.length() < 40; s3 = s3 + " ")
				;
			dd.d(s3);
			dd.j(i);
			dd.j(j);
			dd.j(l);
			dd.e();
			dd.tb();
			int loginResponseId = dd.yb();
			dd.xb();
			System.out.println("Newplayer response: " + loginResponseId);
		} catch (Exception exception) {
			host1 = host2;
			System.out.println(String.valueOf(exception));
			return;
		}
	}

	public void mb() {
		if (dd != null) {
			dd.i(1);
			dd.e();
			bd = "";
			cd = "";
		}
	}

	public void r(String s1, String s2) {
		bd = s1;
		s1 = o.xm(s1, 20);
		cd = s2;
		s2 = o.xm(s2, 20);
		if (s1.length() == 0 || s2.length() == 0) {
			return;
		}
		for (long l = System.currentTimeMillis(); System.currentTimeMillis() - l < 60000L;) {
			s(loginResponse[2], loginResponse[3]);
			try {
				if (qj())
					dd = GameSocket.a(host1, this, port);
				else
					dd = GameSocket.a(host1, null, port);
				dd.i(19);
				dd.f(o.rm(s1));
				dd.d(s2);
				dd.k(xc);
				dd.c();
				dd.tb();
				int i = dd.yb();
				if (i == 0) {
					kb();
					return;
				}
				if (i >= 1 && i <= 4) {
					s1 = "";
					s2 = "";
					return;
				}
			} catch (Exception _ex) {
				s(loginResponse[2], loginResponse[3]);
			}
			s(loginResponse[2], loginResponse[3]);
			try {
				Thread.sleep(5000L);
			} catch (Exception _ex) {
			}
		}

		s1 = "";
		s2 = "";
	}

	public void sendDisconnection() {
		System.out.println("Lost connection");
		r(bd, cd);
	}

	public void s(String s1, String s2) {
		Graphics g = getGraphics();
		Font font = new Font("Helvetica", 1, 15);
		int i = ej();
		int j = oj();
		g.setColor(Color.black);
		g.fillRect(i / 2 - 140, j / 2 - 25, 280, 50);
		g.setColor(Color.white);
		g.drawRect(i / 2 - 140, j / 2 - 25, 280, 50);
		zi(g, s1, font, i / 2, j / 2 - 10);
		zi(g, s2, font, i / 2, j / 2 + 10);
	}

	public void kb() {
		ed = 0;
		fd = 0;
		wc = -500;
		jd = 0;
	}

	public void ib() {
		long l = System.currentTimeMillis();
		id = l;
	}

	public void z() {
		try {
			long l = System.currentTimeMillis();
			if (l - id > 5000L) {
				id = l;
				dd.i(5);
				dd.c();
			}
			if (!jb())
				return;
			wc++;
			if (wc > vc) {
				kb();
				sendDisconnection();
				return;
			}
			if (ed == 0 && dd.pb() >= 2)
				ed = dd.tb();
			if (ed > 0 && dd.pb() >= ed) {
				dd.sb(ed, gd);
				fd = o.sm(gd[0]);
				wc = 0;
				if (fd == 8) {
				} else if (fd == 23) {
					jd = o.sm(gd[1]);
					for (int i = 0; i < jd; i++) {
						kd[i] = o.qm(gd, 2 + i * 9);
						ld[i] = o.sm(gd[10 + i * 9]);
					}

				} else if (fd == 24) {
					long l1 = o.qm(gd, 1);
					int i1 = gd[9] & 0xff;
					for (int j1 = 0; j1 < jd; j1++)
						if (kd[j1] == l1) {
							ld[j1] = i1;
							ed = 0;
							return;
						}

					kd[jd] = l1;
					ld[jd] = i1;
					jd++;
				} else if (fd == 26) {
					md = o.sm(gd[1]);
					for (int j = 0; j < md; j++)
						nd[j] = o.qm(gd, 2 + j * 8);

				} else if (fd == 27) {
					od = gd[1];
					pd = gd[2];
					qd = gd[3];
					rd = gd[4];
					sd = gd[5];
				} else if (fd == 28) {
					long l2 = o.qm(gd, 1);
					String s2 = new String(gd, 9, ed - 9);
					if (l2 != o.rm(bd))
						s2 = o.nm(s2, true);
				} else {
				}
				ed = 0;
				return;
			}
		} catch (IOException _ex) {
			sendDisconnection();
		}
	}

	public void ab(String s1) {
		s1 = o.xm(s1, 20);
		dd.i(25);
		dd.d(s1);
		dd.e();
	}

	public void nb(int i, int j, int l, int i1, int j1) {
		dd.i(31);
		dd.n(i);
		dd.n(j);
		dd.n(l);
		dd.n(i1);
		dd.n(j1);
		dd.e();
	}

	public void lb(String s1) {
		long l = o.rm(s1);
		dd.i(29);
		dd.f(l);
		dd.e();
		for (int i = 0; i < md; i++)
			if (nd[i] == l)
				return;

		if (md >= 50) {
			return;
		} else {
			nd[md++] = l;
			return;
		}
	}

	public void cb(long l) {
		dd.i(30);
		dd.f(l);
		dd.e();
		for (int i = 0; i < md; i++)
			if (nd[i] == l) {
				md--;
				for (int j = i; j < md; j++)
					nd[j] = nd[j + 1];

				return;
			}

	}

	public void gb(String s1) {
		dd.i(26);
		dd.f(o.rm(s1));
		dd.e();
	}

	public void y(long l) {
		dd.i(27);
		dd.f(l);
		dd.e();
		for (int i = 0; i < jd; i++) {
			if (kd[i] != l)
				continue;
			jd--;
			for (int j = i; j < jd; j++) {
				kd[j] = kd[j + 1];
				ld[j] = ld[j + 1];
			}

			break;
		}
	}

	public void t(long l, String s1) {
		if (s1.length() > 80)
			s1 = s1.substring(0, 80);
		dd.i(28);
		dd.f(l);
		dd.n(s1.length());
		dd.d(s1);
		dd.e();
	}

	public boolean x(String s1) {
		if (s1.toLowerCase().startsWith("tell ")) {
			s1 = s1.substring(5);
			int i = s1.indexOf(' ');
			if (i == -1 || i >= s1.length() - 1) {
				return true;
			} else {
				String s2 = s1.substring(0, i);
				s1 = s1.substring(i + 1);
				t(o.rm(s2), s1);
				return true;
			}
		}
		dd.i(3);
		dd.d(s1);
		dd.e();
		id = hd = System.currentTimeMillis();
		return false;
	}

	public boolean jb() {
		return true;
	}

	public LoginHandler() {
		host1 = "127.0.0.1";
		host2 = "64.23.60.47";
		port = 43594;
		bd = "";
		cd = "";
		gd = new byte[5000];
		kd = new long[50];
		ld = new int[50];
		nd = new long[50];
	}

	public static String loginResponse[];
	public static boolean uc = true;
	public static int vc = 0x5f5e0ff;
	public static int wc;
	public static int xc = 1;
	public String host1;
	public String host2;
	public int port;
	String bd;
	String cd;
	public GameSocket dd;
	int ed;
	int fd;
	byte gd[];
	long hd;
	long id;
	public int jd;
	public long kd[];
	public int ld[];
	public int md;
	public long nd[];
	public int od;
	public int pd;
	public int qd;
	public int rd;
	public int sd;

	static {
		loginResponse = new String[50];
		loginResponse[0] = "You must enter both a username";
		loginResponse[1] = "and a password - Please try again";
		loginResponse[2] = "Connection lost! Please wait...";
		loginResponse[3] = "Attempting to re-establish";
		loginResponse[4] = "That username is already in use";
		loginResponse[5] = "Wait 60 seconds then retry";
		loginResponse[6] = "Please wait...";
		loginResponse[7] = "Connecting to server";
		loginResponse[8] = "Sorry! The server is currently full";
		loginResponse[9] = "Please try again later";
		loginResponse[10] = "Invalid username or password";
		loginResponse[11] = "Try again, or create a new account";
		loginResponse[12] = "Sorry! Unable to connect to server";
		loginResponse[13] = "Please check your internet settings";
		loginResponse[14] = "Username already taken";
		loginResponse[15] = "Please choose another username";
		loginResponse[16] = "The client has been updated";
		loginResponse[17] = "Please clear your cache and reload";
	}
}
