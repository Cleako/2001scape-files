package jagex.client;

import jagex.o;
import jagex.q;

import java.io.IOException;

public class j {

	public j(GameImageProducer l, int i1, int j1, int k1) {
		xl = 50;
		yl = new int[xl];
		zl = new int[xl][256];
		cm = 5;
		dm = 1000;
		em = 1000;
		fm = 20;
		gm = 10;
		jm = false;
		km = 1.1000000000000001D;
		lm = 1;
		mm = false;
		qm = 100;
		rm = new h[qm];
		sm = new int[qm];
		tm = 512;
		um = 256;
		vm = 192;
		wm = 256;
		xm = 256;
		ym = 8;
		zm = 4;
		so = new int[40];
		to = new int[40];
		uo = new int[40];
		vo = new int[40];
		wo = new int[40];
		xo = new int[40];
		yo = false;
		no = l;
		um = l.sj / 2;
		vm = l.tj / 2;
		oo = l.yj;
		gn = 0;
		hn = i1;
		in = new h[hn];
		jn = new int[hn];
		kn = 0;
		ln = new q[j1];
		for (int l1 = 0; l1 < j1; l1++)
			ln[l1] = new q();

		nn = 0;
		vn = new h(k1 * 2, k1);
		on = new int[k1];
		sn = new int[k1];
		tn = new int[k1];
		pn = new int[k1];
		qn = new int[k1];
		rn = new int[k1];
		un = new int[k1];
		if (lo == null)
			lo = new byte[17691];
		an = 0;
		bn = 0;
		cn = 0;
		dn = 0;
		en = 0;
		fn = 0;
		for (int i2 = 0; i2 < 256; i2++) {
			im[i2] = (int) (Math.sin((double) i2 * 0.02454369D) * 32768D);
			im[i2 + 256] = (int) (Math.cos((double) i2 * 0.02454369D) * 32768D);
		}

		for (int j2 = 0; j2 < 1024; j2++) {
			hm[j2] = (int) (Math.sin((double) j2 * 0.00613592315D) * 32768D);
			hm[j2 + 1024] = (int) (Math.cos((double) j2 * 0.00613592315D) * 32768D);
		}

	}

	public void oh(h h1) {
		if (gn < hn) {
			jn[gn] = 0;
			in[gn++] = h1;
		}
	}

	public void sh(h h1) {
		for (int l = 0; l < gn; l++)
			if (in[l] == h1) {
				gn--;
				for (int i1 = l; i1 < gn; i1++) {
					in[i1] = in[i1 + 1];
					jn[i1] = jn[i1 + 1];
				}

			}

	}

	public void ni() {
		vh();
		for (int l = 0; l < gn; l++)
			in[l] = null;

		gn = 0;
	}

	public void vh() {
		nn = 0;
		vn.re();
	}

	public void ei(int l) {
		nn -= l;
		vn.ge(l, l * 2);
		if (nn < 0)
			nn = 0;
	}

	public int si(int l, int i1, int j1, int k1, int l1, int i2) {
		on[nn] = l;
		pn[nn] = i1;
		qn[nn] = j1;
		rn[nn] = k1;
		sn[nn] = l1;
		tn[nn] = i2;
		un[nn] = 0;
		int j2 = vn.zd(i1, j1, k1);
		int k2 = vn.zd(i1, j1 - i2, k1);
		int ai1[] = { j2, k2 };
		vn.ie(2, ai1, 0, 0);
		vn.rh[nn++] = 0;
		return nn - 1;
	}

	public int fh(int l, int i1, int j1, int k1, int l1, int i2, int j2) {
		on[nn] = l;
		pn[nn] = i1;
		qn[nn] = j1;
		rn[nn] = k1;
		sn[nn] = l1;
		tn[nn] = i2;
		un[nn] = 0;
		int k2 = vn.zd(i1, j1, k1);
		int l2 = vn.zd(i1, j1 - i2, k1);
		int ai1[] = { k2, l2 };
		vn.ie(2, ai1, 0, 0);
		vn.qh[nn] = j2;
		vn.rh[nn++] = 0;
		return nn - 1;
	}

	public void gh(int l) {
		vn.rh[l] = 1;
	}

	public void ii(int l, int i1) {
		un[l] = i1;
	}

	public void ph(int l, int i1) {
		nm = l - wm;
		om = i1;
		pm = 0;
		mm = true;
	}

	public int mi() {
		return pm;
	}

	public int[] hi() {
		return sm;
	}

	public h[] ih() {
		return rm;
	}

	public void yh(int l, int i1, int j1, int k1, int l1, int i2) {
		um = j1;
		vm = k1;
		wm = l;
		xm = i1;
		tm = l1;
		ym = i2;
		po = new s[k1 + i1];
		for (int j2 = 0; j2 < k1 + i1; j2++)
			po[j2] = new s();

	}

	private void kh(q aq[], int l, int i1) {
		if (l < i1) {
			int j1 = l - 1;
			int k1 = i1 + 1;
			int l1 = (l + i1) / 2;
			q q1 = aq[l1];
			aq[l1] = aq[l];
			aq[l] = q1;
			int i2 = q1.jfb;
			while (j1 < k1) {
				do
					k1--;
				while (aq[k1].jfb < i2);
				do
					j1++;
				while (aq[j1].jfb > i2);
				if (j1 < k1) {
					q q2 = aq[j1];
					aq[j1] = aq[k1];
					aq[k1] = q2;
				}
			}
			kh(aq, l, k1);
			kh(aq, k1 + 1, i1);
		}
	}

	public void bh(int l, q aq[], int i1) {
		for (int j1 = 0; j1 <= i1; j1++) {
			aq[j1].pfb = false;
			aq[j1].qfb = j1;
			aq[j1].rfb = -1;
		}

		int k1 = 0;
		do {
			while (aq[k1].pfb)
				k1++;
			if (k1 == i1)
				return;
			q q1 = aq[k1];
			q1.pfb = true;
			int l1 = k1;
			int i2 = k1 + l;
			if (i2 >= i1)
				i2 = i1 - 1;
			for (int j2 = i2; j2 >= l1 + 1; j2--) {
				q q2 = aq[j2];
				if (q1.bfb < q2.dfb && q2.bfb < q1.dfb && q1.cfb < q2.efb && q2.cfb < q1.efb && q1.qfb != q2.rfb
						&& !ch(q1, q2) && ah(q2, q1)) {
					pi(aq, l1, j2);
					if (aq[j2] != q2)
						j2++;
					l1 = fp;
					q2.rfb = q1.qfb;
				}
			}

		} while (true);
	}

	public boolean pi(q aq[], int l, int i1) {
		do {
			q q1 = aq[l];
			for (int j1 = l + 1; j1 <= i1; j1++) {
				q q2 = aq[j1];
				if (!ch(q2, q1))
					break;
				aq[l] = q2;
				aq[j1] = q1;
				l = j1;
				if (l == i1) {
					fp = l;
					gp = l - 1;
					return true;
				}
			}

			q q3 = aq[i1];
			for (int k1 = i1 - 1; k1 >= l; k1--) {
				q q4 = aq[k1];
				if (!ch(q3, q4))
					break;
				aq[i1] = q4;
				aq[k1] = q3;
				i1 = k1;
				if (l == i1) {
					fp = i1 + 1;
					gp = i1;
					return true;
				}
			}

			if (l + 1 >= i1) {
				fp = l;
				gp = i1;
				return false;
			}
			if (!pi(aq, l + 1, i1)) {
				fp = l;
				return false;
			}
			i1 = gp;
		} while (true);
	}

	public void li(int l, int i1, int j1) {
		int k1 = -dn + 1024 & 0x3ff;
		int l1 = -en + 1024 & 0x3ff;
		int i2 = -fn + 1024 & 0x3ff;
		if (i2 != 0) {
			int j2 = hm[i2];
			int i3 = hm[i2 + 1024];
			int l3 = i1 * j2 + l * i3 >> 15;
			i1 = i1 * i3 - l * j2 >> 15;
			l = l3;
		}
		if (k1 != 0) {
			int k2 = hm[k1];
			int j3 = hm[k1 + 1024];
			int i4 = i1 * j3 - j1 * k2 >> 15;
			j1 = i1 * k2 + j1 * j3 >> 15;
			i1 = i4;
		}
		if (l1 != 0) {
			int l2 = hm[l1];
			int k3 = hm[l1 + 1024];
			int j4 = j1 * l2 + l * k3 >> 15;
			j1 = j1 * k3 - l * l2 >> 15;
			l = j4;
		}
		if (l < zo)
			zo = l;
		if (l > ap)
			ap = l;
		if (i1 < bp)
			bp = i1;
		if (i1 > cp)
			cp = i1;
		if (j1 < dp)
			dp = j1;
		if (j1 > ep)
			ep = j1;
	}

	public void qi() {
		yo = no.rk;
		int l3 = um * dm >> ym;
		int i4 = vm * dm >> ym;
		zo = 0;
		ap = 0;
		bp = 0;
		cp = 0;
		dp = 0;
		ep = 0;
		li(-l3, -i4, dm);
		li(-l3, i4, dm);
		li(l3, -i4, dm);
		li(l3, i4, dm);
		li(-um, -vm, 0);
		li(-um, vm, 0);
		li(um, -vm, 0);
		li(um, vm, 0);
		zo += an;
		ap += an;
		bp += bn;
		cp += bn;
		dp += cn;
		ep += cn;
		in[gn] = vn;
		vn.eh = 2;
		for (int l = 0; l < gn; l++)
			in[l].de(an, bn, cn, dn, en, fn, ym, cm);

		in[gn].de(an, bn, cn, dn, en, fn, ym, cm);
		kn = 0;
		for (int j4 = 0; j4 < gn; j4++) {
			h h1 = in[j4];
			if (h1.fh) {
				for (int i1 = 0; i1 < h1.rg; i1++) {
					int k4 = h1.sg[i1];
					int ai2[] = h1.tg[i1];
					boolean flag = false;
					for (int j5 = 0; j5 < k4; j5++) {
						int l1 = h1.mg[ai2[j5]];
						if (l1 <= cm || l1 >= dm)
							continue;
						flag = true;
						break;
					}

					if (flag) {
						int k2 = 0;
						for (int j6 = 0; j6 < k4; j6++) {
							int i2 = h1.ng[ai2[j6]];
							if (i2 > -um)
								k2 |= 1;
							if (i2 < um)
								k2 |= 2;
							if (k2 == 3)
								break;
						}

						if (k2 == 3) {
							int l2 = 0;
							for (int k7 = 0; k7 < k4; k7++) {
								int j2 = h1.og[ai2[k7]];
								if (j2 > -vm)
									l2 |= 1;
								if (j2 < vm)
									l2 |= 2;
								if (l2 == 3)
									break;
							}

							if (l2 == 3) {
								q q2 = ln[kn];
								q2.hfb = h1;
								q2.ifb = i1;
								ri(kn);
								int k9;
								if (q2.nfb < 0)
									k9 = h1.ug[i1];
								else
									k9 = h1.vg[i1];
								if (k9 != 0xbc614e) {
									int i3 = 0;
									for (int k10 = 0; k10 < k4; k10++)
										i3 += h1.mg[ai2[k10]];

									q2.jfb = i3 / k4 + h1.dh;
									q2.ofb = k9;
									kn++;
								}
							}
						}
					}
				}

			}
		}

		h h2 = vn;
		if (h2.fh) {
			for (int j1 = 0; j1 < h2.rg; j1++) {
				int ai1[] = h2.tg[j1];
				int i5 = ai1[0];
				int k5 = h2.ng[i5];
				int k6 = h2.og[i5];
				int l7 = h2.mg[i5];
				if (l7 > cm && l7 < em) {
					int l8 = (sn[j1] << ym) / l7;
					int l9 = (tn[j1] << ym) / l7;
					if (k5 - l8 / 2 <= um && k5 + l8 / 2 >= -um && k6 - l9 <= vm && k6 >= -vm) {
						q q3 = ln[kn];
						q3.hfb = h2;
						q3.ifb = j1;
						ti(kn);
						q3.jfb = (l7 + h2.mg[ai1[1]]) / 2;
						kn++;
					}
				}
			}

		}
		if (kn == 0)
			return;
		bm = kn;
		kh(ln, 0, kn - 1);
		bh(100, ln, kn);
		for (int l4 = 0; l4 < kn; l4++) {
			q q1 = ln[l4];
			h h3 = q1.hfb;
			int k1 = q1.ifb;
			if (h3 == vn) {
				int ai3[] = h3.tg[k1];
				int l6 = ai3[0];
				int i8 = h3.ng[l6];
				int i9 = h3.og[l6];
				int i10 = h3.mg[l6];
				int l10 = (sn[k1] << ym) / i10;
				int j11 = (tn[k1] << ym) / i10;
				int l11 = i9 - h3.og[ai3[1]];
				int i12 = ((h3.ng[ai3[1]] - i8) * l11) / j11;
				i12 = h3.ng[ai3[1]] - i8;
				int k12 = i8 - l10 / 2;
				int i13 = (xm + i9) - j11;
				no.yf(k12 + wm, i13, l10, j11, on[k1], i12, (256 << ym) / i10);
				if (mm && pm < qm) {
					k12 += (un[k1] << ym) / i10;
					if (om >= i13 && om <= i13 + j11 && nm >= k12 && nm <= k12 + l10 && !h3.vh && h3.rh[k1] == 0) {
						rm[pm] = h3;
						sm[pm] = k1;
						pm++;
					}
				}
			} else {
				int j9 = 0;
				int i11 = 0;
				int k11 = h3.sg[k1];
				int ai4[] = h3.tg[k1];
				if (h3.yg[k1] != 0xbc614e)
					if (q1.nfb < 0)
						i11 = h3.oj - h3.yg[k1];
					else
						i11 = h3.oj + h3.yg[k1];
				for (int j12 = 0; j12 < k11; j12++) {
					int j3 = ai4[j12];
					vo[j12] = h3.kg[j3];
					wo[j12] = h3.lg[j3];
					xo[j12] = h3.mg[j3];
					if (h3.yg[k1] == 0xbc614e)
						if (q1.nfb < 0)
							i11 = (h3.oj - h3.pg[j3]) + h3.qg[j3];
						else
							i11 = h3.oj + h3.pg[j3] + h3.qg[j3];
					if (h3.mg[j3] >= cm) {
						so[j9] = h3.ng[j3];
						to[j9] = h3.og[j3];
						uo[j9] = i11;
						if (h3.mg[j3] > gm)
							uo[j9] += (h3.mg[j3] - gm) / fm;
						j9++;
					} else {
						int j10;
						if (j12 == 0)
							j10 = ai4[k11 - 1];
						else
							j10 = ai4[j12 - 1];
						if (h3.mg[j10] >= cm) {
							int j8 = h3.mg[j3] - h3.mg[j10];
							int l5 = h3.kg[j3] - ((h3.kg[j3] - h3.kg[j10]) * (h3.mg[j3] - cm)) / j8;
							int i7 = h3.lg[j3] - ((h3.lg[j3] - h3.lg[j10]) * (h3.mg[j3] - cm)) / j8;
							so[j9] = (l5 << ym) / cm;
							to[j9] = (i7 << ym) / cm;
							uo[j9] = i11;
							j9++;
						}
						if (j12 == k11 - 1)
							j10 = ai4[0];
						else
							j10 = ai4[j12 + 1];
						if (h3.mg[j10] >= cm) {
							int k8 = h3.mg[j3] - h3.mg[j10];
							int i6 = h3.kg[j3] - ((h3.kg[j3] - h3.kg[j10]) * (h3.mg[j3] - cm)) / k8;
							int j7 = h3.lg[j3] - ((h3.lg[j3] - h3.lg[j10]) * (h3.mg[j3] - cm)) / k8;
							so[j9] = (i6 << ym) / cm;
							to[j9] = (j7 << ym) / cm;
							uo[j9] = i11;
							j9++;
						}
					}
				}

				for (int l12 = 0; l12 < k11; l12++) {
					if (uo[l12] < 0)
						uo[l12] = 0;
					else if (uo[l12] > 255)
						uo[l12] = 255;
					if (q1.ofb >= 0)
						if (_flddo[q1.ofb] == 1)
							uo[l12] <<= 9;
						else
							uo[l12] <<= 6;
				}

				zg(0, 0, 0, 0, j9, so, to, uo, h3, k1);
				if (ro > qo)
					jh(0, 0, k11, vo, wo, xo, q1.ofb, h3);
			}
		}

		mm = false;
	}

	private void zg(int l, int i1, int j1, int k1, int l1, int ai1[], int ai2[], int ai3[], h h1, int i2) {
		if (l1 == 3) {
			int j2 = ai2[0] + xm;
			int j3 = ai2[1] + xm;
			int j4 = ai2[2] + xm;
			int j5 = ai1[0];
			int k6 = ai1[1];
			int i8 = ai1[2];
			int k9 = ai3[0];
			int i11 = ai3[1];
			int i12 = ai3[2];
			int i13 = (xm + vm) - 1;
			int k13 = 0;
			int i14 = 0;
			int k14 = 0;
			int i15 = 0;
			int k15 = 0xbc614e;
			int i16 = 0xff439eb2;
			if (j4 != j2) {
				i14 = (i8 - j5 << 8) / (j4 - j2);
				i15 = (i12 - k9 << 8) / (j4 - j2);
				if (j2 < j4) {
					k13 = j5 << 8;
					k14 = k9 << 8;
					k15 = j2;
					i16 = j4;
				} else {
					k13 = i8 << 8;
					k14 = i12 << 8;
					k15 = j4;
					i16 = j2;
				}
				if (k15 < 0) {
					k13 -= i14 * k15;
					k14 -= i15 * k15;
					k15 = 0;
				}
				if (i16 > i13)
					i16 = i13;
			}
			int k16 = 0;
			int i17 = 0;
			int k17 = 0;
			int i18 = 0;
			int k18 = 0xbc614e;
			int i19 = 0xff439eb2;
			if (j3 != j2) {
				i17 = (k6 - j5 << 8) / (j3 - j2);
				i18 = (i11 - k9 << 8) / (j3 - j2);
				if (j2 < j3) {
					k16 = j5 << 8;
					k17 = k9 << 8;
					k18 = j2;
					i19 = j3;
				} else {
					k16 = k6 << 8;
					k17 = i11 << 8;
					k18 = j3;
					i19 = j2;
				}
				if (k18 < 0) {
					k16 -= i17 * k18;
					k17 -= i18 * k18;
					k18 = 0;
				}
				if (i19 > i13)
					i19 = i13;
			}
			int k19 = 0;
			int i20 = 0;
			int k20 = 0;
			int i21 = 0;
			int k21 = 0xbc614e;
			int i22 = 0xff439eb2;
			if (j4 != j3) {
				i20 = (i8 - k6 << 8) / (j4 - j3);
				i21 = (i12 - i11 << 8) / (j4 - j3);
				if (j3 < j4) {
					k19 = k6 << 8;
					k20 = i11 << 8;
					k21 = j3;
					i22 = j4;
				} else {
					k19 = i8 << 8;
					k20 = i12 << 8;
					k21 = j4;
					i22 = j3;
				}
				if (k21 < 0) {
					k19 -= i20 * k21;
					k20 -= i21 * k21;
					k21 = 0;
				}
				if (i22 > i13)
					i22 = i13;
			}
			qo = k15;
			if (k18 < qo)
				qo = k18;
			if (k21 < qo)
				qo = k21;
			ro = i16;
			if (i19 > ro)
				ro = i19;
			if (i22 > ro)
				ro = i22;
			int k22 = 0;
			for (j1 = qo; j1 < ro; j1++) {
				if (j1 >= k15 && j1 < i16) {
					l = i1 = k13;
					k1 = k22 = k14;
					k13 += i14;
					k14 += i15;
				} else {
					l = 0xa0000;
					i1 = 0xfff60000;
				}
				if (j1 >= k18 && j1 < i19) {
					if (k16 < l) {
						l = k16;
						k1 = k17;
					}
					if (k16 > i1) {
						i1 = k16;
						k22 = k17;
					}
					k16 += i17;
					k17 += i18;
				}
				if (j1 >= k21 && j1 < i22) {
					if (k19 < l) {
						l = k19;
						k1 = k20;
					}
					if (k19 > i1) {
						i1 = k19;
						k22 = k20;
					}
					k19 += i20;
					k20 += i21;
				}
				s s7 = po[j1];
				s7.nkb = l;
				s7.okb = i1;
				s7.pkb = k1;
				s7.qkb = k22;
			}

			if (qo < xm - vm)
				qo = xm - vm;
		} else if (l1 == 4) {
			int k2 = ai2[0] + xm;
			int k3 = ai2[1] + xm;
			int k4 = ai2[2] + xm;
			int k5 = ai2[3] + xm;
			int l6 = ai1[0];
			int j8 = ai1[1];
			int l9 = ai1[2];
			int j11 = ai1[3];
			int j12 = ai3[0];
			int j13 = ai3[1];
			int l13 = ai3[2];
			int j14 = ai3[3];
			int l14 = (xm + vm) - 1;
			int j15 = 0;
			int l15 = 0;
			int j16 = 0;
			int l16 = 0;
			int j17 = 0xbc614e;
			int l17 = 0xff439eb2;
			if (k5 != k2) {
				l15 = (j11 - l6 << 8) / (k5 - k2);
				l16 = (j14 - j12 << 8) / (k5 - k2);
				if (k2 < k5) {
					j15 = l6 << 8;
					j16 = j12 << 8;
					j17 = k2;
					l17 = k5;
				} else {
					j15 = j11 << 8;
					j16 = j14 << 8;
					j17 = k5;
					l17 = k2;
				}
				if (j17 < 0) {
					j15 -= l15 * j17;
					j16 -= l16 * j17;
					j17 = 0;
				}
				if (l17 > l14)
					l17 = l14;
			}
			int j18 = 0;
			int l18 = 0;
			int j19 = 0;
			int l19 = 0;
			int j20 = 0xbc614e;
			int l20 = 0xff439eb2;
			if (k3 != k2) {
				l18 = (j8 - l6 << 8) / (k3 - k2);
				l19 = (j13 - j12 << 8) / (k3 - k2);
				if (k2 < k3) {
					j18 = l6 << 8;
					j19 = j12 << 8;
					j20 = k2;
					l20 = k3;
				} else {
					j18 = j8 << 8;
					j19 = j13 << 8;
					j20 = k3;
					l20 = k2;
				}
				if (j20 < 0) {
					j18 -= l18 * j20;
					j19 -= l19 * j20;
					j20 = 0;
				}
				if (l20 > l14)
					l20 = l14;
			}
			int j21 = 0;
			int l21 = 0;
			int j22 = 0;
			int l22 = 0;
			int i23 = 0xbc614e;
			int j23 = 0xff439eb2;
			if (k4 != k3) {
				l21 = (l9 - j8 << 8) / (k4 - k3);
				l22 = (l13 - j13 << 8) / (k4 - k3);
				if (k3 < k4) {
					j21 = j8 << 8;
					j22 = j13 << 8;
					i23 = k3;
					j23 = k4;
				} else {
					j21 = l9 << 8;
					j22 = l13 << 8;
					i23 = k4;
					j23 = k3;
				}
				if (i23 < 0) {
					j21 -= l21 * i23;
					j22 -= l22 * i23;
					i23 = 0;
				}
				if (j23 > l14)
					j23 = l14;
			}
			int k23 = 0;
			int l23 = 0;
			int i24 = 0;
			int j24 = 0;
			int k24 = 0xbc614e;
			int l24 = 0xff439eb2;
			if (k5 != k4) {
				l23 = (j11 - l9 << 8) / (k5 - k4);
				j24 = (j14 - l13 << 8) / (k5 - k4);
				if (k4 < k5) {
					k23 = l9 << 8;
					i24 = l13 << 8;
					k24 = k4;
					l24 = k5;
				} else {
					k23 = j11 << 8;
					i24 = j14 << 8;
					k24 = k5;
					l24 = k4;
				}
				if (k24 < 0) {
					k23 -= l23 * k24;
					i24 -= j24 * k24;
					k24 = 0;
				}
				if (l24 > l14)
					l24 = l14;
			}
			qo = j17;
			if (j20 < qo)
				qo = j20;
			if (i23 < qo)
				qo = i23;
			if (k24 < qo)
				qo = k24;
			ro = l17;
			if (l20 > ro)
				ro = l20;
			if (j23 > ro)
				ro = j23;
			if (l24 > ro)
				ro = l24;
			int i25 = 0;
			for (j1 = qo; j1 < ro; j1++) {
				if (j1 >= j17 && j1 < l17) {
					l = i1 = j15;
					k1 = i25 = j16;
					j15 += l15;
					j16 += l16;
				} else {
					l = 0xa0000;
					i1 = 0xfff60000;
				}
				if (j1 >= j20 && j1 < l20) {
					if (j18 < l) {
						l = j18;
						k1 = j19;
					}
					if (j18 > i1) {
						i1 = j18;
						i25 = j19;
					}
					j18 += l18;
					j19 += l19;
				}
				if (j1 >= i23 && j1 < j23) {
					if (j21 < l) {
						l = j21;
						k1 = j22;
					}
					if (j21 > i1) {
						i1 = j21;
						i25 = j22;
					}
					j21 += l21;
					j22 += l22;
				}
				if (j1 >= k24 && j1 < l24) {
					if (k23 < l) {
						l = k23;
						k1 = i24;
					}
					if (k23 > i1) {
						i1 = k23;
						i25 = i24;
					}
					k23 += l23;
					i24 += j24;
				}
				s s8 = po[j1];
				s8.nkb = l;
				s8.okb = i1;
				s8.pkb = k1;
				s8.qkb = i25;
			}

			if (qo < xm - vm)
				qo = xm - vm;
		} else {
			ro = qo = ai2[0] += xm;
			for (j1 = 1; j1 < l1; j1++) {
				int l2;
				if ((l2 = ai2[j1] += xm) < qo)
					qo = l2;
				else if (l2 > ro)
					ro = l2;
			}

			if (qo < xm - vm)
				qo = xm - vm;
			if (ro >= xm + vm)
				ro = (xm + vm) - 1;
			if (qo >= ro)
				return;
			for (j1 = qo; j1 < ro; j1++) {
				s s1 = po[j1];
				s1.nkb = 0xa0000;
				s1.okb = 0xfff60000;
			}

			int i3 = l1 - 1;
			int l3 = ai2[0];
			int l4 = ai2[i3];
			if (l3 < l4) {
				int l5 = ai1[0] << 8;
				int i7 = (ai1[i3] - ai1[0] << 8) / (l4 - l3);
				int k8 = ai3[0] << 8;
				int i10 = (ai3[i3] - ai3[0] << 8) / (l4 - l3);
				if (l3 < 0) {
					l5 -= i7 * l3;
					k8 -= i10 * l3;
					l3 = 0;
				}
				if (l4 > ro)
					l4 = ro;
				for (j1 = l3; j1 <= l4; j1++) {
					s s3 = po[j1];
					s3.nkb = s3.okb = l5;
					s3.pkb = s3.qkb = k8;
					l5 += i7;
					k8 += i10;
				}

			} else if (l3 > l4) {
				int i6 = ai1[i3] << 8;
				int j7 = (ai1[0] - ai1[i3] << 8) / (l3 - l4);
				int l8 = ai3[i3] << 8;
				int j10 = (ai3[0] - ai3[i3] << 8) / (l3 - l4);
				if (l4 < 0) {
					i6 -= j7 * l4;
					l8 -= j10 * l4;
					l4 = 0;
				}
				if (l3 > ro)
					l3 = ro;
				for (j1 = l4; j1 <= l3; j1++) {
					s s4 = po[j1];
					s4.nkb = s4.okb = i6;
					s4.pkb = s4.qkb = l8;
					i6 += j7;
					l8 += j10;
				}

			}
			for (j1 = 0; j1 < i3; j1++) {
				int j6 = j1 + 1;
				int i4 = ai2[j1];
				int i5 = ai2[j6];
				if (i4 < i5) {
					int k7 = ai1[j1] << 8;
					int i9 = (ai1[j6] - ai1[j1] << 8) / (i5 - i4);
					int k10 = ai3[j1] << 8;
					int k11 = (ai3[j6] - ai3[j1] << 8) / (i5 - i4);
					if (i4 < 0) {
						k7 -= i9 * i4;
						k10 -= k11 * i4;
						i4 = 0;
					}
					if (i5 > ro)
						i5 = ro;
					for (int k12 = i4; k12 <= i5; k12++) {
						s s5 = po[k12];
						if (k7 < s5.nkb) {
							s5.nkb = k7;
							s5.pkb = k10;
						}
						if (k7 > s5.okb) {
							s5.okb = k7;
							s5.qkb = k10;
						}
						k7 += i9;
						k10 += k11;
					}

				} else if (i4 > i5) {
					int l7 = ai1[j6] << 8;
					int j9 = (ai1[j1] - ai1[j6] << 8) / (i4 - i5);
					int l10 = ai3[j6] << 8;
					int l11 = (ai3[j1] - ai3[j6] << 8) / (i4 - i5);
					if (i5 < 0) {
						l7 -= j9 * i5;
						l10 -= l11 * i5;
						i5 = 0;
					}
					if (i4 > ro)
						i4 = ro;
					for (int l12 = i5; l12 <= i4; l12++) {
						s s6 = po[l12];
						if (l7 < s6.nkb) {
							s6.nkb = l7;
							s6.pkb = l10;
						}
						if (l7 > s6.okb) {
							s6.okb = l7;
							s6.qkb = l10;
						}
						l7 += j9;
						l10 += l11;
					}

				}
			}

			if (qo < xm - vm)
				qo = xm - vm;
		}
		if (mm && pm < qm && om >= qo && om < ro) {
			s s2 = po[om];
			if (nm >= s2.nkb >> 8 && nm <= s2.okb >> 8 && s2.nkb <= s2.okb && !h1.vh && h1.rh[i2] == 0) {
				rm[pm] = h1;
				sm[pm] = i2;
				pm++;
			}
		}
	}

	private void jh(int l, int i1, int j1, int ai1[], int ai2[], int ai3[], int k1, h h1) {
		if (k1 >= 0) {
			if (k1 >= ao)
				k1 = 0;
			fi(k1);
			int l1 = ai1[0];
			int j2 = ai2[0];
			int i3 = ai3[0];
			int l3 = l1 - ai1[1];
			int j4 = j2 - ai2[1];
			int l4 = i3 - ai3[1];
			j1--;
			int l6 = ai1[j1] - l1;
			int i8 = ai2[j1] - j2;
			int j9 = ai3[j1] - i3;
			if (_flddo[k1] == 1) {
				int k10 = l6 * j2 - i8 * l1 << 12;
				int j11 = i8 * i3 - j9 * j2 << (5 - ym) + 7 + 4;
				int l11 = j9 * l1 - l6 * i3 << (5 - ym) + 7;
				int j12 = l3 * j2 - j4 * l1 << 12;
				int l12 = j4 * i3 - l4 * j2 << (5 - ym) + 7 + 4;
				int j13 = l4 * l1 - l3 * i3 << (5 - ym) + 7;
				int l13 = j4 * l6 - l3 * i8 << 5;
				int j14 = l4 * i8 - j4 * j9 << (5 - ym) + 4;
				int l14 = l3 * j9 - l4 * l6 >> ym - 5;
				int j15 = j11 >> 4;
				int l15 = l12 >> 4;
				int j16 = j14 >> 4;
				int l16 = qo - xm;
				int j17 = tm;
				int l17 = wm + qo * j17;
				byte byte1 = 1;
				k10 += l11 * l16;
				j12 += j13 * l16;
				l13 += l14 * l16;
				if (yo) {
					if ((qo & 1) == 1) {
						qo++;
						k10 += l11;
						j12 += j13;
						l13 += l14;
						l17 += j17;
					}
					l11 <<= 1;
					j13 <<= 1;
					l14 <<= 1;
					j17 <<= 1;
					byte1 = 2;
				}
				if (h1.nh) {
					for (l = qo; l < ro; l += byte1) {
						s s4 = po[l];
						i1 = s4.nkb >> 8;
						int j18 = s4.okb >> 8;
						int j21 = j18 - i1;
						if (j21 <= 0) {
							k10 += l11;
							j12 += j13;
							l13 += l14;
							l17 += j17;
						} else {
							int l22 = s4.pkb;
							int j24 = (s4.qkb - l22) / j21;
							if (i1 < -um) {
								l22 += (-um - i1) * j24;
								i1 = -um;
								j21 = j18 - i1;
							}
							if (j18 > um) {
								int k18 = um;
								j21 = k18 - i1;
							}
							ji(oo, go[k1], 0, 0, k10 + j15 * i1, j12 + l15 * i1, l13 + j16 * i1, j11, l12, j14, j21,
									l17 + i1, l22, j24 << 2);
							k10 += l11;
							j12 += j13;
							l13 += l14;
							l17 += j17;
						}
					}

					return;
				}
				if (!fo[k1]) {
					for (l = qo; l < ro; l += byte1) {
						s s5 = po[l];
						i1 = s5.nkb >> 8;
						int l18 = s5.okb >> 8;
						int k21 = l18 - i1;
						if (k21 <= 0) {
							k10 += l11;
							j12 += j13;
							l13 += l14;
							l17 += j17;
						} else {
							int i23 = s5.pkb;
							int k24 = (s5.qkb - i23) / k21;
							if (i1 < -um) {
								i23 += (-um - i1) * k24;
								i1 = -um;
								k21 = l18 - i1;
							}
							if (l18 > um) {
								int i19 = um;
								k21 = i19 - i1;
							}
							nh(oo, go[k1], 0, 0, k10 + j15 * i1, j12 + l15 * i1, l13 + j16 * i1, j11, l12, j14, k21,
									l17 + i1, i23, k24 << 2);
							k10 += l11;
							j12 += j13;
							l13 += l14;
							l17 += j17;
						}
					}

					return;
				}
				for (l = qo; l < ro; l += byte1) {
					s s6 = po[l];
					i1 = s6.nkb >> 8;
					int j19 = s6.okb >> 8;
					int l21 = j19 - i1;
					if (l21 <= 0) {
						k10 += l11;
						j12 += j13;
						l13 += l14;
						l17 += j17;
					} else {
						int j23 = s6.pkb;
						int l24 = (s6.qkb - j23) / l21;
						if (i1 < -um) {
							j23 += (-um - i1) * l24;
							i1 = -um;
							l21 = j19 - i1;
						}
						if (j19 > um) {
							int k19 = um;
							l21 = k19 - i1;
						}
						ci(oo, 0, 0, 0, go[k1], k10 + j15 * i1, j12 + l15 * i1, l13 + j16 * i1, j11, l12, j14, l21,
								l17 + i1, j23, l24);
						k10 += l11;
						j12 += j13;
						l13 += l14;
						l17 += j17;
					}
				}

				return;
			}
			int l10 = l6 * j2 - i8 * l1 << 11;
			int k11 = i8 * i3 - j9 * j2 << (5 - ym) + 6 + 4;
			int i12 = j9 * l1 - l6 * i3 << (5 - ym) + 6;
			int k12 = l3 * j2 - j4 * l1 << 11;
			int i13 = j4 * i3 - l4 * j2 << (5 - ym) + 6 + 4;
			int k13 = l4 * l1 - l3 * i3 << (5 - ym) + 6;
			int i14 = j4 * l6 - l3 * i8 << 5;
			int k14 = l4 * i8 - j4 * j9 << (5 - ym) + 4;
			int i15 = l3 * j9 - l4 * l6 >> ym - 5;
			int k15 = k11 >> 4;
			int i16 = i13 >> 4;
			int k16 = k14 >> 4;
			int i17 = qo - xm;
			int k17 = tm;
			int i18 = wm + qo * k17;
			byte byte2 = 1;
			l10 += i12 * i17;
			k12 += k13 * i17;
			i14 += i15 * i17;
			if (yo) {
				if ((qo & 1) == 1) {
					qo++;
					l10 += i12;
					k12 += k13;
					i14 += i15;
					i18 += k17;
				}
				i12 <<= 1;
				k13 <<= 1;
				i15 <<= 1;
				k17 <<= 1;
				byte2 = 2;
			}
			if (h1.nh) {
				for (l = qo; l < ro; l += byte2) {
					s s7 = po[l];
					i1 = s7.nkb >> 8;
					int l19 = s7.okb >> 8;
					int i22 = l19 - i1;
					if (i22 <= 0) {
						l10 += i12;
						k12 += k13;
						i14 += i15;
						i18 += k17;
					} else {
						int k23 = s7.pkb;
						int i25 = (s7.qkb - k23) / i22;
						if (i1 < -um) {
							k23 += (-um - i1) * i25;
							i1 = -um;
							i22 = l19 - i1;
						}
						if (l19 > um) {
							int i20 = um;
							i22 = i20 - i1;
						}
						qh(oo, go[k1], 0, 0, l10 + k15 * i1, k12 + i16 * i1, i14 + k16 * i1, k11, i13, k14, i22,
								i18 + i1, k23, i25);
						l10 += i12;
						k12 += k13;
						i14 += i15;
						i18 += k17;
					}
				}

				return;
			}
			if (!fo[k1]) {
				for (l = qo; l < ro; l += byte2) {
					s s8 = po[l];
					i1 = s8.nkb >> 8;
					int j20 = s8.okb >> 8;
					int j22 = j20 - i1;
					if (j22 <= 0) {
						l10 += i12;
						k12 += k13;
						i14 += i15;
						i18 += k17;
					} else {
						int l23 = s8.pkb;
						int j25 = (s8.qkb - l23) / j22;
						if (i1 < -um) {
							l23 += (-um - i1) * j25;
							i1 = -um;
							j22 = j20 - i1;
						}
						if (j20 > um) {
							int k20 = um;
							j22 = k20 - i1;
						}
						ki(oo, go[k1], 0, 0, l10 + k15 * i1, k12 + i16 * i1, i14 + k16 * i1, k11, i13, k14, j22,
								i18 + i1, l23, j25);
						l10 += i12;
						k12 += k13;
						i14 += i15;
						i18 += k17;
					}
				}

				return;
			}
			for (l = qo; l < ro; l += byte2) {
				s s9 = po[l];
				i1 = s9.nkb >> 8;
				int l20 = s9.okb >> 8;
				int k22 = l20 - i1;
				if (k22 <= 0) {
					l10 += i12;
					k12 += k13;
					i14 += i15;
					i18 += k17;
				} else {
					int i24 = s9.pkb;
					int k25 = (s9.qkb - i24) / k22;
					if (i1 < -um) {
						i24 += (-um - i1) * k25;
						i1 = -um;
						k22 = l20 - i1;
					}
					if (l20 > um) {
						int i21 = um;
						k22 = i21 - i1;
					}
					mh(oo, 0, 0, 0, go[k1], l10 + k15 * i1, k12 + i16 * i1, i14 + k16 * i1, k11, i13, k14, k22,
							i18 + i1, i24, k25);
					l10 += i12;
					k12 += k13;
					i14 += i15;
					i18 += k17;
				}
			}

			return;
		}
		for (int i2 = 0; i2 < xl; i2++) {
			if (yl[i2] == k1) {
				am = zl[i2];
				break;
			}
			if (i2 == xl - 1) {
				int k2 = (int) (Math.random() * (double) xl);
				yl[k2] = k1;
				k1 = -1 - k1;
				int j3 = (k1 >> 10 & 0x1f) * 8;
				int i4 = (k1 >> 5 & 0x1f) * 8;
				int k4 = (k1 & 0x1f) * 8;
				for (int i5 = 0; i5 < 256; i5++) {
					int i7 = i5 * i5;
					int j8 = (j3 * i7) / 0x10000;
					int k9 = (i4 * i7) / 0x10000;
					int i11 = (k4 * i7) / 0x10000;
					zl[k2][255 - i5] = (j8 << 16) + (k9 << 8) + i11;
				}

				am = zl[k2];
			}
		}

		int l2 = tm;
		int k3 = wm + qo * l2;
		byte byte0 = 1;
		if (yo) {
			if ((qo & 1) == 1) {
				qo++;
				k3 += l2;
			}
			l2 <<= 1;
			byte0 = 2;
		}
		if (h1.oh) {
			for (l = qo; l < ro; l += byte0) {
				s s1 = po[l];
				i1 = s1.nkb >> 8;
				int j5 = s1.okb >> 8;
				int j7 = j5 - i1;
				if (j7 <= 0) {
					k3 += l2;
				} else {
					int k8 = s1.pkb;
					int l9 = (s1.qkb - k8) / j7;
					if (i1 < -um) {
						k8 += (-um - i1) * l9;
						i1 = -um;
						j7 = j5 - i1;
					}
					if (j5 > um) {
						int k5 = um;
						j7 = k5 - i1;
					}
					bi(oo, -j7, k3 + i1, 0, am, k8, l9);
					k3 += l2;
				}
			}

			return;
		}
		if (jm) {
			for (l = qo; l < ro; l += byte0) {
				s s2 = po[l];
				i1 = s2.nkb >> 8;
				int l5 = s2.okb >> 8;
				int k7 = l5 - i1;
				if (k7 <= 0) {
					k3 += l2;
				} else {
					int l8 = s2.pkb;
					int i10 = (s2.qkb - l8) / k7;
					if (i1 < -um) {
						l8 += (-um - i1) * i10;
						i1 = -um;
						k7 = l5 - i1;
					}
					if (l5 > um) {
						int i6 = um;
						k7 = i6 - i1;
					}
					dh(oo, -k7, k3 + i1, 0, am, l8, i10);
					k3 += l2;
				}
			}

			return;
		}
		for (l = qo; l < ro; l += byte0) {
			s s3 = po[l];
			i1 = s3.nkb >> 8;
			int j6 = s3.okb >> 8;
			int l7 = j6 - i1;
			if (l7 <= 0) {
				k3 += l2;
			} else {
				int i9 = s3.pkb;
				int j10 = (s3.qkb - i9) / l7;
				if (i1 < -um) {
					i9 += (-um - i1) * j10;
					i1 = -um;
					l7 = j6 - i1;
				}
				if (j6 > um) {
					int k6 = um;
					l7 = k6 - i1;
				}
				rh(oo, -l7, k3 + i1, 0, am, i9, j10);
				k3 += l2;
			}
		}

	}

	private static void nh(int ai1[], int ai2[], int l, int i1, int j1, int k1, int l1, int i2, int j2, int k2, int l2,
			int i3, int j3, int k3) {
		if (l2 <= 0)
			return;
		int l3 = 0;
		int i4 = 0;
		int l4 = 0;
		if (l1 != 0) {
			l = j1 / l1 << 7;
			i1 = k1 / l1 << 7;
		}
		if (l < 0)
			l = 0;
		else if (l > 16256)
			l = 16256;
		j1 += i2;
		k1 += j2;
		l1 += k2;
		if (l1 != 0) {
			l3 = j1 / l1 << 7;
			i4 = k1 / l1 << 7;
		}
		if (l3 < 0)
			l3 = 0;
		else if (l3 > 16256)
			l3 = 16256;
		int j4 = l3 - l >> 4;
		int k4 = i4 - i1 >> 4;
		for (int i5 = l2 >> 4; i5 > 0; i5--) {
			l += j3 & 0x600000;
			l4 = j3 >> 23;
			j3 += k3;
			ai1[i3++] = ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4;
			l += j4;
			i1 += k4;
			ai1[i3++] = ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4;
			l += j4;
			i1 += k4;
			ai1[i3++] = ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4;
			l += j4;
			i1 += k4;
			ai1[i3++] = ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4;
			l += j4;
			i1 += k4;
			l = (l & 0x3fff) + (j3 & 0x600000);
			l4 = j3 >> 23;
			j3 += k3;
			ai1[i3++] = ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4;
			l += j4;
			i1 += k4;
			ai1[i3++] = ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4;
			l += j4;
			i1 += k4;
			ai1[i3++] = ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4;
			l += j4;
			i1 += k4;
			ai1[i3++] = ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4;
			l += j4;
			i1 += k4;
			l = (l & 0x3fff) + (j3 & 0x600000);
			l4 = j3 >> 23;
			j3 += k3;
			ai1[i3++] = ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4;
			l += j4;
			i1 += k4;
			ai1[i3++] = ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4;
			l += j4;
			i1 += k4;
			ai1[i3++] = ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4;
			l += j4;
			i1 += k4;
			ai1[i3++] = ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4;
			l += j4;
			i1 += k4;
			l = (l & 0x3fff) + (j3 & 0x600000);
			l4 = j3 >> 23;
			j3 += k3;
			ai1[i3++] = ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4;
			l += j4;
			i1 += k4;
			ai1[i3++] = ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4;
			l += j4;
			i1 += k4;
			ai1[i3++] = ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4;
			l += j4;
			i1 += k4;
			ai1[i3++] = ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4;
			l = l3;
			i1 = i4;
			j1 += i2;
			k1 += j2;
			l1 += k2;
			if (l1 != 0) {
				l3 = j1 / l1 << 7;
				i4 = k1 / l1 << 7;
			}
			if (l3 < 0)
				l3 = 0;
			else if (l3 > 16256)
				l3 = 16256;
			j4 = l3 - l >> 4;
			k4 = i4 - i1 >> 4;
		}

		for (int j5 = 0; j5 < (l2 & 0xf); j5++) {
			if ((j5 & 3) == 0) {
				l = (l & 0x3fff) + (j3 & 0x600000);
				l4 = j3 >> 23;
				j3 += k3;
			}
			ai1[i3++] = ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4;
			l += j4;
			i1 += k4;
		}

	}

	private static void ji(int ai1[], int ai2[], int l, int i1, int j1, int k1, int l1, int i2, int j2, int k2, int l2,
			int i3, int j3, int k3) {
		if (l2 <= 0)
			return;
		int l3 = 0;
		int i4 = 0;
		int l4 = 0;
		if (l1 != 0) {
			l = j1 / l1 << 7;
			i1 = k1 / l1 << 7;
		}
		if (l < 0)
			l = 0;
		else if (l > 16256)
			l = 16256;
		j1 += i2;
		k1 += j2;
		l1 += k2;
		if (l1 != 0) {
			l3 = j1 / l1 << 7;
			i4 = k1 / l1 << 7;
		}
		if (l3 < 0)
			l3 = 0;
		else if (l3 > 16256)
			l3 = 16256;
		int j4 = l3 - l >> 4;
		int k4 = i4 - i1 >> 4;
		for (int i5 = l2 >> 4; i5 > 0; i5--) {
			l += j3 & 0x600000;
			l4 = j3 >> 23;
			j3 += k3;
			ai1[i3++] = (ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4) + (ai1[i3] >> 1 & 0x7f7f7f);
			l += j4;
			i1 += k4;
			ai1[i3++] = (ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4) + (ai1[i3] >> 1 & 0x7f7f7f);
			l += j4;
			i1 += k4;
			ai1[i3++] = (ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4) + (ai1[i3] >> 1 & 0x7f7f7f);
			l += j4;
			i1 += k4;
			ai1[i3++] = (ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4) + (ai1[i3] >> 1 & 0x7f7f7f);
			l += j4;
			i1 += k4;
			l = (l & 0x3fff) + (j3 & 0x600000);
			l4 = j3 >> 23;
			j3 += k3;
			ai1[i3++] = (ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4) + (ai1[i3] >> 1 & 0x7f7f7f);
			l += j4;
			i1 += k4;
			ai1[i3++] = (ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4) + (ai1[i3] >> 1 & 0x7f7f7f);
			l += j4;
			i1 += k4;
			ai1[i3++] = (ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4) + (ai1[i3] >> 1 & 0x7f7f7f);
			l += j4;
			i1 += k4;
			ai1[i3++] = (ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4) + (ai1[i3] >> 1 & 0x7f7f7f);
			l += j4;
			i1 += k4;
			l = (l & 0x3fff) + (j3 & 0x600000);
			l4 = j3 >> 23;
			j3 += k3;
			ai1[i3++] = (ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4) + (ai1[i3] >> 1 & 0x7f7f7f);
			l += j4;
			i1 += k4;
			ai1[i3++] = (ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4) + (ai1[i3] >> 1 & 0x7f7f7f);
			l += j4;
			i1 += k4;
			ai1[i3++] = (ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4) + (ai1[i3] >> 1 & 0x7f7f7f);
			l += j4;
			i1 += k4;
			ai1[i3++] = (ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4) + (ai1[i3] >> 1 & 0x7f7f7f);
			l += j4;
			i1 += k4;
			l = (l & 0x3fff) + (j3 & 0x600000);
			l4 = j3 >> 23;
			j3 += k3;
			ai1[i3++] = (ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4) + (ai1[i3] >> 1 & 0x7f7f7f);
			l += j4;
			i1 += k4;
			ai1[i3++] = (ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4) + (ai1[i3] >> 1 & 0x7f7f7f);
			l += j4;
			i1 += k4;
			ai1[i3++] = (ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4) + (ai1[i3] >> 1 & 0x7f7f7f);
			l += j4;
			i1 += k4;
			ai1[i3++] = (ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4) + (ai1[i3] >> 1 & 0x7f7f7f);
			l = l3;
			i1 = i4;
			j1 += i2;
			k1 += j2;
			l1 += k2;
			if (l1 != 0) {
				l3 = j1 / l1 << 7;
				i4 = k1 / l1 << 7;
			}
			if (l3 < 0)
				l3 = 0;
			else if (l3 > 16256)
				l3 = 16256;
			j4 = l3 - l >> 4;
			k4 = i4 - i1 >> 4;
		}

		for (int j5 = 0; j5 < (l2 & 0xf); j5++) {
			if ((j5 & 3) == 0) {
				l = (l & 0x3fff) + (j3 & 0x600000);
				l4 = j3 >> 23;
				j3 += k3;
			}
			ai1[i3++] = (ai2[(i1 & 0x3f80) + (l >> 7)] >>> l4) + (ai1[i3] >> 1 & 0x7f7f7f);
			l += j4;
			i1 += k4;
		}

	}

	private static void ci(int ai1[], int l, int i1, int j1, int ai2[], int k1, int l1, int i2, int j2, int k2, int l2,
			int i3, int j3, int k3, int l3) {
		if (i3 <= 0)
			return;
		int i4 = 0;
		int j4 = 0;
		l3 <<= 2;
		if (i2 != 0) {
			i4 = k1 / i2 << 7;
			j4 = l1 / i2 << 7;
		}
		if (i4 < 0)
			i4 = 0;
		else if (i4 > 16256)
			i4 = 16256;
		for (int i5 = i3; i5 > 0; i5 -= 16) {
			k1 += j2;
			l1 += k2;
			i2 += l2;
			i1 = i4;
			j1 = j4;
			if (i2 != 0) {
				i4 = k1 / i2 << 7;
				j4 = l1 / i2 << 7;
			}
			if (i4 < 0)
				i4 = 0;
			else if (i4 > 16256)
				i4 = 16256;
			int k4 = i4 - i1 >> 4;
			int l4 = j4 - j1 >> 4;
			int j5 = k3 >> 23;
			i1 += k3 & 0x600000;
			k3 += l3;
			if (i5 < 16) {
				for (int k5 = 0; k5 < i5; k5++) {
					if ((l = ai2[(j1 & 0x3f80) + (i1 >> 7)] >>> j5) != 0)
						ai1[j3] = l;
					j3++;
					i1 += k4;
					j1 += l4;
					if ((k5 & 3) == 3) {
						i1 = (i1 & 0x3fff) + (k3 & 0x600000);
						j5 = k3 >> 23;
						k3 += l3;
					}
				}

			} else {
				if ((l = ai2[(j1 & 0x3f80) + (i1 >> 7)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				if ((l = ai2[(j1 & 0x3f80) + (i1 >> 7)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				if ((l = ai2[(j1 & 0x3f80) + (i1 >> 7)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				if ((l = ai2[(j1 & 0x3f80) + (i1 >> 7)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				i1 = (i1 & 0x3fff) + (k3 & 0x600000);
				j5 = k3 >> 23;
				k3 += l3;
				if ((l = ai2[(j1 & 0x3f80) + (i1 >> 7)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				if ((l = ai2[(j1 & 0x3f80) + (i1 >> 7)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				if ((l = ai2[(j1 & 0x3f80) + (i1 >> 7)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				if ((l = ai2[(j1 & 0x3f80) + (i1 >> 7)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				i1 = (i1 & 0x3fff) + (k3 & 0x600000);
				j5 = k3 >> 23;
				k3 += l3;
				if ((l = ai2[(j1 & 0x3f80) + (i1 >> 7)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				if ((l = ai2[(j1 & 0x3f80) + (i1 >> 7)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				if ((l = ai2[(j1 & 0x3f80) + (i1 >> 7)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				if ((l = ai2[(j1 & 0x3f80) + (i1 >> 7)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				i1 = (i1 & 0x3fff) + (k3 & 0x600000);
				j5 = k3 >> 23;
				k3 += l3;
				if ((l = ai2[(j1 & 0x3f80) + (i1 >> 7)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				if ((l = ai2[(j1 & 0x3f80) + (i1 >> 7)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				if ((l = ai2[(j1 & 0x3f80) + (i1 >> 7)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				if ((l = ai2[(j1 & 0x3f80) + (i1 >> 7)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
			}
		}

	}

	private static void ki(int ai1[], int ai2[], int l, int i1, int j1, int k1, int l1, int i2, int j2, int k2, int l2,
			int i3, int j3, int k3) {
		if (l2 <= 0)
			return;
		int l3 = 0;
		int i4 = 0;
		k3 <<= 2;
		if (l1 != 0) {
			l3 = j1 / l1 << 6;
			i4 = k1 / l1 << 6;
		}
		if (l3 < 0)
			l3 = 0;
		else if (l3 > 4032)
			l3 = 4032;
		for (int l4 = l2; l4 > 0; l4 -= 16) {
			j1 += i2;
			k1 += j2;
			l1 += k2;
			l = l3;
			i1 = i4;
			if (l1 != 0) {
				l3 = j1 / l1 << 6;
				i4 = k1 / l1 << 6;
			}
			if (l3 < 0)
				l3 = 0;
			else if (l3 > 4032)
				l3 = 4032;
			int j4 = l3 - l >> 4;
			int k4 = i4 - i1 >> 4;
			int i5 = j3 >> 20;
			l += j3 & 0xc0000;
			j3 += k3;
			if (l4 < 16) {
				for (int j5 = 0; j5 < l4; j5++) {
					ai1[i3++] = ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5;
					l += j4;
					i1 += k4;
					if ((j5 & 3) == 3) {
						l = (l & 0xfff) + (j3 & 0xc0000);
						i5 = j3 >> 20;
						j3 += k3;
					}
				}

			} else {
				ai1[i3++] = ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5;
				l += j4;
				i1 += k4;
				ai1[i3++] = ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5;
				l += j4;
				i1 += k4;
				ai1[i3++] = ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5;
				l += j4;
				i1 += k4;
				ai1[i3++] = ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5;
				l += j4;
				i1 += k4;
				l = (l & 0xfff) + (j3 & 0xc0000);
				i5 = j3 >> 20;
				j3 += k3;
				ai1[i3++] = ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5;
				l += j4;
				i1 += k4;
				ai1[i3++] = ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5;
				l += j4;
				i1 += k4;
				ai1[i3++] = ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5;
				l += j4;
				i1 += k4;
				ai1[i3++] = ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5;
				l += j4;
				i1 += k4;
				l = (l & 0xfff) + (j3 & 0xc0000);
				i5 = j3 >> 20;
				j3 += k3;
				ai1[i3++] = ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5;
				l += j4;
				i1 += k4;
				ai1[i3++] = ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5;
				l += j4;
				i1 += k4;
				ai1[i3++] = ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5;
				l += j4;
				i1 += k4;
				ai1[i3++] = ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5;
				l += j4;
				i1 += k4;
				l = (l & 0xfff) + (j3 & 0xc0000);
				i5 = j3 >> 20;
				j3 += k3;
				ai1[i3++] = ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5;
				l += j4;
				i1 += k4;
				ai1[i3++] = ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5;
				l += j4;
				i1 += k4;
				ai1[i3++] = ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5;
				l += j4;
				i1 += k4;
				ai1[i3++] = ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5;
			}
		}

	}

	private static void qh(int ai1[], int ai2[], int l, int i1, int j1, int k1, int l1, int i2, int j2, int k2, int l2,
			int i3, int j3, int k3) {
		if (l2 <= 0)
			return;
		int l3 = 0;
		int i4 = 0;
		k3 <<= 2;
		if (l1 != 0) {
			l3 = j1 / l1 << 6;
			i4 = k1 / l1 << 6;
		}
		if (l3 < 0)
			l3 = 0;
		else if (l3 > 4032)
			l3 = 4032;
		for (int l4 = l2; l4 > 0; l4 -= 16) {
			j1 += i2;
			k1 += j2;
			l1 += k2;
			l = l3;
			i1 = i4;
			if (l1 != 0) {
				l3 = j1 / l1 << 6;
				i4 = k1 / l1 << 6;
			}
			if (l3 < 0)
				l3 = 0;
			else if (l3 > 4032)
				l3 = 4032;
			int j4 = l3 - l >> 4;
			int k4 = i4 - i1 >> 4;
			int i5 = j3 >> 20;
			l += j3 & 0xc0000;
			j3 += k3;
			if (l4 < 16) {
				for (int j5 = 0; j5 < l4; j5++) {
					ai1[i3++] = (ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5) + (ai1[i3] >> 1 & 0x7f7f7f);
					l += j4;
					i1 += k4;
					if ((j5 & 3) == 3) {
						l = (l & 0xfff) + (j3 & 0xc0000);
						i5 = j3 >> 20;
						j3 += k3;
					}
				}

			} else {
				ai1[i3++] = (ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5) + (ai1[i3] >> 1 & 0x7f7f7f);
				l += j4;
				i1 += k4;
				ai1[i3++] = (ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5) + (ai1[i3] >> 1 & 0x7f7f7f);
				l += j4;
				i1 += k4;
				ai1[i3++] = (ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5) + (ai1[i3] >> 1 & 0x7f7f7f);
				l += j4;
				i1 += k4;
				ai1[i3++] = (ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5) + (ai1[i3] >> 1 & 0x7f7f7f);
				l += j4;
				i1 += k4;
				l = (l & 0xfff) + (j3 & 0xc0000);
				i5 = j3 >> 20;
				j3 += k3;
				ai1[i3++] = (ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5) + (ai1[i3] >> 1 & 0x7f7f7f);
				l += j4;
				i1 += k4;
				ai1[i3++] = (ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5) + (ai1[i3] >> 1 & 0x7f7f7f);
				l += j4;
				i1 += k4;
				ai1[i3++] = (ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5) + (ai1[i3] >> 1 & 0x7f7f7f);
				l += j4;
				i1 += k4;
				ai1[i3++] = (ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5) + (ai1[i3] >> 1 & 0x7f7f7f);
				l += j4;
				i1 += k4;
				l = (l & 0xfff) + (j3 & 0xc0000);
				i5 = j3 >> 20;
				j3 += k3;
				ai1[i3++] = (ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5) + (ai1[i3] >> 1 & 0x7f7f7f);
				l += j4;
				i1 += k4;
				ai1[i3++] = (ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5) + (ai1[i3] >> 1 & 0x7f7f7f);
				l += j4;
				i1 += k4;
				ai1[i3++] = (ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5) + (ai1[i3] >> 1 & 0x7f7f7f);
				l += j4;
				i1 += k4;
				ai1[i3++] = (ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5) + (ai1[i3] >> 1 & 0x7f7f7f);
				l += j4;
				i1 += k4;
				l = (l & 0xfff) + (j3 & 0xc0000);
				i5 = j3 >> 20;
				j3 += k3;
				ai1[i3++] = (ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5) + (ai1[i3] >> 1 & 0x7f7f7f);
				l += j4;
				i1 += k4;
				ai1[i3++] = (ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5) + (ai1[i3] >> 1 & 0x7f7f7f);
				l += j4;
				i1 += k4;
				ai1[i3++] = (ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5) + (ai1[i3] >> 1 & 0x7f7f7f);
				l += j4;
				i1 += k4;
				ai1[i3++] = (ai2[(i1 & 0xfc0) + (l >> 6)] >>> i5) + (ai1[i3] >> 1 & 0x7f7f7f);
			}
		}

	}

	private static void mh(int ai1[], int l, int i1, int j1, int ai2[], int k1, int l1, int i2, int j2, int k2, int l2,
			int i3, int j3, int k3, int l3) {
		if (i3 <= 0)
			return;
		int i4 = 0;
		int j4 = 0;
		l3 <<= 2;
		if (i2 != 0) {
			i4 = k1 / i2 << 6;
			j4 = l1 / i2 << 6;
		}
		if (i4 < 0)
			i4 = 0;
		else if (i4 > 4032)
			i4 = 4032;
		for (int i5 = i3; i5 > 0; i5 -= 16) {
			k1 += j2;
			l1 += k2;
			i2 += l2;
			i1 = i4;
			j1 = j4;
			if (i2 != 0) {
				i4 = k1 / i2 << 6;
				j4 = l1 / i2 << 6;
			}
			if (i4 < 0)
				i4 = 0;
			else if (i4 > 4032)
				i4 = 4032;
			int k4 = i4 - i1 >> 4;
			int l4 = j4 - j1 >> 4;
			int j5 = k3 >> 20;
			i1 += k3 & 0xc0000;
			k3 += l3;
			if (i5 < 16) {
				for (int k5 = 0; k5 < i5; k5++) {
					if ((l = ai2[(j1 & 0xfc0) + (i1 >> 6)] >>> j5) != 0)
						ai1[j3] = l;
					j3++;
					i1 += k4;
					j1 += l4;
					if ((k5 & 3) == 3) {
						i1 = (i1 & 0xfff) + (k3 & 0xc0000);
						j5 = k3 >> 20;
						k3 += l3;
					}
				}

			} else {
				if ((l = ai2[(j1 & 0xfc0) + (i1 >> 6)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				if ((l = ai2[(j1 & 0xfc0) + (i1 >> 6)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				if ((l = ai2[(j1 & 0xfc0) + (i1 >> 6)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				if ((l = ai2[(j1 & 0xfc0) + (i1 >> 6)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				i1 = (i1 & 0xfff) + (k3 & 0xc0000);
				j5 = k3 >> 20;
				k3 += l3;
				if ((l = ai2[(j1 & 0xfc0) + (i1 >> 6)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				if ((l = ai2[(j1 & 0xfc0) + (i1 >> 6)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				if ((l = ai2[(j1 & 0xfc0) + (i1 >> 6)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				if ((l = ai2[(j1 & 0xfc0) + (i1 >> 6)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				i1 = (i1 & 0xfff) + (k3 & 0xc0000);
				j5 = k3 >> 20;
				k3 += l3;
				if ((l = ai2[(j1 & 0xfc0) + (i1 >> 6)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				if ((l = ai2[(j1 & 0xfc0) + (i1 >> 6)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				if ((l = ai2[(j1 & 0xfc0) + (i1 >> 6)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				if ((l = ai2[(j1 & 0xfc0) + (i1 >> 6)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				i1 = (i1 & 0xfff) + (k3 & 0xc0000);
				j5 = k3 >> 20;
				k3 += l3;
				if ((l = ai2[(j1 & 0xfc0) + (i1 >> 6)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				if ((l = ai2[(j1 & 0xfc0) + (i1 >> 6)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				if ((l = ai2[(j1 & 0xfc0) + (i1 >> 6)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
				i1 += k4;
				j1 += l4;
				if ((l = ai2[(j1 & 0xfc0) + (i1 >> 6)] >>> j5) != 0)
					ai1[j3] = l;
				j3++;
			}
		}

	}

	private static void dh(int ai1[], int l, int i1, int j1, int ai2[], int k1, int l1) {
		if (l >= 0)
			return;
		l1 <<= 1;
		j1 = ai2[k1 >> 8 & 0xff];
		k1 += l1;
		int i2 = l / 8;
		for (int j2 = i2; j2 < 0; j2++) {
			ai1[i1++] = j1;
			ai1[i1++] = j1;
			j1 = ai2[k1 >> 8 & 0xff];
			k1 += l1;
			ai1[i1++] = j1;
			ai1[i1++] = j1;
			j1 = ai2[k1 >> 8 & 0xff];
			k1 += l1;
			ai1[i1++] = j1;
			ai1[i1++] = j1;
			j1 = ai2[k1 >> 8 & 0xff];
			k1 += l1;
			ai1[i1++] = j1;
			ai1[i1++] = j1;
			j1 = ai2[k1 >> 8 & 0xff];
			k1 += l1;
		}

		i2 = -(l % 8);
		for (int k2 = 0; k2 < i2; k2++) {
			ai1[i1++] = j1;
			if ((k2 & 1) == 1) {
				j1 = ai2[k1 >> 8 & 0xff];
				k1 += l1;
			}
		}

	}

	private static void bi(int ai1[], int l, int i1, int j1, int ai2[], int k1, int l1) {
		if (l >= 0)
			return;
		l1 <<= 2;
		j1 = ai2[k1 >> 8 & 0xff];
		k1 += l1;
		int i2 = l / 16;
		for (int j2 = i2; j2 < 0; j2++) {
			ai1[i1++] = j1 + (ai1[i1] >> 1 & 0x7f7f7f);
			ai1[i1++] = j1 + (ai1[i1] >> 1 & 0x7f7f7f);
			ai1[i1++] = j1 + (ai1[i1] >> 1 & 0x7f7f7f);
			ai1[i1++] = j1 + (ai1[i1] >> 1 & 0x7f7f7f);
			j1 = ai2[k1 >> 8 & 0xff];
			k1 += l1;
			ai1[i1++] = j1 + (ai1[i1] >> 1 & 0x7f7f7f);
			ai1[i1++] = j1 + (ai1[i1] >> 1 & 0x7f7f7f);
			ai1[i1++] = j1 + (ai1[i1] >> 1 & 0x7f7f7f);
			ai1[i1++] = j1 + (ai1[i1] >> 1 & 0x7f7f7f);
			j1 = ai2[k1 >> 8 & 0xff];
			k1 += l1;
			ai1[i1++] = j1 + (ai1[i1] >> 1 & 0x7f7f7f);
			ai1[i1++] = j1 + (ai1[i1] >> 1 & 0x7f7f7f);
			ai1[i1++] = j1 + (ai1[i1] >> 1 & 0x7f7f7f);
			ai1[i1++] = j1 + (ai1[i1] >> 1 & 0x7f7f7f);
			j1 = ai2[k1 >> 8 & 0xff];
			k1 += l1;
			ai1[i1++] = j1 + (ai1[i1] >> 1 & 0x7f7f7f);
			ai1[i1++] = j1 + (ai1[i1] >> 1 & 0x7f7f7f);
			ai1[i1++] = j1 + (ai1[i1] >> 1 & 0x7f7f7f);
			ai1[i1++] = j1 + (ai1[i1] >> 1 & 0x7f7f7f);
			j1 = ai2[k1 >> 8 & 0xff];
			k1 += l1;
		}

		i2 = -(l % 16);
		for (int k2 = 0; k2 < i2; k2++) {
			ai1[i1++] = j1 + (ai1[i1] >> 1 & 0x7f7f7f);
			if ((k2 & 3) == 3) {
				j1 = ai2[k1 >> 8 & 0xff];
				k1 += l1;
				k1 += l1;
			}
		}

	}

	private static void rh(int ai1[], int l, int i1, int j1, int ai2[], int k1, int l1) {
		if (l >= 0)
			return;
		l1 <<= 2;
		j1 = ai2[k1 >> 8 & 0xff];
		k1 += l1;
		int i2 = l / 16;
		for (int j2 = i2; j2 < 0; j2++) {
			ai1[i1++] = j1;
			ai1[i1++] = j1;
			ai1[i1++] = j1;
			ai1[i1++] = j1;
			j1 = ai2[k1 >> 8 & 0xff];
			k1 += l1;
			ai1[i1++] = j1;
			ai1[i1++] = j1;
			ai1[i1++] = j1;
			ai1[i1++] = j1;
			j1 = ai2[k1 >> 8 & 0xff];
			k1 += l1;
			ai1[i1++] = j1;
			ai1[i1++] = j1;
			ai1[i1++] = j1;
			ai1[i1++] = j1;
			j1 = ai2[k1 >> 8 & 0xff];
			k1 += l1;
			ai1[i1++] = j1;
			ai1[i1++] = j1;
			ai1[i1++] = j1;
			ai1[i1++] = j1;
			j1 = ai2[k1 >> 8 & 0xff];
			k1 += l1;
		}

		i2 = -(l % 16);
		for (int k2 = 0; k2 < i2; k2++) {
			ai1[i1++] = j1;
			if ((k2 & 3) == 3) {
				j1 = ai2[k1 >> 8 & 0xff];
				k1 += l1;
			}
		}

	}

	public void uh(int l, int i1, int j1, int k1, int l1, int i2, int j2) {
		dn = 1024 - k1 & 0x3ff;
		en = 1024 - l1 & 0x3ff;
		fn = 1024 - i2 & 0x3ff;
		int k2 = 0;
		int l2 = 0;
		int i3 = j2;
		if (k1 != 0) {
			int j3 = hm[k1];
			int i4 = hm[k1 + 1024];
			int l4 = l2 * i4 - i3 * j3 >> 15;
			i3 = l2 * j3 + i3 * i4 >> 15;
			l2 = l4;
		}
		if (l1 != 0) {
			int k3 = hm[l1];
			int j4 = hm[l1 + 1024];
			int i5 = i3 * k3 + k2 * j4 >> 15;
			i3 = i3 * j4 - k2 * k3 >> 15;
			k2 = i5;
		}
		if (i2 != 0) {
			int l3 = hm[i2];
			int k4 = hm[i2 + 1024];
			int j5 = l2 * l3 + k2 * k4 >> 15;
			l2 = l2 * k4 - k2 * l3 >> 15;
			k2 = j5;
		}
		an = l - k2;
		bn = i1 - l2;
		cn = j1 - i3;
	}

	private void ri(int l) {
		q q1 = ln[l];
		h h1 = q1.hfb;
		int i1 = q1.ifb;
		int ai1[] = h1.tg[i1];
		int j1 = h1.sg[i1];
		int k1 = h1.xg[i1];
		int i2 = h1.kg[ai1[0]];
		int j2 = h1.lg[ai1[0]];
		int k2 = h1.mg[ai1[0]];
		int l2 = h1.kg[ai1[1]] - i2;
		int i3 = h1.lg[ai1[1]] - j2;
		int j3 = h1.mg[ai1[1]] - k2;
		int k3 = h1.kg[ai1[2]] - i2;
		int l3 = h1.lg[ai1[2]] - j2;
		int i4 = h1.mg[ai1[2]] - k2;
		int j4 = i3 * i4 - l3 * j3;
		int k4 = j3 * k3 - i4 * l2;
		int l4 = l2 * l3 - k3 * i3;
		if (k1 == -1) {
			k1 = 0;
			for (; j4 > 25000 || k4 > 25000 || l4 > 25000 || j4 < -25000 || k4 < -25000 || l4 < -25000; l4 >>= 1) {
				k1++;
				j4 >>= 1;
				k4 >>= 1;
			}

			h1.xg[i1] = k1;
			h1.wg[i1] = (int) ((double) zm * Math.sqrt(j4 * j4 + k4 * k4 + l4 * l4));
		} else {
			j4 >>= k1;
			k4 >>= k1;
			l4 >>= k1;
		}
		q1.nfb = i2 * j4 + j2 * k4 + k2 * l4;
		q1.kfb = j4;
		q1.lfb = k4;
		q1.mfb = l4;
		int i5 = h1.mg[ai1[0]];
		int j5 = i5;
		int k5 = h1.ng[ai1[0]];
		int l5 = k5;
		int i6 = h1.og[ai1[0]];
		int j6 = i6;
		for (int k6 = 1; k6 < j1; k6++) {
			int l1 = h1.mg[ai1[k6]];
			if (l1 > j5)
				j5 = l1;
			else if (l1 < i5)
				i5 = l1;
			l1 = h1.ng[ai1[k6]];
			if (l1 > l5)
				l5 = l1;
			else if (l1 < k5)
				k5 = l1;
			l1 = h1.og[ai1[k6]];
			if (l1 > j6)
				j6 = l1;
			else if (l1 < i6)
				i6 = l1;
		}

		q1.ffb = i5;
		q1.gfb = j5;
		q1.bfb = k5;
		q1.dfb = l5;
		q1.cfb = i6;
		q1.efb = j6;
	}

	private void ti(int l) {
		q q1 = ln[l];
		h h1 = q1.hfb;
		int i1 = q1.ifb;
		int ai1[] = h1.tg[i1];
		int k1 = 0;
		int l1 = 0;
		int i2 = 1;
		int j2 = h1.kg[ai1[0]];
		int k2 = h1.lg[ai1[0]];
		int l2 = h1.mg[ai1[0]];
		h1.wg[i1] = 1;
		h1.xg[i1] = 0;
		q1.nfb = j2 * k1 + k2 * l1 + l2 * i2;
		q1.kfb = k1;
		q1.lfb = l1;
		q1.mfb = i2;
		int i3 = h1.mg[ai1[0]];
		int j3 = i3;
		int k3 = h1.ng[ai1[0]];
		int l3 = k3;
		if (h1.ng[ai1[1]] < k3)
			k3 = h1.ng[ai1[1]];
		else
			l3 = h1.ng[ai1[1]];
		int i4 = h1.og[ai1[1]];
		int j4 = h1.og[ai1[0]];
		int j1 = h1.mg[ai1[1]];
		if (j1 > j3)
			j3 = j1;
		else if (j1 < i3)
			i3 = j1;
		j1 = h1.ng[ai1[1]];
		if (j1 > l3)
			l3 = j1;
		else if (j1 < k3)
			k3 = j1;
		j1 = h1.og[ai1[1]];
		if (j1 > j4)
			j4 = j1;
		else if (j1 < i4)
			i4 = j1;
		q1.ffb = i3;
		q1.gfb = j3;
		q1.bfb = k3 - 20;
		q1.dfb = l3 + 20;
		q1.cfb = i4;
		q1.efb = j4;
	}

	private boolean ch(q q1, q q2) {
		if (q1.bfb >= q2.dfb)
			return true;
		if (q2.bfb >= q1.dfb)
			return true;
		if (q1.cfb >= q2.efb)
			return true;
		if (q2.cfb >= q1.efb)
			return true;
		if (q1.ffb >= q2.gfb)
			return true;
		if (q2.ffb > q1.gfb)
			return false;
		h h1 = q1.hfb;
		h h2 = q2.hfb;
		int l = q1.ifb;
		int i1 = q2.ifb;
		int ai1[] = h1.tg[l];
		int ai2[] = h2.tg[i1];
		int j1 = h1.sg[l];
		int k1 = h2.sg[i1];
		int j3 = h2.kg[ai2[0]];
		int k3 = h2.lg[ai2[0]];
		int l3 = h2.mg[ai2[0]];
		int i4 = q2.kfb;
		int j4 = q2.lfb;
		int k4 = q2.mfb;
		int l4 = h2.wg[i1];
		int i5 = q2.nfb;
		boolean flag = false;
		for (int j5 = 0; j5 < j1; j5++) {
			int l1 = ai1[j5];
			int l2 = (j3 - h1.kg[l1]) * i4 + (k3 - h1.lg[l1]) * j4 + (l3 - h1.mg[l1]) * k4;
			if ((l2 >= -l4 || i5 >= 0) && (l2 <= l4 || i5 <= 0))
				continue;
			flag = true;
			break;
		}

		if (!flag)
			return true;
		j3 = h1.kg[ai1[0]];
		k3 = h1.lg[ai1[0]];
		l3 = h1.mg[ai1[0]];
		i4 = q1.kfb;
		j4 = q1.lfb;
		k4 = q1.mfb;
		l4 = h1.wg[l];
		i5 = q1.nfb;
		flag = false;
		for (int k5 = 0; k5 < k1; k5++) {
			int i2 = ai2[k5];
			int i3 = (j3 - h2.kg[i2]) * i4 + (k3 - h2.lg[i2]) * j4 + (l3 - h2.mg[i2]) * k4;
			if ((i3 >= -l4 || i5 <= 0) && (i3 <= l4 || i5 >= 0))
				continue;
			flag = true;
			break;
		}

		if (!flag)
			return true;
		int ai3[];
		int ai4[];
		if (j1 == 2) {
			ai3 = new int[4];
			ai4 = new int[4];
			int l5 = ai1[0];
			int j2 = ai1[1];
			ai3[0] = h1.ng[l5] - 20;
			ai3[1] = h1.ng[j2] - 20;
			ai3[2] = h1.ng[j2] + 20;
			ai3[3] = h1.ng[l5] + 20;
			ai4[0] = ai4[3] = h1.og[l5];
			ai4[1] = ai4[2] = h1.og[j2];
		} else {
			ai3 = new int[j1];
			ai4 = new int[j1];
			for (int i6 = 0; i6 < j1; i6++) {
				int l6 = ai1[i6];
				ai3[i6] = h1.ng[l6];
				ai4[i6] = h1.og[l6];
			}

		}
		int ai5[];
		int ai6[];
		if (k1 == 2) {
			ai5 = new int[4];
			ai6 = new int[4];
			int j6 = ai2[0];
			int k2 = ai2[1];
			ai5[0] = h2.ng[j6] - 20;
			ai5[1] = h2.ng[k2] - 20;
			ai5[2] = h2.ng[k2] + 20;
			ai5[3] = h2.ng[j6] + 20;
			ai6[0] = ai6[3] = h2.og[j6];
			ai6[1] = ai6[2] = h2.og[k2];
		} else {
			ai5 = new int[k1];
			ai6 = new int[k1];
			for (int k6 = 0; k6 < k1; k6++) {
				int i7 = ai2[k6];
				ai5[k6] = h2.ng[i7];
				ai6[k6] = h2.og[i7];
			}

		}
		return !eh(ai3, ai4, ai5, ai6);
	}

	private boolean ah(q q1, q q2) {
		h h1 = q1.hfb;
		h h2 = q2.hfb;
		int l = q1.ifb;
		int i1 = q2.ifb;
		int ai1[] = h1.tg[l];
		int ai2[] = h2.tg[i1];
		int j1 = h1.sg[l];
		int k1 = h2.sg[i1];
		int l2 = h2.kg[ai2[0]];
		int i3 = h2.lg[ai2[0]];
		int j3 = h2.mg[ai2[0]];
		int k3 = q2.kfb;
		int l3 = q2.lfb;
		int i4 = q2.mfb;
		int j4 = h2.wg[i1];
		int k4 = q2.nfb;
		boolean flag = false;
		for (int l4 = 0; l4 < j1; l4++) {
			int l1 = ai1[l4];
			int j2 = (l2 - h1.kg[l1]) * k3 + (i3 - h1.lg[l1]) * l3 + (j3 - h1.mg[l1]) * i4;
			if ((j2 >= -j4 || k4 >= 0) && (j2 <= j4 || k4 <= 0))
				continue;
			flag = true;
			break;
		}

		if (!flag)
			return true;
		l2 = h1.kg[ai1[0]];
		i3 = h1.lg[ai1[0]];
		j3 = h1.mg[ai1[0]];
		k3 = q1.kfb;
		l3 = q1.lfb;
		i4 = q1.mfb;
		j4 = h1.wg[l];
		k4 = q1.nfb;
		flag = false;
		for (int i5 = 0; i5 < k1; i5++) {
			int i2 = ai2[i5];
			int k2 = (l2 - h2.kg[i2]) * k3 + (i3 - h2.lg[i2]) * l3 + (j3 - h2.mg[i2]) * i4;
			if ((k2 >= -j4 || k4 <= 0) && (k2 <= j4 || k4 >= 0))
				continue;
			flag = true;
			break;
		}

		return !flag;
	}

	public void yg(String s1, int l, int i1, int j1, GameApplet k1) {
		try {
			io = k1.wi(s1, "textures", j1);
			byte abyte0[] = o.lm("textures.txt", 0, io);
			GameSocket a1 = new GameSocket(abyte0);
			a1.qb();
			ao = a1.vb();
			bo = new String[ao];
			fo = new boolean[ao];
			eo = new long[ao];
			co = new int[ao];
			_flddo = new int[ao];
			go = new int[ao][];
			for (int l1 = 0; l1 < ao; l1++) {
				a1.qb();
				bo[l1] = a1.wb();
				co[l1] = a1.rb();
				_flddo[l1] = a1.vb();
				fo[l1] = false;
				go[l1] = null;
				eo[l1] = 0L;
			}

			ho = 0L;
			jo = new int[l][];
			ko = new int[i1][];
			for (int i2 = 0; i2 < ao; i2++)
				fi(i2);

			return;
		} catch (IOException _ex) {
			System.out.println("Error loading texture set");
		}
	}

	public void ai(int l) {
		if (go[l] == null)
			return;
		int ai1[] = go[l];
		for (int i1 = 0; i1 < 64; i1++) {
			int j1 = i1 + 4032;
			int k1 = ai1[j1];
			for (int i2 = 0; i2 < 63; i2++) {
				ai1[j1] = ai1[j1 - 64];
				j1 -= 64;
			}

			go[l][j1] = k1;
		}

		char c = '\u1000';
		for (int l1 = 0; l1 < c; l1++) {
			int j2 = ai1[l1];
			ai1[c + l1] = j2 - (j2 >>> 3) & 0xf8f8ff;
			ai1[c * 2 + l1] = j2 - (j2 >>> 2) & 0xf8f8ff;
			ai1[c * 3 + l1] = j2 - (j2 >>> 2) - (j2 >>> 3) & 0xf8f8ff;
		}

	}

	public void fi(int l) {
		if (l < 0)
			return;
		eo[l] = ho++;
		if (go[l] != null)
			return;
		if (_flddo[l] == 0) {
			for (int i1 = 0; i1 < jo.length; i1++)
				if (jo[i1] == null) {
					jo[i1] = new int[16384];
					go[l] = jo[i1];
					o.ym(bo[l] + ".tga", 0, io, lo);
					di(l);
					return;
				}

			long l1 = 1L << 30;
			int k1 = 0;
			for (int j2 = 0; j2 < ao; j2++)
				if (j2 != l && _flddo[j2] == 0 && go[j2] != null && eo[j2] < l1) {
					l1 = eo[j2];
					k1 = j2;
				}

			go[l] = go[k1];
			go[k1] = null;
			o.ym(bo[l] + ".tga", 0, io, lo);
			di(l);
			return;
		}
		for (int j1 = 0; j1 < ko.length; j1++)
			if (ko[j1] == null) {
				ko[j1] = new int[0x10000];
				go[l] = ko[j1];
				o.ym(bo[l] + ".tga", 0, io, lo);
				di(l);
				return;
			}

		long l2 = 1L << 30;
		int i2 = 0;
		for (int k2 = 0; k2 < ao; k2++)
			if (k2 != l && _flddo[k2] == 1 && go[k2] != null && eo[k2] < l2) {
				l2 = eo[k2];
				i2 = k2;
			}

		go[l] = go[i2];
		go[i2] = null;
		o.ym(bo[l] + ".tga", 0, io, lo);
		di(l);
	}

	public void gi(String s1) {
		try {
			GameSocket a1 = new GameSocket(s1 + "/textures.txt");
			a1.qb();
			ao = a1.vb();
			bo = new String[ao];
			fo = new boolean[ao];
			eo = new long[ao];
			co = new int[ao];
			_flddo = new int[ao];
			go = new int[ao][];
			for (int l = 0; l < ao; l++) {
				a1.qb();
				bo[l] = a1.wb();
				co[l] = a1.rb();
				_flddo[l] = a1.vb();
				fo[l] = false;
			}

			a1.xb();
			for (int i1 = 0; i1 < ao; i1++) {
				char c;
				if (_flddo[i1] == 0)
					c = '\u151B';
				else
					c = '\u451B';
				char c1;
				if (_flddo[i1] == 0)
					c1 = '@';
				else
					c1 = '\200';
				go[i1] = new int[c1 * c1 * 4];
				o.jm(s1 + "/" + bo[i1] + ".tga", lo, c);
				di(i1);
			}

			return;
		} catch (IOException _ex) {
			System.out.println("Error loading texture set");
		}
	}

	private void di(int l) {
		char c;
		if (_flddo[l] == 0)
			c = '@';
		else
			c = '\200';
		int ai1[] = go[l];
		int i1 = 0;
		for (int j1 = 0; j1 < 256; j1++)
			mo[j1] = ((lo[20 + j1 * 3] & 0xff) << 16) + ((lo[19 + j1 * 3] & 0xff) << 8) + (lo[18 + j1 * 3] & 0xff);

		for (int k1 = c - 1; k1 >= 0; k1--) {
			for (int l1 = 0; l1 < c; l1++) {
				int j2 = mo[lo[786 + l1 + k1 * c] & 0xff];
				if (j2 != 0xff00ff && co[l] != 0) {
					int l2 = j2 >> 16 & 0xff;
					int i3 = j2 >> 8 & 0xff;
					int j3 = j2 & 0xff;
					if (l2 == i3 && i3 == j3) {
						int k3 = co[l] >> 16 & 0xff;
						int l3 = co[l] >> 8 & 0xff;
						int i4 = co[l] & 0xff;
						j2 = ((l2 * k3 >> 8) << 16) + ((i3 * l3 >> 8) << 8) + (j3 * i4 >> 8);
					}
				}
				j2 &= 0xf8f8ff;
				if (j2 == 0)
					j2 = 1;
				else if (j2 == 0xf800ff) {
					j2 = 0;
					fo[l] = true;
				}
				ai1[i1++] = j2;
			}

		}

		for (int i2 = 0; i2 < i1; i2++) {
			int k2 = ai1[i2];
			ai1[i1 + i2] = k2 - (k2 >>> 3) & 0xf8f8ff;
			ai1[i1 * 2 + i2] = k2 - (k2 >>> 2) & 0xf8f8ff;
			ai1[i1 * 3 + i2] = k2 - (k2 >>> 2) - (k2 >>> 3) & 0xf8f8ff;
		}

	}

	public int oi(int l) {
		if (l == 0xbc614e)
			return 0;
		fi(l);
		if (l >= 0)
			return go[l][0];
		if (l < 0) {
			l = -(l + 1);
			int i1 = l >> 10 & 0x1f;
			int j1 = l >> 5 & 0x1f;
			int k1 = l & 0x1f;
			return (i1 << 19) + (j1 << 11) + (k1 << 3);
		} else {
			return 0;
		}
	}

	public void xh(int l, int i1, int j1) {
		if (l == 0 && i1 == 0 && j1 == 0)
			l = 32;
		for (int k1 = 0; k1 < gn; k1++)
			in[k1].se(l, i1, j1);

	}

	public void th(boolean flag, int l, int i1, int j1, int k1, int l1) {
		if (j1 == 0 && k1 == 0 && l1 == 0)
			j1 = 32;
		for (int i2 = 0; i2 < gn; i2++)
			in[i2].ne(flag, l, i1, j1, k1, l1);

	}

	public static int zh(int l, int i1, int j1) {
		return -1 - (l / 8) * 1024 - (i1 / 8) * 32 - j1 / 8;
	}

	public int hh(int l, int i1, int j1, int k1, int l1) {
		if (k1 == i1)
			return l;
		else
			return l + ((j1 - l) * (l1 - i1)) / (k1 - i1);
	}

	public boolean wh(int l, int i1, int j1, int k1, boolean flag) {
		if (flag && l <= j1 || l < j1) {
			if (l > k1)
				return true;
			if (i1 > j1)
				return true;
			if (i1 > k1)
				return true;
			return !flag;
		}
		if (l < k1)
			return true;
		if (i1 < j1)
			return true;
		if (i1 < k1)
			return true;
		else
			return flag;
	}

	public boolean lh(int l, int i1, int j1, boolean flag) {
		if (flag && l <= j1 || l < j1) {
			if (i1 > j1)
				return true;
			return !flag;
		}
		if (i1 < j1)
			return true;
		else
			return flag;
	}

	public boolean eh(int ai1[], int ai2[], int ai3[], int ai4[]) {
		int l = ai1.length;
		int i1 = ai3.length;
		byte byte0 = 0;
		int l20;
		int j21 = l20 = ai2[0];
		int j1 = 0;
		int i21;
		int k21 = i21 = ai4[0];
		int l1 = 0;
		for (int l21 = 1; l21 < l; l21++)
			if (ai2[l21] < l20) {
				l20 = ai2[l21];
				j1 = l21;
			} else if (ai2[l21] > j21)
				j21 = ai2[l21];

		for (int i22 = 1; i22 < i1; i22++)
			if (ai4[i22] < i21) {
				i21 = ai4[i22];
				l1 = i22;
			} else if (ai4[i22] > k21)
				k21 = ai4[i22];

		if (i21 >= j21)
			return false;
		if (l20 >= k21)
			return false;
		int k1;
		int i2;
		boolean flag;
		if (ai2[j1] < ai4[l1]) {
			for (k1 = j1; ai2[k1] < ai4[l1]; k1 = (k1 + 1) % l)
				;
			for (; ai2[j1] < ai4[l1]; j1 = ((j1 - 1) + l) % l)
				;
			int j2 = hh(ai1[(j1 + 1) % l], ai2[(j1 + 1) % l], ai1[j1], ai2[j1], ai4[l1]);
			int j7 = hh(ai1[((k1 - 1) + l) % l], ai2[((k1 - 1) + l) % l], ai1[k1], ai2[k1], ai4[l1]);
			int k11 = ai3[l1];
			flag = (j2 < k11) | (j7 < k11);
			if (lh(j2, j7, k11, flag))
				return true;
			i2 = (l1 + 1) % i1;
			l1 = ((l1 - 1) + i1) % i1;
			if (j1 == k1)
				byte0 = 1;
		} else {
			for (i2 = l1; ai4[i2] < ai2[j1]; i2 = (i2 + 1) % i1)
				;
			for (; ai4[l1] < ai2[j1]; l1 = ((l1 - 1) + i1) % i1)
				;
			int k2 = ai1[j1];
			int l11 = hh(ai3[(l1 + 1) % i1], ai4[(l1 + 1) % i1], ai3[l1], ai4[l1], ai2[j1]);
			int k16 = hh(ai3[((i2 - 1) + i1) % i1], ai4[((i2 - 1) + i1) % i1], ai3[i2], ai4[i2], ai2[j1]);
			flag = (k2 < l11) | (k2 < k16);
			if (lh(l11, k16, k2, !flag))
				return true;
			k1 = (j1 + 1) % l;
			j1 = ((j1 - 1) + l) % l;
			if (l1 == i2)
				byte0 = 2;
		}
		while (byte0 == 0)
			if (ai2[j1] < ai2[k1]) {
				if (ai2[j1] < ai4[l1]) {
					if (ai2[j1] < ai4[i2]) {
						int l2 = ai1[j1];
						int k7 = hh(ai1[((k1 - 1) + l) % l], ai2[((k1 - 1) + l) % l], ai1[k1], ai2[k1], ai2[j1]);
						int i12 = hh(ai3[(l1 + 1) % i1], ai4[(l1 + 1) % i1], ai3[l1], ai4[l1], ai2[j1]);
						int l16 = hh(ai3[((i2 - 1) + i1) % i1], ai4[((i2 - 1) + i1) % i1], ai3[i2], ai4[i2], ai2[j1]);
						if (wh(l2, k7, i12, l16, flag))
							return true;
						j1 = ((j1 - 1) + l) % l;
						if (j1 == k1)
							byte0 = 1;
					} else {
						int i3 = hh(ai1[(j1 + 1) % l], ai2[(j1 + 1) % l], ai1[j1], ai2[j1], ai4[i2]);
						int l7 = hh(ai1[((k1 - 1) + l) % l], ai2[((k1 - 1) + l) % l], ai1[k1], ai2[k1], ai4[i2]);
						int j12 = hh(ai3[(l1 + 1) % i1], ai4[(l1 + 1) % i1], ai3[l1], ai4[l1], ai4[i2]);
						int i17 = ai3[i2];
						if (wh(i3, l7, j12, i17, flag))
							return true;
						i2 = (i2 + 1) % i1;
						if (l1 == i2)
							byte0 = 2;
					}
				} else if (ai4[l1] < ai4[i2]) {
					int j3 = hh(ai1[(j1 + 1) % l], ai2[(j1 + 1) % l], ai1[j1], ai2[j1], ai4[l1]);
					int i8 = hh(ai1[((k1 - 1) + l) % l], ai2[((k1 - 1) + l) % l], ai1[k1], ai2[k1], ai4[l1]);
					int k12 = ai3[l1];
					int j17 = hh(ai3[((i2 - 1) + i1) % i1], ai4[((i2 - 1) + i1) % i1], ai3[i2], ai4[i2], ai4[l1]);
					if (wh(j3, i8, k12, j17, flag))
						return true;
					l1 = ((l1 - 1) + i1) % i1;
					if (l1 == i2)
						byte0 = 2;
				} else {
					int k3 = hh(ai1[(j1 + 1) % l], ai2[(j1 + 1) % l], ai1[j1], ai2[j1], ai4[i2]);
					int j8 = hh(ai1[((k1 - 1) + l) % l], ai2[((k1 - 1) + l) % l], ai1[k1], ai2[k1], ai4[i2]);
					int l12 = hh(ai3[(l1 + 1) % i1], ai4[(l1 + 1) % i1], ai3[l1], ai4[l1], ai4[i2]);
					int k17 = ai3[i2];
					if (wh(k3, j8, l12, k17, flag))
						return true;
					i2 = (i2 + 1) % i1;
					if (l1 == i2)
						byte0 = 2;
				}
			} else if (ai2[k1] < ai4[l1]) {
				if (ai2[k1] < ai4[i2]) {
					int l3 = hh(ai1[(j1 + 1) % l], ai2[(j1 + 1) % l], ai1[j1], ai2[j1], ai2[k1]);
					int k8 = ai1[k1];
					int i13 = hh(ai3[(l1 + 1) % i1], ai4[(l1 + 1) % i1], ai3[l1], ai4[l1], ai2[k1]);
					int l17 = hh(ai3[((i2 - 1) + i1) % i1], ai4[((i2 - 1) + i1) % i1], ai3[i2], ai4[i2], ai2[k1]);
					if (wh(l3, k8, i13, l17, flag))
						return true;
					k1 = (k1 + 1) % l;
					if (j1 == k1)
						byte0 = 1;
				} else {
					int i4 = hh(ai1[(j1 + 1) % l], ai2[(j1 + 1) % l], ai1[j1], ai2[j1], ai4[i2]);
					int l8 = hh(ai1[((k1 - 1) + l) % l], ai2[((k1 - 1) + l) % l], ai1[k1], ai2[k1], ai4[i2]);
					int j13 = hh(ai3[(l1 + 1) % i1], ai4[(l1 + 1) % i1], ai3[l1], ai4[l1], ai4[i2]);
					int i18 = ai3[i2];
					if (wh(i4, l8, j13, i18, flag))
						return true;
					i2 = (i2 + 1) % i1;
					if (l1 == i2)
						byte0 = 2;
				}
			} else if (ai4[l1] < ai4[i2]) {
				int j4 = hh(ai1[(j1 + 1) % l], ai2[(j1 + 1) % l], ai1[j1], ai2[j1], ai4[l1]);
				int i9 = hh(ai1[((k1 - 1) + l) % l], ai2[((k1 - 1) + l) % l], ai1[k1], ai2[k1], ai4[l1]);
				int k13 = ai3[l1];
				int j18 = hh(ai3[((i2 - 1) + i1) % i1], ai4[((i2 - 1) + i1) % i1], ai3[i2], ai4[i2], ai4[l1]);
				if (wh(j4, i9, k13, j18, flag))
					return true;
				l1 = ((l1 - 1) + i1) % i1;
				if (l1 == i2)
					byte0 = 2;
			} else {
				int k4 = hh(ai1[(j1 + 1) % l], ai2[(j1 + 1) % l], ai1[j1], ai2[j1], ai4[i2]);
				int j9 = hh(ai1[((k1 - 1) + l) % l], ai2[((k1 - 1) + l) % l], ai1[k1], ai2[k1], ai4[i2]);
				int l13 = hh(ai3[(l1 + 1) % i1], ai4[(l1 + 1) % i1], ai3[l1], ai4[l1], ai4[i2]);
				int k18 = ai3[i2];
				if (wh(k4, j9, l13, k18, flag))
					return true;
				i2 = (i2 + 1) % i1;
				if (l1 == i2)
					byte0 = 2;
			}
		while (byte0 == 1)
			if (ai2[j1] < ai4[l1]) {
				if (ai2[j1] < ai4[i2]) {
					int l4 = ai1[j1];
					int i14 = hh(ai3[(l1 + 1) % i1], ai4[(l1 + 1) % i1], ai3[l1], ai4[l1], ai2[j1]);
					int l18 = hh(ai3[((i2 - 1) + i1) % i1], ai4[((i2 - 1) + i1) % i1], ai3[i2], ai4[i2], ai2[j1]);
					return lh(i14, l18, l4, !flag);
				}
				int i5 = hh(ai1[(j1 + 1) % l], ai2[(j1 + 1) % l], ai1[j1], ai2[j1], ai4[i2]);
				int k9 = hh(ai1[((k1 - 1) + l) % l], ai2[((k1 - 1) + l) % l], ai1[k1], ai2[k1], ai4[i2]);
				int j14 = hh(ai3[(l1 + 1) % i1], ai4[(l1 + 1) % i1], ai3[l1], ai4[l1], ai4[i2]);
				int i19 = ai3[i2];
				if (wh(i5, k9, j14, i19, flag))
					return true;
				i2 = (i2 + 1) % i1;
				if (l1 == i2)
					byte0 = 0;
			} else if (ai4[l1] < ai4[i2]) {
				int j5 = hh(ai1[(j1 + 1) % l], ai2[(j1 + 1) % l], ai1[j1], ai2[j1], ai4[l1]);
				int l9 = hh(ai1[((k1 - 1) + l) % l], ai2[((k1 - 1) + l) % l], ai1[k1], ai2[k1], ai4[l1]);
				int k14 = ai3[l1];
				int j19 = hh(ai3[((i2 - 1) + i1) % i1], ai4[((i2 - 1) + i1) % i1], ai3[i2], ai4[i2], ai4[l1]);
				if (wh(j5, l9, k14, j19, flag))
					return true;
				l1 = ((l1 - 1) + i1) % i1;
				if (l1 == i2)
					byte0 = 0;
			} else {
				int k5 = hh(ai1[(j1 + 1) % l], ai2[(j1 + 1) % l], ai1[j1], ai2[j1], ai4[i2]);
				int i10 = hh(ai1[((k1 - 1) + l) % l], ai2[((k1 - 1) + l) % l], ai1[k1], ai2[k1], ai4[i2]);
				int l14 = hh(ai3[(l1 + 1) % i1], ai4[(l1 + 1) % i1], ai3[l1], ai4[l1], ai4[i2]);
				int k19 = ai3[i2];
				if (wh(k5, i10, l14, k19, flag))
					return true;
				i2 = (i2 + 1) % i1;
				if (l1 == i2)
					byte0 = 0;
			}
		while (byte0 == 2)
			if (ai4[l1] < ai2[j1]) {
				if (ai4[l1] < ai2[k1]) {
					int l5 = hh(ai1[(j1 + 1) % l], ai2[(j1 + 1) % l], ai1[j1], ai2[j1], ai4[l1]);
					int j10 = hh(ai1[((k1 - 1) + l) % l], ai2[((k1 - 1) + l) % l], ai1[k1], ai2[k1], ai4[l1]);
					int i15 = ai3[l1];
					return lh(l5, j10, i15, flag);
				}
				int i6 = hh(ai1[(j1 + 1) % l], ai2[(j1 + 1) % l], ai1[j1], ai2[j1], ai2[k1]);
				int k10 = ai1[k1];
				int j15 = hh(ai3[(l1 + 1) % i1], ai4[(l1 + 1) % i1], ai3[l1], ai4[l1], ai2[k1]);
				int l19 = hh(ai3[((i2 - 1) + i1) % i1], ai4[((i2 - 1) + i1) % i1], ai3[i2], ai4[i2], ai2[k1]);
				if (wh(i6, k10, j15, l19, flag))
					return true;
				k1 = (k1 + 1) % l;
				if (j1 == k1)
					byte0 = 0;
			} else if (ai2[j1] < ai2[k1]) {
				int j6 = ai1[j1];
				int l10 = hh(ai1[((k1 - 1) + l) % l], ai2[((k1 - 1) + l) % l], ai1[k1], ai2[k1], ai2[j1]);
				int k15 = hh(ai3[(l1 + 1) % i1], ai4[(l1 + 1) % i1], ai3[l1], ai4[l1], ai2[j1]);
				int i20 = hh(ai3[((i2 - 1) + i1) % i1], ai4[((i2 - 1) + i1) % i1], ai3[i2], ai4[i2], ai2[j1]);
				if (wh(j6, l10, k15, i20, flag))
					return true;
				j1 = ((j1 - 1) + l) % l;
				if (j1 == k1)
					byte0 = 0;
			} else {
				int k6 = hh(ai1[(j1 + 1) % l], ai2[(j1 + 1) % l], ai1[j1], ai2[j1], ai2[k1]);
				int i11 = ai1[k1];
				int l15 = hh(ai3[(l1 + 1) % i1], ai4[(l1 + 1) % i1], ai3[l1], ai4[l1], ai2[k1]);
				int j20 = hh(ai3[((i2 - 1) + i1) % i1], ai4[((i2 - 1) + i1) % i1], ai3[i2], ai4[i2], ai2[k1]);
				if (wh(k6, i11, l15, j20, flag))
					return true;
				k1 = (k1 + 1) % l;
				if (j1 == k1)
					byte0 = 0;
			}
		if (ai2[j1] < ai4[l1]) {
			int l6 = ai1[j1];
			int i16 = hh(ai3[(l1 + 1) % i1], ai4[(l1 + 1) % i1], ai3[l1], ai4[l1], ai2[j1]);
			int k20 = hh(ai3[((i2 - 1) + i1) % i1], ai4[((i2 - 1) + i1) % i1], ai3[i2], ai4[i2], ai2[j1]);
			return lh(i16, k20, l6, !flag);
		}
		int i7 = hh(ai1[(j1 + 1) % l], ai2[(j1 + 1) % l], ai1[j1], ai2[j1], ai4[l1]);
		int j11 = hh(ai1[((k1 - 1) + l) % l], ai2[((k1 - 1) + l) % l], ai1[k1], ai2[k1], ai4[l1]);
		int j16 = ai3[l1];
		return lh(i7, j11, j16, flag);
	}

	int xl;
	int yl[];
	int zl[][];
	int am[];
	public int bm;
	public int cm;
	public int dm;
	public int em;
	public int fm;
	public int gm;
	public static int hm[] = new int[2048];
	private static int im[] = new int[512];
	public boolean jm;
	public double km;
	public int lm;
	private boolean mm;
	private int nm;
	private int om;
	private int pm;
	private int qm;
	private h rm[];
	private int sm[];
	private int tm;
	private int um;
	private int vm;
	private int wm;
	private int xm;
	private int ym;
	private int zm;
	private int an;
	private int bn;
	private int cn;
	private int dn;
	private int en;
	private int fn;
	private int gn;
	private int hn;
	private h in[];
	private int jn[];
	private int kn;
	private q ln[];
	private int nn;
	private int on[];
	private int pn[];
	private int qn[];
	private int rn[];
	private int sn[];
	private int tn[];
	private int un[];
	public h vn;
	int ao;
	String bo[];
	int co[];
	int _flddo[];
	long eo[];
	boolean fo[];
	int go[][];
	private static long ho;
	byte io[];
	int jo[][];
	int ko[][];
	private static byte lo[];
	private static int mo[] = new int[256];
	GameImageProducer no;
	public int oo[];
	s po[];
	int qo;
	int ro;
	int so[];
	int to[];
	int uo[];
	int vo[];
	int wo[];
	int xo[];
	boolean yo;
	static int zo;
	static int ap;
	static int bp;
	static int cp;
	static int dp;
	static int ep;
	int fp;
	int gp;

}
