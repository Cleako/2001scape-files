import jagex.Util;
import jagex.client.Model;
import jagex.client.MultiGame;
import jagex.client.a;
import jagex.client.i;
import jagex.client.Pane;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;

@SuppressWarnings("serial")
public class Client extends MultiGame {

	public static void main(String args[]) {
		Client client = new Client();
		client.applet = false;
		client.startApplication(client.lab, client.mab + 22, "Runescape by Andrew Gower", false);
		client.ds = 10;
	}

	public void load() {
		if(applet) {
			String host = getDocumentBase().getHost().toLowerCase();
			if(!host.endsWith("127.0.0.1")) {
				invalidHost = true;
				return;
			}
		}
		int last = 0;
		for(int i = 0; i < 99; i++) {
			int next = i + 1;
			int amount = (int)((double)next + 300D * Math.pow(2D, (double)next / 7D));
			last += amount;
			experience[i] = last;
		}

		super.wf = 43594;
		super.jr = -11;
		MultiGame.rf = 1000;
		MultiGame.qf = false;
		MultiGame.clientRevision = Versions.network;
		loadConfig();
		pab = 2000;
		oab = pab + 100;
		tcb = oab + 50;
		rab = tcb + 300;
		eab = getGraphics();
		ij(50);
		gab = new g(lab, mab + 12, 2600, this);
		gab.pg = this;
		gab.dh(0, 0, lab, mab + 12);
		Pane.eu = false;
		Pane.fu = oab;
		feb = new Pane(gab, 5);
		int k1 = ((i) (gab)).ni - 199;
		byte byte0 = 36;
		geb = feb.nl(k1, byte0 + 24, 196, 90, 1, 500, true);
		jeb = new Pane(gab, 5);
		keb = jeb.nl(k1, byte0 + 40, 196, 126, 1, 500, true);
		neb = new Pane(gab, 5);
		oeb = neb.nl(k1, byte0 + 24, 196, 226, 1, 500, true);
		co();
		sp(true);
		fab = new a(gab, 15000, 15000, 1000);
		fab.vb(lab / 2, mab / 2, lab / 2, mab / 2, lab, nab);
		fab.g = 2400;
		fab.h = 2400;
		fab.i = 1;
		fab.j = 2300;
		fab.m(-50, -10, -50);
		abb = new d(fab, gab);
		abb.ie = pab;
		try {
		jo();
		wo();
		bo();
		ti(100, "Starting game...");
		wn();
		so();
		to();
		jp();
		rp();
		xn();
		yi();
		nn(); } catch (Exception e) { e.printStackTrace(); }
	}

	public void loadConfig() {
		if(applet()) {
			byte[] data = null;
			try {
				data = xi("config" + Versions.config + ".jag", "Configuration", 10);
			}
			catch(IOException ioexception) {
				System.out.println("Load error:" + ioexception);
				ioexception.printStackTrace();
			}
			Config.loadConfig(data);
		} else {
			lj(10, "Loading configuration");
			Config.loadConfig();
		}
	}

	public void co() {
		if(applet()) {
			byte archive[] = null;
			try {
				archive = xi("media" + Versions.media + ".jag", "2d graphics", 20);
			}
			catch(IOException ioexception) {
				System.out.println("Load error:" + ioexception);
			}
			gab.qg(archive, Util.archiveOffset("inv1.tga", archive), pab, true, false);
			gab.fh(archive, Util.archiveOffset("inv2.tga", archive), pab + 1, true, 1, 6, false);
			gab.qg(archive, Util.archiveOffset("bubble.tga", archive), pab + 9, true, false);
			gab.qg(archive, Util.archiveOffset("runescape.tga", archive), pab + 10, true, false);
			gab.ng(archive, Util.archiveOffset("splat.tga", archive), pab + 11, true, 3, false);
			gab.fh(archive, Util.archiveOffset("icon.tga", archive), pab + 14, true, 4, 2, false);
			gab.qg(archive, Util.archiveOffset("hbar.tga", archive), pab + 22, false, false);
			gab.qg(archive, Util.archiveOffset("hbar2.tga", archive), pab + 23, true, false);
			gab.qg(archive, Util.archiveOffset("compass.tga", archive), pab + 24, true, false);
			gab.ng(archive, Util.archiveOffset("scrollbar.tga", archive), oab, true, 2, false);
			gab.ng(archive, Util.archiveOffset("corners.tga", archive), oab + 2, true, 4, false);
			gab.ng(archive, Util.archiveOffset("arrows.tga", archive), oab + 6, true, 2, false);
			hab = decodeTga(Util.archiveEntry("hbar.tga", 0, archive));
			int l = Config.uk;
			for(int j1 = 1; l > 0; j1++) {
				int l1 = l;
				l -= 30;
				if(l1 > 30) {
					l1 = 30;
				}
				gab.fh(archive, Util.archiveOffset("objects" + j1 + ".tga", archive), tcb + (j1 - 1) * 30, true, 10, (l1 + 9) / 10, false);
			}

			gab.ng(archive, Util.archiveOffset("projectile.tga", archive), rab, true, Config.ho, false);
			return;
		}
		byte abyte1[] = new byte[0x186a0];
		ti(20, "Loading 2d graphics");
		try {
			Util.read("../gamedata/media/inv1.tga", abyte1, 0x186a0);
			gab.qg(abyte1, 0, pab, true, false);
			Util.read("../gamedata/media/inv2.tga", abyte1, 0x186a0);
			gab.fh(abyte1, 0, pab + 1, true, 1, 6, false);
			Util.read("../gamedata/media/bubble.tga", abyte1, 0x186a0);
			gab.qg(abyte1, 0, pab + 9, true, false);
			Util.read("../gamedata/media/runescape.tga", abyte1, 0x186a0);
			gab.qg(abyte1, 0, pab + 10, true, false);
			Util.read("../gamedata/media/splat.tga", abyte1, 0x186a0);
			gab.ng(abyte1, 0, pab + 11, true, 3, false);
			Util.read("../gamedata/media/icon.tga", abyte1, 0x186a0);
			gab.fh(abyte1, 0, pab + 14, true, 4, 2, false);
			Util.read("../gamedata/media/hbar.tga", abyte1, 0x186a0);
			gab.qg(abyte1, 0, pab + 22, false, false);
			hab = decodeTga(abyte1);
			Util.read("../gamedata/media/hbar2.tga", abyte1, 0x186a0);
			gab.qg(abyte1, 0, pab + 23, true, false);
			Util.read("../gamedata/media/compass.tga", abyte1, 0x186a0);
			gab.qg(abyte1, 0, pab + 24, true, false);
			Util.read("../gamedata/media/scrollbar.tga", abyte1, 0x186a0);
			gab.ng(abyte1, 0, oab, true, 2, false);
			Util.read("../gamedata/media/corners.tga", abyte1, 0x186a0);
			gab.ng(abyte1, 0, oab + 2, true, 4, false);
			Util.read("../gamedata/media/arrows.tga", abyte1, 0x186a0);
			gab.ng(abyte1, 0, oab + 6, true, 2, false);
			int i1 = Config.uk;
			for(int k1 = 1; i1 > 0; k1++) {
				int i2 = i1;
				i1 -= 30;
				if(i2 > 30) {
					i2 = 30;
				}
				Util.read("../gamedata/media/objects" + k1 + ".tga", abyte1, 0x186a0);
				gab.fh(abyte1, 0, tcb + (k1 - 1) * 30, true, 10, (i2 + 9) / 10, false);
			}

			Util.read("../gamedata/media/projectile.tga", abyte1, 0x186a0);
			gab.ng(abyte1, 0, rab, true, Config.ho, false);
			return;
		} catch(IOException _ex) {
			System.out.println("ERROR: in raw media loader");
		}
	}

	public void sp(boolean flag) {
		rlb = 0;
		slb = rlb;
		byte archive[] = null;
		if(applet() && flag) {
			String s = "entity" + Versions.entities + ".jag";
			try {
				archive = xi(s, "people and monsters", 30);
			}
			catch(IOException ioexception) {
				System.out.println("Load error:" + ioexception);
			}
		} else {
			ti(30, "Loading people and monsters");
		}
		int l = 0;
label0:
		for(int i1 = 0; i1 < Config.entityCount; i1++) {
			String s1 = Config.um[i1];
			for(int j1 = 0; j1 < i1; j1++) {
				if(!Config.um[j1].equalsIgnoreCase(s1)) {
					continue;
				}
				Config.zm[i1] = Config.zm[j1];
				continue label0;
			}

			if(flag) {
				if(applet()) {
					boolean flag1 = true;
					if(Config.wm[i1] != 0) {
						flag1 = false;
					}
					gab.ng(Util.archiveEntry(s1 + ".tga", 0, archive), 0, slb, true, 15, flag1);
					l += 15;
					if(Config.xm[i1] == 1) {
						gab.ng(Util.archiveEntry(s1 + "a.tga", 0, archive), 0, slb + 15, true, 3, true);
						l += 3;
					}
					if(Config.ym[i1] == 1) {
						gab.ng(Util.archiveEntry(s1 + "f.tga", 0, archive), 0, slb + 18, true, 9, true);
						l += 9;
					}
				} else {
					try {
						byte abyte1[] = new byte[0x493e0];
						Util.read("../gamedata/entity/" + s1 + ".tga", abyte1, 0x493e0);
						l += 15;
						boolean flag2 = true;
						if(Config.wm[i1] != 0) {
							flag2 = false;
						}
						gab.ng(abyte1, 0, slb, true, 15, flag2);
						if(Config.xm[i1] == 1) {
							Util.read("../gamedata/entity/" + s1 + "a.tga", abyte1, 0x493e0);
							l += 3;
							gab.ng(abyte1, 0, slb + 15, true, 3, true);
						}
						if(Config.ym[i1] == 1) {
							Util.read("../gamedata/entity/" + s1 + "f.tga", abyte1, 0x493e0);
							l += 9;
							gab.ng(abyte1, 0, slb + 18, true, 9, true);
						}
					}
					catch(IOException _ex) {
						System.out.println("ERROR: in raw entity loader - no:" + i1 + " " + s1);
					}
				}
			}
			Config.zm[i1] = slb;
			slb += 27;
		}

		System.out.println("Loaded: " + l + " frames of animation");
	}

	public void jo() {
		if(applet()) {
			fab.loadTextures("textures" + Versions.textures + ".jag", 7, 11, 50, this);
			return;
		} else {
			ti(50, "Loading textures");
			fab.loadTextures("../gamedata/textures");
			return;
		}
	}

	public void wo() {
		Config.matchModel("torcha2");
		Config.matchModel("torcha3");
		Config.matchModel("torcha4");
		Config.matchModel("skulltorcha2");
		Config.matchModel("skulltorcha3");
		Config.matchModel("skulltorcha4");
		Config.matchModel("firea2");
		Config.matchModel("firea3");
		if(applet()) {
			byte archive[] = null;
			try {
				archive = xi("models" + Versions.models + ".jag", "3d models", 70);
			}
			catch(IOException ioexception) {
				System.out.println("Load error:" + ioexception);
			}
			for(int i = 0; i < Config.modelCount; i++) {
				int offset = Util.archiveOffset(Config.qp[i] + ".ob2", archive);
				if(offset != 0) {
					models[i] = new Model(archive, offset);
				} else {
					models[i] = new Model(1, 1);
				}
			}

			return;
		}
		ti(70, "Loading 3d models");
		for(int i = 0; i < Config.modelCount; i++) {
			models[i] = new Model("../gamedata/models/" + Config.qp[i] + ".ob2");
		}
	}

	public void bo() {
		if(applet()) {
			abb.qe = null;
			try {
				abb.qe = xi("maps" + Versions.maps + ".jag", "map", 90);
				return;
			}
			catch(IOException ioexception) {
				System.out.println("Load error:" + ioexception);
			}
			return;
		} else {
			abb.ee = false;
			return;
		}
	}

	public void wn() {
		rfb = new Pane(gab, 10);
		sfb = rfb.uj(5, 269, 502, 56, 1, 20, true);
		tfb = rfb.cl(7, 324, 498, 14, 1, 80, false, true);
		ufb = rfb.uj(5, 269, 502, 56, 1, 20, true);
		vfb = rfb.uj(5, 269, 502, 56, 1, 20, true);
		rfb.sk(tfb);
	}

	public void fj() {
		if(invalidHost) {
			return;
		}
		if(wz) {
			return;
		}
		try {
			yz++;
			if(kab == 0) {
				super.kr = 0;
				gp();
			}
			if(kab == 1) {
				jhb++;
				super.kr++;
				uo();
			}
			if(tz > 0) {
				tz--;
			}
			if(sz && tz == 0) {
				sz = false;
				super.zf.yb();
				tz = 24;
			}
			super.hs = 0;
			super.js = 0;
			if(nfb > 0) {
				nfb--;
			}
			if(ofb > 0) {
				ofb--;
			}
			if(pfb > 0) {
				pfb--;
			}
			if(qfb > 0) {
				qfb--;
				return;
			}
		}
		catch(OutOfMemoryError _ex) {
			cp();
			wz = true;
		}
	}

	public void oj() {
		if(invalidHost) {
			Graphics g1 = getGraphics();
			g1.setColor(Color.black);
			g1.fillRect(0, 0, 512, 356);
			g1.setFont(new Font("Helvetica", 1, 20));
			g1.setColor(Color.white);
			g1.drawString("Error - unable to load game!", 50, 50);
			g1.drawString("To play RuneScape make sure you play from", 50, 100);
			g1.drawString("http://www.runescape.com", 50, 150);
			ij(1);
			return;
		}
		if(wz) {
			Graphics g2 = getGraphics();
			g2.setColor(Color.black);
			g2.fillRect(0, 0, 512, 356);
			g2.setFont(new Font("Helvetica", 1, 20));
			g2.setColor(Color.white);
			g2.drawString("Error - out of memory!", 50, 50);
			g2.drawString("Close ALL unnecessary programs", 50, 100);
			g2.drawString("and windows before loading the game", 50, 150);
			g2.drawString("RuneScape needs about 48meg of spare RAM", 50, 200);
			ij(1);
			return;
		}
		try {
			if(kab == 0) {
				gab.qj = false;
				mp();
			}
			if(kab == 1) {
				gab.qj = true;
				pn();
				return;
			}
		}
		catch(OutOfMemoryError _ex) {
			cp();
			wz = true;
		}
	}

	public void rj() {
		ue();
		cp();
	}

	public void cp() {
		try {
			if(gab != null) {
				gab.xg();
				gab.ti = null;
				gab = null;
			}
			if(fab != null) {
				fab.x();
				fab = null;
			}
			models = null;
			adb = null;
			jdb = null;
			ccb = null;
			dcb = null;
			ncb = null;
			ocb = null;
			fcb = null;
			if(abb != null) {
				abb.hf = null;
				abb._fldif = null;
				abb.jf = null;
				abb.kf = null;
				abb = null;
			}
			System.gc();
			return;
		}
		catch(Exception _ex) {
			return;
		}
	}

	public void yi() {
		eab.drawImage(hab, 0, 0, this);
	}

	public void hj(int l) {
		if(kab == 0) {
			if(nhb == 0) {
				ohb.ck(l);
			}
			if(nhb == 1) {
				rhb.ck(l);
			}
			if(nhb == 2) {
				aib.ck(l);
			}
		}
		if(kab == 1) {
			if(djb == 1) {
				ejb.ck(l);
				return;
			}
			if(djb == 2) {
				fjb.ck(l);
				return;
			}
			if(djb == 3) {
				gjb.ck(l);
				return;
			}
			if(djb == 4) {
				hjb.ck(l);
				return;
			}
			if(ulb) {
				nib.ck(l);
				return;
			}
			if(rkb) {
				if(vkb == -1) {
					skb.ck(l);
				}
				return;
			}
			if(fhb == 0 && ehb == 0) {
				rfb.ck(l);
			}
			if(fhb == 3 || fhb == 4 || fhb == 5) {
				fhb = 0;
			}
		}
	}

	public void qn() {
		super.zf.kc();
		sz = true;
		ye();
	}

	public void xn() {
		kab = 0;
		nhb = 0;
		lib = "";
		mib = "";
		jib = "Please enter a username:";
		kib = "*" + lib + "*";
		zbb = 0;
		lcb = 0;
	}

	public void ep() {
		super.ns = "";
		super.os = "";
	}

	public void trylogout() {
		if(kab == 0) {
			return;
		}
		if(ihb > 450) {
			up("@cya@You can't logout during combat!", 3);
			return;
		}
		if(ihb > 0) {
			up("@cya@You can't logout for 10 seconds after combat", 3);
			return;
		} else {
			super.zf.begin(6);
			super.zf.end();
			hhb = 1000;
			return;
		}
	}

	public void rp() {
		skb = new Pane(gab, 100);
		int l = 8;
		tkb = skb.qk(256, l, "@yel@Please provide 5 security questions in case you lose your password", 1, true);
		l += 22;
		skb.qk(256, l, "If you ever lose your password, you will need these to prove you own your account.", 1, true);
		l += 13;
		skb.qk(256, l, "Your answers are encrypted and are ONLY used for password recovery purposes.", 1, true);
		l += 22;
		skb.qk(256, l, "@ora@IMPORTANT:@whi@ To recover your password you must give the EXACT same answers you", 1, true);
		l += 13;
		skb.qk(256, l, "give here. If you think you might forget an answer, or someone else could guess the", 1, true);
		l += 13;
		skb.qk(256, l, "answer, then press the 'different question' button to get a better question.", 1, true);
		l += 35;
		for(int i1 = 0; i1 < 5; i1++) {
			skb.wj(170, l, 310, 30);
			blb[i1] = "~:" + alb[i1];
			wkb[i1] = skb.qk(170, l - 7, (i1 + 1) + ": " + jmb[alb[i1]], 1, true);
			xkb[i1] = skb.tj(170, l + 7, 310, 30, 1, 80, false, true);
			skb.wj(370, l, 80, 30);
			skb.qk(370, l - 7, "Different", 1, true);
			skb.qk(370, l + 7, "Question", 1, true);
			ykb[i1] = skb.wk(370, l, 80, 30);
			skb.wj(455, l, 80, 30);
			skb.qk(455, l - 7, "Enter own", 1, true);
			skb.qk(455, l + 7, "Question", 1, true);
			zkb[i1] = skb.wk(455, l, 80, 30);
			l += 35;
		}

		skb.sk(xkb[0]);
		l += 10;
		skb.wj(256, l, 250, 30);
		skb.qk(256, l, "Click here when finished", 4, true);
		ukb = skb.wk(256, l, 250, 30);
	}

	public void op() {
		if(vkb != -1) {
			if(super.os.length() > 0) {
				blb[vkb] = super.os;
				skb.ak(wkb[vkb], (vkb + 1) + ": " + blb[vkb]);
				skb.ak(xkb[vkb], "");
				vkb = -1;
			}
			return;
		}
		skb.jk(super.es, super.fs, super.hs, super.gs);
		for(int l = 0; l < 5; l++) {
			if(skb.lk(ykb[l])) {
				for(boolean flag = false; !flag;) {
					alb[l] = (alb[l] + 1) % jmb.length;
					flag = true;
					for(int j1 = 0; j1 < 5; j1++) {
						if(j1 != l && alb[j1] == alb[l]) {
							flag = false;
						}
					}

				}

				blb[l] = "~:" + alb[l];
				skb.ak(wkb[l], (l + 1) + ": " + jmb[alb[l]]);
				skb.ak(xkb[l], "");
			}
		}

		for(int i1 = 0; i1 < 5; i1++) {
			if(skb.lk(zkb[i1])) {
				vkb = i1;
				super.ns = "";
				super.os = "";
			}
		}

		if(skb.lk(ukb)) {
			for(int k1 = 0; k1 < 5; k1++) {
				String s = skb.kl(xkb[k1]);
				if(s == null || s.length() < 3) {
					skb.ak(tkb, "@yel@Please provide a longer answer to question: " + (k1 + 1));
					return;
				}
			}

			for(int l1 = 0; l1 < 5; l1++) {
				String s1 = skb.kl(xkb[l1]);
				for(int j2 = 0; j2 < l1; j2++) {
					String s3 = skb.kl(xkb[j2]);
					if(s1.equalsIgnoreCase(s3)) {
						skb.ak(tkb, "@yel@Each question must have a different answer");
						return;
					}
				}

			}

			super.zf.begin(208);
			for(int i2 = 0; i2 < 5; i2++) {
				String s2 = blb[i2];
				if(s2 == null || s2.length() == 0) {
					s2 = String.valueOf(i2 + 1);
				}
				if(s2.length() > 50) {
					s2 = s2.substring(0, 50);
				}
				super.zf.fc(s2.length());
				super.zf.zb(s2);
				super.zf.wb(Util.encode47(skb.kl(xkb[i2])));
			}

			super.zf.end();
			for(int k2 = 0; k2 < 5; k2++) {
				alb[k2] = k2;
				blb[k2] = "~:" + alb[k2];
				skb.ak(xkb[k2], "");
				skb.ak(wkb[k2], (k2 + 1) + ": " + jmb[alb[k2]]);
			}

			gab.ag();
			rkb = false;
		}
	}

	public void tp() {
		gab.mj = false;
		gab.ag();
		skb.ik();
		if(vkb != -1) {
			int l = 150;
			gab.sg(26, l, 460, 60, 0);
			gab.bg(26, l, 460, 60, 0xffffff);
			l += 22;
			gab.ug("Please enter your question", 256, l, 4, 0xffffff);
			l += 25;
			gab.ug(super.ns + "*", 256, l, 4, 0xffffff);
		}
		gab.rf(0, mab, pab + 22);
		gab.eg(eab, 0, 11);
	}

	public void jp() {
		ejb = new Pane(gab, 100);
		ejb.qk(256, 10, "Carry 5 extra objects at once in RuneScape!", 4, true);
		int l = 45;
		int i1 = 15;
		ejb.zj(256, (l + i1 * 7) - 4, 400, (int)((double)i1 * 16.5D - 9D));
		ejb.qk(256, l, "Please consider taking the time to answer a few questions and", 1, true);
		l += i1;
		ejb.qk(256, l, "sign up for some great email offers with Yoptin. It's free!", 1, true);
		l += i1;
		l += 12;
		ejb.qk(256, l, "You will be sent the information YOU want about products,", 1, true);
		l += i1;
		ejb.qk(256, l, "services, and special-offers that fit your interests.", 1, true);
		l += i1;
		ejb.qk(256, l, "Should you ever decide that you don't want", 1, true);
		l += i1;
		ejb.qk(256, l, "to receive any additional Yoptin email, you can simply optout.", 1, true);
		l += i1;
		l += 12;
		ejb.qk(256, l, "@yel@Signing up will help us to continue to run the game for", 1, true);
		l += i1;
		ejb.qk(256, l, "@yel@free. So as a special thank-you, as long as you are a yoptin", 1, true);
		l += i1;
		ejb.qk(256, l, "@yel@member your player will be able to carry 5 extra items at once!", 1, true);
		l += i1;
		l += 12;
		ejb.qk(256, l, "After you have signed up we will email you a special personalised", 1, true);
		l += i1;
		ejb.qk(256, l, "code which you can use to active this bonus, so be sure to enter all", 1, true);
		l += i1;
		ejb.qk(256, l, "your details correctly.", 1, true);
		l += i1;
		l += 30;
		ejb.wj(160, l, 150, 35);
		ejb.qk(160, l, "Yes, sounds great!", 1, true);
		xjb = ejb.wk(160, l, 150, 35);
		ejb.wj(352, l, 150, 35);
		ejb.qk(352, l, "No thank-you", 1, true);
		yjb = ejb.wk(352, l, 150, 35);
		l += 30;
		ejb.qk(256, l, "View Yoptin Privacy Statement", 1, true);
		zjb = ejb.wk(256, l, 250, 20);
		fjb = new Pane(gab, 100);
		jjb = fjb.qk(256, 5, "Please fill in the details below", 4, true);
		char c1 = 'U';
		char c2 = '\233';
		l = 39;
		String as[] = {
			"Mr", "Ms", "Miss", "Mrs"
		};
		fjb.zj(c1, l, c2, 35);
		kjb = fjb.xj(c1, l, as, 1, true);
		l += 39;
		fjb.zj(c1, l, c2, 35);
		fjb.qk(c1, l - 9, "First Name", 1, true);
		ljb = fjb.tj(c1, l + 9, c2, 20, 1, 30, false, true);
		fjb.sk(ljb);
		l += 39;
		c1 = '\367';
		c2 = '\233';
		l = 39;
		fjb.zj(c1, l, c2, 35);
		fjb.qk(c1, l - 9, "Surname", 1, true);
		mjb = fjb.tj(c1, l + 9, c2, 20, 1, 30, false, true);
		l += 39;
		fjb.zj(c1, l, c2, 35);
		fjb.qk(c1, l - 9, "Postcode", 1, true);
		njb = fjb.tj(c1, l + 9, c2, 20, 1, 20, false, true);
		l += 39;
		c1 = '\245';
		c2 = '\u013B';
		String as1[] = {
			"None", "1", "2", "3", "4", "5", "6 or more"
		};
		fjb.zj(c1, l, c2, 35);
		fjb.qk(c1, l - 9, "How many children live in your household?", 1, true);
		ojb = fjb.xj(c1, l + 9, as1, 1, true);
		l += 39;
		String as2[] = {
			"0", "1-2", "3-5", "6-10", "11-50", "51-100", "101-250", "251-500", "500+"
		};
		fjb.zj(c1, l, c2, 35);
		fjb.qk(c1, l - 9, "What is the size of your company?", 1, true);
		pjb = fjb.xj(c1, l + 9, as2, 1, true);
		l += 39;
		fjb.zj(c1, l, c2, 35);
		fkb = 15;
		fjb.gk(c1 - 95 - 25, l + 6, Pane.fu + 7);
		ikb = fjb.il(c1 - 95 - 25, l + 6, 20, 20);
		okb = fjb.qk(c1 - 95, l + 6, "15th", 1, true);
		fjb.gk((c1 - 95) + 25, l + 6, Pane.fu + 6);
		lkb = fjb.il((c1 - 95) + 25, l + 6, 20, 20);
		gkb = 6;
		fjb.gk(c1 - 40, l + 6, Pane.fu + 7);
		jkb = fjb.il(c1 - 40, l + 6, 20, 20);
		pkb = fjb.qk(c1, l + 6, "June", 1, true);
		fjb.gk(c1 + 40, l + 6, Pane.fu + 6);
		mkb = fjb.il(c1 + 40, l + 6, 20, 20);
		hkb = 1980;
		fjb.gk((c1 + 95) - 30, l + 6, Pane.fu + 7);
		kkb = fjb.il((c1 + 95) - 30, l + 6, 20, 20);
		qkb = fjb.qk(c1 + 95, l + 6, "1980", 1, true);
		fjb.gk(c1 + 95 + 30, l + 6, Pane.fu + 6);
		nkb = fjb.il(c1 + 95 + 30, l + 6, 20, 20);
		fjb.qk(c1, l - 9, "Date of birth", 1, true);
		l += 39;
		String as3[] = {
			"Would consider", "Have done already", "No"
		};
		fjb.zj(c1, l, c2, 35);
		fjb.qk(c1, l - 9, "Would you consider buying over the internet?", 1, true);
		qjb = fjb.xj(c1, l + 9, as3, 1, true);
		l += 39;
		fjb.zj(c1, l, c2, 35);
		fjb.qk(c1, l - 9, "Your email address", 1, true);
		rjb = fjb.tj(c1, l + 9, c2, 20, 0, 80, false, true);
		l += 39;
		c1 = 'U';
		c2 = '\233';
		String as4[] = {
			"Text", "Html", "AOL"
		};
		fjb.zj(c1, l, c2, 35);
		fjb.qk(c1, l - 9, "You receive email in", 1, true);
		sjb = fjb.xj(c1, l + 9, as4, 1, true);
		fjb.rk(sjb, 1);
		c1 = '\367';
		c2 = '\233';
		fjb.wj(c1, l, c2, 35);
		fjb.qk(c1, l, "Register me", 4, false);
		tjb = fjb.wk(c1, l, c2, 35);
		c1 = '\u01A4';
		l = 27;
		c2 = '\257';
		fjb.qk(c1, l - 11, "Country", 1, true);
		l += 40;
		String as5[] = {
			"Albania", "Andorra", "Armenia", "Austria", "Azerbaijan", "Belgium", "Bosnia and Herzogvina", "Belarus", "Bulgaria", "Canada", 
			"Croatia", "Cyprus", "Czech Republic", "Denmark", "Finland", "France", "Georgia", "Germany", "Gibraltar", "Greece", 
			"Hungary", "Iceland", "Ireland", "Italy", "Latvia", "LIechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Malta", 
			"Moldova", "Monaco", "Netherlands", "Norway", "Portugal", "Romania", "Russia", "Slovakia", "Slovenia", "Spain", 
			"Sweden", "Switzerland", "Turkey", "Ukraine", "United Kingdom", "United States", "Vatican", "Yugoslavia", "Other Country"
		};
		fjb.zj(c1, l, c2, 90);
		ujb = fjb.ml(c1 + 3, l, c2 - 6, 90, 0, 50, true);
		for(int j1 = 0; j1 < as5.length; j1++) {
			fjb.ll(ujb, j1, as5[j1]);
		}

		fjb.rk(ujb, as5.length - 5);
		fjb.xs[ujb] = as5.length - 7;
		l += 70;
		fjb.qk(c1, l - 11, "Your Profession", 1, true);
		l += 40;
		String as6[] = {
			"Craftsman/Tradesman", "Farmer", "Housewife", "Manual/Factory worker", "Middle Manager/Technician", "Office/Clerical", "Profession/Senior Manager", "Self-employed/Business owner", "Shop worker", "Student", 
			"Other"
		};
		fjb.zj(c1, l, c2, 90);
		vjb = fjb.ml(c1 + 3, l, c2 - 6, 90, 0, 50, true);
		for(int k1 = 0; k1 < as6.length; k1++) {
			fjb.ll(vjb, k1, as6[k1]);
		}

		l += 70;
		fjb.qk(c1, l - 11, "Industry sector", 1, true);
		l += 40;
		String as7[] = {
			"Agriculture", "Bank/Finance", "Computers/Software/Internet", "Construction", "Corporate Services/Consulting", "Health", "Legal/Insurance", "Manufacturing/Industries", "Marketing/Advertising", "News/Media", 
			"Supply/Trade", "Telecommunications/Networks", "Travel/Transportation", "Other"
		};
		fjb.zj(c1, l, c2, 90);
		wjb = fjb.ml(c1 + 3, l, c2 - 6, 90, 0, 50, true);
		for(int l1 = 0; l1 < as7.length; l1++) {
			fjb.ll(wjb, l1, as7[l1]);
		}

		l += 70;
		String as8[] = {
			"Home computing", "Internet", "Telecommunication, IT", "CD-Rom, Games", "Music, CD, Video, DVD", "Books, Newspapers", "Sport", "Beauty, Health, Fitness", "Food and Wine", "Gifts, Flowers", 
			"Pets", "Fashion, Clothing", "DIY, Interior Design, Gardening", "Cars, Motorbikes", "Leisure activities", "Travel, Holidays, Short Breaks", "Charity, Voluntary work", "Finances, Insurance", "Career, Jobs", "Lottery, Gambling, Betting"
		};
		gjb = new Pane(gab, 100);
		akb = gjb.qk(256, 10, "@yel@Please indicate your interests", 4, true);
		gjb.qk(256, 25, "Your bonus code will then be emailed to you", 4, true);
		c1 = ' ';
		l = 60;
		for(int i2 = 0; i2 < 20; i2++) {
			ckb[i2] = gjb.vj(c1, l - 9, 16);
			gjb.tk(c1 + 20, l, as8[i2], 3, true);
			l += 23;
			if(i2 == 9) {
				c1 = '\u0100';
				l = 60;
			}
		}

		gjb.wj(256, 310, 100, 32);
		gjb.qk(256, 310, "Ok", 4, true);
		bkb = gjb.wk(256, 310, 100, 32);
		hjb = new Pane(gab, 50);
		int j2 = 20;
		hjb.qk(250, j2, "Yoptin Privacy Statement", 5, true);
		j2 += 30;
		hjb.dl(40, j2 - 10, 420, 220);
		dkb = hjb.uj(50, j2, 400, 200, 1, 1000, true);
		j2 += 240;
		hjb.wj(256, j2, 170, 40);
		hjb.qk(256, j2, "Ok", 1, false);
		ekb = hjb.wk(256, j2, 170, 40);
		Pane o1 = hjb;
		int k2 = dkb;
		o1.pk(k2, "@yel@Privacy statement", false);
		o1.pk(k2, "Yoptin is a service offered by Consodata UK Ltd., a company that", false);
		o1.pk(k2, "specialises in the collection of households' and Internet users", false);
		o1.pk(k2, "consumption patterns. At Yoptin, we are committed to maintaining", false);
		o1.pk(k2, "your privacy. If we make changes to this policy, we will", false);
		o1.pk(k2, "immediately notify you by updating this statement on this web site.", false);
		o1.pk(k2, "", false);
		o1.pk(k2, "@yel@What will the data be used for?", false);
		o1.pk(k2, "Yoptin collects data about you, your interests and the subjects you", false);
		o1.pk(k2, "would like to receive email communications about. We use the", false);
		o1.pk(k2, "information you give us to make sure that what is sent to you is", false);
		o1.pk(k2, "relevant and as useful as possible. We may also merge the data we", false);
		o1.pk(k2, "gain from you with other data sources for profiling and marketing", false);
		o1.pk(k2, "purposes. Yoptin will manage the data under the guidelines of the", false);
		o1.pk(k2, "Data Protection Act 1988 and all information is processed in", false);
		o1.pk(k2, "accordance with the principles laid down by the Act.", false);
		o1.pk(k2, "Consodata UK Ltd will act as data controller and may pass the", false);
		o1.pk(k2, "information you provide to other companies, who will use the data", false);
		o1.pk(k2, "for their own market research and analysis purposes. These", false);
		o1.pk(k2, "carefully chosen companies may send you, by mail or other media,", false);
		o1.pk(k2, "details of their products and services where you have indicated", false);
		o1.pk(k2, "that you are happy to receive such communications.", false);
		o1.pk(k2, "", false);
		o1.pk(k2, "We comply with the standards, procedures and requirements laid", false);
		o1.pk(k2, "down by the UK Data Protection Act 1988, to ensure that the", false);
		o1.pk(k2, "personal information you give us is kept secure and processed", false);
		o1.pk(k2, "fairly and lawfully.", false);
		o1.pk(k2, "", false);
		o1.pk(k2, "@yel@Security", false);
		o1.pk(k2, "Your information is held on secure internal servers and is not", false);
		o1.pk(k2, "publicly available through this site.", false);
		o1.pk(k2, "", false);
		o1.pk(k2, "@yel@Your Feedback", false);
		o1.pk(k2, "By subscribing to this site, you consent to the information you", false);
		o1.pk(k2, "give us being processed for any of the purposes we have explained", false);
		o1.pk(k2, "above except where we have received your 'unsubscribe message'.", false);
		o1.pk(k2, "To unsubscribe simply send an email entitled UNSUBSCRIBE to us", false);
		o1.pk(k2, "at unsubscribe_uk@yoptin.com. If you have any queries or", false);
		o1.pk(k2, "complaints relating to our privacy policy, please email", false);
		o1.pk(k2, "membercare_uk@yoptin.com.", false);
		o1.pk(k2, "", false);
		o1.pk(k2, "Updated: 18 may 2001", false);
		o1.pk(k2, "To be reviewed: 18th October 2001", false);
	}

	public void eo() {
		gab.mj = false;
		gab.ag();
		if(djb == 1) {
			ejb.ik();
		}
		if(djb == 2) {
			fjb.ik();
		}
		if(djb == 3) {
			gjb.ik();
		}
		if(djb == 4) {
			hjb.ik();
		}
		gab.rf(0, mab, pab + 22);
		gab.eg(eab, 0, 11);
	}

	public void yn() {
		if(djb == 1) {
			ejb.jk(super.es, super.fs, super.hs, super.gs);
			if(ejb.lk(xjb)) {
				djb = 2;
			}
			if(ejb.lk(yjb)) {
				gab.ag();
				djb = 0;
				super.zf.begin(209);
				super.zf.end();
			}
			if(ejb.lk(zjb)) {
				djb = 4;
				return;
			}
		} else
		if(djb == 2) {
			String as[] = {
				"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", 
				"November", "December"
			};
			fjb.jk(super.es, super.fs, super.hs, super.gs);
			if(fjb.lk(ikb) && fkb > 1) {
				fkb--;
				if(fkb == 1) {
					fjb.ak(okb, "1st");
				} else
				if(fkb == 2) {
					fjb.ak(okb, "2nd");
				} else {
					fjb.ak(okb, fkb + "th");
				}
			}
			if(fjb.lk(lkb) && fkb < 31) {
				fkb++;
				if(fkb == 1) {
					fjb.ak(okb, "1st");
				} else
				if(fkb == 2) {
					fjb.ak(okb, "2nd");
				} else {
					fjb.ak(okb, fkb + "th");
				}
			}
			if(fjb.lk(jkb) && gkb > 1) {
				gkb--;
				fjb.ak(pkb, as[gkb - 1]);
			}
			if(fjb.lk(mkb) && gkb < 12) {
				gkb++;
				fjb.ak(pkb, as[gkb - 1]);
			}
			if(fjb.lk(kkb) && hkb > 1900) {
				hkb--;
				fjb.ak(qkb, String.valueOf(hkb));
			}
			if(fjb.lk(nkb) && hkb < 2000) {
				hkb++;
				fjb.ak(qkb, String.valueOf(hkb));
			}
			if(fjb.lk(tjb)) {
				if(fjb.kl(ljb) != null && fjb.kl(ljb).length() == 0 || fjb.kl(mjb) != null && fjb.kl(mjb).length() == 0 || fjb.kl(njb) != null && fjb.kl(njb).length() == 0 || fjb.kl(rjb) != null && fjb.kl(rjb).length() == 0 || fjb.sj(kjb) == -1 || fjb.sj(ojb) == -1 || fjb.sj(pjb) == -1 || fjb.sj(qjb) == -1 || fjb.sj(sjb) == -1 || fjb.sj(ujb) == -1 || fjb.sj(vjb) == -1 || fjb.sj(wjb) == -1) {
					fjb.ak(jjb, "@yel@Please fill in ALL requested details");
					return;
				}
				String s = fjb.kl(rjb);
				if(s.indexOf('@') == -1 || s.indexOf('.') == 1) {
					fjb.ak(jjb, "@yel@You must enter a valid email address");
					return;
				} else {
					djb = 3;
					return;
				}
			}
		} else
		if(djb == 3) {
			gjb.jk(super.es, super.fs, super.hs, super.gs);
			if(gjb.lk(bkb)) {
				boolean flag = false;
				for(int l = 0; l < 20; l++) {
					if(gjb.sj(ckb[l]) == 1) {
						flag = true;
					}
				}

				if(!flag) {
					gjb.ak(akb, "@red@Use the tickboxes below to indicate your interests");
					return;
				}
				super.zf.begin(210);
				super.zf.zb(Util.pad(fjb.kl(ljb), 30));
				super.zf.zb(Util.pad(fjb.kl(mjb), 30));
				super.zf.zb(Util.pad(fjb.kl(njb), 20));
				super.zf.zb(Util.pad(fjb.kl(rjb), 80));
				super.zf.fc(fjb.sj(kjb));
				super.zf.fc(fjb.sj(ojb));
				super.zf.fc(fjb.sj(pjb));
				super.zf.fc(fjb.sj(qjb));
				super.zf.fc(fjb.sj(sjb));
				super.zf.fc(fjb.sj(ujb));
				super.zf.fc(fjb.sj(vjb));
				super.zf.fc(fjb.sj(wjb));
				super.zf.fc(fkb);
				super.zf.fc(gkb);
				super.zf.ec(hkb);
				for(int i1 = 0; i1 < 20; i1++) {
					super.zf.fc(gjb.sj(ckb[i1]));
				}

				super.zf.end();
				djb = 0;
				return;
			}
		} else
		if(djb == 4) {
			hjb.jk(super.es, super.fs, super.hs, super.gs);
			if(hjb.lk(ekb)) {
				djb = 1;
			}
		}
	}

	public void to() {
		nib = new Pane(gab, 100);
		nib.qk(256, 10, "Design Your Character", 4, true);
		char c1 = '\214';
		int l = 34;
		nib.wj(c1, l, 200, 25);
		nib.qk(c1, l, "Appearance", 4, false);
		l += 15;
		nib.qk(c1 - 55, l + 110, "Front", 3, true);
		nib.qk(c1, l + 110, "Side", 3, true);
		nib.qk(c1 + 55, l + 110, "Back", 3, true);
		byte byte0 = 54;
		l += 145;
		nib.zj(c1 - byte0, l, 53, 41);
		nib.qk(c1 - byte0, l - 8, "Head", 1, true);
		nib.qk(c1 - byte0, l + 8, "Type", 1, true);
		nib.gk(c1 - byte0 - 40, l, Pane.fu + 7);
		oib = nib.wk(c1 - byte0 - 40, l, 20, 20);
		nib.gk((c1 - byte0) + 40, l, Pane.fu + 6);
		pib = nib.wk((c1 - byte0) + 40, l, 20, 20);
		nib.zj(c1 + byte0, l, 53, 41);
		nib.qk(c1 + byte0, l - 8, "Hair", 1, true);
		nib.qk(c1 + byte0, l + 8, "Color", 1, true);
		nib.gk((c1 + byte0) - 40, l, Pane.fu + 7);
		qib = nib.wk((c1 + byte0) - 40, l, 20, 20);
		nib.gk(c1 + byte0 + 40, l, Pane.fu + 6);
		rib = nib.wk(c1 + byte0 + 40, l, 20, 20);
		l += 50;
		nib.zj(c1 - byte0, l, 53, 41);
		nib.qk(c1 - byte0, l, "Gender", 1, true);
		nib.gk(c1 - byte0 - 40, l, Pane.fu + 7);
		sib = nib.wk(c1 - byte0 - 40, l, 20, 20);
		nib.gk((c1 - byte0) + 40, l, Pane.fu + 6);
		tib = nib.wk((c1 - byte0) + 40, l, 20, 20);
		nib.zj(c1 + byte0, l, 53, 41);
		nib.qk(c1 + byte0, l - 8, "Top", 1, true);
		nib.qk(c1 + byte0, l + 8, "Color", 1, true);
		nib.gk((c1 + byte0) - 40, l, Pane.fu + 7);
		uib = nib.wk((c1 + byte0) - 40, l, 20, 20);
		nib.gk(c1 + byte0 + 40, l, Pane.fu + 6);
		vib = nib.wk(c1 + byte0 + 40, l, 20, 20);
		l += 50;
		nib.zj(c1 - byte0, l, 53, 41);
		nib.qk(c1 - byte0, l - 8, "Skin", 1, true);
		nib.qk(c1 - byte0, l + 8, "Color", 1, true);
		nib.gk(c1 - byte0 - 40, l, Pane.fu + 7);
		wib = nib.wk(c1 - byte0 - 40, l, 20, 20);
		nib.gk((c1 - byte0) + 40, l, Pane.fu + 6);
		xib = nib.wk((c1 - byte0) + 40, l, 20, 20);
		nib.zj(c1 + byte0, l, 53, 41);
		nib.qk(c1 + byte0, l - 8, "Bottom", 1, true);
		nib.qk(c1 + byte0, l + 8, "Color", 1, true);
		nib.gk((c1 + byte0) - 40, l, Pane.fu + 7);
		yib = nib.wk((c1 + byte0) - 40, l, 20, 20);
		nib.gk(c1 + byte0 + 40, l, Pane.fu + 6);
		zib = nib.wk(c1 + byte0 + 40, l, 20, 20);
		c1 = '\u0174';
		l = 35;
		nib.wj(c1, l, 200, 25);
		nib.qk(c1, l, "Character Type", 4, false);
		l += 22;
		nib.qk(c1, l, "Each character type has different starting", 0, true);
		l += 13;
		nib.qk(c1, l, "bonuses. But the choice you make here", 0, true);
		l += 13;
		nib.qk(c1, l, "isn't permanent, and will change depending", 0, true);
		l += 13;
		nib.qk(c1, l, "on how you play the game.", 0, true);
		l += 73;
		nib.zj(c1, l, 215, 125);
		String as[] = {
			"Adventurer", "Warrior", "Wizard", "Ranger", "Miner"
		};
		bjb = nib.dk(c1, l + 2, as, 3, true);
		l += 82;
		nib.wj(c1, l, 200, 30);
		nib.qk(c1, l, "Start Game", 4, false);
		ajb = nib.wk(c1, l, 200, 30);
	}

	public void xp() {
		gab.mj = false;
		gab.ag();
		nib.ik();
		char c1 = '\214';
		byte byte0 = 50;
		gab.ih(c1 - 32 - 55, byte0, 64, 102, Config.zm[xlb], dmb[amb]);
		gab.mh(c1 - 32 - 55, byte0, 64, 102, Config.zm[wlb], dmb[zlb], fmb[bmb], 0, false);
		gab.mh(c1 - 32 - 55, byte0, 64, 102, Config.zm[vlb], emb[ylb], fmb[bmb], 0, false);
		gab.ih(c1 - 32, byte0, 64, 102, Config.zm[xlb] + 6, dmb[amb]);
		gab.mh(c1 - 32, byte0, 64, 102, Config.zm[wlb] + 6, dmb[zlb], fmb[bmb], 0, false);
		gab.mh(c1 - 32, byte0, 64, 102, Config.zm[vlb] + 6, emb[ylb], fmb[bmb], 0, false);
		gab.ih((c1 - 32) + 55, byte0, 64, 102, Config.zm[xlb] + 12, dmb[amb]);
		gab.mh((c1 - 32) + 55, byte0, 64, 102, Config.zm[wlb] + 12, dmb[zlb], fmb[bmb], 0, false);
		gab.mh((c1 - 32) + 55, byte0, 64, 102, Config.zm[vlb] + 12, emb[ylb], fmb[bmb], 0, false);
		gab.rf(0, mab, pab + 22);
		gab.eg(eab, 0, 11);
	}

	public void go() {
		nib.jk(super.es, super.fs, super.hs, super.gs);
		if(nib.lk(oib)) {
			do {
				vlb = ((vlb - 1) + Config.entityCount) % Config.entityCount;
			} while((Config.wm[vlb] & 3) != 1 || (Config.wm[vlb] & 4 * cmb) == 0);
		}
		if(nib.lk(pib)) {
			do {
				vlb = (vlb + 1) % Config.entityCount;
			} while((Config.wm[vlb] & 3) != 1 || (Config.wm[vlb] & 4 * cmb) == 0);
		}
		if(nib.lk(qib)) {
			ylb = ((ylb - 1) + emb.length) % emb.length;
		}
		if(nib.lk(rib)) {
			ylb = (ylb + 1) % emb.length;
		}
		if(nib.lk(sib) || nib.lk(tib)) {
			for(cmb = 3 - cmb; (Config.wm[vlb] & 3) != 1 || (Config.wm[vlb] & 4 * cmb) == 0; vlb = (vlb + 1) % Config.entityCount) { }
			for(; (Config.wm[wlb] & 3) != 2 || (Config.wm[wlb] & 4 * cmb) == 0; wlb = (wlb + 1) % Config.entityCount) { }
		}
		if(nib.lk(uib)) {
			zlb = ((zlb - 1) + dmb.length) % dmb.length;
		}
		if(nib.lk(vib)) {
			zlb = (zlb + 1) % dmb.length;
		}
		if(nib.lk(wib)) {
			bmb = ((bmb - 1) + fmb.length) % fmb.length;
		}
		if(nib.lk(xib)) {
			bmb = (bmb + 1) % fmb.length;
		}
		if(nib.lk(yib)) {
			amb = ((amb - 1) + dmb.length) % dmb.length;
		}
		if(nib.lk(zib)) {
			amb = (amb + 1) % dmb.length;
		}
		if(nib.lk(ajb)) {
			super.zf.begin(236);
			super.zf.fc(cmb);
			super.zf.fc(vlb);
			super.zf.fc(wlb);
			super.zf.fc(xlb);
			super.zf.fc(ylb);
			super.zf.fc(zlb);
			super.zf.fc(amb);
			super.zf.fc(bmb);
			super.zf.fc(nib.sj(bjb));
			super.zf.end();
			gab.ag();
			ulb = false;
		}
	}

	public void so() {
		ohb = new Pane(gab, 50);
		int l = 40;
		ohb.qk(256, 200 + l, "Click on an option", 5, true);
		ohb.wj(156, 240 + l, 120, 35);
		ohb.wj(356, 240 + l, 120, 35);
		ohb.qk(156, 240 + l, "New User", 5, false);
		ohb.qk(356, 240 + l, "Existing User", 5, false);
		phb = ohb.wk(156, 240 + l, 120, 35);
		qhb = ohb.wk(356, 240 + l, 120, 35);
		rhb = new Pane(gab, 50);
		l = 5;
		shb = rhb.qk(256, l + 8, "To create an account please enter all the requested details", 4, true);
		l += 25;
		rhb.wj(256, l + 17, 250, 34);
		rhb.qk(256, l + 8, "Choose a Username", 4, false);
		whb = rhb.tj(256, l + 25, 200, 40, 4, 12, false, false);
		rhb.sk(whb);
		l += 40;
		rhb.wj(141, l + 17, 220, 34);
		rhb.qk(141, l + 8, "Choose a Password", 4, false);
		xhb = rhb.tj(141, l + 25, 220, 40, 4, 20, true, false);
		rhb.wj(371, l + 17, 220, 34);
		rhb.qk(371, l + 8, "Confirm Password", 4, false);
		yhb = rhb.tj(371, l + 25, 220, 40, 4, 20, true, false);
		l += 40;
		rhb.dl(46, l, 420, 150);
		thb = rhb.uj(56, l + 5, 400, 140, 1, 1000, true);
		yo(rhb, thb);
		l += 160;
		zhb = rhb.bl(120, l, 14);
		rhb.tk(135, l, "I agree to the terms+conditions above", 4, true);
		l += 20;
		rhb.wj(156, l + 17, 150, 34);
		rhb.qk(156, l + 17, "Submit", 5, false);
		vhb = rhb.wk(156, l + 17, 150, 34);
		rhb.wj(356, l + 17, 150, 34);
		rhb.qk(356, l + 17, "Cancel", 5, false);
		uhb = rhb.wk(356, l + 17, 150, 34);
		aib = new Pane(gab, 50);
		l = 230;
		bib = aib.qk(256, l - 10, "Please enter your username and password", 4, true);
		l += 28;
		aib.wj(146, l, 200, 40);
		aib.qk(146, l - 10, "Username:", 4, false);
		cib = aib.tj(146, l + 10, 200, 40, 4, 12, false, false);
		l += 47;
		aib.wj(195, l, 200, 40);
		aib.qk(195, l - 10, "Password:", 4, false);
		dib = aib.tj(195, l + 10, 200, 40, 4, 20, true, false);
		l -= 45;
		aib.wj(410, l, 110, 25);
		aib.qk(410, l, "Ok", 4, false);
		eib = aib.wk(410, l, 110, 25);
		l += 30;
		aib.wj(410, l, 110, 25);
		aib.qk(410, l, "Cancel", 4, false);
		fib = aib.wk(410, l, 110, 25);
		aib.sk(cib);
	}

	public void mp() {
		gab.mj = false;
		gab.ag();
		if(nhb == 0 || nhb == 2) {
			int l = (yz * 2) % 3072;
			if(l < 1024) {
				gab.rf(0, 10, 2500);
				if(l > 768) {
					gab.hg(0, 10, 2501, l - 768);
				}
			} else
			if(l < 2048) {
				gab.rf(0, 10, 2501);
				if(l > 1792) {
					gab.hg(0, 10, pab + 10, l - 1792);
				}
			} else {
				gab.rf(0, 10, pab + 10);
				if(l > 2816) {
					gab.hg(0, 10, 2500, l - 2816);
				}
			}
		}
		if(nhb == 0) {
			ohb.ik();
		}
		if(nhb == 1) {
			rhb.ik();
		}
		if(nhb == 2) {
			aib.ik();
		}
		gab.rf(0, mab, pab + 22);
		gab.eg(eab, 0, 11);
	}

	public void nn() {
		int l = 0;
		byte byte0 = 50;
		byte byte1 = 50;
		abb.md(byte0 * 48 + 23, byte1 * 48 + 23, l);
		abb.yd(models);
		char c1 = '\u2600';
		char c2 = '\u1900';
		char c3 = '\u044C';
		char c4 = '\u0378';
		fab.g = 4100;
		fab.h = 4100;
		fab.i = 1;
		fab.j = 4000;
		fab.g(c1, -abb.vd(c1, c2), c2, 912, c4, 0, c3 * 2);
		fab.i();
		gab.jg();
		gab.jg();
		gab.sg(0, 0, 512, 6, 0);
		for(int i1 = 6; i1 >= 1; i1--) {
			gab.pg(0, i1, 0, i1, 512, 8);
		}

		gab.sg(0, 194, 512, 20, 0);
		for(int j1 = 6; j1 >= 1; j1--) {
			gab.pg(0, j1, 0, 194 - j1, 512, 8);
		}

		gab.rf(15, 15, pab + 10);
		gab.dg(2500, 0, 0, 512, 200);
		c1 = '\u2400';
		c2 = '\u2400';
		c3 = '\u044C';
		c4 = '\u0378';
		fab.g = 4100;
		fab.h = 4100;
		fab.i = 1;
		fab.j = 4000;
		fab.g(c1, -abb.vd(c1, c2), c2, 912, c4, 0, c3 * 2);
		fab.i();
		gab.jg();
		gab.jg();
		gab.sg(0, 0, 512, 6, 0);
		for(int k1 = 6; k1 >= 1; k1--) {
			gab.pg(0, k1, 0, k1, 512, 8);
		}

		gab.sg(0, 194, 512, 20, 0);
		for(int l1 = 6; l1 >= 1; l1--) {
			gab.pg(0, l1, 0, 194 - l1, 512, 8);
		}

		gab.rf(15, 15, pab + 10);
		gab.dg(2501, 0, 0, 512, 200);
		for(int i2 = 0; i2 < 64; i2++) {
			fab.cb(abb.jf[0][i2]);
			fab.cb(abb._fldif[1][i2]);
			fab.cb(abb.jf[1][i2]);
			fab.cb(abb._fldif[2][i2]);
			fab.cb(abb.jf[2][i2]);
		}

		c1 = '\u2B80';
		c2 = '\u2880';
		c3 = '\u01F4';
		c4 = '\u0178';
		fab.g = 4100;
		fab.h = 4100;
		fab.i = 1;
		fab.j = 4000;
		fab.g(c1, -abb.vd(c1, c2), c2, 912, c4, 0, c3 * 2);
		fab.i();
		gab.jg();
		gab.jg();
		gab.sg(0, 0, 512, 6, 0);
		for(int j2 = 6; j2 >= 1; j2--) {
			gab.pg(0, j2, 0, j2, 512, 8);
		}

		gab.sg(0, 194, 512, 20, 0);
		for(int k2 = 6; k2 >= 1; k2--) {
			gab.pg(0, k2, 0, 194, 512, 8);
		}

		gab.rf(15, 15, pab + 10);
		gab.dg(pab + 10, 0, 0, 512, 200);
	}

	public void gp() {
		if(nhb == 0) {
			ohb.jk(super.es, super.fs, super.hs, super.gs);
			if(ohb.lk(phb)) {
				nhb = 1;
				rhb.ak(whb, "");
				rhb.ak(xhb, "");
				rhb.ak(yhb, "");
				rhb.sk(whb);
				rhb.rk(zhb, 0);
				rhb.ak(shb, "To create an account please enter all the requested details");
			}
			if(ohb.lk(qhb)) {
				nhb = 2;
				aib.ak(bib, "Please enter your username and password");
				aib.ak(cib, "");
				aib.ak(dib, "");
				aib.sk(cib);
				return;
			}
		} else
		if(nhb == 1) {
			rhb.jk(super.es, super.fs, super.hs, super.gs);
			if(rhb.lk(whb)) {
				rhb.sk(xhb);
			}
			if(rhb.lk(xhb)) {
				rhb.sk(yhb);
			}
			if(rhb.lk(yhb)) {
				rhb.sk(whb);
			}
			if(rhb.lk(uhb)) {
				nhb = 0;
			}
			if(rhb.lk(vhb)) {
				if(rhb.kl(whb) == null || rhb.kl(whb).length() == 0 || rhb.kl(xhb) == null || rhb.kl(xhb).length() == 0) {
					rhb.ak(shb, "@yel@Please fill in ALL requested information to continue!");
					return;
				}
				if(!rhb.kl(xhb).equalsIgnoreCase(rhb.kl(yhb))) {
					rhb.ak(shb, "@yel@The two passwords entered are not the same as each other!");
					return;
				}
				if(rhb.kl(xhb).length() < 5) {
					rhb.ak(shb, "@yel@Your password must be at least 5 letters long");
					return;
				}
				if(rhb.sj(zhb) == 0) {
					rhb.ak(shb, "@yel@You must agree to the terms+conditions to continue");
					return;
				} else {
					rhb.ak(shb, "Please wait... Creating new account");
					mp();
					qj();
					String s = rhb.kl(whb);
					String s1 = rhb.kl(xhb);
					df(s, s1);
					return;
				}
			}
		} else
		if(nhb == 2) {
			aib.jk(super.es, super.fs, super.hs, super.gs);
			if(aib.lk(fib)) {
				nhb = 0;
			}
			if(aib.lk(cib)) {
				aib.sk(dib);
			}
			if(aib.lk(dib) || aib.lk(eib)) {
				lib = aib.kl(cib);
				mib = aib.kl(dib);
				me(lib, mib);
			}
		}
	}

	public void gf(String s, String s1) {
		if(nhb == 1) {
			rhb.ak(shb, s + " " + s1);
		}
		if(nhb == 2) {
			aib.ak(bib, s + " " + s1);
		}
		kib = s1;
		mp();
		qj();
	}

	public void ff() {
		ne();
	}

	public void ef() {
		hhb = 0;
		up("@cya@Sorry, you can't logout at the moment", 3);
	}

	public void jf() {
		if(hhb != 0) {
			lib = "";
			mib = "";
			xe();
			return;
		} else {
			System.out.println("Lost connection");
			lf(lib, mib);
			return;
		}
	}

	public void xe() {
		nhb = 0;
		kab = 0;
		hhb = 0;
	}

	public void ne() {
		dhb = 0;
		hhb = 0;
		nhb = 0;
		kab = 1;
		ep();
		gab.ag();
		gab.eg(eab, 0, 11);
		for(int l = 0; l < zcb; l++) {
			fab.cb(adb[l]);
			abb.gd(bdb[l], cdb[l], ddb[l]);
		}

		for(int i1 = 0; i1 < idb; i1++) {
			fab.cb(jdb[i1]);
			abb.ke(kdb[i1], ldb[i1], mdb[i1], ndb[i1]);
		}

		zcb = 0;
		idb = 0;
		scb = 0;
		zbb = 0;
		for(int j1 = 0; j1 < xbb; j1++) {
			ccb[j1] = null;
		}

		for(int k1 = 0; k1 < ybb; k1++) {
			dcb[k1] = null;
		}

		lcb = 0;
		for(int l1 = 0; l1 < jcb; l1++) {
			ncb[l1] = null;
		}

		for(int i2 = 0; i2 < kcb; i2++) {
			ocb[i2] = null;
		}

		for(int j2 = 0; j2 < 50; j2++) {
			teb[j2] = false;
		}

		aab = 0;
		super.hs = 0;
		super.gs = 0;
	}

	public void we() {
		String s = rhb.kl(whb);
		String s1 = rhb.kl(xhb);
		nhb = 2;
		aib.ak(bib, "Please enter your username and password");
		aib.ak(cib, s);
		aib.ak(dib, s1);
		mp();
		qj();
		me(s, s1);
	}

	public void yo(Pane o1, int l) {
		o1.pk(l, "Runescape rules of use", false);
		o1.pk(l, "", false);
		o1.pk(l, "In order to keep runescape enjoyable for everyone there are a few", false);
		o1.pk(l, "rules you must observe. You must agree to these rules to play", false);
		o1.pk(l, "", false);
		o1.pk(l, "When using the built in chat facility you must not use any language", false);
		o1.pk(l, "which may be considered by others to be offensive, racist or", false);
		o1.pk(l, "obscene. You must not use the chat facility to harass, threaten or", false);
		o1.pk(l, "deceive other players.", false);
		o1.pk(l, "", false);
		o1.pk(l, "You must not exploit any cheats or errors which you find in the", false);
		o1.pk(l, "game, to give yourself an unfair advantage. Any exploits which you", false);
		o1.pk(l, "find must be immediately reported to Jagex Software.", false);
		o1.pk(l, "", false);
		o1.pk(l, "You must not attempt to use other programs in conjunction with", false);
		o1.pk(l, "RuneScape to give yourself an unfair advantage at the game. You", false);
		o1.pk(l, "may not use any bots or macros to control your character for you.", false);
		o1.pk(l, "When you are not playing the game you must log-out. You may not", false);
		o1.pk(l, "circumvent any of our mechanisms designed to log out inactive", false);
		o1.pk(l, "players automatically.", false);
		o1.pk(l, "", false);
		o1.pk(l, "You must not create multiple characters and use them to help each", false);
		o1.pk(l, "other. You may create more than one character, but if you do, you", false);
		o1.pk(l, "may not log in more than one at any time, and they must not interact", false);
		o1.pk(l, "with each other in any way. If you wish to form an adventuring", false);
		o1.pk(l, "party you should do so by cooperating with other players in the game", false);
		o1.pk(l, "", false);
		o1.pk(l, "Terms and conditions", false);
		o1.pk(l, "", false);
		o1.pk(l, "You agree that your character and account in runescape, is the", false);
		o1.pk(l, "property of, and remains the property of Jagex Software. You may", false);
		o1.pk(l, "not sell, transfer, or lend your character to anyone else. We may", false);
		o1.pk(l, "delete or modify your character at any time for any reason.", false);
		o1.pk(l, "For instance failing to follow the rules above may be cause for", false);
		o1.pk(l, "IMMEDIATE DELETION of all your characters.", false);
		o1.pk(l, "", false);
		o1.pk(l, "You agree that for purposes such as preventing offensive language", false);
		o1.pk(l, "we may automatically or manually censor the chat as we see fit,", false);
		o1.pk(l, "and that we may record the chat to help us identify offenders.", false);
		o1.pk(l, "", false);
		o1.pk(l, "No Warranty is supplied with this Software. All implied warranties", false);
		o1.pk(l, "conditions or terms are excluded to the fullest extent permitted by", false);
		o1.pk(l, "law. We do not warrant that the operation of the Software will be", false);
		o1.pk(l, "uninterrupted or error free. We accept no responsibility for any", false);
		o1.pk(l, "consequential or indirect loss or damages. You use this software at", false);
		o1.pk(l, "your own risk, and assume full responsibility for any and all real,", false);
		o1.pk(l, "claimed, or supposed damages that may occur as a result of running", false);
		o1.pk(l, "this software.", false);
		o1.pk(l, "", false);
		o1.pk(l, "We reserve all rights related to the runescape name, logo, web site,", false);
		o1.pk(l, "and game. All materials associated with runescape are protected", false);
		o1.pk(l, "by UK copyright laws and all other applicable national laws, and", false);
		o1.pk(l, "may not be copied, reproduced, republished, uploaded, posted,", false);
		o1.pk(l, "transmitted, or distributed in any way without our prior written", false);
		o1.pk(l, "consent. We reserve the right to modify or remove this game at any", false);
		o1.pk(l, "time. You agree that we may change this service, and these terms", false);
		o1.pk(l, "and conditions, as and when we deem necessary.", false);
		o1.pk(l, "", false);
		o1.pk(l, "We accept no responsibility for the actions of other users of our", false);
		o1.pk(l, "website. You acknowledge that it is inpractical for us to control", false);
		o1.pk(l, "and monitor everything that users do in our game or post on our", false);
		o1.pk(l, "message boards, and that we therefore cannot be held responsible", false);
		o1.pk(l, "for any abusive or inappropriate content which appears on our site", false);
		o1.pk(l, "as a result.", false);
		o1.pk(l, "", false);
		o1.pk(l, "Occasionally we may accept ideas and game additions from the", false);
		o1.pk(l, "players. You agree that by submitting material for inclusion in", false);
		o1.pk(l, "runescape you are giving us a non-exclusive, perpetual, worldwide,", false);
		o1.pk(l, "royalty-free license to use or modify the submission as we see", false);
		o1.pk(l, "fit. You agree that you will not withdraw the submission or attempt", false);
		o1.pk(l, "to make a charge for its use. Furthermore you warrant that you", false);
		o1.pk(l, "are the exclusive copyright holder of the submission, and that the", false);
		o1.pk(l, "submission in no way violates any other person or entity's rights", false);
		o1.pk(l, "", false);
		o1.pk(l, "These Terms shall be governed by the laws of England, and the", false);
		o1.pk(l, "courts of England shall have exclusive jurisdiction in all matters", false);
		o1.pk(l, "arising.", false);
	}

	public void uo() {
		qe();
		if(hhb > 0) {
			hhb--;
		}
		if(super.kr > 4500 && ihb == 0 && hhb == 0) {
			super.kr -= 500;
			trylogout();
			return;
		}
		if(fcb.ry == 8 || fcb.ry == 9) {
			ihb = 500;
		}
		if(ihb > 0) {
			ihb--;
		}
		if(djb != 0) {
			yn();
			return;
		}
		if(ulb) {
			go();
			return;
		}
		if(rkb) {
			op();
			return;
		}
		for(int l = 0; l < zbb; l++) {
			Character r1 = dcb[l];
			int j1 = (r1.uy + 1) % 10;
			if(r1.ty != j1) {
				int l1 = -1;
				int k3 = r1.ty;
				int k4;
				if(k3 < j1) {
					k4 = j1 - k3;
				} else {
					k4 = (10 + j1) - k3;
				}
				int j5 = 4;
				if(k4 > 2) {
					j5 = (k4 - 1) * 4;
				}
				if(r1.vy[k3] - r1.ny > jab * 3 || r1.wy[k3] - r1.oy > jab * 3 || r1.vy[k3] - r1.ny < -jab * 3 || r1.wy[k3] - r1.oy < -jab * 3 || k4 > 8) {
					r1.ny = r1.vy[k3];
					r1.oy = r1.wy[k3];
				} else {
					if(r1.ny < r1.vy[k3]) {
						r1.ny += j5;
						r1.qy++;
						l1 = 2;
					} else
					if(r1.ny > r1.vy[k3]) {
						r1.ny -= j5;
						r1.qy++;
						l1 = 6;
					}
					if(r1.ny - r1.vy[k3] < j5 && r1.ny - r1.vy[k3] > -j5) {
						r1.ny = r1.vy[k3];
					}
					if(r1.oy < r1.wy[k3]) {
						r1.oy += j5;
						r1.qy++;
						if(l1 == -1) {
							l1 = 4;
						} else
						if(l1 == 2) {
							l1 = 3;
						} else {
							l1 = 5;
						}
					} else
					if(r1.oy > r1.wy[k3]) {
						r1.oy -= j5;
						r1.qy++;
						if(l1 == -1) {
							l1 = 0;
						} else
						if(l1 == 2) {
							l1 = 1;
						} else {
							l1 = 7;
						}
					}
					if(r1.oy - r1.wy[k3] < j5 && r1.oy - r1.wy[k3] > -j5) {
						r1.oy = r1.wy[k3];
					}
				}
				if(l1 != -1) {
					r1.ry = l1;
				}
				if(r1.ny == r1.vy[k3] && r1.oy == r1.wy[k3]) {
					r1.ty = (k3 + 1) % 10;
				}
			} else {
				r1.ry = r1.sy;
			}
			if(r1.zy > 0) {
				r1.zy--;
			}
			if(r1.bz > 0) {
				r1.bz--;
			}
			if(r1.fz > 0) {
				r1.fz--;
			}
			if(lhb > 0) {
				lhb--;
				if(lhb == 0) {
					up("You have been granted another life. Be more careful this time!", 3);
				}
				if(lhb == 0) {
					up("You retain your skills. Unless you attacked another player recently,", 3);
				}
				if(lhb == 0) {
					up("you also keep your best 3 items. Everything else lands where you died.", 3);
				}
			}
		}

		for(int i1 = 0; i1 < lcb; i1++) {
			Character r2 = ocb[i1];
			int i2 = (r2.uy + 1) % 10;
			if(r2.ty != i2) {
				int l3 = -1;
				int l4 = r2.ty;
				int k5;
				if(l4 < i2) {
					k5 = i2 - l4;
				} else {
					k5 = (10 + i2) - l4;
				}
				int l5 = 4;
				if(k5 > 2) {
					l5 = (k5 - 1) * 4;
				}
				if(r2.vy[l4] - r2.ny > jab * 3 || r2.wy[l4] - r2.oy > jab * 3 || r2.vy[l4] - r2.ny < -jab * 3 || r2.wy[l4] - r2.oy < -jab * 3 || k5 > 8) {
					r2.ny = r2.vy[l4];
					r2.oy = r2.wy[l4];
				} else {
					if(r2.ny < r2.vy[l4]) {
						r2.ny += l5;
						r2.qy++;
						l3 = 2;
					} else
					if(r2.ny > r2.vy[l4]) {
						r2.ny -= l5;
						r2.qy++;
						l3 = 6;
					}
					if(r2.ny - r2.vy[l4] < l5 && r2.ny - r2.vy[l4] > -l5) {
						r2.ny = r2.vy[l4];
					}
					if(r2.oy < r2.wy[l4]) {
						r2.oy += l5;
						r2.qy++;
						if(l3 == -1) {
							l3 = 4;
						} else
						if(l3 == 2) {
							l3 = 3;
						} else {
							l3 = 5;
						}
					} else
					if(r2.oy > r2.wy[l4]) {
						r2.oy -= l5;
						r2.qy++;
						if(l3 == -1) {
							l3 = 0;
						} else
						if(l3 == 2) {
							l3 = 1;
						} else {
							l3 = 7;
						}
					}
					if(r2.oy - r2.wy[l4] < l5 && r2.oy - r2.wy[l4] > -l5) {
						r2.oy = r2.wy[l4];
					}
				}
				if(l3 != -1) {
					r2.ry = l3;
				}
				if(r2.ny == r2.vy[l4] && r2.oy == r2.wy[l4]) {
					r2.ty = (l4 + 1) % 10;
				}
			} else {
				r2.ry = r2.sy;
			}
			if(r2.zy > 0) {
				r2.zy--;
			}
			if(r2.bz > 0) {
				r2.bz--;
			}
			if(r2.fz > 0) {
				r2.fz--;
			}
		}

		for(int k1 = 0; k1 < zbb; k1++) {
			Character r3 = dcb[k1];
			if(r3.oz > 0) {
				r3.oz--;
			}
		}

		if(ueb) {
			if(rbb - fcb.ny < -500 || rbb - fcb.ny > 500 || sbb - fcb.oy < -500 || sbb - fcb.oy > 500) {
				rbb = fcb.ny;
				sbb = fcb.oy;
			}
		} else {
			if(rbb - fcb.ny < -500 || rbb - fcb.ny > 500 || sbb - fcb.oy < -500 || sbb - fcb.oy > 500) {
				rbb = fcb.ny;
				sbb = fcb.oy;
			}
			if(rbb != fcb.ny) {
				rbb += (fcb.ny - rbb) / (16 + (pbb - 500) / 15);
			}
			if(sbb != fcb.oy) {
				sbb += (fcb.oy - sbb) / (16 + (pbb - 500) / 15);
			}
			if(veb) {
				int j2 = tbb * 32;
				int i4 = j2 - vbb;
				byte byte0 = 1;
				if(i4 != 0) {
					ubb++;
					if(i4 > 128) {
						byte0 = -1;
						i4 = 256 - i4;
					} else
					if(i4 > 0) {
						byte0 = 1;
					} else
					if(i4 < -128) {
						byte0 = 1;
						i4 = 256 + i4;
					} else
					if(i4 < 0) {
						byte0 = -1;
						i4 = -i4;
					}
					vbb += ((ubb * i4 + 255) / 256) * byte0;
					vbb &= 0xff;
				} else {
					ubb = 0;
				}
			}
		}
		if(super.fs > mab - 4) {
			if(super.es > 15 && super.es < 96 && super.hs == 1) {
				wfb = 0;
			}
			if(super.es > 110 && super.es < 194 && super.hs == 1) {
				wfb = 1;
				rfb.xs[sfb] = 0xf423f;
			}
			if(super.es > 215 && super.es < 295 && super.hs == 1) {
				wfb = 2;
				rfb.xs[ufb] = 0xf423f;
			}
			if(super.es > 315 && super.es < 395 && super.hs == 1) {
				wfb = 3;
				rfb.xs[vfb] = 0xf423f;
			}
			super.hs = 0;
			super.gs = 0;
		}
		rfb.jk(super.es, super.fs, super.hs, super.gs);
		if(wfb > 0 && super.es >= 494 && super.fs >= mab - 66) {
			super.hs = 0;
		}
		if(rfb.lk(tfb)) {
			String s = rfb.kl(tfb);
			rfb.ak(tfb, "");
			if(s.equalsIgnoreCase("lostcon99") && !applet) {
				super.zf.close();
			} else
			if(s.equalsIgnoreCase("closecon99") && !applet) {
				ue();
			} else
			if(!_mthif(s)) {
				fcb.zy = 150;
				fcb.yy = s;
				up(fcb.ky + ": " + s, 2);
			}
		}
		if(wfb == 0) {
			for(int k2 = 0; k2 < xfb; k2++) {
				if(zfb[k2] > 0) {
					zfb[k2]--;
				}
			}

		}
		if(lhb != 0) {
			super.hs = 0;
		}
		if(agb) {
			if(super.gs != 0) {
				kgb++;
			} else {
				kgb = 0;
			}
			if(kgb > 300) {
				lgb += 50;
			} else
			if(kgb > 150) {
				lgb += 5;
			} else
			if(kgb > 50) {
				lgb++;
			} else
			if(kgb > 20 && (kgb & 5) == 0) {
				lgb++;
			}
		} else {
			kgb = 0;
			lgb = 0;
		}
		if(super.hs == 1) {
			aab = 1;
		} else
		if(super.hs == 2) {
			aab = 2;
		}
		fab.n(super.es, super.fs);
		super.hs = 0;
		if(veb) {
			if(ubb == 0 || ueb) {
				if(super.xr) {
					tbb = tbb + 1 & 7;
					super.xr = false;
					if(!qbb) {
						if((tbb & 1) == 0) {
							tbb = tbb + 1 & 7;
						}
						for(int l2 = 0; l2 < 8; l2++) {
							if(zp(tbb)) {
								break;
							}
							tbb = tbb + 1 & 7;
						}

					}
				}
				if(super.yr) {
					tbb = tbb + 7 & 7;
					super.yr = false;
					if(!qbb) {
						if((tbb & 1) == 0) {
							tbb = tbb + 7 & 7;
						}
						for(int i3 = 0; i3 < 8; i3++) {
							if(zp(tbb)) {
								break;
							}
							tbb = tbb + 7 & 7;
						}

					}
				}
			}
		} else
		if(super.xr) {
			vbb = vbb + 2 & 0xff;
		} else
		if(super.yr) {
			vbb = vbb - 2 & 0xff;
		}
		if(qbb && pbb > 550) {
			pbb -= 4;
		} else
		if(!qbb && pbb < 750) {
			pbb += 4;
		}
		if(xab > 0) {
			xab--;
		} else
		if(xab < 0) {
			xab++;
		}
		fab.ub(17);
		sab++;
		if(sab > 5) {
			sab = 0;
			tab = tab + 1 & 3;
			uab = (uab + 1) % 3;
		}
		for(int j3 = 0; j3 < zcb; j3++) {
			int j4 = bdb[j3];
			int i5 = cdb[j3];
			if(j4 >= 0 && i5 >= 0 && j4 < 96 && i5 < 96 && ddb[j3] == 74) {
				adb[j3].sm(1, 0, 0);
			}
		}

	}

	public void up(String s, int l) {
		if(l == 2 || l == 4 || l == 6) {
			for(; s.length() > 5 && s.charAt(0) == '@' && s.charAt(4) == '@'; s = s.substring(5)) { }
			int i1 = s.indexOf(":");
			if(i1 != -1) {
				String s1 = s.substring(0, i1);
				long l1 = Util.encode37(s1);
				for(int k1 = 0; k1 < super.ig; k1++) {
					if(super.jg[k1] == l1) {
						return;
					}
				}

			}
		}
		if(l == 2) {
			s = "@yel@" + s;
		}
		if(l == 3 || l == 4) {
			s = "@whi@" + s;
		}
		if(l == 6) {
			s = "@cya@" + s;
		}
		if(wfb != 0) {
			if(l == 4 || l == 3) {
				nfb = 200;
			}
			if(l == 2 && wfb != 1) {
				ofb = 200;
			}
			if(l == 5 && wfb != 2) {
				pfb = 200;
			}
			if(l == 6 && wfb != 3) {
				qfb = 200;
			}
			if(l == 3 && wfb != 0) {
				wfb = 0;
			}
			if(l == 6 && wfb != 3 && wfb != 0) {
				wfb = 3;
			}
		}
		for(int j1 = xfb - 1; j1 > 0; j1--) {
			yfb[j1] = yfb[j1 - 1];
			zfb[j1] = zfb[j1 - 1];
		}

		yfb[0] = s;
		zfb[0] = 300;
		if(l == 2) {
			if(rfb.xs[sfb] == rfb.ys[sfb] - 4) {
				rfb.pk(sfb, s, true);
			} else {
				rfb.pk(sfb, s, false);
			}
		}
		if(l == 5) {
			if(rfb.xs[ufb] == rfb.ys[ufb] - 4) {
				rfb.pk(ufb, s, true);
			} else {
				rfb.pk(ufb, s, false);
			}
		}
		if(l == 6) {
			if(rfb.xs[vfb] == rfb.ys[vfb] - 4) {
				rfb.pk(vfb, s, true);
				return;
			}
			rfb.pk(vfb, s, false);
		}
	}

	public void ze(String s) {
		if(s.startsWith("@cha@")) {
			up(s, 2);
			return;
		}
		if(s.startsWith("@bor@")) {
			up(s, 4);
			return;
		}
		if(s.startsWith("@que@")) {
			up("@whi@" + s, 5);
			return;
		}
		if(s.startsWith("@pri@")) {
			up(s, 6);
			return;
		} else {
			up(s, 3);
			return;
		}
	}

	public Character jn(int l, int i1, int j1, int k1) {
		if(ccb[l] == null) {
			ccb[l] = new Character();
			ccb[l].ly = l;
			ccb[l].my = 0;
		}
		Character r1 = ccb[l];
		boolean flag = false;
		for(int l1 = 0; l1 < acb; l1++) {
			if(ecb[l1].ly != l) {
				continue;
			}
			flag = true;
			break;
		}

		if(flag) {
			r1.sy = k1;
			int i2 = r1.uy;
			if(i1 != r1.vy[i2] || j1 != r1.wy[i2]) {
				r1.uy = i2 = (i2 + 1) % 10;
				r1.vy[i2] = i1;
				r1.wy[i2] = j1;
			}
		} else {
			r1.ly = l;
			r1.ty = 0;
			r1.uy = 0;
			r1.vy[0] = r1.ny = i1;
			r1.wy[0] = r1.oy = j1;
			r1.sy = r1.ry = k1;
			r1.qy = 0;
		}
		dcb[zbb++] = r1;
		return r1;
	}

	public Character fp(int l, int i1, int j1, int k1, int l1) {
		if(ncb[l] == null) {
			ncb[l] = new Character();
			ncb[l].ly = l;
		}
		Character r1 = ncb[l];
		boolean flag = false;
		for(int i2 = 0; i2 < mcb; i2++) {
			if(pcb[i2].ly != l) {
				continue;
			}
			flag = true;
			break;
		}

		if(flag) {
			r1.py = l1;
			r1.sy = k1;
			int j2 = r1.uy;
			if(i1 != r1.vy[j2] || j1 != r1.wy[j2]) {
				r1.uy = j2 = (j2 + 1) % 10;
				r1.vy[j2] = i1;
				r1.wy[j2] = j1;
			}
		} else {
			r1.ly = l;
			r1.ty = 0;
			r1.uy = 0;
			r1.vy[0] = r1.ny = i1;
			r1.wy[0] = r1.oy = j1;
			r1.py = l1;
			r1.sy = r1.ry = k1;
			r1.qy = 0;
		}
		ocb[lcb++] = r1;
		return r1;
	}

	public void ve(int l, int i1, byte abyte0[]) {
		try {
			if(l == 255) {
				acb = zbb;
				for(int j1 = 0; j1 < acb; j1++) {
					ecb[j1] = dcb[j1];
				}

				int j6 = 8;
				gcb = Util.qc(abyte0, j6, 10);
				j6 += 10;
				hcb = Util.qc(abyte0, j6, 12);
				j6 += 12;
				int l10 = Util.qc(abyte0, j6, 4);
				j6 += 4;
				boolean flag = _mthdo(gcb, hcb);
				gcb -= fbb;
				hcb -= gbb;
				int k18 = gcb * jab + 64;
				int k21 = hcb * jab + 64;
				if(flag) {
					fcb.uy = 0;
					fcb.ty = 0;
					fcb.ny = fcb.vy[0] = k18;
					fcb.oy = fcb.wy[0] = k21;
				}
				zbb = 0;
				fcb = jn(icb, k18, k21, l10);
				int l24 = Util.qc(abyte0, j6, 8);
				j6 += 8;
				for(int k29 = 0; k29 < l24; k29++) {
					Character r4 = ecb[k29 + 1];
					int j35 = Util.qc(abyte0, j6, 1);
					j6++;
					if(j35 != 0) {
						int j37 = Util.qc(abyte0, j6, 1);
						j6++;
						if(j37 == 0) {
							int k38 = Util.qc(abyte0, j6, 3);
							j6 += 3;
							int k39 = r4.uy;
							int i40 = r4.vy[k39];
							int j40 = r4.wy[k39];
							if(k38 == 2 || k38 == 1 || k38 == 3) {
								i40 += jab;
							}
							if(k38 == 6 || k38 == 5 || k38 == 7) {
								i40 -= jab;
							}
							if(k38 == 4 || k38 == 3 || k38 == 5) {
								j40 += jab;
							}
							if(k38 == 0 || k38 == 1 || k38 == 7) {
								j40 -= jab;
							}
							r4.sy = k38;
							r4.uy = k39 = (k39 + 1) % 10;
							r4.vy[k39] = i40;
							r4.wy[k39] = j40;
						} else {
							int l38 = Util.qc(abyte0, j6, 4);
							if((l38 & 0xc) == 12) {
								j6 += 2;
								continue;
							}
							r4.sy = Util.qc(abyte0, j6, 4);
							j6 += 4;
						}
					}
					dcb[zbb++] = r4;
				}

				int l32 = 0;
				while(j6 + 24 < i1 * 8)  {
					int k35 = Util.qc(abyte0, j6, 11);
					j6 += 11;
					int k37 = Util.qc(abyte0, j6, 5);
					j6 += 5;
					if(k37 > 15) {
						k37 -= 32;
					}
					int i39 = Util.qc(abyte0, j6, 5);
					j6 += 5;
					if(i39 > 15) {
						i39 -= 32;
					}
					int i11 = Util.qc(abyte0, j6, 4);
					j6 += 4;
					int l39 = Util.qc(abyte0, j6, 1);
					j6++;
					int l18 = (gcb + k37) * jab + 64;
					int l21 = (hcb + i39) * jab + 64;
					jn(k35, l18, l21, i11);
					if(l39 == 0) {
						qcb[l32++] = k35;
					}
				}
				if(l32 > 0) {
					super.zf.begin(254);
					super.zf.ec(l32);
					for(int l35 = 0; l35 < l32; l35++) {
						Character r5 = ccb[qcb[l35]];
						super.zf.ec(r5.ly);
						super.zf.ec(r5.my);
					}

					super.zf.end();
					l32 = 0;
					return;
				}
			} else {
				if(l == 254) {
					for(int k1 = 1; k1 < i1;) {
						if(Util.unsign(abyte0[k1]) == 255) {
							int k6 = 0;
							int j11 = gcb + abyte0[k1 + 1] >> 3;
							int i15 = hcb + abyte0[k1 + 2] >> 3;
							k1 += 3;
							for(int i19 = 0; i19 < scb; i19++) {
								int i22 = (ucb[i19] >> 3) - j11;
								int i25 = (vcb[i19] >> 3) - i15;
								if(i22 != 0 || i25 != 0) {
									if(i19 != k6) {
										ucb[k6] = ucb[i19];
										vcb[k6] = vcb[i19];
										wcb[k6] = wcb[i19];
										xcb[k6] = xcb[i19];
									}
									k6++;
								}
							}

							scb = k6;
						} else {
							int l6 = Util.vc(abyte0, k1);
							k1 += 2;
							int k11 = gcb + abyte0[k1++];
							int j15 = hcb + abyte0[k1++];
							if((l6 & 0x8000) == 0) {
								ucb[scb] = k11;
								vcb[scb] = j15;
								wcb[scb] = l6;
								xcb[scb] = 0;
								for(int j19 = 0; j19 < zcb; j19++) {
									if(bdb[j19] != k11 || cdb[j19] != j15) {
										continue;
									}
									xcb[scb] = Config.kn[ddb[j19]];
									break;
								}

								scb++;
							} else {
								l6 &= 0x7fff;
								int k19 = 0;
								for(int j22 = 0; j22 < scb; j22++) {
									if(ucb[j22] != k11 || vcb[j22] != j15 || wcb[j22] != l6) {
										if(j22 != k19) {
											ucb[k19] = ucb[j22];
											vcb[k19] = vcb[j22];
											wcb[k19] = wcb[j22];
											xcb[k19] = xcb[j22];
										}
										k19++;
									} else {
										l6 = -123;
									}
								}

								scb = k19;
							}
						}
					}

					return;
				}
				if(l == 253) {
					for(int l1 = 1; l1 < i1;) {
						if(Util.unsign(abyte0[l1]) == 255) {
							int i7 = 0;
							int l11 = gcb + abyte0[l1 + 1] >> 3;
							int k15 = hcb + abyte0[l1 + 2] >> 3;
							l1 += 3;
							for(int l19 = 0; l19 < zcb; l19++) {
								int k22 = (bdb[l19] >> 3) - l11;
								int j25 = (cdb[l19] >> 3) - k15;
								if(k22 != 0 || j25 != 0) {
									if(l19 != i7) {
										adb[i7] = adb[l19];
										adb[i7].qv = i7;
										bdb[i7] = bdb[l19];
										cdb[i7] = cdb[l19];
										ddb[i7] = ddb[l19];
										edb[i7] = edb[l19];
									}
									i7++;
								} else {
									fab.cb(adb[l19]);
									abb.gd(bdb[l19], cdb[l19], ddb[l19]);
								}
							}

							zcb = i7;
						} else {
							int j7 = Util.vc(abyte0, l1);
							l1 += 2;
							int i12 = gcb + abyte0[l1++];
							int l15 = hcb + abyte0[l1++];
							int i20 = 0;
							for(int l22 = 0; l22 < zcb; l22++) {
								if(bdb[l22] != i12 || cdb[l22] != l15) {
									if(l22 != i20) {
										adb[i20] = adb[l22];
										adb[i20].qv = i20;
										bdb[i20] = bdb[l22];
										cdb[i20] = cdb[l22];
										ddb[i20] = ddb[l22];
										edb[i20] = edb[l22];
									}
									i20++;
								} else {
									fab.cb(adb[l22]);
									abb.gd(bdb[l22], cdb[l22], ddb[l22]);
								}
							}

							zcb = i20;
							if(j7 != 60000) {
								int k25 = abb.td(i12, l15);
								int l29;
								int i33;
								if(k25 == 0 || k25 == 4) {
									l29 = Config.gn[j7];
									i33 = Config.hn[j7];
								} else {
									i33 = Config.gn[j7];
									l29 = Config.hn[j7];
								}
								int i36 = ((i12 + i12 + l29) * jab) / 2;
								int l37 = ((l15 + l15 + i33) * jab) / 2;
								int j39 = Config.fn[j7];
								Model p2 = models[j39].bm();
								fab.ib(p2);
								p2.qv = zcb;
								p2.sm(0, k25 * 32, 0);
								p2.fm(i36, -abb.vd(i36, l37), l37);
								p2.pm(true, 48, 48, -50, -10, -50);
								abb.jd(i12, l15, j7);
								if(j7 == 74) {
									p2.fm(0, -480, 0);
								}
								bdb[zcb] = i12;
								cdb[zcb] = l15;
								ddb[zcb] = j7;
								edb[zcb] = k25;
								adb[zcb++] = p2;
							}
						}
					}

					return;
				}
				if(l == 252) {
					rdb = 0;
					for(int i2 = 8; i2 + 9 < i1 * 8;) {
						int k7 = Util.qc(abyte0, i2, 10);
						i2 += 10;
						int j12 = 0;
						if(Config.jl[k7] != 0) {
							j12 = Util.qc(abyte0, i2, 1);
							i2++;
						}
						int i16 = 1;
						if(Config.bl[k7] == 0) {
							i16 = Util.qc(abyte0, i2, 16);
							i2 += 16;
						}
						sdb[rdb] = k7;
						udb[rdb] = j12;
						tdb[rdb] = i16;
						rdb++;
					}

					return;
				}
				if(l == 250) {
					int j2 = Util.vc(abyte0, 1);
					int l7 = 3;
					for(int k12 = 0; k12 < j2; k12++) {
						int j16 = Util.vc(abyte0, l7);
						l7 += 2;
						Character r1 = ccb[j16];
						byte byte4 = abyte0[l7];
						l7++;
						if(byte4 == 0) {
							int l25 = Util.vc(abyte0, l7);
							l7 += 2;
							if(r1 != null) {
								r1.bz = 150;
								r1.az = l25;
							}
						} else
						if(byte4 == 1) {
							byte byte5 = abyte0[l7];
							l7++;
							if(r1 != null) {
								String s1 = new String(abyte0, l7, byte5);
								if(s1.startsWith("@que@")) {
									r1.zy = 150;
									r1.yy = s1;
									if(r1 == fcb) {
										up("@yel@" + r1.ky + ": " + r1.yy, 5);
									}
								} else
								if(r1 != fcb) {
									boolean flag3 = false;
									for(int j36 = 0; j36 < super.ig; j36++) {
										if(super.jg[j36] == r1.jy) {
											flag3 = true;
										}
									}

									if(!flag3) {
										s1 = Util.rc(s1, true);
										r1.zy = 150;
										r1.yy = s1;
										up(r1.ky + ": " + r1.yy, 2);
									}
								}
							}
							l7 += byte5;
						} else
						if(byte4 == 2) {
							int i26 = Util.unsign(abyte0[l7]);
							l7++;
							int i30 = Util.unsign(abyte0[l7]);
							l7++;
							int j33 = Util.unsign(abyte0[l7]);
							l7++;
							if(r1 != null) {
								r1.cz = i26;
								r1.dz = i30;
								r1.ez = j33;
								r1.fz = 200;
								if(r1 == fcb) {
									ydb[3] = i30;
									zdb[3] = j33;
								}
							}
						} else
						if(byte4 == 3) {
							int j26 = Util.vc(abyte0, l7);
							l7 += 2;
							int j30 = Util.vc(abyte0, l7);
							l7 += 2;
							if(r1 != null) {
								r1.lz = j26;
								r1.nz = j30;
								r1.mz = -1;
								r1.oz = qab;
							}
						} else
						if(byte4 == 4) {
							int k26 = Util.vc(abyte0, l7);
							l7 += 2;
							int k30 = Util.vc(abyte0, l7);
							l7 += 2;
							if(r1 != null) {
								r1.lz = k26;
								r1.mz = k30;
								r1.nz = -1;
								r1.oz = qab;
							}
						} else
						if(byte4 == 5) {
							if(r1 != null) {
								r1.my = Util.vc(abyte0, l7);
								l7 += 2;
								r1.jy = Util.pc(abyte0, l7);
								l7 += 8;
								r1.ky = Util.decode37(r1.jy);
								int l26 = Util.unsign(abyte0[l7]);
								l7++;
								for(int l30 = 0; l30 < l26; l30++) {
									r1.xy[l30] = Util.unsign(abyte0[l7]);
									l7++;
								}

								for(int k33 = l26; k33 < 12; k33++) {
									r1.xy[k33] = 0;
								}

								r1.hz = abyte0[l7++] & 0xff;
								r1.iz = abyte0[l7++] & 0xff;
								r1.jz = abyte0[l7++] & 0xff;
								r1.kz = abyte0[l7++] & 0xff;
								r1.gz = abyte0[l7++] & 0xff;
								r1.rz = abyte0[l7++] & 0xff;
							} else {
								l7 += 14;
								int i27 = Util.unsign(abyte0[l7]);
								l7 += i27 + 1;
							}
						}
					}

					return;
				}
				if(l == 249) {
					for(int k2 = 1; k2 < i1;) {
						if(Util.unsign(abyte0[k2]) == 255) {
							int i8 = 0;
							int l12 = gcb + abyte0[k2 + 1] >> 3;
							int k16 = hcb + abyte0[k2 + 2] >> 3;
							k2 += 3;
							for(int j20 = 0; j20 < idb; j20++) {
								int i23 = (kdb[j20] >> 3) - l12;
								int j27 = (ldb[j20] >> 3) - k16;
								if(i23 != 0 || j27 != 0) {
									if(j20 != i8) {
										jdb[i8] = jdb[j20];
										jdb[i8].qv = i8 + 10000;
										kdb[i8] = kdb[j20];
										ldb[i8] = ldb[j20];
										mdb[i8] = mdb[j20];
										ndb[i8] = ndb[j20];
									}
									i8++;
								} else {
									fab.cb(jdb[j20]);
									abb.ke(kdb[j20], ldb[j20], mdb[j20], ndb[j20]);
								}
							}

							idb = i8;
						} else {
							int j8 = Util.vc(abyte0, k2);
							k2 += 2;
							int i13 = gcb + abyte0[k2++];
							int l16 = hcb + abyte0[k2++];
							byte byte3 = abyte0[k2++];
							int j23 = 0;
							for(int k27 = 0; k27 < idb; k27++) {
								if(kdb[k27] != i13 || ldb[k27] != l16 || mdb[k27] != byte3) {
									if(k27 != j23) {
										jdb[j23] = jdb[k27];
										jdb[j23].qv = j23 + 10000;
										kdb[j23] = kdb[k27];
										ldb[j23] = ldb[k27];
										mdb[j23] = mdb[k27];
										ndb[j23] = ndb[k27];
									}
									j23++;
								} else {
									fab.cb(jdb[k27]);
									abb.ke(kdb[k27], ldb[k27], mdb[k27], ndb[k27]);
								}
							}

							idb = j23;
							if(j8 != 65535) {
								abb.ed(i13, l16, byte3, j8);
								Model p1 = hn(i13, l16, byte3, j8, idb);
								jdb[idb] = p1;
								kdb[idb] = i13;
								ldb[idb] = l16;
								ndb[idb] = j8;
								mdb[idb++] = byte3;
							}
						}
					}

					return;
				}
				if(l == 248) {
					mcb = lcb;
					lcb = 0;
					for(int l2 = 0; l2 < mcb; l2++) {
						pcb[l2] = ocb[l2];
					}

					int k8 = 8;
					int j13 = Util.qc(abyte0, k8, 8);
					k8 += 8;
					for(int i17 = 0; i17 < j13; i17++) {
						Character r2 = pcb[i17];
						int k23 = Util.qc(abyte0, k8, 1);
						k8++;
						if(k23 != 0) {
							int l27 = Util.qc(abyte0, k8, 1);
							k8++;
							if(l27 == 0) {
								int i31 = Util.qc(abyte0, k8, 3);
								k8 += 3;
								int l33 = r2.uy;
								int k36 = r2.vy[l33];
								int i38 = r2.wy[l33];
								if(i31 == 2 || i31 == 1 || i31 == 3) {
									k36 += jab;
								}
								if(i31 == 6 || i31 == 5 || i31 == 7) {
									k36 -= jab;
								}
								if(i31 == 4 || i31 == 3 || i31 == 5) {
									i38 += jab;
								}
								if(i31 == 0 || i31 == 1 || i31 == 7) {
									i38 -= jab;
								}
								r2.sy = i31;
								r2.uy = l33 = (l33 + 1) % 10;
								r2.vy[l33] = k36;
								r2.wy[l33] = i38;
							} else {
								int j31 = Util.qc(abyte0, k8, 4);
								if((j31 & 0xc) == 12) {
									k8 += 2;
									continue;
								}
								r2.sy = Util.qc(abyte0, k8, 4);
								k8 += 4;
							}
						}
						ocb[lcb++] = r2;
					}

					while(k8 + 31 < i1 * 8)  {
						int k20 = Util.qc(abyte0, k8, 10);
						k8 += 10;
						int l23 = Util.qc(abyte0, k8, 5);
						k8 += 5;
						if(l23 > 15) {
							l23 -= 32;
						}
						int i28 = Util.qc(abyte0, k8, 5);
						k8 += 5;
						if(i28 > 15) {
							i28 -= 32;
						}
						int k31 = Util.qc(abyte0, k8, 4);
						k8 += 4;
						int i34 = (gcb + l23) * jab + 64;
						int l36 = (hcb + i28) * jab + 64;
						int j38 = Util.qc(abyte0, k8, 8);
						k8 += 8;
						if(j38 >= Config.npcCount) {
							j38 = 24;
						}
						fp(k20, i34, l36, k31, j38);
					}
					return;
				}
				if(l == 247) {
					int i3 = Util.vc(abyte0, 1);
					int l8 = 3;
					for(int k13 = 0; k13 < i3; k13++) {
						int j17 = Util.vc(abyte0, l8);
						l8 += 2;
						Character r3 = ncb[j17];
						int i24 = Util.unsign(abyte0[l8]);
						l8++;
						if(i24 == 1) {
							int j28 = Util.vc(abyte0, l8);
							l8 += 2;
							byte byte6 = abyte0[l8];
							l8++;
							if(r3 != null) {
								String s2 = new String(abyte0, l8, byte6);
								r3.zy = 150;
								r3.yy = s2;
								if(j28 == fcb.ly) {
									up("@yel@" + Config.npcNames[r3.py][0] + ": " + r3.yy, 5);
								}
							}
							l8 += byte6;
						} else
						if(i24 == 2) {
							int k28 = Util.unsign(abyte0[l8]);
							l8++;
							int l31 = Util.unsign(abyte0[l8]);
							l8++;
							int j34 = Util.unsign(abyte0[l8]);
							l8++;
							if(r3 != null) {
								r3.cz = k28;
								r3.dz = l31;
								r3.ez = j34;
								r3.fz = 200;
							}
						}
					}

					return;
				}
				if(l == 246) {
					ahb = true;
					int j3 = Util.unsign(abyte0[1]);
					bhb = j3;
					int i9 = 2;
					for(int l13 = 0; l13 < j3; l13++) {
						int k17 = Util.unsign(abyte0[i9]);
						i9++;
						chb[l13] = new String(abyte0, i9, k17);
						i9 += k17;
					}

					return;
				}
				if(l == 245) {
					ahb = false;
					return;
				}
				if(l == 244) {
					icb = Util.vc(abyte0, 1);
					bbb = Util.vc(abyte0, 3);
					cbb = Util.vc(abyte0, 5);
					hbb = Util.vc(abyte0, 7);
					dbb = Util.vc(abyte0, 9);
					cbb -= hbb * dbb;
					return;
				}
				if(l == 243) {
					int k3 = 1;
					for(int j9 = 0; j9 < 16; j9++) {
						ydb[j9] = Util.unsign(abyte0[k3++]);
					}

					for(int i14 = 0; i14 < 16; i14++) {
						zdb[i14] = Util.unsign(abyte0[k3++]);
					}

					for(int l17 = 0; l17 < 16; l17++) {
						aeb[l17] = Util.uc(abyte0, k3);
						k3 += 4;
					}

					ceb = Util.unsign(abyte0[k3++]);
					return;
				}
				if(l == 242) {
					for(int l3 = 0; l3 < 5; l3++) {
						beb[l3] = Util.unsign(abyte0[1 + l3]);
					}

					return;
				}
				if(l == 241) {
					lhb = 250;
					return;
				}
				if(l == 240) {
					int i4 = (i1 - 1) / 4;
					for(int k9 = 0; k9 < i4; k9++) {
						int j14 = gcb + Util.bd(abyte0, 1 + k9 * 4) >> 3;
						int i18 = hcb + Util.bd(abyte0, 3 + k9 * 4) >> 3;
						int l20 = 0;
						for(int j24 = 0; j24 < scb; j24++) {
							int l28 = (ucb[j24] >> 3) - j14;
							int i32 = (vcb[j24] >> 3) - i18;
							if(l28 != 0 || i32 != 0) {
								if(j24 != l20) {
									ucb[l20] = ucb[j24];
									vcb[l20] = vcb[j24];
									wcb[l20] = wcb[j24];
									xcb[l20] = xcb[j24];
								}
								l20++;
							}
						}

						scb = l20;
						l20 = 0;
						for(int i29 = 0; i29 < zcb; i29++) {
							int j32 = (bdb[i29] >> 3) - j14;
							int k34 = (cdb[i29] >> 3) - i18;
							if(j32 != 0 || k34 != 0) {
								if(i29 != l20) {
									adb[l20] = adb[i29];
									adb[l20].qv = l20;
									bdb[l20] = bdb[i29];
									cdb[l20] = cdb[i29];
									ddb[l20] = ddb[i29];
									edb[l20] = edb[i29];
								}
								l20++;
							} else {
								fab.cb(adb[i29]);
								abb.gd(bdb[i29], cdb[i29], ddb[i29]);
							}
						}

						zcb = l20;
						l20 = 0;
						for(int k32 = 0; k32 < idb; k32++) {
							int l34 = (kdb[k32] >> 3) - j14;
							int i37 = (ldb[k32] >> 3) - i18;
							if(l34 != 0 || i37 != 0) {
								if(k32 != l20) {
									jdb[l20] = jdb[k32];
									jdb[l20].qv = l20 + 10000;
									kdb[l20] = kdb[k32];
									ldb[l20] = ldb[k32];
									mdb[l20] = mdb[k32];
									ndb[l20] = ndb[k32];
								}
								l20++;
							} else {
								fab.cb(jdb[k32]);
								abb.ke(kdb[k32], ldb[k32], mdb[k32], ndb[k32]);
							}
						}

						idb = l20;
					}

					return;
				}
				if(l == 239) {
					ulb = true;
					return;
				}
				if(l == 238) {
					int j4 = Util.vc(abyte0, 1);
					if(ccb[j4] != null) {
						bgb = ccb[j4].ky;
					}
					agb = true;
					igb = false;
					jgb = false;
					cgb = 0;
					fgb = 0;
					return;
				}
				if(l == 237) {
					agb = false;
					return;
				}
				if(l == 236) {
					fgb = abyte0[1] & 0xff;
					int k4 = 2;
					for(int l9 = 0; l9 < fgb; l9++) {
						ggb[l9] = Util.vc(abyte0, k4);
						k4 += 2;
						hgb[l9] = Util.vc(abyte0, k4);
						k4 += 2;
					}

					igb = false;
					jgb = false;
					return;
				}
				if(l == 235) {
					byte byte0 = abyte0[1];
					if(byte0 == 1) {
						igb = true;
						return;
					} else {
						igb = false;
						return;
					}
				}
				if(l == 234) {
					mgb = true;
					int l4 = 1;
					int i10 = abyte0[l4++] & 0xff;
					byte byte2 = abyte0[l4++];
					ngb = abyte0[l4++] & 0xff;
					ogb = abyte0[l4++] & 0xff;
					for(int j18 = 0; j18 < 40; j18++) {
						pgb[j18] = -1;
					}

					for(int i21 = 0; i21 < i10; i21++) {
						pgb[i21] = Util.vc(abyte0, l4);
						l4 += 2;
						qgb[i21] = Util.vc(abyte0, l4);
						l4 += 2;
						rgb[i21] = abyte0[l4++];
					}

					if(byte2 == 1) {
						int k24 = 39;
						for(int j29 = 0; j29 < rdb; j29++) {
							if(k24 < i10) {
								break;
							}
							boolean flag2 = false;
							for(int i35 = 0; i35 < 40; i35++) {
								if(pgb[i35] != sdb[j29]) {
									continue;
								}
								flag2 = true;
								break;
							}

							if(sdb[j29] == 10) {
								flag2 = true;
							}
							if(!flag2) {
								pgb[k24] = sdb[j29] & 0x7fff;
								qgb[k24] = 0;
								rgb[k24] = 0;
								k24--;
							}
						}

					}
					if(sgb >= 0 && sgb < 40 && pgb[sgb] != tgb) {
						sgb = -1;
						tgb = -2;
						return;
					}
				} else {
					if(l == 233) {
						mgb = false;
						return;
					}
					if(l == 229) {
						byte byte1 = abyte0[1];
						if(byte1 == 1) {
							jgb = true;
							return;
						} else {
							jgb = false;
							return;
						}
					}
					if(l == 228) {
						System.out.println("Got config");
						veb = Util.unsign(abyte0[2]) == 1;
						dfb = Util.unsign(abyte0[4]) == 1;
						return;
					}
					if(l == 227) {
						for(int i5 = 0; i5 < i1 - 1; i5++) {
							teb[i5] = abyte0[i5 + 1] == 1;
						}

						return;
					}
					if(l == 226) {
						for(int j5 = 0; j5 < qeb; j5++) {
							seb[j5] = abyte0[j5 + 1] == 1;
						}

						return;
					}
					if(l == 225) {
						djb = 1;
						return;
					}
					if(l == 224) {
						rkb = true;
						for(int k5 = 0; k5 < 5; k5++) {
							alb[k5] = k5;
							blb[k5] = "~:" + alb[k5];
							skb.ak(xkb[k5], "");
							skb.ak(wkb[k5], (k5 + 1) + ": " + jmb[alb[k5]]);
						}

						return;
					}
					if(l == 223) {
						qdb = abyte0[1] & 0xff;
						return;
					}
					if(l == 222) {
						ugb = true;
						vgb = 0;
						for(int l5 = 8; l5 + 9 < i1 * 8;) {
							wgb[vgb] = Util.qc(abyte0, l5, 10);
							l5 += 10;
							int j10 = Util.qc(abyte0, l5, 1);
							l5++;
							if(j10 == 0) {
								xgb[vgb] = Util.qc(abyte0, l5, 8);
								l5 += 8;
							} else {
								xgb[vgb] = Util.qc(abyte0, l5, 24);
								l5 += 24;
							}
							vgb++;
						}

						for(int k10 = 0; k10 < rdb; k10++) {
							if(vgb >= 48) {
								break;
							}
							int k14 = sdb[k10];
							boolean flag1 = false;
							for(int j21 = 0; j21 < vgb; j21++) {
								if(wgb[j21] != k14) {
									continue;
								}
								flag1 = true;
								break;
							}

							if(!flag1) {
								wgb[vgb] = k14;
								xgb[vgb] = 0;
								vgb++;
							}
						}

						return;
					}
					if(l == 221) {
						ugb = false;
						return;
					}
					if(l == 220) {
						int i6 = abyte0[1] & 0xff;
						aeb[i6] = Util.uc(abyte0, 2);
					}
				}
			}
			return;
		}
		catch(RuntimeException runtimeexception) {
			if(uz < 3) {
				super.zf.begin(17);
				super.zf.zb(runtimeexception.toString());
				qn();
				super.zf.begin(17);
				super.zf.zb("p-type:" + l + " p-size:" + i1);
				qn();
				super.zf.begin(17);
				super.zf.zb("rx:" + gcb + " ry:" + hcb + " num3l:" + zcb);
				qn();
				String s = "";
				for(int l14 = 0; l14 < 80 && l14 < i1; l14++) {
					s = s + abyte0[l14] + " ";
				}

				super.zf.begin(17);
				super.zf.zb(s);
				qn();
				uz++;
			}
		}
	}

	public boolean zp(int l) {
		int i1 = fcb.ny / 128;
		int j1 = fcb.oy / 128;
		for(int k1 = 2; k1 >= 1; k1--) {
			if(l == 1 && ((abb.ef[i1][j1 - k1] & 0x80) == 128 || (abb.ef[i1 - k1][j1] & 0x80) == 128 || (abb.ef[i1 - k1][j1 - k1] & 0x80) == 128)) {
				return false;
			}
			if(l == 3 && ((abb.ef[i1][j1 + k1] & 0x80) == 128 || (abb.ef[i1 - k1][j1] & 0x80) == 128 || (abb.ef[i1 - k1][j1 + k1] & 0x80) == 128)) {
				return false;
			}
			if(l == 5 && ((abb.ef[i1][j1 + k1] & 0x80) == 128 || (abb.ef[i1 + k1][j1] & 0x80) == 128 || (abb.ef[i1 + k1][j1 + k1] & 0x80) == 128)) {
				return false;
			}
			if(l == 7 && ((abb.ef[i1][j1 - k1] & 0x80) == 128 || (abb.ef[i1 + k1][j1] & 0x80) == 128 || (abb.ef[i1 + k1][j1 - k1] & 0x80) == 128)) {
				return false;
			}
			if(l == 0 && (abb.ef[i1][j1 - k1] & 0x80) == 128) {
				return false;
			}
			if(l == 2 && (abb.ef[i1 - k1][j1] & 0x80) == 128) {
				return false;
			}
			if(l == 4 && (abb.ef[i1][j1 + k1] & 0x80) == 128) {
				return false;
			}
			if(l == 6 && (abb.ef[i1 + k1][j1] & 0x80) == 128) {
				return false;
			}
		}

		return true;
	}

	public void un() {
		if((tbb & 1) == 1 && zp(tbb)) {
			return;
		}
		if((tbb & 1) == 0 && zp(tbb)) {
			if(zp(tbb + 1 & 7)) {
				tbb = tbb + 1 & 7;
				return;
			}
			if(zp(tbb + 7 & 7)) {
				tbb = tbb + 7 & 7;
			}
			return;
		}
		int ai[] = {
			1, -1, 2, -2, 3, -3, 4
		};
		for(int l = 0; l < 7; l++) {
			if(!zp(tbb + ai[l] + 8 & 7)) {
				continue;
			}
			tbb = tbb + ai[l] + 8 & 7;
			break;
		}

		if((tbb & 1) == 0 && zp(tbb)) {
			if(zp(tbb + 1 & 7)) {
				tbb = tbb + 1 & 7;
				return;
			}
			if(zp(tbb + 7 & 7)) {
				tbb = tbb + 7 & 7;
			}
			return;
		} else {
			return;
		}
	}

	public void pn() {
		if(lhb != 0) {
			gab.jg();
			gab.ug("Oh dear! You are dead...", lab / 2, mab / 2, 7, 0xff0000);
			mn();
			gab.eg(eab, 0, 11);
			return;
		}
		if(djb != 0) {
			eo();
			return;
		}
		if(ulb) {
			xp();
			return;
		}
		if(rkb) {
			tp();
			return;
		}
		if(!abb.gf) {
			return;
		}
		for(int l = 0; l < 64; l++) {
			fab.cb(abb.jf[ebb][l]);
			if(ebb == 0) {
				fab.cb(abb._fldif[1][l]);
				fab.cb(abb.jf[1][l]);
				fab.cb(abb._fldif[2][l]);
				fab.cb(abb.jf[2][l]);
			}
			qbb = true;
			if(ebb == 0 && (abb.ef[fcb.ny / 128][fcb.oy / 128] & 0x80) == 0) {
				fab.ib(abb.jf[ebb][l]);
				if(ebb == 0) {
					fab.ib(abb._fldif[1][l]);
					fab.ib(abb.jf[1][l]);
					fab.ib(abb._fldif[2][l]);
					fab.ib(abb.jf[2][l]);
				}
				qbb = false;
			}
		}

		if(tab != vab) {
			vab = tab;
			for(int i1 = 0; i1 < zcb; i1++) {
				if(ddb[i1] == 51) {
					int l1 = bdb[i1];
					int l2 = cdb[i1];
					int i4 = l1 - fcb.ny / 128;
					int j5 = l2 - fcb.oy / 128;
					byte byte0 = 7;
					if(l1 >= 0 && l2 >= 0 && l1 < 96 && l2 < 96 && i4 > -byte0 && i4 < byte0 && j5 > -byte0 && j5 < byte0) {
						fab.cb(adb[i1]);
						String s = "torcha" + (tab + 1);
						int i10 = Config.matchModel(s);
						Model p1 = models[i10].bm();
						fab.ib(p1);
						p1.pm(true, 48, 48, -50, -10, -50);
						p1.lm(adb[i1]);
						p1.qv = i1;
						adb[i1] = p1;
					}
				}
				if(ddb[i1] == 143) {
					int i2 = bdb[i1];
					int i3 = cdb[i1];
					int j4 = i2 - fcb.ny / 128;
					int k5 = i3 - fcb.oy / 128;
					byte byte1 = 7;
					if(i2 >= 0 && i3 >= 0 && i2 < 96 && i3 < 96 && j4 > -byte1 && j4 < byte1 && k5 > -byte1 && k5 < byte1) {
						fab.cb(adb[i1]);
						String s1 = "skulltorcha" + (tab + 1);
						int j10 = Config.matchModel(s1);
						Model p2 = models[j10].bm();
						fab.ib(p2);
						p2.pm(true, 48, 48, -50, -10, -50);
						p2.lm(adb[i1]);
						p2.qv = i1;
						adb[i1] = p2;
					}
				}
			}

		}
		if(uab != wab) {
			wab = uab;
			for(int j1 = 0; j1 < zcb; j1++) {
				if(ddb[j1] == 97) {
					int j2 = bdb[j1];
					int j3 = cdb[j1];
					int k4 = j2 - fcb.ny / 128;
					int l5 = j3 - fcb.oy / 128;
					byte byte2 = 9;
					if(j2 >= 0 && j3 >= 0 && j2 < 96 && j3 < 96 && k4 > -byte2 && k4 < byte2 && l5 > -byte2 && l5 < byte2) {
						fab.cb(adb[j1]);
						String s2 = "firea" + (uab + 1);
						int k10 = Config.matchModel(s2);
						Model p3 = models[k10].bm();
						fab.ib(p3);
						p3.pm(true, 48, 48, -50, -10, -50);
						p3.lm(adb[j1]);
						p3.qv = j1;
						adb[j1] = p3;
					}
				}
			}

		}
		fab.s(bcb);
		bcb = 0;
		for(int k1 = 0; k1 < zbb; k1++) {
			Character r1 = dcb[k1];
			if(r1.jz != 255) {
				int k3 = r1.ny;
				int l4 = r1.oy;
				int i6 = -abb.vd(k3, l4);
				int l7 = fab.ab(5000 + k1, k3, i6, l4, 145, 220, k1 + 10000);
				bcb++;
				if(r1 == fcb) {
					fab.bb(l7);
				}
				if(r1.ry == 8) {
					fab.jb(l7, -30);
				}
				if(r1.ry == 9) {
					fab.jb(l7, 30);
				}
			}
		}

		for(int k2 = 0; k2 < zbb; k2++) {
			Character r2 = dcb[k2];
			if(r2.oz > 0) {
				Character r3 = null;
				if(r2.nz != -1) {
					r3 = ncb[r2.nz];
				} else
				if(r2.mz != -1) {
					r3 = ccb[r2.mz];
				}
				if(r3 != null) {
					int j6 = r2.ny;
					int i8 = r2.oy;
					int k9 = -abb.vd(j6, i8) - 110;
					int l10 = r3.ny;
					int j11 = r3.oy;
					int k11 = -abb.vd(l10, j11) - Config.mm[r3.py] / 2;
					int l11 = (j6 * r2.oz + l10 * (qab - r2.oz)) / qab;
					int i12 = (k9 * r2.oz + k11 * (qab - r2.oz)) / qab;
					int j12 = (i8 * r2.oz + j11 * (qab - r2.oz)) / qab;
					fab.ab(rab + r2.lz, l11, i12, j12, 32, 32, 0);
					bcb++;
				}
			}
		}

		for(int l3 = 0; l3 < lcb; l3++) {
			Character r4 = ocb[l3];
			int k6 = r4.ny;
			int j8 = r4.oy;
			int l9 = -abb.vd(k6, j8);
			int i11 = fab.ab(20000 + l3, k6, l9, j8, Config.lm[r4.py], Config.mm[r4.py], l3 + 30000);
			bcb++;
			if(r4.ry == 8) {
				fab.jb(i11, -30);
			}
			if(r4.ry == 9) {
				fab.jb(i11, 30);
			}
		}

		for(int i5 = 0; i5 < scb; i5++) {
			int l6 = ucb[i5] * jab + 64;
			int k8 = vcb[i5] * jab + 64;
			fab.ab(40000 + wcb[i5], l6, -abb.vd(l6, k8) - xcb[i5], k8, 96, 64, i5 + 20000);
			bcb++;
		}

		gab.mj = false;
		gab.ag();
		gab.mj = super.ks;
		if(ebb == 3) {
			int i7 = 40 + (int)(Math.random() * 3D);
			int l8 = 40 + (int)(Math.random() * 7D);
			fab.z(i7, l8, -50, -10, -50);
		}
		ilb = 0;
		clb = 0;
		nlb = 0;
		if(ueb) {
			if(veb && !qbb) {
				int j7 = tbb;
				un();
				if(tbb != j7) {
					rbb = fcb.ny;
					sbb = fcb.oy;
				}
			}
			fab.g = 3000;
			fab.h = 3000;
			fab.i = 1;
			fab.j = 2800;
			vbb = tbb * 32;
			fab.g(rbb, -abb.vd(rbb, sbb), sbb, 912, vbb * 4, 0, 2000);
		} else {
			if(veb && !qbb) {
				un();
			}
			if(!super.ks) {
				fab.g = 2400;
				fab.h = 2400;
				fab.i = 1;
				fab.j = 2300;
			} else {
				fab.g = 2200;
				fab.h = 2200;
				fab.i = 1;
				fab.j = 2100;
			}
			fab.g(rbb, -abb.vd(rbb, sbb), sbb, 912, vbb * 4, 0, pbb * 2);
		}
		fab.i();
		dp();
		if(xab > 0) {
			gab.rf(yab - 8, zab - 8, pab + 14 + (24 - xab) / 6);
		}
		if(xab < 0) {
			gab.rf(yab - 8, zab - 8, pab + 18 + (24 + xab) / 6);
		}
		int k7 = 2203 - (hcb + cbb + gbb);
		if(k7 > 0) {
			int i9 = 1 + k7 / 6;
			gab.rf(453, mab - 56, pab + 13);
			gab.ug("Wilderness", 465, mab - 20, 1, 0xffff00);
			gab.ug("Level: " + i9, 465, mab - 7, 1, 0xffff00);
			if(mhb == 0) {
				mhb = 2;
			}
		}
		if(mhb == 0 && k7 > -10 && k7 <= 0) {
			mhb = 1;
		}
		if(wfb == 0) {
			for(int j9 = 0; j9 < xfb; j9++) {
				if(zfb[j9] > 0) {
					String s3 = yfb[j9];
					gab.bh(s3, 7, mab - 18 - j9 * 12, 1, 0xffff00);
				}
			}

		}
		rfb.xk(sfb);
		rfb.xk(ufb);
		rfb.xk(vfb);
		if(wfb == 1) {
			rfb.ek(sfb);
		} else
		if(wfb == 2) {
			rfb.ek(ufb);
		} else
		if(wfb == 3) {
			rfb.ek(vfb);
		}
		Pane.ju = 2;
		rfb.ik();
		Pane.ju = 0;
		gab.hg(((i) (gab)).ni - 3 - 197, 3, pab, 128);
		fo();
		gab.qj = false;
		mn();
		gab.eg(eab, 0, 11);
	}

	public void mn() {
		gab.rf(0, mab - 4, pab + 23);
		int l = i.rg(200, 200, 255);
		if(wfb == 0) {
			l = i.rg(255, 200, 50);
		}
		if(nfb % 30 > 15) {
			l = i.rg(255, 50, 50);
		}
		gab.ug("All messages", 54, mab + 6, 0, l);
		l = i.rg(200, 200, 255);
		if(wfb == 1) {
			l = i.rg(255, 200, 50);
		}
		if(ofb % 30 > 15) {
			l = i.rg(255, 50, 50);
		}
		gab.ug("Chat history", 155, mab + 6, 0, l);
		l = i.rg(200, 200, 255);
		if(wfb == 2) {
			l = i.rg(255, 200, 50);
		}
		if(pfb % 30 > 15) {
			l = i.rg(255, 50, 50);
		}
		gab.ug("Quest history", 255, mab + 6, 0, l);
		l = i.rg(200, 200, 255);
		if(wfb == 3) {
			l = i.rg(255, 200, 50);
		}
		if(qfb % 30 > 15) {
			l = i.rg(255, 50, 50);
		}
		gab.ug("Private history", 355, mab + 6, 0, l);
	}

	public void qp(int l, int i1, int j1, int k1, int l1, int i2, int j2) {
		int k2 = Config.zk[l1] + tcb;
		int l2 = Config.ll[l1];
		gab.mh(l, i1, j1, k1, k2, l2, 0, 0, false);
	}

	public void ip(int l, int i1, int j1, int k1, int l1, int i2, int j2) {
		Character r1 = ocb[l1];
		int k2 = r1.ry + (vbb + 16) / 32 & 7;
		boolean flag = false;
		int l2 = k2;
		if(l2 == 5) {
			l2 = 3;
			flag = true;
		} else
		if(l2 == 6) {
			l2 = 2;
			flag = true;
		} else
		if(l2 == 7) {
			l2 = 1;
			flag = true;
		}
		int i3 = l2 * 3 + gmb[(r1.qy / Config.nm[r1.py]) % 4];
		if(r1.ry == 8) {
			l2 = 5;
			flag = false;
			l -= (Config.pm[r1.py] * j2) / 100;
			i3 = l2 * 3 + hmb[(yz / (Config.om[r1.py] - 1)) % 8];
		} else
		if(r1.ry == 9) {
			l2 = 5;
			flag = true;
			l += (Config.pm[r1.py] * j2) / 100;
			i3 = l2 * 3 + imb[(yz / Config.om[r1.py]) % 8];
		}
		for(int j3 = 0; j3 < 12; j3++) {
			int k3 = tlb[l2][j3];
			int j4 = Config.npcEntityIds[r1.py][k3];
			if(j4 >= 0) {
				int l4 = 0;
				int i5 = 0;
				int j5 = i3;
				if(flag && l2 >= 1 && l2 <= 3 && Config.ym[j4] == 1) {
					j5 += 15;
				}
				if(l2 != 5 || Config.xm[j4] == 1) {
					int k5 = j5 + Config.zm[j4];
					l4 = (l4 * j1) / ((i) (gab)).ej[k5];
					i5 = (i5 * k1) / ((i) (gab)).fj[k5];
					int l5 = (j1 * ((i) (gab)).ej[k5]) / ((i) (gab)).ej[Config.zm[j4]];
					l4 -= (l5 - j1) / 2;
					int i6 = Config.vm[j4];
					int j6 = 0;
					if(i6 == 1) {
						i6 = Config.npcColors1[r1.py];
						j6 = Config.npcColors4[r1.py];
					} else
					if(i6 == 2) {
						i6 = Config.npcColors2[r1.py];
						j6 = Config.npcColors4[r1.py];
					} else
					if(i6 == 3) {
						i6 = Config.npcColors3[r1.py];
						j6 = Config.npcColors4[r1.py];
					}
					gab.mh(l + l4, i1 + i5, l5, k1, k5, i6, j6, i2, flag);
				}
			}
		}

		if(r1.zy > 0) {
			glb[clb] = gab.nf(r1.yy, 1) / 2;
			hlb[clb] = gab.pf(1);
			if(glb[clb] > 300) {
				glb[clb] = 300;
				hlb[clb] *= 2;
			}
			elb[clb] = l + j1 / 2;
			flb[clb] = i1;
			dlb[clb++] = r1.yy;
		}
		if(r1.ry == 8 || r1.ry == 9 || r1.fz != 0) {
			if(r1.fz > 0) {
				int l3 = l;
				if(r1.ry == 8) {
					l3 -= (20 * j2) / 100;
				} else
				if(r1.ry == 9) {
					l3 += (20 * j2) / 100;
				}
				int k4 = (r1.dz * 30) / r1.ez;
				olb[nlb] = l3 + j1 / 2;
				plb[nlb] = i1;
				qlb[nlb++] = k4;
			}
			if(r1.fz > 150) {
				int i4 = l;
				if(r1.ry == 8) {
					i4 -= (10 * j2) / 100;
				} else
				if(r1.ry == 9) {
					i4 += (10 * j2) / 100;
				}
				gab.rf((i4 + j1 / 2) - 12, (i1 + k1 / 2) - 12, pab + 12);
				gab.ug(String.valueOf(r1.cz), (i4 + j1 / 2) - 1, i1 + k1 / 2 + 5, 3, 0xffffff);
			}
		}
	}

	public void mo(int l, int i1, int j1, int k1, int l1, int i2, int j2) {
		Character r1 = dcb[l1];
		if(r1.jz == 255) {
			return;
		}
		int k2 = r1.ry + (vbb + 16) / 32 & 7;
		boolean flag = false;
		int l2 = k2;
		if(l2 == 5) {
			l2 = 3;
			flag = true;
		} else
		if(l2 == 6) {
			l2 = 2;
			flag = true;
		} else
		if(l2 == 7) {
			l2 = 1;
			flag = true;
		}
		int i3 = l2 * 3 + gmb[(r1.qy / 6) % 4];
		if(r1.ry == 8) {
			l2 = 5;
			k2 = 2;
			flag = false;
			l -= (5 * j2) / 100;
			i3 = l2 * 3 + hmb[(yz / 5) % 8];
		} else
		if(r1.ry == 9) {
			l2 = 5;
			k2 = 2;
			flag = true;
			l += (5 * j2) / 100;
			i3 = l2 * 3 + imb[(yz / 6) % 8];
		}
		for(int j3 = 0; j3 < 12; j3++) {
			int k3 = tlb[k2][j3];
			int k4 = r1.xy[k3] - 1;
			if(k4 >= 0) {
				int j5 = 0;
				int l5 = 0;
				int i6 = i3;
				if(flag && l2 >= 1 && l2 <= 3) {
					if(Config.ym[k4] == 1) {
						i6 += 15;
					} else
					if(k3 == 4 && l2 == 1) {
						j5 = -22;
						l5 = -3;
						i6 = l2 * 3 + gmb[(2 + r1.qy / 6) % 4];
					} else
					if(k3 == 4 && l2 == 2) {
						j5 = 0;
						l5 = -8;
						i6 = l2 * 3 + gmb[(2 + r1.qy / 6) % 4];
					} else
					if(k3 == 4 && l2 == 3) {
						j5 = 26;
						l5 = -5;
						i6 = l2 * 3 + gmb[(2 + r1.qy / 6) % 4];
					} else
					if(k3 == 3 && l2 == 1) {
						j5 = 22;
						l5 = 3;
						i6 = l2 * 3 + gmb[(2 + r1.qy / 6) % 4];
					} else
					if(k3 == 3 && l2 == 2) {
						j5 = 0;
						l5 = 8;
						i6 = l2 * 3 + gmb[(2 + r1.qy / 6) % 4];
					} else
					if(k3 == 3 && l2 == 3) {
						j5 = -26;
						l5 = 5;
						i6 = l2 * 3 + gmb[(2 + r1.qy / 6) % 4];
					}
				}
				if(l2 != 5 || Config.xm[k4] == 1) {
					int j6 = i6 + Config.zm[k4];
					j5 = (j5 * j1) / ((i) (gab)).ej[j6];
					l5 = (l5 * k1) / ((i) (gab)).fj[j6];
					int k6 = (j1 * ((i) (gab)).ej[j6]) / ((i) (gab)).ej[Config.zm[k4]];
					j5 -= (k6 - j1) / 2;
					int l6 = Config.vm[k4];
					int i7 = fmb[r1.kz];
					if(l6 == 1) {
						l6 = emb[r1.hz];
					} else
					if(l6 == 2) {
						l6 = dmb[r1.iz];
					} else
					if(l6 == 3) {
						l6 = dmb[r1.jz];
					}
					gab.mh(l + j5, i1 + l5, k6, k1, j6, l6, i7, i2, flag);
				}
			}
		}

		if(r1.zy > 0) {
			glb[clb] = gab.nf(r1.yy, 1) / 2;
			hlb[clb] = gab.pf(1);
			if(glb[clb] > 300) {
				glb[clb] = 300;
				hlb[clb] *= 2;
			}
			elb[clb] = l + j1 / 2;
			flb[clb] = i1;
			dlb[clb++] = r1.yy;
		}
		if(r1.bz > 0) {
			jlb[ilb] = l + j1 / 2;
			klb[ilb] = i1;
			llb[ilb] = j2;
			mlb[ilb++] = r1.az;
		}
		if(r1.ry == 8 || r1.ry == 9 || r1.fz != 0) {
			if(r1.fz > 0) {
				int l3 = l;
				if(r1.ry == 8) {
					l3 -= (20 * j2) / 100;
				} else
				if(r1.ry == 9) {
					l3 += (20 * j2) / 100;
				}
				int l4 = (r1.dz * 30) / r1.ez;
				olb[nlb] = l3 + j1 / 2;
				plb[nlb] = i1;
				qlb[nlb++] = l4;
			}
			if(r1.fz > 150) {
				int i4 = l;
				if(r1.ry == 8) {
					i4 -= (10 * j2) / 100;
				} else
				if(r1.ry == 9) {
					i4 += (10 * j2) / 100;
				}
				gab.rf((i4 + j1 / 2) - 12, (i1 + k1 / 2) - 12, pab + 11);
				gab.ug(String.valueOf(r1.cz), (i4 + j1 / 2) - 1, i1 + k1 / 2 + 5, 3, 0xffffff);
			}
		}
		if(r1.rz == 1 && r1.bz == 0) {
			int j4 = i2 + l + j1 / 2;
			if(r1.ry == 8) {
				j4 -= (20 * j2) / 100;
			} else
			if(r1.ry == 9) {
				j4 += (20 * j2) / 100;
			}
			int i5 = (16 * j2) / 100;
			int k5 = (16 * j2) / 100;
			gab.vg(j4 - i5 / 2, i1 - k5 / 2 - (10 * j2) / 100, i5, k5, pab + 13);
		}
	}

	public void dp() {
		for(int l = 0; l < clb; l++) {
			int i1 = elb[l];
			int k1 = flb[l];
			int j2 = glb[l];
			int i3 = hlb[l];
			boolean flag = true;
			while(flag)  {
				flag = false;
				for(int j4 = 0; j4 < l; j4++) {
					if(k1 > flb[j4] - hlb[j4] && k1 - i3 < flb[j4] && i1 - j2 < elb[j4] + glb[j4] && i1 + j2 > elb[j4] - glb[j4]) {
						k1 = flb[j4] - i3;
						flag = true;
					}
				}

			}
			flb[l] = k1;
			gab.gg(dlb[l], i1, k1, 1, 0xffff00, 300);
		}

		for(int j1 = 0; j1 < ilb; j1++) {
			int l1 = jlb[j1];
			int k2 = klb[j1];
			int j3 = llb[j1];
			int l3 = mlb[j1];
			int k4 = (39 * j3) / 100;
			int l4 = (27 * j3) / 100;
			int i5 = k2 - l4;
			gab.zf(l1 - k4 / 2, i5, k4, l4, pab + 9, 85);
			int j5 = (36 * j3) / 100;
			int k5 = (24 * j3) / 100;
			gab.mh(l1 - j5 / 2, (i5 + l4 / 2) - k5 / 2, j5, k5, Config.zk[l3] + tcb, Config.ll[l3], 0, 0, false);
		}

		for(int i2 = 0; i2 < nlb; i2++) {
			int l2 = olb[i2];
			int k3 = plb[i2];
			int i4 = qlb[i2];
			gab.gh(l2 - 15, k3 - 3, i4, 5, 65280, 192);
			gab.gh((l2 - 15) + i4, k3 - 3, 30 - i4, 5, 0xff0000, 192);
		}

	}

	public int ro(int l) {
		int i1 = 0;
		for(int j1 = 0; j1 < rdb; j1++) {
			if(sdb[j1] == l) {
				if(Config.bl[l] == 1) {
					i1++;
				} else {
					i1 += tdb[j1];
				}
			}
		}

		return i1;
	}

	public boolean rn(int l, int i1) {
		if(l == 31 && ko(197)) {
			return true;
		}
		if(l == 32 && ko(102)) {
			return true;
		}
		if(l == 33 && ko(101)) {
			return true;
		}
		if(l == 34 && ko(103)) {
			return true;
		}
		return ro(l) >= i1;
	}

	public boolean ko(int l) {
		for(int i1 = 0; i1 < rdb; i1++) {
			if(sdb[i1] == l && udb[i1] == 1) {
				return true;
			}
		}

		return false;
	}

	public void ln(int l, int i1, int j1) {
		gab.yg(l, i1, j1);
		gab.yg(l - 1, i1, j1);
		gab.yg(l + 1, i1, j1);
		gab.yg(l, i1 - 1, j1);
		gab.yg(l, i1 + 1, j1);
	}

	public void np(int l, int i1, int j1, int k1, boolean flag) {
		lo(l, i1, j1, k1, j1, k1, false, flag);
	}

	public void zn(int l, int i1, int j1, int k1, boolean flag) {
		if(lo(l, i1, j1, k1, j1, k1, false, flag)) {
			return;
		} else {
			lo(l, i1, j1, k1, j1, k1, true, flag);
			return;
		}
	}

	public void pp(int l, int i1, int j1, int k1) {
		int l1;
		int i2;
		if(j1 == 0 || j1 == 4) {
			l1 = Config.gn[k1];
			i2 = Config.hn[k1];
		} else {
			i2 = Config.gn[k1];
			l1 = Config.hn[k1];
		}
		if(Config.in[k1] == 2 || Config.in[k1] == 3) {
			if(j1 == 0) {
				l--;
				l1++;
			}
			if(j1 == 2) {
				i2++;
			}
			if(j1 == 4) {
				l1++;
			}
			if(j1 == 6) {
				i1--;
				i2++;
			}
			lo(gcb, hcb, l, i1, (l + l1) - 1, (i1 + i2) - 1, false, true);
			return;
		} else {
			lo(gcb, hcb, l, i1, (l + l1) - 1, (i1 + i2) - 1, true, true);
			return;
		}
	}

	public void kp(int l, int i1, int j1) {
		if(j1 == 0) {
			lo(gcb, hcb, l, i1 - 1, l, i1, false, true);
			return;
		}
		if(j1 == 1) {
			lo(gcb, hcb, l - 1, i1, l, i1, false, true);
			return;
		} else {
			lo(gcb, hcb, l, i1, l, i1, true, true);
			return;
		}
	}

	public boolean lo(int l, int i1, int j1, int k1, int l1, int i2, boolean flag, 
			boolean flag1) {
		int j2 = abb.od(l, i1, j1, k1, l1, i2, cab, dab, flag);
		if(j2 == -1) {
			return false;
		}
		j2--;
		l = cab[j2];
		i1 = dab[j2];
		j2--;
		if(flag1) {
			super.zf.begin(215);
		} else {
			super.zf.begin(255);
		}
		super.zf.ec(l + fbb);
		super.zf.ec(i1 + gbb);
		for(int k2 = j2; k2 >= 0 && k2 > j2 - 25; k2--) {
			super.zf.fc(cab[k2] - l);
			super.zf.fc(dab[k2] - i1);
		}

		qn();
		xab = -24;
		yab = super.es;
		zab = super.fs;
		return true;
	}

	public boolean _mthdo(int l, int i1) {
		if(lhb != 0) {
			abb.gf = false;
			return false;
		}
		l += bbb;
		i1 += cbb;
		if(ebb == hbb && l > ibb && l < kbb && i1 > jbb && i1 < lbb) {
			abb.gf = true;
			return false;
		}
		gab.ug("Loading... Please wait", 256, 192, 1, 0xffffff);
		mn();
		gab.eg(eab, 0, 11);
		int j1 = fbb;
		int k1 = gbb;
		int l1 = (l + 24) / 48;
		int i2 = (i1 + 24) / 48;
		ebb = hbb;
		fbb = l1 * 48 - 48;
		gbb = i2 * 48 - 48;
		ibb = l1 * 48 - 32;
		jbb = i2 * 48 - 32;
		kbb = l1 * 48 + 32;
		lbb = i2 * 48 + 32;
		abb.md(l, i1, ebb);
		fbb -= bbb;
		gbb -= cbb;
		int j2 = fbb - j1;
		int k2 = gbb - k1;
		for(int l2 = 0; l2 < zcb; l2++) {
			bdb[l2] -= j2;
			cdb[l2] -= k2;
			int i3 = bdb[l2];
			int k3 = cdb[l2];
			int j4 = ddb[l2];
			Model p1 = adb[l2];
			try {
				int k5 = edb[l2];
				int j6;
				int l6;
				if(k5 == 0 || k5 == 4) {
					j6 = Config.gn[j4];
					l6 = Config.hn[j4];
				} else {
					l6 = Config.gn[j4];
					j6 = Config.hn[j4];
				}
				int i7 = ((i3 + i3 + j6) * jab) / 2;
				int j7 = ((k3 + k3 + l6) * jab) / 2;
				if(i3 >= 0 && k3 >= 0 && i3 < 96 && k3 < 96) {
					fab.ib(p1);
					p1.ql(i7, -abb.vd(i7, j7), j7);
					abb.jd(i3, k3, j4);
					if(j4 == 74) {
						p1.fm(0, -480, 0);
					}
				}
			}
			catch(RuntimeException runtimeexception) {
				System.out.println("Loc Error: " + runtimeexception.getMessage());
				System.out.println("i:" + l2 + " obj:" + p1);
				runtimeexception.printStackTrace();
			}
		}

		for(int j3 = 0; j3 < idb; j3++) {
			kdb[j3] -= j2;
			ldb[j3] -= k2;
			int l3 = kdb[j3];
			int k4 = ldb[j3];
			int i5 = ndb[j3];
			int l5 = mdb[j3];
			try {
				abb.ed(l3, k4, l5, i5);
				Model p2 = hn(l3, k4, l5, i5, j3);
				jdb[j3] = p2;
			}
			catch(RuntimeException runtimeexception1) {
				System.out.println("Bound Error: " + runtimeexception1.getMessage());
				runtimeexception1.printStackTrace();
			}
		}

		for(int i4 = 0; i4 < scb; i4++) {
			ucb[i4] -= j2;
			vcb[i4] -= k2;
		}

		for(int l4 = 0; l4 < zbb; l4++) {
			Character r1 = dcb[l4];
			r1.ny -= j2 * jab;
			r1.oy -= k2 * jab;
			for(int i6 = 0; i6 <= r1.uy; i6++) {
				r1.vy[i6] -= j2 * jab;
				r1.wy[i6] -= k2 * jab;
			}

		}

		for(int j5 = 0; j5 < lcb; j5++) {
			Character r2 = ocb[j5];
			r2.ny -= j2 * jab;
			r2.oy -= k2 * jab;
			for(int k6 = 0; k6 <= r2.uy; k6++) {
				r2.vy[k6] -= j2 * jab;
				r2.wy[k6] -= k2 * jab;
			}

		}

		abb.gf = true;
		return true;
	}

	public Model hn(int l, int i1, int j1, int k1, int l1) {
		int i2 = l;
		int j2 = i1;
		int k2 = l;
		int l2 = i1;
		int i3 = Config.rn[k1];
		int j3 = Config.sn[k1];
		int k3 = Config.qn[k1];
		Model p1 = new Model(4, 1);
		if(j1 == 0) {
			k2 = l + 1;
		}
		if(j1 == 1) {
			l2 = i1 + 1;
		}
		if(j1 == 2) {
			i2 = l + 1;
			l2 = i1 + 1;
		}
		if(j1 == 3) {
			k2 = l + 1;
			l2 = i1 + 1;
		}
		i2 *= jab;
		j2 *= jab;
		k2 *= jab;
		l2 *= jab;
		int l3 = p1.gm(i2, -abb.vd(i2, j2), j2);
		int i4 = p1.gm(i2, -abb.vd(i2, j2) - k3, j2);
		int j4 = p1.gm(k2, -abb.vd(k2, l2) - k3, l2);
		int k4 = p1.gm(k2, -abb.vd(k2, l2), l2);
		int ai[] = {
			l3, i4, j4, k4
		};
		p1.im(4, ai, i3, j3);
		p1.pm(false, 60, 24, -50, -10, -50);
		if(l >= 0 && i1 >= 0 && l < 96 && i1 < 96) {
			fab.ib(p1);
		}
		p1.qv = l1 + 10000;
		return p1;
	}

	public void fo() {
		if(hhb != 0) {
			ap();
		} else
		if(mhb == 1) {
			lp();
		} else
		if(ugb && ihb == 0) {
			oo();
		} else
		if(mgb && ihb == 0) {
			no();
		} else
		if(agb) {
			yp();
		} else
		if(fhb != 0) {
			zo();
		} else
		if(ehb != 0) {
			vn();
		} else
		if(!khb && jhb > 0x2bf20 && ihb == 0) {
			ho();
		} else {
			if(ahb) {
				xo();
			}
			if(fcb.fz > 0) {
				bp();
			}
			wp();
			boolean flag = !ahb && !web;
			if(flag) {
				bfb = 0;
			}
			if(pdb == 0 && flag) {
				in();
			}
			if(pdb == 1) {
				qo(flag);
			}
			if(pdb == 2) {
				tn(flag);
			}
			if(pdb == 3) {
				kn(flag);
			}
			if(pdb == 4) {
				io(flag);
			}
			if(pdb == 5) {
				on(flag);
			}
			if(pdb == 6) {
				vo(flag);
			}
			if(!web && !ahb) {
				po();
			}
			if(web && !ahb) {
				vp();
			}
		}
		aab = 0;
	}

	public void xo() {
		if(aab != 0) {
			for(int l = 0; l < bhb; l++) {
				if(super.es >= gab.nf(chb[l], 1) || super.fs <= l * 12 || super.fs >= 12 + l * 12) {
					continue;
				}
				super.zf.begin(237);
				super.zf.fc(l);
				qn();
				break;
			}

			aab = 0;
			ahb = false;
			return;
		}
		for(int i1 = 0; i1 < bhb; i1++) {
			int j1 = 65535;
			if(super.es < gab.nf(chb[i1], 1) && super.fs > i1 * 12 && super.fs < 12 + i1 * 12) {
				j1 = 0xff0000;
			}
			gab.bh(chb[i1], 6, 12 + i1 * 12, 1, j1);
		}

	}

	public void bp() {
		byte byte0 = 7;
		byte byte1 = 15;
		char c1 = '\257';
		if(aab != 0) {
			for(int l = 0; l < 5; l++) {
				if(l <= 0 || super.es <= byte0 || super.es >= byte0 + c1 || super.fs <= byte1 + l * 20 || super.fs >= byte1 + l * 20 + 20) {
					continue;
				}
				dhb = l - 1;
				aab = 0;
				super.zf.begin(231);
				super.zf.fc(dhb);
				qn();
				break;
			}

		}
		for(int i1 = 0; i1 < 5; i1++) {
			if(i1 == dhb + 1) {
				gab.gh(byte0, byte1 + i1 * 20, c1, 20, i.rg(255, 0, 0), 128);
			} else {
				gab.gh(byte0, byte1 + i1 * 20, c1, 20, i.rg(190, 190, 190), 128);
			}
			gab.tg(byte0, byte1 + i1 * 20, c1, 0);
			gab.tg(byte0, byte1 + i1 * 20 + 20, c1, 0);
		}

		gab.ug("Select combat style", byte0 + c1 / 2, byte1 + 16, 3, 0xffffff);
		gab.ug("Controlled (+1 of each)", byte0 + c1 / 2, byte1 + 36, 3, 0);
		gab.ug("Aggressive (+3 strength)", byte0 + c1 / 2, byte1 + 56, 3, 0);
		gab.ug("Accurate   (+3 attack)", byte0 + c1 / 2, byte1 + 76, 3, 0);
		gab.ug("Defensive  (+3 defense)", byte0 + c1 / 2, byte1 + 96, 3, 0);
	}

	public void ho() {
		if(aab != 0) {
			aab = 0;
			if(super.es > 200 && super.es < 300 && super.fs > 230 && super.fs < 253) {
				khb = true;
				return;
			}
		}
		int l = 90;
		gab.sg(106, 70, 300, 190, 0);
		gab.bg(106, 70, 300, 190, 0xffffff);
		gab.ug("You have been playing for", 256, l, 4, 0xffffff);
		l += 20;
		gab.ug("over 1 hour. Please consider", 256, l, 4, 0xffffff);
		l += 20;
		gab.ug("visiting our advertiser if you", 256, l, 4, 0xffffff);
		l += 20;
		gab.ug("see an advert which interests you.", 256, l, 4, 0xffffff);
		l += 40;
		gab.ug("Doing so will help ensure", 256, l, 4, 0xffffff);
		l += 20;
		gab.ug("Runescape remains free.", 256, l, 4, 0xffffff);
		l += 40;
		int i1 = 0xffffff;
		if(super.es > 200 && super.es < 300 && super.fs > l - 20 && super.fs < l + 3) {
			i1 = 0xffff00;
		}
		gab.ug("Close", 256, l, 4, i1);
	}

	public void ap() {
		gab.sg(126, 137, 260, 60, 0);
		gab.bg(126, 137, 260, 60, 0xffffff);
		gab.ug("Logging out...", 256, 173, 5, 0xffffff);
	}

	public void lp() {
		int l = 97;
		gab.sg(86, 77, 340, 180, 0);
		gab.bg(86, 77, 340, 180, 0xffffff);
		gab.ug("Warning! Proceed with caution", 256, l, 4, 0xff0000);
		l += 26;
		gab.ug("If you go much further north you will enter the", 256, l, 1, 0xffffff);
		l += 13;
		gab.ug("wilderness. This a very dangerous area where", 256, l, 1, 0xffffff);
		l += 13;
		gab.ug("other players can attack you!", 256, l, 1, 0xffffff);
		l += 22;
		gab.ug("The further north you go the more dangerous it", 256, l, 1, 0xffffff);
		l += 13;
		gab.ug("becomes, but the more treasure you will find.", 256, l, 1, 0xffffff);
		l += 22;
		gab.ug("In the wilderness an indicator at the bottom-right", 256, l, 1, 0xffffff);
		l += 13;
		gab.ug("of the screen will show the current level of danger", 256, l, 1, 0xffffff);
		l += 22;
		int i1 = 0xffffff;
		if(super.fs > l - 12 && super.fs <= l && super.es > 181 && super.es < 331) {
			i1 = 0xff0000;
		}
		gab.ug("Click here to close window", 256, l, 1, i1);
		if(aab != 0) {
			if(super.fs > l - 12 && super.fs <= l && super.es > 181 && super.es < 331) {
				mhb = 2;
			}
			if(super.es < 86 || super.es > 426 || super.fs < 77 || super.fs > 257) {
				mhb = 2;
			}
			aab = 0;
		}
	}

	public void zo() {
		if(aab != 0) {
			aab = 0;
			if(super.es < 106 || super.fs < 150 || super.es > 406 || super.fs > 210) {
				fhb = 0;
				return;
			}
		}
		int l = 150;
		gab.sg(106, l, 300, 60, 0);
		gab.bg(106, l, 300, 60, 0xffffff);
		l += 22;
		if(fhb == 1) {
			gab.ug("Please enter your new password", 256, l, 4, 0xffffff);
			l += 25;
			String s = "*";
			for(int i1 = 0; i1 < super.ls.length(); i1++) {
				s = "X" + s;
			}

			gab.ug(s, 256, l, 4, 0xffffff);
			if(super.ms.length() > 0) {
				ghb = super.ms;
				super.ls = "";
				super.ms = "";
				if(ghb.length() >= 5) {
					fhb = 2;
					return;
				} else {
					fhb = 5;
					return;
				}
			}
		} else
		if(fhb == 2) {
			gab.ug("Enter password again to confirm", 256, l, 4, 0xffffff);
			l += 25;
			String s1 = "*";
			for(int j1 = 0; j1 < super.ls.length(); j1++) {
				s1 = "X" + s1;
			}

			gab.ug(s1, 256, l, 4, 0xffffff);
			if(super.ms.length() > 0) {
				if(super.ms.equalsIgnoreCase(ghb)) {
					fhb = 4;
					bf(ghb);
					return;
				} else {
					fhb = 3;
					return;
				}
			}
		} else {
			if(fhb == 3) {
				gab.ug("Passwords do not match!", 256, l, 4, 0xffffff);
				l += 25;
				gab.ug("Press any key to close", 256, l, 4, 0xffffff);
				return;
			}
			if(fhb == 4) {
				gab.ug("Ok, your request has been sent", 256, l, 4, 0xffffff);
				l += 25;
				gab.ug("Press any key to close", 256, l, 4, 0xffffff);
				return;
			}
			if(fhb == 5) {
				gab.ug("Password must be at", 256, l, 4, 0xffffff);
				l += 25;
				gab.ug("least 5 letters long", 256, l, 4, 0xffffff);
			}
		}
	}

	public void vn() {
		if(aab != 0) {
			aab = 0;
			if(ehb == 1 && (super.es < 106 || super.fs < 145 || super.es > 406 || super.fs > 215)) {
				ehb = 0;
				return;
			}
			if(ehb == 2 && (super.es < 6 || super.fs < 145 || super.es > 506 || super.fs > 215)) {
				ehb = 0;
				return;
			}
			if(ehb == 3 && (super.es < 106 || super.fs < 145 || super.es > 406 || super.fs > 215)) {
				ehb = 0;
				return;
			}
			if(super.es > 236 && super.es < 276 && super.fs > 193 && super.fs < 213) {
				ehb = 0;
				return;
			}
		}
		int l = 145;
		if(ehb == 1) {
			gab.sg(106, l, 300, 70, 0);
			gab.bg(106, l, 300, 70, 0xffffff);
			l += 20;
			gab.ug("Enter name to add to friends list", 256, l, 4, 0xffffff);
			l += 20;
			gab.ug(super.ls + "*", 256, l, 4, 0xffffff);
			if(super.ms.length() > 0) {
				String s = super.ms.trim();
				super.ls = "";
				super.ms = "";
				ehb = 0;
				if(s.length() > 0 && Util.encode37(s) != fcb.jy) {
					hf(s);
				}
			}
		}
		if(ehb == 2) {
			gab.sg(6, l, 500, 70, 0);
			gab.bg(6, l, 500, 70, 0xffffff);
			l += 20;
			gab.ug("Enter message to send to " + Util.decode37(meb), 256, l, 4, 0xffffff);
			l += 20;
			gab.ug(super.ns + "*", 256, l, 4, 0xffffff);
			if(super.os.length() > 0) {
				String s1 = super.os;
				super.ns = "";
				super.os = "";
				ehb = 0;
				pe(meb, s1);
			}
		}
		if(ehb == 3) {
			gab.sg(106, l, 300, 70, 0);
			gab.bg(106, l, 300, 70, 0xffffff);
			l += 20;
			gab.ug("Enter name to add to ignore list", 256, l, 4, 0xffffff);
			l += 20;
			gab.ug(super.ls + "*", 256, l, 4, 0xffffff);
			if(super.ms.length() > 0) {
				String s2 = super.ms.trim();
				super.ls = "";
				super.ms = "";
				ehb = 0;
				if(s2.length() > 0 && Util.encode37(s2) != fcb.jy) {
					cf(s2);
				}
			}
		}
		int i1 = 0xffffff;
		if(super.es > 236 && super.es < 276 && super.fs > 193 && super.fs < 213) {
			i1 = 0xffff00;
		}
		gab.ug("Cancel", 256, 208, 1, i1);
	}

	public void oo() {
		char c1 = '\u0198';
		char c2 = '\u014E';
		if(ygb >= vgb) {
			ygb = -1;
		}
		if(ygb >= 0 && ygb < 48 && wgb[ygb] != zgb) {
			ygb = -1;
			zgb = -2;
		}
		if(aab != 0) {
			aab = 0;
			int l = super.es - (256 - c1 / 2);
			int j1 = super.fs - (170 - c2 / 2);
			if(l >= 0 && j1 >= 12 && l < 408 && j1 < 280) {
				int l1 = 0;
				for(int k2 = 0; k2 < 6; k2++) {
					for(int i6 = 0; i6 < 8; i6++) {
						int l6 = 7 + i6 * 49;
						int k7 = 28 + k2 * 34;
						if(l > l6 && l < l6 + 49 && j1 > k7 && j1 < k7 + 34 && l1 < vgb && wgb[l1] != -1) {
							zgb = wgb[l1];
							ygb = l1;
						}
						l1++;
					}

				}

				l = 256 - c1 / 2;
				j1 = 170 - c2 / 2;
				int j6;
				if(ygb < 0) {
					j6 = -1;
				} else {
					j6 = wgb[ygb];
				}
				if(j6 != -1) {
					int i2 = xgb[ygb];
					if(Config.bl[j6] == 1 && i2 > 1) {
						i2 = 1;
					}
					if(i2 >= 1 && super.es >= l + 220 && super.fs >= j1 + 238 && super.es < l + 250 && super.fs <= j1 + 249) {
						super.zf.begin(206);
						super.zf.ec(j6);
						super.zf.ec(1);
						qn();
					}
					if(i2 >= 5 && super.es >= l + 250 && super.fs >= j1 + 238 && super.es < l + 280 && super.fs <= j1 + 249) {
						super.zf.begin(206);
						super.zf.ec(j6);
						super.zf.ec(5);
						qn();
					}
					if(i2 >= 25 && super.es >= l + 280 && super.fs >= j1 + 238 && super.es < l + 305 && super.fs <= j1 + 249) {
						super.zf.begin(206);
						super.zf.ec(j6);
						super.zf.ec(25);
						qn();
					}
					if(i2 >= 100 && super.es >= l + 305 && super.fs >= j1 + 238 && super.es < l + 335 && super.fs <= j1 + 249) {
						super.zf.begin(206);
						super.zf.ec(j6);
						super.zf.ec(100);
						qn();
					}
					if(i2 >= 500 && super.es >= l + 335 && super.fs >= j1 + 238 && super.es < l + 368 && super.fs <= j1 + 249) {
						super.zf.begin(206);
						super.zf.ec(j6);
						super.zf.ec(500);
						qn();
					}
					if(i2 >= 2500 && super.es >= l + 370 && super.fs >= j1 + 238 && super.es < l + 400 && super.fs <= j1 + 249) {
						super.zf.begin(206);
						super.zf.ec(j6);
						super.zf.ec(2500);
						qn();
					}
					if(ro(j6) >= 1 && super.es >= l + 220 && super.fs >= j1 + 263 && super.es < l + 250 && super.fs <= j1 + 274) {
						super.zf.begin(205);
						super.zf.ec(j6);
						super.zf.ec(1);
						qn();
					}
					if(ro(j6) >= 5 && super.es >= l + 250 && super.fs >= j1 + 263 && super.es < l + 280 && super.fs <= j1 + 274) {
						super.zf.begin(205);
						super.zf.ec(j6);
						super.zf.ec(5);
						qn();
					}
					if(ro(j6) >= 25 && super.es >= l + 280 && super.fs >= j1 + 263 && super.es < l + 305 && super.fs <= j1 + 274) {
						super.zf.begin(205);
						super.zf.ec(j6);
						super.zf.ec(25);
						qn();
					}
					if(ro(j6) >= 100 && super.es >= l + 305 && super.fs >= j1 + 263 && super.es < l + 335 && super.fs <= j1 + 274) {
						super.zf.begin(205);
						super.zf.ec(j6);
						super.zf.ec(100);
						qn();
					}
					if(ro(j6) >= 500 && super.es >= l + 335 && super.fs >= j1 + 263 && super.es < l + 368 && super.fs <= j1 + 274) {
						super.zf.begin(205);
						super.zf.ec(j6);
						super.zf.ec(500);
						qn();
					}
					if(ro(j6) >= 2500 && super.es >= l + 370 && super.fs >= j1 + 263 && super.es < l + 400 && super.fs <= j1 + 274) {
						super.zf.begin(205);
						super.zf.ec(j6);
						super.zf.ec(2500);
						qn();
					}
				}
			} else {
				super.zf.begin(207);
				qn();
				ugb = false;
				return;
			}
		}
		int i1 = 256 - c1 / 2;
		int k1 = 170 - c2 / 2;
		gab.sg(i1, k1, 408, 12, 192);
		int j2 = 0x989898;
		gab.gh(i1, k1 + 12, 408, 17, j2, 160);
		gab.gh(i1, k1 + 29, 8, 204, j2, 160);
		gab.gh(i1 + 399, k1 + 29, 9, 204, j2, 160);
		gab.gh(i1, k1 + 233, 408, 47, j2, 160);
		gab.bh("Bank", i1 + 1, k1 + 10, 1, 0xffffff);
		int l2 = 0xffffff;
		if(super.es > i1 + 320 && super.fs >= k1 && super.es < i1 + 408 && super.fs < k1 + 12) {
			l2 = 0xff0000;
		}
		gab.lg("Close window", i1 + 406, k1 + 10, 1, l2);
		gab.bh("Number in bank in green", i1 + 7, k1 + 24, 1, 65280);
		gab.bh("Number held in blue", i1 + 289, k1 + 24, 1, 65535);
		int k6 = 0xd0d0d0;
		int i7 = 0;
		for(int l7 = 0; l7 < 6; l7++) {
			for(int i8 = 0; i8 < 8; i8++) {
				int k8 = i1 + 7 + i8 * 49;
				int l8 = k1 + 28 + l7 * 34;
				if(ygb == i7) {
					gab.gh(k8, l8, 49, 34, 0xff0000, 160);
				} else {
					gab.gh(k8, l8, 49, 34, k6, 160);
				}
				gab.bg(k8, l8, 50, 35, 0);
				if(i7 < vgb && wgb[i7] != -1) {
					gab.mh(k8, l8, 48, 32, tcb + Config.zk[wgb[i7]], Config.ll[wgb[i7]], 0, 0, false);
					gab.bh(String.valueOf(xgb[i7]), k8 + 1, l8 + 10, 1, 65280);
					gab.lg(String.valueOf(ro(wgb[i7])), k8 + 47, l8 + 29, 1, 65535);
				}
				i7++;
			}

		}

		gab.tg(i1 + 5, k1 + 256, 398, 0);
		if(ygb == -1) {
			gab.ug("Select an object to withdraw or deposit", i1 + 204, k1 + 248, 3, 0xffff00);
			return;
		}
		int j8;
		if(ygb < 0) {
			j8 = -1;
		} else {
			j8 = wgb[ygb];
		}
		if(j8 != -1) {
			int j7 = xgb[ygb];
			if(Config.bl[j8] == 1 && j7 > 1) {
				j7 = 1;
			}
			if(j7 > 0) {
				gab.bh("Withdraw " + Config.vk[j8][0], i1 + 2, k1 + 248, 1, 0xffffff);
				int i3 = 0xffffff;
				if(super.es >= i1 + 220 && super.fs >= k1 + 238 && super.es < i1 + 250 && super.fs <= k1 + 249) {
					i3 = 0xff0000;
				}
				gab.bh("One", i1 + 222, k1 + 248, 1, i3);
				if(j7 >= 5) {
					int j3 = 0xffffff;
					if(super.es >= i1 + 250 && super.fs >= k1 + 238 && super.es < i1 + 280 && super.fs <= k1 + 249) {
						j3 = 0xff0000;
					}
					gab.bh("Five", i1 + 252, k1 + 248, 1, j3);
				}
				if(j7 >= 25) {
					int k3 = 0xffffff;
					if(super.es >= i1 + 280 && super.fs >= k1 + 238 && super.es < i1 + 305 && super.fs <= k1 + 249) {
						k3 = 0xff0000;
					}
					gab.bh("25", i1 + 282, k1 + 248, 1, k3);
				}
				if(j7 >= 100) {
					int l3 = 0xffffff;
					if(super.es >= i1 + 305 && super.fs >= k1 + 238 && super.es < i1 + 335 && super.fs <= k1 + 249) {
						l3 = 0xff0000;
					}
					gab.bh("100", i1 + 307, k1 + 248, 1, l3);
				}
				if(j7 >= 500) {
					int i4 = 0xffffff;
					if(super.es >= i1 + 335 && super.fs >= k1 + 238 && super.es < i1 + 368 && super.fs <= k1 + 249) {
						i4 = 0xff0000;
					}
					gab.bh("500", i1 + 337, k1 + 248, 1, i4);
				}
				if(j7 >= 2500) {
					int j4 = 0xffffff;
					if(super.es >= i1 + 370 && super.fs >= k1 + 238 && super.es < i1 + 400 && super.fs <= k1 + 249) {
						j4 = 0xff0000;
					}
					gab.bh("2500", i1 + 370, k1 + 248, 1, j4);
				}
			}
			if(ro(j8) > 0) {
				gab.bh("Deposit " + Config.vk[j8][0], i1 + 2, k1 + 273, 1, 0xffffff);
				int k4 = 0xffffff;
				if(super.es >= i1 + 220 && super.fs >= k1 + 263 && super.es < i1 + 250 && super.fs <= k1 + 274) {
					k4 = 0xff0000;
				}
				gab.bh("One", i1 + 222, k1 + 273, 1, k4);
				if(ro(j8) >= 5) {
					int l4 = 0xffffff;
					if(super.es >= i1 + 250 && super.fs >= k1 + 263 && super.es < i1 + 280 && super.fs <= k1 + 274) {
						l4 = 0xff0000;
					}
					gab.bh("Five", i1 + 252, k1 + 273, 1, l4);
				}
				if(ro(j8) >= 25) {
					int i5 = 0xffffff;
					if(super.es >= i1 + 280 && super.fs >= k1 + 263 && super.es < i1 + 305 && super.fs <= k1 + 274) {
						i5 = 0xff0000;
					}
					gab.bh("25", i1 + 282, k1 + 273, 1, i5);
				}
				if(ro(j8) >= 100) {
					int j5 = 0xffffff;
					if(super.es >= i1 + 305 && super.fs >= k1 + 263 && super.es < i1 + 335 && super.fs <= k1 + 274) {
						j5 = 0xff0000;
					}
					gab.bh("100", i1 + 307, k1 + 273, 1, j5);
				}
				if(ro(j8) >= 500) {
					int k5 = 0xffffff;
					if(super.es >= i1 + 335 && super.fs >= k1 + 263 && super.es < i1 + 368 && super.fs <= k1 + 274) {
						k5 = 0xff0000;
					}
					gab.bh("500", i1 + 337, k1 + 273, 1, k5);
				}
				if(ro(j8) >= 2500) {
					int l5 = 0xffffff;
					if(super.es >= i1 + 370 && super.fs >= k1 + 263 && super.es < i1 + 400 && super.fs <= k1 + 274) {
						l5 = 0xff0000;
					}
					gab.bh("2500", i1 + 370, k1 + 273, 1, l5);
				}
			}
		}
	}

	public void no() {
		if(aab != 0) {
			aab = 0;
			int l = super.es - 52;
			int i1 = super.fs - 44;
			if(l >= 0 && i1 >= 12 && l < 408 && i1 < 246) {
				int j1 = 0;
				for(int l1 = 0; l1 < 5; l1++) {
					for(int l2 = 0; l2 < 8; l2++) {
						int k3 = 7 + l2 * 49;
						int k4 = 28 + l1 * 34;
						if(l > k3 && l < k3 + 49 && i1 > k4 && i1 < k4 + 34 && pgb[j1] != -1) {
							sgb = j1;
							tgb = pgb[j1];
						}
						j1++;
					}

				}

				if(sgb >= 0) {
					int i3 = pgb[sgb];
					if(i3 != -1) {
						if(qgb[sgb] > 0 && l > 298 && i1 >= 204 && l < 408 && i1 <= 215) {
							int l3 = ogb + rgb[sgb];
							if(l3 < 10) {
								l3 = 10;
							}
							int l4 = (l3 * Config.al[i3]) / 100;
							super.zf.begin(217);
							super.zf.ec(pgb[sgb]);
							super.zf.ec(l4);
							qn();
						}
						if(ro(i3) > 0 && l > 2 && i1 >= 229 && l < 112 && i1 <= 240) {
							int i4 = ngb + rgb[sgb];
							if(i4 < 10) {
								i4 = 10;
							}
							int i5 = (i4 * Config.al[i3]) / 100;
							super.zf.begin(216);
							super.zf.ec(pgb[sgb]);
							super.zf.ec(i5);
							qn();
						}
					}
				}
			} else {
				super.zf.begin(218);
				qn();
				mgb = false;
				return;
			}
		}
		byte byte0 = 52;
		byte byte1 = 44;
		gab.sg(byte0, byte1, 408, 12, 192);
		int k1 = 0x989898;
		gab.gh(byte0, byte1 + 12, 408, 17, k1, 160);
		gab.gh(byte0, byte1 + 29, 8, 170, k1, 160);
		gab.gh(byte0 + 399, byte1 + 29, 9, 170, k1, 160);
		gab.gh(byte0, byte1 + 199, 408, 47, k1, 160);
		gab.bh("Buying and selling items", byte0 + 1, byte1 + 10, 1, 0xffffff);
		int i2 = 0xffffff;
		if(super.es > byte0 + 320 && super.fs >= byte1 && super.es < byte0 + 408 && super.fs < byte1 + 12) {
			i2 = 0xff0000;
		}
		gab.lg("Close window", byte0 + 406, byte1 + 10, 1, i2);
		gab.bh("Shops stock in green", byte0 + 2, byte1 + 24, 1, 65280);
		gab.bh("Number you own in blue", byte0 + 135, byte1 + 24, 1, 65535);
		gab.bh("Your money: " + ro(10) + "gp", byte0 + 280, byte1 + 24, 1, 0xffff00);
		int j3 = 0xd0d0d0;
		int j4 = 0;
		for(int j5 = 0; j5 < 5; j5++) {
			for(int k5 = 0; k5 < 8; k5++) {
				int i6 = byte0 + 7 + k5 * 49;
				int l6 = byte1 + 28 + j5 * 34;
				if(sgb == j4) {
					gab.gh(i6, l6, 49, 34, 0xff0000, 160);
				} else {
					gab.gh(i6, l6, 49, 34, j3, 160);
				}
				gab.bg(i6, l6, 50, 35, 0);
				if(pgb[j4] != -1) {
					gab.mh(i6, l6, 48, 32, tcb + Config.zk[pgb[j4]], Config.ll[pgb[j4]], 0, 0, false);
					gab.bh(String.valueOf(qgb[j4]), i6 + 1, l6 + 10, 1, 65280);
					gab.lg(String.valueOf(ro(pgb[j4])), i6 + 47, l6 + 10, 1, 65535);
				}
				j4++;
			}

		}

		gab.tg(byte0 + 5, byte1 + 222, 398, 0);
		if(sgb == -1) {
			gab.ug("Select an object to buy or sell", byte0 + 204, byte1 + 214, 3, 0xffff00);
			return;
		}
		int l5 = pgb[sgb];
		if(l5 != -1) {
			if(qgb[sgb] > 0) {
				int j6 = ogb + rgb[sgb];
				if(j6 < 10) {
					j6 = 10;
				}
				int i7 = (j6 * Config.al[l5]) / 100;
				gab.bh("Buy a new " + Config.vk[l5][0] + " for " + i7 + "gp", byte0 + 2, byte1 + 214, 1, 0xffff00);
				int j2 = 0xffffff;
				if(super.es > byte0 + 298 && super.fs >= byte1 + 204 && super.es < byte0 + 408 && super.fs <= byte1 + 215) {
					j2 = 0xff0000;
				}
				gab.lg("Click here to buy", byte0 + 405, byte1 + 214, 3, j2);
			} else {
				gab.ug("This item is not currently available to buy", byte0 + 204, byte1 + 214, 3, 0xffff00);
			}
			if(ro(l5) > 0) {
				int k6 = ngb + rgb[sgb];
				if(k6 < 10) {
					k6 = 10;
				}
				int j7 = (k6 * Config.al[l5]) / 100;
				gab.lg("Sell your " + Config.vk[l5][0] + " for " + j7 + "gp", byte0 + 405, byte1 + 239, 1, 0xffff00);
				int k2 = 0xffffff;
				if(super.es > byte0 + 2 && super.fs >= byte1 + 229 && super.es < byte0 + 112 && super.fs <= byte1 + 240) {
					k2 = 0xff0000;
				}
				gab.bh("Click here to sell", byte0 + 2, byte1 + 239, 3, k2);
				return;
			}
			gab.ug("You do not have any of this item to sell", byte0 + 204, byte1 + 239, 3, 0xffff00);
		}
	}

	public void yp() {
		if(aab != 0 && lgb == 0) {
			lgb = 1;
		}
		if(lgb > 0) {
			int l = super.es - 22;
			int i1 = super.fs - 36;
			if(l >= 0 && i1 >= 0 && l < 468 && i1 < 262) {
				if(l > 216 && i1 > 30 && l < 462 && i1 < 235) {
					int j1 = (l - 217) / 49 + ((i1 - 31) / 34) * 5;
					if(j1 >= 0 && j1 < rdb) {
						boolean flag = false;
						int k2 = 0;
						int j3 = sdb[j1];
						for(int j4 = 0; j4 < cgb; j4++) {
							if(dgb[j4] == j3) {
								if(Config.bl[j3] == 0) {
									for(int l4 = 0; l4 < lgb; l4++) {
										if(egb[j4] < tdb[j1]) {
											egb[j4]++;
										}
										flag = true;
									}

								} else {
									k2++;
								}
							}
						}

						if(ro(j3) <= k2) {
							flag = true;
						}
						if(!flag && cgb < 12) {
							dgb[cgb] = j3;
							egb[cgb] = 1;
							cgb++;
							flag = true;
						}
						if(flag) {
							super.zf.begin(234);
							super.zf.fc(cgb);
							for(int i5 = 0; i5 < cgb; i5++) {
								super.zf.ec(dgb[i5]);
								super.zf.ec(egb[i5]);
							}

							qn();
							igb = false;
							jgb = false;
						}
					}
				}
				if(l > 8 && i1 > 30 && l < 205 && i1 < 133) {
					int k1 = (l - 9) / 49 + ((i1 - 31) / 34) * 4;
					if(k1 >= 0 && k1 < cgb) {
						int i2 = dgb[k1];
						for(int l2 = 0; l2 < lgb; l2++) {
							if(Config.bl[i2] == 0 && egb[k1] > 1) {
								egb[k1]--;
								continue;
							}
							cgb--;
							kgb = 0;
							for(int k3 = k1; k3 < cgb; k3++) {
								dgb[k3] = dgb[k3 + 1];
								egb[k3] = egb[k3 + 1];
							}

							break;
						}

						super.zf.begin(234);
						super.zf.fc(cgb);
						for(int l3 = 0; l3 < cgb; l3++) {
							super.zf.ec(dgb[l3]);
							super.zf.ec(egb[l3]);
						}

						qn();
						igb = false;
						jgb = false;
					}
				}
				if(l > 143 && i1 > 141 && l < 154 && i1 < 152) {
					jgb = !jgb;
					super.zf.begin(232);
					super.zf.fc(jgb ? 1 : 0);
					qn();
				}
				if(l > 413 && i1 > 237 && l < 462 && i1 < 258) {
					agb = false;
					super.zf.begin(233);
					qn();
				}
			} else
			if(aab != 0) {
				agb = false;
				super.zf.begin(233);
				qn();
			}
			aab = 0;
			lgb = 0;
		}
		if(!agb) {
			return;
		}
		byte byte0 = 22;
		byte byte1 = 36;
		gab.sg(byte0, byte1, 468, 12, 192);
		int l1 = 0x989898;
		gab.gh(byte0, byte1 + 12, 468, 18, l1, 160);
		gab.gh(byte0, byte1 + 30, 8, 248, l1, 160);
		gab.gh(byte0 + 205, byte1 + 30, 11, 248, l1, 160);
		gab.gh(byte0 + 462, byte1 + 30, 6, 248, l1, 160);
		gab.gh(byte0 + 8, byte1 + 133, 197, 22, l1, 160);
		gab.gh(byte0 + 8, byte1 + 258, 197, 20, l1, 160);
		gab.gh(byte0 + 216, byte1 + 235, 246, 43, l1, 160);
		int j2 = 0xd0d0d0;
		gab.gh(byte0 + 8, byte1 + 30, 197, 103, j2, 160);
		gab.gh(byte0 + 8, byte1 + 155, 197, 103, j2, 160);
		gab.gh(byte0 + 216, byte1 + 30, 246, 205, j2, 160);
		for(int i3 = 0; i3 < 4; i3++) {
			gab.tg(byte0 + 8, byte1 + 30 + i3 * 34, 197, 0);
		}

		for(int i4 = 0; i4 < 4; i4++) {
			gab.tg(byte0 + 8, byte1 + 155 + i4 * 34, 197, 0);
		}

		for(int k4 = 0; k4 < 7; k4++) {
			gab.tg(byte0 + 216, byte1 + 30 + k4 * 34, 246, 0);
		}

		for(int j5 = 0; j5 < 6; j5++) {
			if(j5 < 5) {
				gab.lh(byte0 + 8 + j5 * 49, byte1 + 30, 103, 0);
			}
			if(j5 < 5) {
				gab.lh(byte0 + 8 + j5 * 49, byte1 + 155, 103, 0);
			}
			gab.lh(byte0 + 216 + j5 * 49, byte1 + 30, 205, 0);
		}

		gab.bh("Trading with: " + bgb, byte0 + 1, byte1 + 10, 1, 0xffffff);
		gab.bh("Your Offer", byte0 + 9, byte1 + 27, 4, 0xffffff);
		gab.bh("Opponent's Offer", byte0 + 9, byte1 + 152, 4, 0xffffff);
		gab.bh("Your Inventory", byte0 + 216, byte1 + 27, 4, 0xffffff);
		gab.lg("Accepted", byte0 + 204, byte1 + 27, 4, 65280);
		gab.bg(byte0 + 125, byte1 + 16, 11, 11, 65280);
		if(igb) {
			gab.sg(byte0 + 127, byte1 + 18, 7, 7, 65280);
		}
		gab.lg("Accept", byte0 + 204, byte1 + 152, 4, 65280);
		gab.bg(byte0 + 143, byte1 + 141, 11, 11, 65280);
		if(jgb) {
			gab.sg(byte0 + 145, byte1 + 143, 7, 7, 65280);
		}
		gab.lg("Close", byte0 + 408 + 49, byte1 + 251, 4, 0xc00000);
		gab.bg(byte0 + 364 + 49, byte1 + 237, 49, 21, 0xc00000);
		for(int k5 = 0; k5 < rdb; k5++) {
			int l5 = 217 + byte0 + (k5 % 5) * 49;
			int j6 = 31 + byte1 + (k5 / 5) * 34;
			gab.mh(l5, j6, 48, 32, tcb + Config.zk[sdb[k5]], Config.ll[sdb[k5]], 0, 0, false);
			if(Config.bl[sdb[k5]] == 0) {
				gab.bh(String.valueOf(tdb[k5]), l5 + 1, j6 + 10, 1, 0xffff00);
			}
		}

		for(int i6 = 0; i6 < cgb; i6++) {
			int k6 = 9 + byte0 + (i6 % 4) * 49;
			int i7 = 31 + byte1 + (i6 / 4) * 34;
			gab.mh(k6, i7, 48, 32, tcb + Config.zk[dgb[i6]], Config.ll[dgb[i6]], 0, 0, false);
			if(Config.bl[dgb[i6]] == 0) {
				gab.bh(String.valueOf(egb[i6]), k6 + 1, i7 + 10, 1, 0xffff00);
			}
			if(super.es > k6 && super.es < k6 + 48 && super.fs > i7 && super.fs < i7 + 32) {
				gab.bh(Config.vk[dgb[i6]][0] + ": @whi@" + Config.wk[dgb[i6]], byte0 + 8, byte1 + 273, 1, 0xffff00);
			}
		}

		for(int l6 = 0; l6 < fgb; l6++) {
			int j7 = 9 + byte0 + (l6 % 4) * 49;
			int k7 = 156 + byte1 + (l6 / 4) * 34;
			gab.mh(j7, k7, 48, 32, tcb + Config.zk[ggb[l6]], Config.ll[ggb[l6]], 0, 0, false);
			if(Config.bl[ggb[l6]] == 0) {
				gab.bh(String.valueOf(hgb[l6]), j7 + 1, k7 + 10, 1, 0xffff00);
			}
			if(super.es > j7 && super.es < j7 + 48 && super.fs > k7 && super.fs < k7 + 32) {
				gab.bh(Config.vk[ggb[l6]][0] + ": @whi@" + Config.wk[ggb[l6]], byte0 + 8, byte1 + 273, 1, 0xffff00);
			}
		}

	}

	public void wp() {
		if(pdb == 0 && super.es >= ((i) (gab)).ni - 35 && super.fs >= 3 && super.es < ((i) (gab)).ni - 3 && super.fs < 35) {
			pdb = 1;
		}
		if(pdb == 0 && super.es >= ((i) (gab)).ni - 35 - 33 && super.fs >= 3 && super.es < ((i) (gab)).ni - 3 - 33 && super.fs < 35) {
			pdb = 2;
		}
		if(pdb == 0 && super.es >= ((i) (gab)).ni - 35 - 66 && super.fs >= 3 && super.es < ((i) (gab)).ni - 3 - 66 && super.fs < 35) {
			pdb = 3;
		}
		if(pdb == 0 && super.es >= ((i) (gab)).ni - 35 - 99 && super.fs >= 3 && super.es < ((i) (gab)).ni - 3 - 99 && super.fs < 35) {
			pdb = 4;
		}
		if(pdb == 0 && super.es >= ((i) (gab)).ni - 35 - 132 && super.fs >= 3 && super.es < ((i) (gab)).ni - 3 - 132 && super.fs < 35) {
			pdb = 5;
		}
		if(pdb == 0 && super.es >= ((i) (gab)).ni - 35 - 165 && super.fs >= 3 && super.es < ((i) (gab)).ni - 3 - 165 && super.fs < 35) {
			pdb = 6;
		}
		if(pdb != 0 && super.es >= ((i) (gab)).ni - 35 && super.fs >= 3 && super.es < ((i) (gab)).ni - 3 && super.fs < 26) {
			pdb = 1;
		}
		if(pdb != 0 && super.es >= ((i) (gab)).ni - 35 - 33 && super.fs >= 3 && super.es < ((i) (gab)).ni - 3 - 33 && super.fs < 26) {
			pdb = 2;
		}
		if(pdb != 0 && super.es >= ((i) (gab)).ni - 35 - 66 && super.fs >= 3 && super.es < ((i) (gab)).ni - 3 - 66 && super.fs < 26) {
			pdb = 3;
		}
		if(pdb != 0 && super.es >= ((i) (gab)).ni - 35 - 99 && super.fs >= 3 && super.es < ((i) (gab)).ni - 3 - 99 && super.fs < 26) {
			pdb = 4;
		}
		if(pdb != 0 && super.es >= ((i) (gab)).ni - 35 - 132 && super.fs >= 3 && super.es < ((i) (gab)).ni - 3 - 132 && super.fs < 26) {
			pdb = 5;
		}
		if(pdb != 0 && super.es >= ((i) (gab)).ni - 35 - 165 && super.fs >= 3 && super.es < ((i) (gab)).ni - 3 - 165 && super.fs < 26) {
			pdb = 6;
		}
		if(pdb == 1 && (super.es < ((i) (gab)).ni - 248 || super.fs > 36 + (qdb / 5) * 34)) {
			pdb = 0;
		}
		if(pdb == 3 && (super.es < ((i) (gab)).ni - 199 || super.fs > 284)) {
			pdb = 0;
		}
		if((pdb == 2 || pdb == 4 || pdb == 5) && (super.es < ((i) (gab)).ni - 199 || super.fs > 240)) {
			pdb = 0;
		}
		if(pdb == 6 && (super.es < ((i) (gab)).ni - 199 || super.fs > 252)) {
			pdb = 0;
		}
	}

	public void qo(boolean flag) {
		int l = ((i) (gab)).ni - 248;
		gab.rf(l, 3, pab + 1);
		for(int i1 = 0; i1 < qdb; i1++) {
			int j1 = l + (i1 % 5) * 49;
			int l1 = 36 + (i1 / 5) * 34;
			if(i1 < rdb && udb[i1] == 1) {
				gab.gh(j1, l1, 49, 34, 0xff0000, 128);
			} else {
				gab.gh(j1, l1, 49, 34, i.rg(181, 181, 181), 128);
			}
			if(i1 < rdb) {
				gab.mh(j1, l1, 48, 32, tcb + Config.zk[sdb[i1]], Config.ll[sdb[i1]], 0, 0, false);
				if(Config.bl[sdb[i1]] == 0) {
					gab.bh(String.valueOf(tdb[i1]), j1 + 1, l1 + 10, 1, 0xffff00);
				}
			}
		}

		for(int k1 = 1; k1 <= 4; k1++) {
			gab.lh(l + k1 * 49, 36, (qdb / 5) * 34, 0);
		}

		for(int i2 = 1; i2 <= qdb / 5 - 1; i2++) {
			gab.tg(l, 36 + i2 * 34, 245, 0);
		}

		if(!flag) {
			return;
		}
		l = super.es - (((i) (gab)).ni - 248);
		int j2 = super.fs - 36;
		if(l >= 0 && j2 >= 0 && l < 248 && j2 < (qdb / 5) * 34) {
			int k2 = l / 49 + (j2 / 34) * 5;
			if(k2 < rdb) {
				int l2 = sdb[k2];
				if(ieb >= 0) {
					if(Config.vo[ieb] == 3) {
						ffb[bfb] = "Cast " + Config.ro[ieb] + " on";
						efb[bfb] = "@lre@" + Config.vk[l2][0];
						gfb[bfb] = 600;
						jfb[bfb] = k2;
						kfb[bfb] = ieb;
						bfb++;
						return;
					}
				} else {
					if(vdb >= 0) {
						ffb[bfb] = "Use " + wdb + " with";
						efb[bfb] = "@lre@" + Config.vk[l2][0];
						gfb[bfb] = 610;
						jfb[bfb] = k2;
						kfb[bfb] = vdb;
						bfb++;
						return;
					}
					if(udb[k2] == 1) {
						ffb[bfb] = "Remove";
						efb[bfb] = "@lre@" + Config.vk[l2][0];
						gfb[bfb] = 620;
						jfb[bfb] = k2;
						bfb++;
					} else
					if(Config.jl[l2] != 0) {
						if((Config.jl[l2] & 0x18) != 0) {
							ffb[bfb] = "Wield";
						} else {
							ffb[bfb] = "Wear";
						}
						efb[bfb] = "@lre@" + Config.vk[l2][0];
						gfb[bfb] = 630;
						jfb[bfb] = k2;
						bfb++;
					}
					if(!Config.yk[l2].equals("_")) {
						ffb[bfb] = Config.yk[l2];
						efb[bfb] = "@lre@" + Config.vk[l2][0];
						gfb[bfb] = 640;
						jfb[bfb] = k2;
						bfb++;
					}
					ffb[bfb] = "Use";
					efb[bfb] = "@lre@" + Config.vk[l2][0];
					gfb[bfb] = 650;
					jfb[bfb] = k2;
					bfb++;
					ffb[bfb] = "Drop";
					efb[bfb] = "@lre@" + Config.vk[l2][0];
					gfb[bfb] = 660;
					jfb[bfb] = k2;
					bfb++;
					ffb[bfb] = "Examine";
					efb[bfb] = "@lre@" + Config.vk[l2][0];
					gfb[bfb] = 3600;
					jfb[bfb] = l2;
					bfb++;
				}
			}
		}
	}

	public void tn(boolean flag) {
		int l = ((i) (gab)).ni - 199;
		char c1 = '\234';
		char c3 = '\230';
		gab.rf(l - 49, 3, pab + 2);
		l += 40;
		gab.sg(l, 36, c1, c3, 0);
		gab.dh(l, 36, l + c1, 36 + c3);
		char c5 = '\300';
		int j1 = ((fcb.ny - 6040) * 3 * c5) / 2048;
		int l2 = ((fcb.oy - 6040) * 3 * c5) / 2048;
		int j4 = a.k[1024 - vbb * 4 & 0x3ff];
		int l4 = a.k[(1024 - vbb * 4 & 0x3ff) + 1024];
		int j5 = l2 * j4 + j1 * l4 >> 18;
		l2 = l2 * l4 - j1 * j4 >> 18;
		j1 = j5;
		gab.fg((l + c1 / 2) - j1, 36 + c3 / 2 + l2, pab - 1, vbb + 64 & 0xff, c5);
		for(int l6 = 0; l6 < zcb; l6++) {
			int k1 = (((bdb[l6] * jab + 64) - fcb.ny) * 3 * c5) / 2048;
			int i3 = (((cdb[l6] * jab + 64) - fcb.oy) * 3 * c5) / 2048;
			int k5 = i3 * j4 + k1 * l4 >> 18;
			i3 = i3 * l4 - k1 * j4 >> 18;
			k1 = k5;
			ln(l + c1 / 2 + k1, (36 + c3 / 2) - i3, 65535);
		}

		for(int i7 = 0; i7 < scb; i7++) {
			int l1 = (((ucb[i7] * jab + 64) - fcb.ny) * 3 * c5) / 2048;
			int j3 = (((vcb[i7] * jab + 64) - fcb.oy) * 3 * c5) / 2048;
			int l5 = j3 * j4 + l1 * l4 >> 18;
			j3 = j3 * l4 - l1 * j4 >> 18;
			l1 = l5;
			ln(l + c1 / 2 + l1, (36 + c3 / 2) - j3, 0xff0000);
		}

		for(int j7 = 0; j7 < lcb; j7++) {
			Character r1 = ocb[j7];
			int i2 = ((r1.ny - fcb.ny) * 3 * c5) / 2048;
			int k3 = ((r1.oy - fcb.oy) * 3 * c5) / 2048;
			int i6 = k3 * j4 + i2 * l4 >> 18;
			k3 = k3 * l4 - i2 * j4 >> 18;
			i2 = i6;
			ln(l + c1 / 2 + i2, (36 + c3 / 2) - k3, 0xffff00);
		}

		for(int k7 = 0; k7 < zbb; k7++) {
			Character r2 = dcb[k7];
			int j2 = ((r2.ny - fcb.ny) * 3 * c5) / 2048;
			int l3 = ((r2.oy - fcb.oy) * 3 * c5) / 2048;
			int j6 = l3 * j4 + j2 * l4 >> 18;
			l3 = l3 * l4 - j2 * j4 >> 18;
			j2 = j6;
			ln(l + c1 / 2 + j2, (36 + c3 / 2) - l3, 0xffffff);
		}

		gab.oh(l + c1 / 2, 36 + c3 / 2, 2, 0xffffff, 255);
		gab.fg(l + 19, 55, pab + 24, vbb + 128 & 0xff, 128);
		gab.dh(0, 0, lab, mab + 12);
		if(!flag) {
			return;
		}
		l = super.es - (((i) (gab)).ni - 199);
		int l7 = super.fs - 36;
		if(l >= 40 && l7 >= 0 && l < 196 && l7 < 152) {
			char c2 = '\234';
			char c4 = '\230';
			char c6 = '\300';
			int i1 = ((i) (gab)).ni - 199;
			i1 += 40;
			int k2 = ((super.es - (i1 + c2 / 2)) * 16384) / (3 * c6);
			int i4 = ((super.fs - (36 + c4 / 2)) * 16384) / (3 * c6);
			int k4 = a.k[1024 - vbb * 4 & 0x3ff];
			int i5 = a.k[(1024 - vbb * 4 & 0x3ff) + 1024];
			int k6 = i4 * k4 + k2 * i5 >> 15;
			i4 = i4 * i5 - k2 * k4 >> 15;
			k2 = k6;
			k2 += fcb.ny;
			i4 = fcb.oy - i4;
			if(aab == 1) {
				np(gcb, hcb, k2 / 128, i4 / 128, false);
			}
			aab = 0;
		}
	}

	public void kn(boolean flag) {
		int l = ((i) (gab)).ni - 199;
		int i1 = 36;
		gab.rf(l - 49, 3, pab + 3);
		char c1 = '\304';
		char c2 = '\372';
		int k1;
		int j1 = k1 = i.rg(160, 160, 160);
		if(peb == 0) {
			j1 = i.rg(220, 220, 220);
		} else {
			k1 = i.rg(220, 220, 220);
		}
		gab.gh(l, i1, c1 / 2, 24, j1, 128);
		gab.gh(l + c1 / 2, i1, c1 / 2, 24, k1, 128);
		gab.gh(l, i1 + 24, c1, c2 - 24, i.rg(220, 220, 220), 128);
		gab.tg(l, i1 + 24, c1, 0);
		gab.lh(l + c1 / 2, i1, 24, 0);
		gab.ug("Stats", l + c1 / 4, i1 + 16, 4, 0);
		gab.ug("Quests", l + c1 / 4 + c1 / 2, i1 + 16, 4, 0);
		if(peb == 0) {
			int l1 = 72;
			int j2 = -1;
			gab.bh("Skills", l + 5, l1, 3, 0xffff00);
			l1 += 13;
			for(int k2 = 0; k2 < 8; k2++) {
				int l2 = 0xffffff;
				if(super.es > l + 3 && super.fs >= l1 - 11 && super.fs < l1 + 2 && super.es < l + 90) {
					l2 = 0xff0000;
					j2 = k2;
				}
				gab.bh(deb[k2] + ":@yel@" + ydb[k2] + "/" + zdb[k2], l + 5, l1, 1, l2);
				l2 = 0xffffff;
				if(super.es >= l + 90 && super.fs >= l1 - 13 - 11 && super.fs < (l1 - 13) + 2 && super.es < l + 196) {
					l2 = 0xff0000;
					j2 = k2 + 8;
				}
				gab.bh(deb[k2 + 8] + ":@yel@" + ydb[k2 + 8] + "/" + zdb[k2 + 8], (l + c1 / 2) - 8, l1 - 13, 1, l2);
				l1 += 13;
			}

			gab.bh("Quest Points:@yel@" + ceb, (l + c1 / 2) - 8, l1 - 13, 1, 0xffffff);
			l1 += 8;
			gab.bh("Equipment Status", l + 5, l1, 3, 0xffff00);
			l1 += 12;
			for(int i3 = 0; i3 < 3; i3++) {
				gab.bh(eeb[i3] + ":@yel@" + beb[i3], l + 5, l1, 1, 0xffffff);
				if(i3 < 2) {
					gab.bh(eeb[i3 + 3] + ":@yel@" + beb[i3 + 3], l + c1 / 2 + 25, l1, 1, 0xffffff);
				}
				l1 += 13;
			}

			l1 += 6;
			gab.tg(l, l1 - 15, c1, 0);
			if(j2 != -1) {
				gab.bh(deb[j2] + " skill", l + 5, l1, 1, 0xffff00);
				l1 += 12;
				int j3 = experience[0];
				for(int k3 = 0; k3 < 98; k3++) {
					if(aeb[j2] >= experience[k3]) {
						j3 = experience[k3 + 1];
					}
				}

				gab.bh("Total xp: " + aeb[j2] / 4, l + 5, l1, 1, 0xffffff);
				l1 += 12;
				gab.bh("Next level at: " + j3 / 4, l + 5, l1, 1, 0xffffff);
			}
		}
		if(peb == 1) {
			neb.fk(oeb);
			neb.ll(oeb, 0, "@whi@Quest-list (green=completed)");
			for(int i2 = 0; i2 < qeb; i2++) {
				neb.ll(oeb, i2 + 1, (seb[i2] ? "@gre@" : "@red@") + reb[i2]);
			}

			neb.ik();
		}
		if(!flag) {
			return;
		}
		l = super.es - (((i) (gab)).ni - 199);
		i1 = super.fs - 36;
		if(l >= 0 && i1 >= 0 && l < c1 && i1 < c2) {
			if(peb == 1) {
				neb.jk(l + (((i) (gab)).ni - 199), i1 + 36, super.hs, super.gs);
			}
			if(i1 <= 24 && aab == 1) {
				if(l < 98) {
					peb = 0;
					return;
				}
				if(l > 98) {
					peb = 1;
				}
			}
		}
	}

	public void io(boolean flag) {
		int l = ((i) (gab)).ni - 199;
		int i1 = 36;
		gab.rf(l - 49, 3, pab + 4);
		char c1 = '\304';
		char c2 = '\266';
		int k1;
		int j1 = k1 = i.rg(160, 160, 160);
		if(heb == 0) {
			j1 = i.rg(220, 220, 220);
		} else {
			k1 = i.rg(220, 220, 220);
		}
		gab.gh(l, i1, c1 / 2, 24, j1, 128);
		gab.gh(l + c1 / 2, i1, c1 / 2, 24, k1, 128);
		gab.gh(l, i1 + 24, c1, 90, i.rg(220, 220, 220), 128);
		gab.gh(l, i1 + 24 + 90, c1, c2 - 90 - 24, i.rg(160, 160, 160), 128);
		gab.tg(l, i1 + 24, c1, 0);
		gab.lh(l + c1 / 2, i1, 24, 0);
		gab.tg(l, i1 + 113, c1, 0);
		gab.ug("Magic", l + c1 / 4, i1 + 16, 4, 0);
		gab.ug("Prayers", l + c1 / 4 + c1 / 2, i1 + 16, 4, 0);
		if(heb == 0) {
			feb.fk(geb);
			int l1 = 0;
			for(int l2 = 0; l2 < Config.qo; l2++) {
				String s = "@yel@";
				for(int k4 = 0; k4 < Config.uo[l2]; k4++) {
					int j5 = Config.wo[l2][k4];
					if(rn(j5, Config.xo[l2][k4])) {
						continue;
					}
					s = "@whi@";
					break;
				}

				int k5 = ydb[6];
				if(Config.to[l2] > k5) {
					s = "@bla@";
				}
				feb.ll(geb, l1++, s + "Level " + Config.to[l2] + ": " + Config.ro[l2]);
			}

			feb.ik();
			int l3 = feb.jl(geb);
			if(l3 != -1) {
				gab.bh("Level " + Config.to[l3] + ": " + Config.ro[l3], l + 2, i1 + 124, 1, 0xffff00);
				gab.bh(Config.so[l3], l + 2, i1 + 136, 0, 0xffffff);
				for(int l4 = 0; l4 < Config.uo[l3]; l4++) {
					int l5 = Config.wo[l3][l4];
					gab.rf(l + 2 + l4 * 44, i1 + 150, tcb + Config.zk[l5]);
					int i6 = ro(l5);
					int j6 = Config.xo[l3][l4];
					String s2 = "@red@";
					if(rn(l5, j6)) {
						s2 = "@gre@";
					}
					gab.bh(s2 + i6 + "/" + j6, l + 2 + l4 * 44, i1 + 150, 1, 0xffffff);
				}

			} else {
				gab.bh("Point at a spell for a description", l + 2, i1 + 124, 1, 0);
			}
		}
		if(heb == 1) {
			feb.fk(geb);
			int i2 = 0;
			for(int i3 = 0; i3 < Config.ip; i3++) {
				String s1 = "@whi@";
				if(Config.lp[i3] > zdb[5]) {
					s1 = "@bla@";
				}
				if(teb[i3]) {
					s1 = "@gre@";
				}
				feb.ll(geb, i2++, s1 + "Level " + Config.lp[i3] + ": " + Config.jp[i3]);
			}

			feb.ik();
			int i4 = feb.jl(geb);
			if(i4 != -1) {
				gab.ug("Level " + Config.lp[i4] + ": " + Config.jp[i4], l + c1 / 2, i1 + 130, 1, 0xffff00);
				gab.ug(Config.kp[i4], l + c1 / 2, i1 + 145, 0, 0xffffff);
				gab.ug("Drain rate: " + Config.mp[i4], l + c1 / 2, i1 + 160, 1, 0);
			} else {
				gab.bh("Point at a prayer for a description", l + 2, i1 + 124, 1, 0);
			}
		}
		if(!flag) {
			return;
		}
		l = super.es - (((i) (gab)).ni - 199);
		i1 = super.fs - 36;
		if(l >= 0 && i1 >= 0 && l < 196 && i1 < 182) {
			feb.jk(l + (((i) (gab)).ni - 199), i1 + 36, super.hs, super.gs);
			if(i1 <= 24 && aab == 1) {
				if(l < 98 && heb == 1) {
					heb = 0;
					feb.uk(geb);
				} else
				if(l > 98 && heb == 0) {
					heb = 1;
					feb.uk(geb);
				}
			}
			if(aab == 1 && heb == 0) {
				int j2 = feb.jl(geb);
				if(j2 != -1) {
					int j3 = ydb[6];
					if(Config.to[j2] > j3) {
						up("Your magic ability is not high enough for this spell", 3);
					} else {
						int j4;
						for(j4 = 0; j4 < Config.uo[j2]; j4++) {
							int i5 = Config.wo[j2][j4];
							if(rn(i5, Config.xo[j2][j4])) {
								continue;
							}
							up("You don't have all the reagents you need for this spell", 3);
							j4 = -1;
							break;
						}

						if(j4 == Config.uo[j2]) {
							ieb = j2;
							vdb = -1;
						}
					}
				}
			}
			if(aab == 1 && heb == 1) {
				int k2 = feb.jl(geb);
				if(k2 != -1) {
					int k3 = zdb[5];
					if(Config.lp[k2] > k3) {
						up("Your prayer ability is not high enough for this prayer", 3);
					} else
					if(ydb[5] == 0) {
						up("You have run out of prayer points. Return to a church to recharge", 3);
					} else
					if(teb[k2]) {
						super.zf.begin(211);
						super.zf.fc(k2);
						super.zf.end();
						teb[k2] = false;
					} else {
						super.zf.begin(212);
						super.zf.fc(k2);
						super.zf.end();
						teb[k2] = true;
					}
				}
			}
			aab = 0;
		}
	}

	public void on(boolean flag) {
		int l = ((i) (gab)).ni - 199;
		int i1 = 36;
		gab.rf(l - 49, 3, pab + 5);
		char c1 = '\304';
		char c2 = '\266';
		int k1;
		int j1 = k1 = i.rg(160, 160, 160);
		if(leb == 0) {
			j1 = i.rg(220, 220, 220);
		} else {
			k1 = i.rg(220, 220, 220);
		}
		gab.gh(l, i1, c1 / 2, 24, j1, 128);
		gab.gh(l + c1 / 2, i1, c1 / 2, 24, k1, 128);
		gab.gh(l, i1 + 24, c1, c2 - 24, i.rg(220, 220, 220), 128);
		gab.tg(l, i1 + 24, c1, 0);
		gab.lh(l + c1 / 2, i1, 24, 0);
		gab.tg(l, (i1 + c2) - 16, c1, 0);
		gab.ug("Friends", l + c1 / 4, i1 + 16, 4, 0);
		gab.ug("Ignore", l + c1 / 4 + c1 / 2, i1 + 16, 4, 0);
		jeb.fk(keb);
		if(leb == 0) {
			for(int l1 = 0; l1 < super.fg; l1++) {
				String s;
				if(super.hg[l1] == 2) {
					s = "@gre@";
				} else
				if(super.hg[l1] == 1) {
					s = "@yel@";
				} else {
					s = "@red@";
				}
				jeb.ll(keb, l1, s + Util.decode37(super.gg[l1]) + "~439~@whi@Remove         WWWWWWWWWW");
			}

		}
		if(leb == 1) {
			for(int i2 = 0; i2 < super.ig; i2++) {
				jeb.ll(keb, i2, "@yel@" + Util.decode37(super.jg[i2]) + "~439~@whi@Remove         WWWWWWWWWW");
			}

		}
		jeb.ik();
		if(leb == 0) {
			int j2 = jeb.jl(keb);
			if(j2 >= 0 && super.es < 489) {
				if(super.es > 429) {
					gab.ug("Click to remove " + Util.decode37(super.gg[j2]), l + c1 / 2, i1 + 35, 1, 0xffffff);
				} else
				if(super.hg[j2] == 2) {
					gab.ug("Click to message " + Util.decode37(super.gg[j2]), l + c1 / 2, i1 + 35, 1, 0xffffff);
				} else
				if(super.hg[j2] == 1) {
					gab.ug(Util.decode37(super.gg[j2]) + " is on a different server", l + c1 / 2, i1 + 35, 1, 0xffffff);
				} else {
					gab.ug(Util.decode37(super.gg[j2]) + " is offline", l + c1 / 2, i1 + 35, 1, 0xffffff);
				}
			} else {
				gab.ug("Click a name to send a message", l + c1 / 2, i1 + 35, 1, 0xffffff);
			}
			int j3;
			if(super.es > l && super.es < l + c1 && super.fs > (i1 + c2) - 16 && super.fs < i1 + c2) {
				j3 = 0xffff00;
			} else {
				j3 = 0xffffff;
			}
			gab.ug("Click here to add a friend", l + c1 / 2, (i1 + c2) - 3, 1, j3);
		}
		if(leb == 1) {
			int k2 = jeb.jl(keb);
			if(k2 >= 0 && super.es < 489 && super.es > 429) {
				if(super.es > 429) {
					gab.ug("Click to remove " + Util.decode37(super.jg[k2]), l + c1 / 2, i1 + 35, 1, 0xffffff);
				}
			} else {
				gab.ug("Blocking messages from:", l + c1 / 2, i1 + 35, 1, 0xffffff);
			}
			int k3;
			if(super.es > l && super.es < l + c1 && super.fs > (i1 + c2) - 16 && super.fs < i1 + c2) {
				k3 = 0xffff00;
			} else {
				k3 = 0xffffff;
			}
			gab.ug("Click here to add a name", l + c1 / 2, (i1 + c2) - 3, 1, k3);
		}
		if(!flag) {
			return;
		}
		l = super.es - (((i) (gab)).ni - 199);
		i1 = super.fs - 36;
		if(l >= 0 && i1 >= 0 && l < 196 && i1 < 182) {
			jeb.jk(l + (((i) (gab)).ni - 199), i1 + 36, super.hs, super.gs);
			if(i1 <= 24 && aab == 1) {
				if(l < 98 && leb == 1) {
					leb = 0;
					jeb.uk(keb);
				} else
				if(l > 98 && leb == 0) {
					leb = 1;
					jeb.uk(keb);
				}
			}
			if(aab == 1 && leb == 0) {
				int l2 = jeb.jl(keb);
				if(l2 >= 0 && super.es < 489) {
					if(super.es > 429) {
						se(super.gg[l2]);
					} else
					if(super.hg[l2] != 0) {
						ehb = 2;
						meb = super.gg[l2];
						super.ns = "";
						super.os = "";
					}
				}
			}
			if(aab == 1 && leb == 1) {
				int i3 = jeb.jl(keb);
				if(i3 >= 0 && super.es < 489 && super.es > 429) {
					mf(super.jg[i3]);
				}
			}
			if(i1 > 166 && aab == 1 && leb == 0) {
				ehb = 1;
				super.ls = "";
				super.ms = "";
			}
			if(i1 > 166 && aab == 1 && leb == 1) {
				ehb = 3;
				super.ls = "";
				super.ms = "";
			}
			aab = 0;
		}
	}

	public void vo(boolean flag) {
		int l = ((i) (gab)).ni - 199;
		int i1 = 36;
		gab.rf(l - 49, 3, pab + 6);
		char c1 = '\304';
		gab.gh(l, 36, c1, 75, i.rg(181, 181, 181), 160);
		gab.gh(l, 111, c1, 105, i.rg(201, 2011, 201), 160);
		gab.gh(l, 216, c1, 30, i.rg(181, 181, 181), 160);
		int j1 = l + 3;
		int l1 = i1 + 15;
		gab.bh("Game options - click to toggle", j1, l1, 1, 0);
		l1 += 15;
		if(veb) {
			gab.bh("Camera angle mode - @gre@Auto", j1, l1, 1, 0xffffff);
		} else {
			gab.bh("Camera angle mode - @red@Manual", j1, l1, 1, 0xffffff);
		}
		l1 += 15;
		if(dfb) {
			gab.bh("Mouse buttons - @red@One", j1, l1, 1, 0xffffff);
		} else {
			gab.bh("Mouse buttons - @gre@Two", j1, l1, 1, 0xffffff);
		}
		l1 += 15;
		int j2 = 0xffffff;
		if(super.es > j1 && super.es < j1 + c1 && super.fs > l1 - 12 && super.fs < l1 + 4) {
			j2 = 0xffff00;
		}
		gab.bh("Change password", j1, l1, 1, j2);
		l1 += 15;
		l1 += 15;
		gab.bh("Privacy settings. Will be applied to", l + 3, l1, 1, 0);
		l1 += 15;
		gab.bh("all people not on your friends list", l + 3, l1, 1, 0);
		l1 += 15;
		if(super.kg == 0) {
			gab.bh("Hide online-status: @red@<off>", l + 3, l1, 1, 0xffffff);
		} else {
			gab.bh("Hide online-status: @gre@<on>", l + 3, l1, 1, 0xffffff);
		}
		l1 += 15;
		if(super.lg == 0) {
			gab.bh("Block chat messages: @red@<off>", l + 3, l1, 1, 0xffffff);
		} else {
			gab.bh("Block chat messages: @gre@<on>", l + 3, l1, 1, 0xffffff);
		}
		l1 += 15;
		if(super.mg == 0) {
			gab.bh("Block private messages: @red@<off>", l + 3, l1, 1, 0xffffff);
		} else {
			gab.bh("Block private messages: @gre@<on>", l + 3, l1, 1, 0xffffff);
		}
		l1 += 15;
		if(super.ng == 0) {
			gab.bh("Block trade requests: @red@<off>", l + 3, l1, 1, 0xffffff);
		} else {
			gab.bh("Block trade requests: @gre@<on>", l + 3, l1, 1, 0xffffff);
		}
		l1 += 15;
		l1 += 15;
		j2 = 0xffffff;
		if(super.es > j1 && super.es < j1 + c1 && super.fs > l1 - 12 && super.fs < l1 + 4) {
			j2 = 0xffff00;
		}
		gab.bh("Click here to logout", l + 3, l1, 1, j2);
		if(!flag) {
			return;
		}
		l = super.es - (((i) (gab)).ni - 199);
		i1 = super.fs - 36;
		if(l >= 0 && i1 >= 0 && l < 196 && i1 < 231) {
			int k2 = ((i) (gab)).ni - 199;
			byte byte0 = 36;
			char c2 = '\304';
			int k1 = k2 + 3;
			int i2 = byte0 + 30;
			if(super.es > k1 && super.es < k1 + c2 && super.fs > i2 - 12 && super.fs < i2 + 4 && aab == 1) {
				veb = !veb;
				super.zf.begin(213);
				super.zf.fc(0);
				super.zf.fc(veb ? 1 : 0);
				super.zf.end();
			}
			i2 += 15;
			if(super.es > k1 && super.es < k1 + c2 && super.fs > i2 - 12 && super.fs < i2 + 4 && aab == 1) {
				dfb = !dfb;
				super.zf.begin(213);
				super.zf.fc(2);
				super.zf.fc(dfb ? 1 : 0);
				super.zf.end();
			}
			i2 += 15;
			if(super.es > k1 && super.es < k1 + c2 && super.fs > i2 - 12 && super.fs < i2 + 4 && aab == 1) {
				fhb = 1;
				super.ls = "";
				super.ms = "";
			}
			boolean flag1 = false;
			i2 += 60;
			if(super.es > k1 && super.es < k1 + c2 && super.fs > i2 - 12 && super.fs < i2 + 4 && aab == 1) {
				super.kg = 1 - super.kg;
				flag1 = true;
			}
			i2 += 15;
			if(super.es > k1 && super.es < k1 + c2 && super.fs > i2 - 12 && super.fs < i2 + 4 && aab == 1) {
				super.lg = 1 - super.lg;
				flag1 = true;
			}
			i2 += 15;
			if(super.es > k1 && super.es < k1 + c2 && super.fs > i2 - 12 && super.fs < i2 + 4 && aab == 1) {
				super.mg = 1 - super.mg;
				flag1 = true;
			}
			i2 += 15;
			if(super.es > k1 && super.es < k1 + c2 && super.fs > i2 - 12 && super.fs < i2 + 4 && aab == 1) {
				super.ng = 1 - super.ng;
				flag1 = true;
			}
			if(flag1) {
				af(super.kg, super.lg, super.mg, super.ng, 0);
			}
			i2 += 30;
			if(super.es > k1 && super.es < k1 + c2 && super.fs > i2 - 12 && super.fs < i2 + 4 && aab == 1) {
				trylogout();
			}
			aab = 0;
		}
	}

	public void in() {
		int l = -1;
		for(int i1 = 0; i1 < zcb; i1++) {
			gdb[i1] = false;
		}

		for(int j1 = 0; j1 < idb; j1++) {
			odb[j1] = false;
		}

		int k1 = fab.q();
		Model ap1[] = fab.r();
		int ai[] = fab.k();
		for(int l1 = 0; l1 < k1; l1++) {
			int i2 = ai[l1];
			Model p1 = ap1[l1];
			if(p1.rv[i2] <= 65535 || p1.rv[i2] >= 0x30d40 && p1.rv[i2] <= 0x493e0) {
				if(p1 == fab.yb) {
					int k2 = p1.rv[i2] % 10000;
					int j3 = p1.rv[i2] / 10000;
					if(j3 == 1) {
						String s = "";
						int i4 = 0;
						if(fcb.gz > 0 && dcb[k2].gz > 0) {
							i4 = fcb.gz - dcb[k2].gz;
						}
						if(i4 < 0) {
							s = "@or1@";
						}
						if(i4 < -3) {
							s = "@or2@";
						}
						if(i4 < -6) {
							s = "@or3@";
						}
						if(i4 < -9) {
							s = "@red@";
						}
						if(i4 > 0) {
							s = "@gr1@";
						}
						if(i4 > 3) {
							s = "@gr2@";
						}
						if(i4 > 6) {
							s = "@gr3@";
						}
						if(i4 > 9) {
							s = "@gre@";
						}
						s = " " + s + "(level-" + dcb[k2].gz + ")";
						if(ieb >= 0) {
							if(Config.vo[ieb] == 1 || Config.vo[ieb] == 2 && hcb < 2203) {
								ffb[bfb] = "Cast " + Config.ro[ieb] + " on";
								efb[bfb] = "@whi@" + dcb[k2].ky;
								gfb[bfb] = 800;
								hfb[bfb] = dcb[k2].ny;
								ifb[bfb] = dcb[k2].oy;
								jfb[bfb] = dcb[k2].ly;
								kfb[bfb] = ieb;
								bfb++;
							}
						} else
						if(vdb >= 0) {
							ffb[bfb] = "Use " + wdb + " with";
							efb[bfb] = "@whi@" + dcb[k2].ky;
							gfb[bfb] = 810;
							hfb[bfb] = dcb[k2].ny;
							ifb[bfb] = dcb[k2].oy;
							jfb[bfb] = dcb[k2].ly;
							kfb[bfb] = vdb;
							bfb++;
						} else {
							if(hcb + cbb + gbb < 2203 && (dcb[k2].oy - 64) / jab + cbb + gbb < 2203) {
								ffb[bfb] = "Attack";
								efb[bfb] = "@whi@" + dcb[k2].ky + s;
								if(i4 >= 0 && i4 < 5) {
									gfb[bfb] = 805;
								} else {
									gfb[bfb] = 2805;
								}
								hfb[bfb] = dcb[k2].ny;
								ifb[bfb] = dcb[k2].oy;
								jfb[bfb] = dcb[k2].ly;
								bfb++;
							}
							ffb[bfb] = "Trade with";
							efb[bfb] = "@whi@" + dcb[k2].ky;
							gfb[bfb] = 2810;
							jfb[bfb] = dcb[k2].ly;
							bfb++;
							ffb[bfb] = "Follow";
							efb[bfb] = "@whi@" + dcb[k2].ky;
							gfb[bfb] = 2820;
							jfb[bfb] = dcb[k2].ly;
							bfb++;
						}
					} else
					if(j3 == 2) {
						if(ieb >= 0) {
							if(Config.vo[ieb] == 3) {
								ffb[bfb] = "Cast " + Config.ro[ieb] + " on";
								efb[bfb] = "@lre@" + Config.vk[wcb[k2]][0];
								gfb[bfb] = 200;
								hfb[bfb] = ucb[k2];
								ifb[bfb] = vcb[k2];
								jfb[bfb] = wcb[k2];
								kfb[bfb] = ieb;
								bfb++;
							}
						} else
						if(vdb >= 0) {
							ffb[bfb] = "Use " + wdb + " with";
							efb[bfb] = "@lre@" + Config.vk[wcb[k2]][0];
							gfb[bfb] = 210;
							hfb[bfb] = ucb[k2];
							ifb[bfb] = vcb[k2];
							jfb[bfb] = wcb[k2];
							kfb[bfb] = vdb;
							bfb++;
						} else {
							ffb[bfb] = "Take";
							efb[bfb] = "@lre@" + Config.vk[wcb[k2]][0];
							gfb[bfb] = 220;
							hfb[bfb] = ucb[k2];
							ifb[bfb] = vcb[k2];
							jfb[bfb] = wcb[k2];
							bfb++;
							ffb[bfb] = "Examine";
							efb[bfb] = "@lre@" + Config.vk[wcb[k2]][0];
							gfb[bfb] = 3200;
							jfb[bfb] = wcb[k2];
							bfb++;
						}
					} else
					if(j3 == 3) {
						String s1 = "";
						int diff = -1;
						int k4 = ocb[k2].py;
						if(Config.vl[k4] > 0) {
							int l4 = (Config.rl[k4] + Config.ul[k4] + Config.sl[k4] + Config.tl[k4]) / 4;
							int i5 = (zdb[0] + zdb[1] + zdb[2] + zdb[3] + 27) / 4;
							diff = i5 - l4;
							s1 = "@yel@";
							if(diff < 0) {
								s1 = "@or1@";
							}
							if(diff < -3) {
								s1 = "@or2@";
							}
							if(diff < -6) {
								s1 = "@or3@";
							}
							if(diff < -9) {
								s1 = "@red@";
							}
							if(diff > 0) {
								s1 = "@gr1@";
							}
							if(diff > 3) {
								s1 = "@gr2@";
							}
							if(diff > 6) {
								s1 = "@gr3@";
							}
							if(diff > 9) {
								s1 = "@gre@";
							}
							s1 = " " + s1 + "(level-" + l4 + ")";
						}
						if(ieb >= 0) {
							if(Config.vo[ieb] == 2) {
								ffb[bfb] = "Cast " + Config.ro[ieb] + " on";
								efb[bfb] = "@yel@" + Config.npcNames[ocb[k2].py][0];
								gfb[bfb] = 700;
								hfb[bfb] = ocb[k2].ny;
								ifb[bfb] = ocb[k2].oy;
								jfb[bfb] = ocb[k2].ly;
								kfb[bfb] = ieb;
								bfb++;
							}
						} else
						if(vdb >= 0) {
							ffb[bfb] = "Use " + wdb + " with";
							efb[bfb] = "@yel@" + Config.npcNames[ocb[k2].py][0];
							gfb[bfb] = 710;
							hfb[bfb] = ocb[k2].ny;
							ifb[bfb] = ocb[k2].oy;
							jfb[bfb] = ocb[k2].ly;
							kfb[bfb] = vdb;
							bfb++;
						} else {
							if(Config.vl[k4] > 0) {
								ffb[bfb] = "Attack";
								efb[bfb] = "@yel@" + Config.npcNames[ocb[k2].py][0] + s1;
								if(diff >= 0) {
									gfb[bfb] = 715;
								} else {
									gfb[bfb] = 2715;
								}
								hfb[bfb] = ocb[k2].ny;
								ifb[bfb] = ocb[k2].oy;
								jfb[bfb] = ocb[k2].ly;
								bfb++;
							}
							ffb[bfb] = "Talk-to";
							efb[bfb] = "@yel@" + Config.npcNames[ocb[k2].py][0];
							gfb[bfb] = 720;
							hfb[bfb] = ocb[k2].ny;
							ifb[bfb] = ocb[k2].oy;
							jfb[bfb] = ocb[k2].ly;
							bfb++;
							ffb[bfb] = "Examine";
							efb[bfb] = "@yel@" + Config.npcNames[ocb[k2].py][0];
							gfb[bfb] = 3700;
							jfb[bfb] = ocb[k2].py;
							bfb++;
						}
					}
				} else
				if(p1 != null && p1.qv >= 10000) {
					int l2 = p1.qv - 10000;
					int k3 = ndb[l2];
					if(!odb[l2]) {
						if(ieb >= 0) {
							if(Config.vo[ieb] == 4) {
								ffb[bfb] = "Cast " + Config.ro[ieb] + " on";
								efb[bfb] = "@cya@" + Config.mn[k3][0];
								gfb[bfb] = 300;
								hfb[bfb] = kdb[l2];
								ifb[bfb] = ldb[l2];
								jfb[bfb] = mdb[l2];
								kfb[bfb] = ieb;
								bfb++;
							}
						} else
						if(vdb >= 0) {
							ffb[bfb] = "Use " + wdb + " with";
							efb[bfb] = "@cya@" + Config.mn[k3][0];
							gfb[bfb] = 310;
							hfb[bfb] = kdb[l2];
							ifb[bfb] = ldb[l2];
							jfb[bfb] = mdb[l2];
							kfb[bfb] = vdb;
							bfb++;
						} else {
							if(!Config.on[k3].equalsIgnoreCase("WalkTo")) {
								ffb[bfb] = Config.on[k3];
								efb[bfb] = "@cya@" + Config.mn[k3][0];
								gfb[bfb] = 320;
								hfb[bfb] = kdb[l2];
								ifb[bfb] = ldb[l2];
								jfb[bfb] = mdb[l2];
								bfb++;
							}
							if(!Config.pn[k3].equalsIgnoreCase("Examine")) {
								ffb[bfb] = Config.pn[k3];
								efb[bfb] = "@cya@" + Config.mn[k3][0];
								gfb[bfb] = 2300;
								hfb[bfb] = kdb[l2];
								ifb[bfb] = ldb[l2];
								jfb[bfb] = mdb[l2];
								bfb++;
							}
							ffb[bfb] = "Examine";
							efb[bfb] = "@cya@" + Config.mn[k3][0];
							gfb[bfb] = 3300;
							jfb[bfb] = k3;
							bfb++;
						}
						odb[l2] = true;
					}
				} else
				if(p1 != null && p1.qv >= 0) {
					int i3 = p1.qv;
					int l3 = ddb[i3];
					if(!gdb[i3]) {
						if(ieb >= 0) {
							if(Config.vo[ieb] == 5) {
								ffb[bfb] = "Cast " + Config.ro[ieb] + " on";
								efb[bfb] = "@cya@" + Config.bn[l3][0];
								gfb[bfb] = 400;
								hfb[bfb] = bdb[i3];
								ifb[bfb] = cdb[i3];
								jfb[bfb] = edb[i3];
								kfb[bfb] = ddb[i3];
								lfb[bfb] = ieb;
								bfb++;
							}
						} else
						if(vdb >= 0) {
							ffb[bfb] = "Use " + wdb + " with";
							efb[bfb] = "@cya@" + Config.bn[l3][0];
							gfb[bfb] = 410;
							hfb[bfb] = bdb[i3];
							ifb[bfb] = cdb[i3];
							jfb[bfb] = edb[i3];
							kfb[bfb] = ddb[i3];
							lfb[bfb] = vdb;
							bfb++;
						} else {
							if(!Config.dn[l3].equalsIgnoreCase("WalkTo")) {
								ffb[bfb] = Config.dn[l3];
								efb[bfb] = "@cya@" + Config.bn[l3][0];
								gfb[bfb] = 420;
								hfb[bfb] = bdb[i3];
								ifb[bfb] = cdb[i3];
								jfb[bfb] = edb[i3];
								kfb[bfb] = ddb[i3];
								bfb++;
							}
							if(!Config.en[l3].equalsIgnoreCase("Examine")) {
								ffb[bfb] = Config.en[l3];
								efb[bfb] = "@cya@" + Config.bn[l3][0];
								gfb[bfb] = 2400;
								hfb[bfb] = bdb[i3];
								ifb[bfb] = cdb[i3];
								jfb[bfb] = edb[i3];
								kfb[bfb] = ddb[i3];
								bfb++;
							}
							ffb[bfb] = "Examine";
							efb[bfb] = "@cya@" + Config.bn[l3][0];
							gfb[bfb] = 3400;
							jfb[bfb] = l3;
							bfb++;
						}
						gdb[i3] = true;
					}
				} else {
					if(i2 >= 0) {
						i2 = p1.rv[i2] - 0x30d40;
					}
					if(i2 >= 0) {
						l = i2;
					}
				}
			}
		}

		if(ieb >= 0 && Config.vo[ieb] <= 1) {
			ffb[bfb] = "Cast " + Config.ro[ieb] + " on self";
			efb[bfb] = "";
			gfb[bfb] = 1000;
			jfb[bfb] = ieb;
			bfb++;
		}
		if(l != -1) {
			int j2 = l;
			if(ieb >= 0) {
				if(Config.vo[ieb] == 6) {
					ffb[bfb] = "Cast " + Config.ro[ieb] + " on ground";
					efb[bfb] = "";
					gfb[bfb] = 900;
					hfb[bfb] = abb.bf[j2];
					ifb[bfb] = abb.cf[j2];
					jfb[bfb] = ieb;
					bfb++;
					return;
				}
			} else
			if(vdb < 0) {
				ffb[bfb] = "Walk here";
				efb[bfb] = "";
				gfb[bfb] = 920;
				hfb[bfb] = abb.bf[j2];
				ifb[bfb] = abb.cf[j2];
				bfb++;
			}
		}
	}

	public void vp() {
		if(aab != 0) {
			for(int l = 0; l < bfb; l++) {
				int j1 = xeb + 2;
				int l1 = yeb + 27 + l * 15;
				if(super.es <= j1 - 2 || super.fs <= l1 - 12 || super.fs >= l1 + 4 || super.es >= (j1 - 3) + zeb) {
					continue;
				}
				hp(mfb[l]);
				break;
			}

			aab = 0;
			web = false;
			return;
		}
		if(super.es < xeb - 10 || super.fs < yeb - 10 || super.es > xeb + zeb + 10 || super.fs > yeb + afb + 10) {
			web = false;
			return;
		}
		gab.gh(xeb, yeb, zeb, afb, 0xd0d0d0, 160);
		gab.bh("Choose option", xeb + 2, yeb + 12, 1, 65535);
		for(int i1 = 0; i1 < bfb; i1++) {
			int k1 = xeb + 2;
			int i2 = yeb + 27 + i1 * 15;
			int j2 = 0xffffff;
			if(super.es > k1 - 2 && super.fs > i2 - 12 && super.fs < i2 + 4 && super.es < (k1 - 3) + zeb) {
				j2 = 0xffff00;
			}
			gab.bh(ffb[mfb[i1]] + " " + efb[mfb[i1]], k1, i2, 1, j2);
		}

	}

	public void po() {
		if(ieb >= 0 || vdb >= 0) {
			ffb[bfb] = "Cancel";
			efb[bfb] = "";
			gfb[bfb] = 4000;
			bfb++;
		}
		for(int l = 0; l < bfb; l++) {
			mfb[l] = l;
		}

		for(boolean flag = false; !flag;) {
			flag = true;
			for(int i1 = 0; i1 < bfb - 1; i1++) {
				int k1 = mfb[i1];
				int i2 = mfb[i1 + 1];
				if(gfb[k1] > gfb[i2]) {
					mfb[i1] = i2;
					mfb[i1 + 1] = k1;
					flag = false;
				}
			}

		}

		if(bfb > 20) {
			bfb = 20;
		}
		if(bfb > 0) {
			int j1 = -1;
			for(int l1 = 0; l1 < bfb; l1++) {
				if(efb[mfb[l1]] == null || efb[mfb[l1]].length() <= 0) {
					continue;
				}
				j1 = l1;
				break;
			}

			String s = null;
			if((vdb >= 0 || ieb >= 0) && bfb == 1) {
				s = "Choose a target";
			} else
			if((vdb >= 0 || ieb >= 0) && bfb > 1) {
				s = "@whi@" + ffb[mfb[0]] + " " + efb[mfb[0]];
			} else
			if(j1 != -1) {
				s = efb[mfb[j1]] + ": @whi@" + ffb[mfb[0]];
			}
			if(bfb == 2 && s != null) {
				s = s + "@whi@ / 1 more option";
			}
			if(bfb > 2 && s != null) {
				s = s + "@whi@ / " + (bfb - 1) + " more options";
			}
			if(s != null) {
				gab.bh(s, 6, 14, 1, 0xffff00);
			}
			if(!dfb && aab == 1 || dfb && aab == 1 && bfb == 1) {
				hp(mfb[0]);
				aab = 0;
				return;
			}
			if(!dfb && aab == 2 || dfb && aab == 1) {
				afb = (bfb + 1) * 15;
				zeb = gab.nf("Choose option", 1) + 5;
				for(int j2 = 0; j2 < bfb; j2++) {
					int k2 = gab.nf(ffb[j2] + " " + efb[j2], 1) + 5;
					if(k2 > zeb) {
						zeb = k2;
					}
				}

				xeb = super.es - zeb / 2;
				yeb = super.fs - 7;
				web = true;
				if(xeb < 0) {
					xeb = 0;
				}
				if(yeb < 0) {
					yeb = 0;
				}
				if(xeb + zeb > 510) {
					xeb = 510 - zeb;
				}
				if(yeb + afb > 315) {
					yeb = 315 - afb;
				}
				aab = 0;
			}
		}
	}

	public void hp(int l) {
		int i1 = hfb[l];
		int j1 = ifb[l];
		int k1 = jfb[l];
		int l1 = kfb[l];
		int i2 = lfb[l];
		int j2 = gfb[l];
		if(j2 == 200) {
			zn(gcb, hcb, i1, j1, true);
			super.zf.begin(224);
			super.zf.ec(i1 + fbb);
			super.zf.ec(j1 + gbb);
			super.zf.ec(k1);
			super.zf.ec(l1);
			qn();
			ieb = -1;
		}
		if(j2 == 210) {
			zn(gcb, hcb, i1, j1, true);
			super.zf.begin(250);
			super.zf.ec(i1 + fbb);
			super.zf.ec(j1 + gbb);
			super.zf.ec(k1);
			super.zf.ec(l1);
			qn();
			vdb = -1;
		}
		if(j2 == 220) {
			zn(gcb, hcb, i1, j1, true);
			super.zf.begin(252);
			super.zf.ec(i1 + fbb);
			super.zf.ec(j1 + gbb);
			super.zf.ec(k1);
			qn();
		}
		if(j2 == 3200) {
			up(Config.wk[k1], 3);
		}
		if(j2 == 300) {
			kp(i1, j1, k1);
			super.zf.begin(223);
			super.zf.ec(i1 + fbb);
			super.zf.ec(j1 + gbb);
			super.zf.fc(k1);
			super.zf.ec(l1);
			qn();
			ieb = -1;
		}
		if(j2 == 310) {
			kp(i1, j1, k1);
			super.zf.begin(239);
			super.zf.ec(i1 + fbb);
			super.zf.ec(j1 + gbb);
			super.zf.fc(k1);
			super.zf.ec(l1);
			qn();
			vdb = -1;
		}
		if(j2 == 320) {
			kp(i1, j1, k1);
			super.zf.begin(238);
			super.zf.ec(i1 + fbb);
			super.zf.ec(j1 + gbb);
			super.zf.fc(k1);
			qn();
		}
		if(j2 == 2300) {
			kp(i1, j1, k1);
			super.zf.begin(229);
			super.zf.ec(i1 + fbb);
			super.zf.ec(j1 + gbb);
			super.zf.fc(k1);
			qn();
		}
		if(j2 == 3300) {
			up(Config.nn[k1], 3);
		}
		if(j2 == 400) {
			pp(i1, j1, k1, l1);
			super.zf.begin(222);
			super.zf.ec(i1 + fbb);
			super.zf.ec(j1 + gbb);
			super.zf.ec(i2);
			qn();
			ieb = -1;
		}
		if(j2 == 410) {
			pp(i1, j1, k1, l1);
			super.zf.begin(241);
			super.zf.ec(i1 + fbb);
			super.zf.ec(j1 + gbb);
			super.zf.ec(i2);
			qn();
			vdb = -1;
		}
		if(j2 == 420) {
			pp(i1, j1, k1, l1);
			super.zf.begin(242);
			super.zf.ec(i1 + fbb);
			super.zf.ec(j1 + gbb);
			qn();
		}
		if(j2 == 2400) {
			pp(i1, j1, k1, l1);
			super.zf.begin(230);
			super.zf.ec(i1 + fbb);
			super.zf.ec(j1 + gbb);
			qn();
		}
		if(j2 == 3400) {
			up(Config.cn[k1], 3);
		}
		if(j2 == 600) {
			super.zf.begin(220);
			super.zf.ec(k1);
			super.zf.ec(l1);
			qn();
			ieb = -1;
		}
		if(j2 == 610) {
			super.zf.begin(240);
			super.zf.ec(k1);
			super.zf.ec(l1);
			qn();
			vdb = -1;
		}
		if(j2 == 620) {
			super.zf.begin(248);
			super.zf.ec(k1);
			qn();
		}
		if(j2 == 630) {
			super.zf.begin(249);
			super.zf.ec(k1);
			qn();
		}
		if(j2 == 640) {
			super.zf.begin(246);
			super.zf.ec(k1);
			qn();
		}
		if(j2 == 650) {
			vdb = k1;
			pdb = 0;
			wdb = Config.vk[sdb[vdb]][0];
		}
		if(j2 == 660) {
			super.zf.begin(251);
			super.zf.ec(k1);
			qn();
			vdb = -1;
			pdb = 0;
			up("Dropping " + Config.vk[sdb[k1]][0], 4);
		}
		if(j2 == 3600) {
			up(Config.wk[k1], 3);
		}
		if(j2 == 700) {
			int k2 = (i1 - 64) / jab;
			int j4 = (j1 - 64) / jab;
			np(gcb, hcb, k2, j4, true);
			super.zf.begin(225);
			super.zf.ec(k1);
			super.zf.ec(l1);
			qn();
			ieb = -1;
		}
		if(j2 == 710) {
			int l2 = (i1 - 64) / jab;
			int k4 = (j1 - 64) / jab;
			np(gcb, hcb, l2, k4, true);
			super.zf.begin(243);
			super.zf.ec(k1);
			super.zf.ec(l1);
			qn();
			vdb = -1;
		}
		if(j2 == 720) {
			int i3 = (i1 - 64) / jab;
			int l4 = (j1 - 64) / jab;
			np(gcb, hcb, i3, l4, true);
			super.zf.begin(245);
			super.zf.ec(k1);
			qn();
		}
		if(j2 == 715 || j2 == 2715) {
			int j3 = (i1 - 64) / jab;
			int i5 = (j1 - 64) / jab;
			np(gcb, hcb, j3, i5, true);
			super.zf.begin(244);
			super.zf.ec(k1);
			qn();
		}
		if(j2 == 3700) {
			up(Config.npcExamines[k1], 3);
		}
		if(j2 == 800) {
			int k3 = (i1 - 64) / jab;
			int j5 = (j1 - 64) / jab;
			np(gcb, hcb, k3, j5, true);
			super.zf.begin(226);
			super.zf.ec(k1);
			super.zf.ec(l1);
			qn();
			ieb = -1;
		}
		if(j2 == 810) {
			int l3 = (i1 - 64) / jab;
			int k5 = (j1 - 64) / jab;
			np(gcb, hcb, l3, k5, true);
			super.zf.begin(219);
			super.zf.ec(k1);
			super.zf.ec(l1);
			qn();
			vdb = -1;
		}
		if(j2 == 805 || j2 == 2805) {
			int i4 = (i1 - 64) / jab;
			int l5 = (j1 - 64) / jab;
			np(gcb, hcb, i4, l5, true);
			super.zf.begin(228);
			super.zf.ec(k1);
			qn();
		}
		if(j2 == 2810) {
			super.zf.begin(235);
			super.zf.ec(k1);
			qn();
		}
		if(j2 == 2820) {
			super.zf.begin(214);
			super.zf.ec(k1);
			qn();
		}
		if(j2 == 900) {
			np(gcb, hcb, i1, j1, true);
			super.zf.begin(221);
			super.zf.ec(i1 + fbb);
			super.zf.ec(j1 + gbb);
			super.zf.ec(k1);
			qn();
			ieb = -1;
		}
		if(j2 == 920) {
			np(gcb, hcb, i1, j1, false);
			if(xab == -24) {
				xab = 24;
			}
		}
		if(j2 == 1000) {
			super.zf.begin(227);
			super.zf.ec(k1);
			qn();
			ieb = -1;
		}
		if(j2 == 4000) {
			vdb = -1;
			ieb = -1;
		}
	}

	public Client() {
		sz = false;
		invalidHost = false;
		wz = false;
		applet = true;
		zz = 0xbc614e;
		bab = 8000;
		cab = new int[bab];
		dab = new int[bab];
		jab = 128;
		lab = 512;
		mab = 334;
		nab = 9;
		qab = 40;
		vab = -1;
		wab = -1;
		ebb = -1;
		hbb = -1;
		pbb = 550;
		qbb = false;
		tbb = 1;
		vbb = 128;
		xbb = 4000;
		ybb = 500;
		ccb = new Character[xbb];
		dcb = new Character[ybb];
		ecb = new Character[ybb];
		fcb = new Character();
		icb = -1;
		jcb = 1000;
		kcb = 500;
		ncb = new Character[jcb];
		ocb = new Character[kcb];
		pcb = new Character[kcb];
		qcb = new int[500];
		rcb = 500;
		ucb = new int[rcb];
		vcb = new int[rcb];
		wcb = new int[rcb];
		xcb = new int[rcb];
		ycb = 1500;
		adb = new Model[ycb];
		bdb = new int[ycb];
		cdb = new int[ycb];
		ddb = new int[ycb];
		edb = new int[ycb];
		models = new Model[200];
		gdb = new boolean[ycb];
		hdb = 500;
		jdb = new Model[hdb];
		kdb = new int[hdb];
		ldb = new int[hdb];
		mdb = new int[hdb];
		ndb = new int[hdb];
		odb = new boolean[hdb];
		qdb = 30;
		sdb = new int[35];
		tdb = new int[35];
		udb = new int[35];
		vdb = -1;
		wdb = "";
		experience = new int[99];
		ydb = new int[16];
		zdb = new int[16];
		aeb = new int[16];
		beb = new int[5];
		ieb = -1;
		qeb = 16;
		seb = new boolean[qeb];
		teb = new boolean[50];
		ueb = false;
		veb = true;
		web = false;
		cfb = 200;
		dfb = false;
		efb = new String[cfb];
		ffb = new String[cfb];
		gfb = new int[cfb];
		hfb = new int[cfb];
		ifb = new int[cfb];
		jfb = new int[cfb];
		kfb = new int[cfb];
		lfb = new int[cfb];
		mfb = new int[cfb];
		xfb = 5;
		yfb = new String[xfb];
		zfb = new int[xfb];
		agb = false;
		bgb = "";
		dgb = new int[14];
		egb = new int[14];
		ggb = new int[14];
		hgb = new int[14];
		igb = false;
		jgb = false;
		mgb = false;
		pgb = new int[256];
		qgb = new int[256];
		rgb = new int[256];
		sgb = -1;
		tgb = -2;
		ugb = false;
		wgb = new int[256];
		xgb = new int[256];
		ygb = -1;
		zgb = -2;
		ahb = false;
		chb = new String[5];
		ghb = "";
		khb = false;
		jib = "";
		kib = "";
		lib = "";
		mib = "";
		ckb = new int[20];
		rkb = false;
		vkb = -1;
		wkb = new int[5];
		xkb = new int[5];
		ykb = new int[5];
		zkb = new int[5];
		blb = new String[5];
		dlb = new String[50];
		elb = new int[50];
		flb = new int[50];
		glb = new int[50];
		hlb = new int[50];
		jlb = new int[50];
		klb = new int[50];
		llb = new int[50];
		mlb = new int[50];
		olb = new int[50];
		plb = new int[50];
		qlb = new int[50];
		ulb = false;
		wlb = 1;
		xlb = 2;
		ylb = 2;
		zlb = 8;
		amb = 14;
		cmb = 1;
	}

	boolean sz;
	int tz;
	int uz;
	boolean invalidHost;
	boolean wz;
	public boolean applet;
	int yz;
	int zz;
	int aab;
	int bab;
	int cab[];
	int dab[];
	Graphics eab;
	a fab;
	g gab;
	Image hab;
	int iab;
	int jab;
	int kab;
	int lab;
	int mab;
	int nab;
	int oab;
	int pab;
	int qab;
	int rab;
	int sab;
	int tab;
	int uab;
	int vab;
	int wab;
	int xab;
	int yab;
	int zab;
	d abb;
	int bbb;
	int cbb;
	int dbb;
	int ebb;
	int fbb;
	int gbb;
	int hbb;
	int ibb;
	int jbb;
	int kbb;
	int lbb;
	int mbb;
	int nbb;
	int obb;
	int pbb;
	boolean qbb;
	int rbb;
	int sbb;
	int tbb;
	int ubb;
	int vbb;
	int wbb;
	int xbb;
	int ybb;
	int zbb;
	int acb;
	int bcb;
	Character ccb[];
	Character dcb[];
	Character ecb[];
	Character fcb;
	int gcb;
	int hcb;
	int icb;
	int jcb;
	int kcb;
	int lcb;
	int mcb;
	Character ncb[];
	Character ocb[];
	Character pcb[];
	int qcb[];
	int rcb;
	int scb;
	int tcb;
	int ucb[];
	int vcb[];
	int wcb[];
	int xcb[];
	int ycb;
	int zcb;
	Model adb[];
	int bdb[];
	int cdb[];
	int ddb[];
	int edb[];
	Model models[];
	boolean gdb[];
	int hdb;
	int idb;
	Model jdb[];
	int kdb[];
	int ldb[];
	int mdb[];
	int ndb[];
	boolean odb[];
	int pdb;
	int qdb;
	int rdb;
	int sdb[];
	int tdb[];
	int udb[];
	int vdb;
	String wdb;
	int experience[];
	int ydb[];
	int zdb[];
	int aeb[];
	int beb[];
	int ceb;
	String deb[] = {
		"Attack", "Defense", "Strength", "Hits", "Ranged", "Prayer", "Magic", "Cooking", "Woodcutting", "Fletching", 
		"Fishing", "Firemaking", "Crafting", "Smithing", "Mining", "Herblaw"
	};
	String eeb[] = {
		"Armour", "WeaponAim", "WeaponPower", "Magic", "Prayer"
	};
	Pane feb;
	int geb;
	int heb;
	int ieb;
	Pane jeb;
	int keb;
	int leb;
	long meb;
	Pane neb;
	int oeb;
	int peb;
	int qeb;
	String reb[] = {
		"Black knight's fortress", "Cook's assistant", "Demon slayer", "Doric's quest", "The restless ghost", "Goblin diplomacy", "Ernest the chicken", "Imp catcher", "Pirate's treasure", "Prince Ali rescue", 
		"Romeo & Juliet", "Sheep shearer", "Shield of Arrav", "The knight's sword", "Vampire slayer", "Witch's potion"
	};
	boolean seb[];
	boolean teb[];
	boolean ueb;
	boolean veb;
	boolean web;
	int xeb;
	int yeb;
	int zeb;
	int afb;
	int bfb;
	int cfb;
	boolean dfb;
	String efb[];
	String ffb[];
	int gfb[];
	int hfb[];
	int ifb[];
	int jfb[];
	int kfb[];
	int lfb[];
	int mfb[];
	int nfb;
	int ofb;
	int pfb;
	int qfb;
	Pane rfb;
	int sfb;
	int tfb;
	int ufb;
	int vfb;
	int wfb;
	int xfb;
	String yfb[];
	int zfb[];
	boolean agb;
	String bgb;
	int cgb;
	int dgb[];
	int egb[];
	int fgb;
	int ggb[];
	int hgb[];
	boolean igb;
	boolean jgb;
	int kgb;
	int lgb;
	boolean mgb;
	int ngb;
	int ogb;
	int pgb[];
	int qgb[];
	int rgb[];
	int sgb;
	int tgb;
	boolean ugb;
	int vgb;
	int wgb[];
	int xgb[];
	int ygb;
	int zgb;
	boolean ahb;
	int bhb;
	String chb[];
	int dhb;
	int ehb;
	int fhb;
	String ghb;
	int hhb;
	int ihb;
	int jhb;
	boolean khb;
	int lhb;
	int mhb;
	int nhb;
	Pane ohb;
	int phb;
	int qhb;
	Pane rhb;
	int shb;
	int thb;
	int uhb;
	int vhb;
	int whb;
	int xhb;
	int yhb;
	int zhb;
	Pane aib;
	int bib;
	int cib;
	int dib;
	int eib;
	int fib;
	int gib;
	int hib;
	int iib;
	String jib;
	String kib;
	String lib;
	String mib;
	Pane nib;
	int oib;
	int pib;
	int qib;
	int rib;
	int sib;
	int tib;
	int uib;
	int vib;
	int wib;
	int xib;
	int yib;
	int zib;
	int ajb;
	int bjb;
	int cjb;
	int djb;
	Pane ejb;
	Pane fjb;
	Pane gjb;
	Pane hjb;
	Pane ijb;
	int jjb;
	int kjb;
	int ljb;
	int mjb;
	int njb;
	int ojb;
	int pjb;
	int qjb;
	int rjb;
	int sjb;
	int tjb;
	int ujb;
	int vjb;
	int wjb;
	int xjb;
	int yjb;
	int zjb;
	int akb;
	int bkb;
	int ckb[];
	int dkb;
	int ekb;
	int fkb;
	int gkb;
	int hkb;
	int ikb;
	int jkb;
	int kkb;
	int lkb;
	int mkb;
	int nkb;
	int okb;
	int pkb;
	int qkb;
	boolean rkb;
	Pane skb;
	int tkb;
	int ukb;
	int vkb;
	int wkb[];
	int xkb[];
	int ykb[];
	int zkb[];
	int alb[] = {
		0, 1, 2, 3, 4
	};
	String blb[];
	int clb;
	String dlb[];
	int elb[];
	int flb[];
	int glb[];
	int hlb[];
	int ilb;
	int jlb[];
	int klb[];
	int llb[];
	int mlb[];
	int nlb;
	int olb[];
	int plb[];
	int qlb[];
	int rlb;
	int slb;
	int tlb[][] = {
		{
			11, 2, 9, 7, 1, 6, 10, 0, 5, 8, 
			3, 4
		}, {
			11, 2, 9, 7, 1, 6, 10, 0, 5, 8, 
			3, 4
		}, {
			11, 3, 2, 9, 7, 1, 6, 10, 0, 5, 
			8, 4
		}, {
			3, 4, 2, 9, 7, 1, 6, 10, 8, 11, 
			0, 5
		}, {
			3, 4, 2, 9, 7, 1, 6, 10, 8, 11, 
			0, 5
		}, {
			4, 3, 2, 9, 7, 1, 6, 10, 8, 11, 
			0, 5
		}, {
			11, 4, 2, 9, 7, 1, 6, 10, 0, 5, 
			8, 3
		}, {
			11, 2, 9, 7, 1, 6, 10, 0, 5, 8, 
			4, 3
		}
	};
	boolean ulb;
	int vlb;
	int wlb;
	int xlb;
	int ylb;
	int zlb;
	int amb;
	int bmb;
	int cmb;
	public int dmb[] = {
		0xff0000, 0xff8000, 0xffe000, 0xa0e000, 57344, 32768, 41088, 45311, 33023, 12528, 
		0xe000e0, 0x303030, 0x604000, 0x805000, 0xffffff
	};
	public int emb[] = {
		0xffc030, 0xffa040, 0x805030, 0x604020, 0x303030, 0xff6020, 0xff4000, 0xffffff, 65280, 65535
	};
	public int fmb[] = {
		0xecded0, 0xccb366, 0xb38c40, 0x997326, 0x906020
	};
	int gmb[] = {
		0, 1, 2, 1
	};
	int hmb[] = {
		0, 1, 2, 1, 0, 0, 0, 0
	};
	int imb[] = {
		0, 0, 0, 0, 0, 1, 2, 1
	};
	String jmb[] = {
		"Where were you born?", "What was your first teacher's name?", "What is your father's middle name?", "Who was your first best friend?", "What is your favourite vacation spot?", "What is your mother's middle name?", "What was your first pet's name?", "What was the name of your first school?", "What is your mother's maiden name?", "Who was your first boyfriend/girlfriend?", 
		"What was the first computer game you purchased?", "Who is your favourite actor/actress?", "Who is your favourite author?", "Who is your favourite musician?", "Who is your favourite cartoon character?", "What is your favourite book?", "What is your favourite food?", "What is your favourite movie?"
	};
}
