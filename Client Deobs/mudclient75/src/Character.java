public class Character {

	public Character() {
		vy = new int[10];
		wy = new int[10];
		xy = new int[12];
		gz = -1;
		pz = false;
		qz = -1;
	}

	public long jy;
	public String ky;
	public int ly;
	public int my;
	public int ny;
	public int oy;
	public int py;
	public int qy;
	public int ry;
	public int sy;
	public int ty;
	public int uy;
	public int vy[];
	public int wy[];
	public int xy[];
	public String yy;
	public int zy;
	public int az;
	public int bz;
	public int cz;
	public int dz;
	public int ez;
	public int fz;
	public int gz;
	public int hz;
	public int iz;
	public int jz;
	public int kz;
	public int lz;
	public int mz;
	public int nz;
	public int oz;
	public boolean pz;
	public int qz;
	public int rz;
}
