package jagex.client;

import java.awt.Event;
import java.awt.Frame;
import java.awt.Graphics;

@SuppressWarnings("serial")
public class Window extends Frame {
	
	int width;
	int height;
	int yOffset;
	Game game;

	public Window(Game n1, int i, int j, String s, boolean flag, boolean flag1) {
		yOffset = 28;
		width = i;
		height = j;
		game = n1;
		if(flag1) {
			yOffset = 48;
		} else {
			yOffset = 28;
		}
		setTitle(s);
		setResizable(flag);
		setVisible(true);
		toFront();
		setSize(width, height);
	}

	@Override
	public Graphics getGraphics() {
		Graphics g = super.getGraphics();
		g.translate(0, 24);
		return g;
	}

	@Override
	public void setSize(int width, int height) {
		super.setSize(width, height + yOffset);
	}

	public int width() {
		return getWidth();
	}

	public int height() {
		return getHeight() - yOffset;
	}

	public boolean handleEvent(Event event) {
		if(event.id == 401) {
			game.keyDown(event, event.key);
		} else
		if(event.id == 402) {
			game.keyUp(event, event.key);
		} else
		if(event.id == 501) {
			game.mouseDown(event, event.x, event.y - 24);
		} else
		if(event.id == 506) {
			game.mouseDrag(event, event.x, event.y - 24);
		} else
		if(event.id == 502) {
			game.mouseUp(event, event.x, event.y - 24);
		} else
		if(event.id == 503) {
			game.mouseMove(event, event.x, event.y - 24);
		} else
		if(event.id == 201) {
			game.destroy();
		} else
		if(event.id == 1001) {
			game.action(event, event.target);
		} else
		if(event.id == 403) {
			game.keyDown(event, event.key);
		} else
		if(event.id == 404) {
			game.keyUp(event, event.key);
		}
		return true;
	}

	public final void paint(Graphics g) {
		game.paint(g);
	}

}
