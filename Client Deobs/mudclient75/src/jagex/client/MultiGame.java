package jagex.client;

import jagex.Util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.io.IOException;
import java.net.InetAddress;

@SuppressWarnings("serial")
public class MultiGame extends Game {

	public int kf() {
		try {
			String s = getParameter("save");
			String s1 = s.substring(0, 10);
			String s2 = s.substring(10, 15);
			int i = Integer.parseInt(s1);
			int j = Integer.parseInt(s2);
			if((i * i * 37 + i * 571 + 0x10fa9bb1 & 0xffff) != j) {
				return 0;
			} else {
				return i;
			}
		}
		catch(Exception _ex) {
			return 0;
		}
	}

	public void me(String s, String s1) {
		try {
			xf = s;
			s = Util.clean(s, 20);
			yf = s1;
			s1 = Util.clean(s1, 20);
			if(s.trim().length() == 0 || s1.trim().length() == 0) {
				gf(pf[0], pf[1]);
				return;
			}
			gf(pf[6], pf[7]);
			if(applet()) {
				zf = b.xb(uf, this, wf);
			} else {
				zf = b.xb(uf, null, wf);
			}
			byte abyte0[] = {
				127, 0, 0, 1
			};
			try {
				InetAddress inetaddress = zf.ac();
				abyte0 = inetaddress.getAddress();
			}
			catch(Exception _ex) { }
			if(abyte0[0] == 127 && abyte0[1] == 0 && abyte0[2] == 0 && abyte0[3] == 1) {
				try {
					InetAddress inetaddress1 = InetAddress.getLocalHost();
					abyte0 = inetaddress1.getAddress();
				}
				catch(Exception _ex) { }
			}
			try {
				if(abyte0[0] == 127 && abyte0[1] == 0 && abyte0[2] == 0 && abyte0[3] == 1 && applet()) {
					String s2 = getParameter("client");
					if(s2 != null && s2.equals("exe")) {
						abyte0[0] = (byte)(int)(Math.random() * 256D);
						abyte0[1] = (byte)(int)(Math.random() * 256D);
						abyte0[2] = (byte)(int)(Math.random() * 256D);
						abyte0[3] = (byte)(int)(Math.random() * 256D);
					}
				}
			}
			catch(Exception _ex) { }
			zf.begin(0);
			zf.wb(Util.encode37(s));
			zf.zb(s1);
			zf.ec(clientRevision);
			zf.fc(abyte0[0]);
			zf.fc(abyte0[1]);
			zf.fc(abyte0[2]);
			zf.fc(abyte0[3]);
			int i = kf();
			zf.cc(i);
			zf.ic();
			zf.ji();
			int j = zf.read();
			System.out.println("Login response: " + j);
			if(j == 0) {
				re();
				ne();
				return;
			}
			if(j == 2) {
				gf(pf[8], pf[9]);
				return;
			}
			if(j == 3) {
				gf(pf[10], pf[11]);
				return;
			}
			if(j == 4) {
				gf(pf[4], pf[5]);
				return;
			}
			if(j == 5) {
				gf(pf[16], pf[17]);
				return;
			}
			if(j == 6) {
				gf(pf[18], pf[19]);
				return;
			}
			if(j == 7) {
				gf(pf[20], pf[21]);
				return;
			}
			if(j == 11) {
				gf(pf[22], pf[23]);
				return;
			}
			if(j == 12) {
				gf(pf[24], pf[25]);
				return;
			} else {
				gf(pf[12], pf[13]);
				return;
			}
		}
		catch(Exception exception) {
			uf = vf;
			System.out.println(String.valueOf(exception));
			gf(pf[12], pf[13]);
			return;
		}
	}

	public void df(String s, String s1) {
		try {
			if(applet()) {
				zf = b.xb(uf, this, wf);
			} else {
				zf = b.xb(uf, null, wf);
			}
			zf.begin(2);
			s = Util.clean(s, 20);
			s1 = Util.clean(s1, 20);
			zf.wb(Util.encode37(s));
			zf.zb(s1);
			int i = kf();
			zf.cc(i);
			zf.end();
			zf.ji();
			int j = zf.read();
			zf.close();
			System.out.println("Newplayer response: " + j);
			if(j == 0) {
				we();
				return;
			}
			if(j == 2) {
				gf(pf[8], pf[9]);
				return;
			}
			if(j == 3) {
				gf(pf[14], pf[15]);
				return;
			}
			if(j == 4) {
				gf(pf[4], pf[5]);
				return;
			}
			if(j == 5) {
				gf(pf[16], pf[17]);
				return;
			}
			if(j == 6) {
				gf(pf[18], pf[19]);
				return;
			}
			if(j == 7) {
				gf(pf[20], pf[21]);
				return;
			}
			if(j == 11) {
				gf(pf[22], pf[23]);
				return;
			}
			if(j == 12) {
				gf(pf[24], pf[25]);
				return;
			} else {
				gf(pf[12], pf[13]);
				return;
			}
		}
		catch(Exception exception) {
			uf = vf;
			System.out.println(String.valueOf(exception));
			gf(pf[12], pf[13]);
			return;
		}
	}

	public void ue() {
		if(zf != null) {
			zf.begin(1);
			zf.end();
			xf = "";
			yf = "";
			xe();
		}
	}

	public void lf(String s, String s1) {
		xf = s;
		s = Util.clean(s, 20);
		yf = s1;
		s1 = Util.clean(s1, 20);
		if(s.length() == 0 || s1.length() == 0) {
			xe();
			return;
		}
		for(long l = System.currentTimeMillis(); System.currentTimeMillis() - l < 60000L;) {
			oe(pf[2], pf[3]);
			try {
				if(applet()) {
					zf = b.xb(uf, this, wf);
				} else {
					zf = b.xb(uf, null, wf);
				}
				byte abyte0[] = {
					127, 0, 0, 1
				};
				try {
					InetAddress inetaddress = zf.ac();
					abyte0 = inetaddress.getAddress();
				}
				catch(Exception _ex) { }
				if(abyte0[0] == 127 && abyte0[1] == 0 && abyte0[2] == 0 && abyte0[3] == 1) {
					try {
						InetAddress inetaddress1 = InetAddress.getLocalHost();
						abyte0 = inetaddress1.getAddress();
					}
					catch(Exception _ex) { }
				}
				zf.begin(19);
				zf.wb(Util.encode37(s));
				zf.zb(s1);
				zf.ec(clientRevision);
				zf.fc(abyte0[0]);
				zf.fc(abyte0[1]);
				zf.fc(abyte0[2]);
				zf.fc(abyte0[3]);
				int i = kf();
				zf.cc(i);
				zf.ic();
				zf.ji();
				int j = zf.read();
				if(j == 0) {
					re();
					ff();
					return;
				}
				if(j >= 1 && j <= 6) {
					s = "";
					s1 = "";
					xe();
					return;
				}
			}
			catch(Exception _ex) {
				oe(pf[2], pf[3]);
			}
			oe(pf[2], pf[3]);
			try {
				Thread.sleep(5000L);
			}
			catch(Exception _ex) { }
		}

		s = "";
		s1 = "";
		xe();
	}

	public void jf() {
		System.out.println("Lost connection");
		lf(xf, yf);
	}

	public void oe(String s, String s1) {
		Graphics g = getGraphics();
		Font font = new Font("Helvetica", 1, 15);
		int i = ui();
		int j = bj();
		g.setColor(Color.black);
		g.fillRect(i / 2 - 140, j / 2 - 25, 280, 50);
		g.setColor(Color.white);
		g.drawRect(i / 2 - 140, j / 2 - 25, 280, 50);
		dj(g, s, font, i / 2, j / 2 - 10);
		dj(g, s1, font, i / 2, j / 2 + 10);
	}

	public void re() {
		ag = 0;
		bg = 0;
		sf = -500;
		fg = 0;
	}

	public void ye() {
		long l = System.currentTimeMillis();
		eg = l;
	}

	public void qe() {
		try {
			long l = System.currentTimeMillis();
			if(l - eg > 5000L) {
				eg = l;
				zf.begin(5);
				zf.ic();
			}
			if(!te()) {
				return;
			}
			sf++;
			if(sf > rf) {
				re();
				jf();
				return;
			}
			if(ag == 0 && zf.available() >= 2) {
				ag = zf.ji();
			}
			if(ag > 0 && zf.available() >= ag) {
				zf.read(ag, cg);
				bg = Util.unsign(cg[0]);
				sf = 0;
				if(bg == 8) {
					String s = new String(cg, 1, ag - 1);
					ze(Util.rc(s, true));
				}
				if(bg == 9) {
					ue();
				}
				if(bg == 10) {
					ef();
				} else
				if(bg == 23) {
					fg = Util.unsign(cg[1]);
					for(int i = 0; i < fg; i++) {
						gg[i] = Util.pc(cg, 2 + i * 9);
						hg[i] = Util.unsign(cg[10 + i * 9]);
					}

				} else
				if(bg == 24) {
					long l1 = Util.pc(cg, 1);
					int k = cg[9] & 0xff;
					for(int i1 = 0; i1 < fg; i1++) {
						if(gg[i1] == l1) {
							if(hg[i1] == 0 && k != 0) {
								ze("@pri@" + Util.decode37(l1) + " has logged in");
							}
							if(hg[i1] != 0 && k == 0) {
								ze("@pri@" + Util.decode37(l1) + " has logged out");
							}
							hg[i1] = k;
							ag = 0;
							return;
						}
					}

					gg[fg] = l1;
					hg[fg] = k;
					fg++;
					ze("@pri@" + Util.decode37(l1) + " has been added to your friends list");
				} else
				if(bg == 26) {
					ig = Util.unsign(cg[1]);
					for(int j = 0; j < ig; j++) {
						jg[j] = Util.pc(cg, 2 + j * 8);
					}

				} else
				if(bg == 27) {
					kg = cg[1];
					lg = cg[2];
					mg = cg[3];
					ng = cg[4];
					og = cg[5];
				} else
				if(bg == 28) {
					long l2 = Util.pc(cg, 1);
					String s1 = new String(cg, 9, ag - 9);
					if(l2 != Util.encode37(xf)) {
						s1 = Util.rc(s1, true);
					}
					ze("@pri@" + Util.decode37(l2) + ": tells you " + s1);
				} else {
					ve(bg, ag, cg);
				}
				ag = 0;
				return;
			}
		}
		catch(IOException _ex) {
			jf();
		}
	}

	public void bf(String s) {
		s = Util.clean(s, 20);
		zf.begin(25);
		zf.zb(s);
		zf.end();
	}

	public void af(int i, int j, int k, int l, int i1) {
		zf.begin(31);
		zf.fc(i);
		zf.fc(j);
		zf.fc(k);
		zf.fc(l);
		zf.fc(i1);
		zf.end();
	}

	public void cf(String s) {
		long l = Util.encode37(s);
		zf.begin(29);
		zf.wb(l);
		zf.end();
		for(int i = 0; i < ig; i++) {
			if(jg[i] == l) {
				return;
			}
		}

		if(ig >= 50) {
			return;
		} else {
			jg[ig++] = l;
			return;
		}
	}

	public void mf(long l) {
		zf.begin(30);
		zf.wb(l);
		zf.end();
		for(int i = 0; i < ig; i++) {
			if(jg[i] == l) {
				ig--;
				for(int j = i; j < ig; j++) {
					jg[j] = jg[j + 1];
				}

				return;
			}
		}

	}

	public void hf(String s) {
		zf.begin(26);
		zf.wb(Util.encode37(s));
		zf.end();
	}

	public void se(long l) {
		zf.begin(27);
		zf.wb(l);
		zf.end();
		for(int i = 0; i < fg; i++) {
			if(gg[i] != l) {
				continue;
			}
			fg--;
			for(int j = i; j < fg; j++) {
				gg[j] = gg[j + 1];
				hg[j] = hg[j + 1];
			}

			break;
		}

		ze("@pri@" + Util.decode37(l) + " has been removed from your friends list");
	}

	public void pe(long l, String s) {
		if(s.length() > 80) {
			s = s.substring(0, 80);
		}
		zf.begin(28);
		zf.wb(l);
		zf.fc(s.length());
		zf.zb(s);
		zf.end();
		ze("@pri@You tell " + Util.decode37(l) + ": " + s);
	}

	public boolean _mthif(String s) {
		if(s.toLowerCase().startsWith("tell ")) {
			s = s.substring(5);
			int i = s.indexOf(' ');
			if(i == -1 || i >= s.length() - 1) {
				ze("You must type a message too!");
				return true;
			} else {
				String s1 = s.substring(0, i);
				s = s.substring(i + 1);
				pe(Util.encode37(s1), s);
				return true;
			}
		}
		zf.begin(3);
		zf.zb(s);
		zf.end();
		eg = dg = System.currentTimeMillis();
		if(qf) {
			ze("@yel@" + xf.trim() + ": @whi@" + s);
		}
		return false;
	}

	public void gf(String s, String s1) {
	}

	public void ff() {
	}

	public void ne() {
	}

	public void xe() {
	}

	public void ef() {
	}

	public void we() {
	}

	public void ve(int i, int j, byte abyte0[]) {
	}

	public void ze(String s) {
	}

	public boolean te() {
		return true;
	}

	public MultiGame() {
		uf = "127.0.0.1";
		vf = "server3.runescape.com";
		wf = 43594;
		xf = "";
		yf = "";
		cg = new byte[5000];
		gg = new long[50];
		hg = new int[50];
		jg = new long[50];
	}

	public static String pf[];
	public static boolean qf = true;
	public static int rf = 0x5f5e0ff;
	public static int sf;
	public static int clientRevision = 1;
	public String uf;
	public String vf;
	public int wf;
	String xf;
	String yf;
	public b zf;
	int ag;
	int bg;
	byte cg[];
	long dg;
	long eg;
	public int fg;
	public long gg[];
	public int hg[];
	public int ig;
	public long jg[];
	public int kg;
	public int lg;
	public int mg;
	public int ng;
	public int og;

	static  {
		pf = new String[50];
		pf[0] = "You must enter both a username";
		pf[1] = "and a password - Please try again";
		pf[2] = "Connection lost! Please wait...";
		pf[3] = "Attempting to re-establish";
		pf[4] = "That username is already in use.";
		pf[5] = "Wait 60 seconds then retry";
		pf[6] = "Please wait...";
		pf[7] = "Connecting to server";
		pf[8] = "Sorry! The server is currently full.";
		pf[9] = "Please try again later";
		pf[10] = "Invalid username or password.";
		pf[11] = "Try again, or create a new account";
		pf[12] = "Sorry! Unable to connect to server.";
		pf[13] = "Check your internet settings";
		pf[14] = "Username already taken.";
		pf[15] = "Please choose another username";
		pf[16] = "The client has been updated.";
		pf[17] = "Please reload this page";
		pf[18] = "You may only use 1 character at once.";
		pf[19] = "Your ip-address is already in use";
		pf[20] = "Login attempts exceeded!";
		pf[21] = "Please try again in 1 minute";
		pf[22] = "Account has been disabled for 1-week";
		pf[23] = "for cheating or abuse";
		pf[24] = "Account has been permanently disabled";
		pf[25] = "for cheating or abuse";
	}
}
