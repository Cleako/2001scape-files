package jagex;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Stream {
	
	static final int equals = '=';
	static final int semicolon = ';';
	static final int asterisk = '*';
	static final int plus = '+';
	static final int comma = ',';
	static final int minus = '-';
	static final int period = '.';
	static final int forwardSlash = '/';
	static final int backSlash = '\\';
	static final int space = ' ';
	static final int pipe = '|';
	static final int exclaimation = '!';
	static final int quotation = '"';

	static final char[] charTable = new char[256];

	static  {
		for(int i = 0; i < 256; i++) {
			charTable[i] = (char) i;
		}

		charTable[equals] = equals;
		charTable[semicolon] = semicolon;
		charTable[asterisk] = asterisk;
		charTable[plus] = plus;
		charTable[comma] = comma;
		charTable[minus] = minus;
		charTable[period] = period;
		charTable[forwardSlash] = forwardSlash;
		charTable[backSlash] = backSlash;
		charTable[pipe] = pipe;
		charTable[exclaimation] = exclaimation;
		charTable[quotation] = quotation;
	}
	
	protected InputStream input;
	protected OutputStream output;
	protected Socket clientSock;
	protected ServerSocket servSock;
	protected boolean closed;
	protected byte buffer[];
	int off;

	public Stream(InputStream in) {
		closed = false;
		input = in;
	}

	public Stream(Socket socket) throws IOException {
		closed = false;
		clientSock = socket;
		input = socket.getInputStream();
		output = socket.getOutputStream();
	}

	public Stream(String file) throws IOException {
		closed = false;
		input = Util.openStream(file);
	}

	public Stream(byte[] data) {
		closed = false;
		buffer = data;
		off = 0;
		closed = true;
	}

	public Stream(byte[] data, int offset) {
		closed = false;
		buffer = data;
		off = offset;
		closed = true;
	}

	public Stream(int port) throws IOException {
		closed = false;
		servSock = new ServerSocket(port);
	}

	public Stream(String ip, int port) throws IOException {
		closed = false;
		clientSock = new Socket(InetAddress.getByName(ip), port);
		clientSock.setSoTimeout(10000);
		input = clientSock.getInputStream();
		output = clientSock.getOutputStream();
	}

	public void close() {
		if(closed) {
			return;
		}
		if(output != null) {
			try {
				output.flush();
			} catch(IOException e) {
				/*  nothing*/
			}
		}
		try {
			if(servSock != null) {
				servSock.close();
			}
			if(clientSock != null) {
				clientSock.close();
			}
			if(input != null) {
				input.close();
			}
			if(output != null) {
				output.close();
			}
		} catch(IOException e) {
			System.out.println("Error closing stream");
		}
	}
	
	public int available() throws IOException {
		if(closed) {
			return 0;
		} else {
			return input.available();
		}
	}

	public int read() throws IOException {
		if(buffer != null) {
			return buffer[off++];
		}
		if(closed) {
			return 0;
		} else {
			return input.read();
		}
	}
	
	public void read(int amount, byte dest[]) throws IOException {
		if(closed) {
			return;
		}
		int read;
		for(int i = 0; i < amount; i += read) {
			if((read = input.read(dest, i, amount - i)) <= 0) {
				throw new IOException("EOF");
			}
		}

	}

	// reads a short?
	public int ji() throws IOException {
		int multiplier = read();
		int advance = read();
		return (multiplier * 256) + advance;
	}

	public void next() throws IOException {
		for(int i = read(); i != equals && i != -1; i = read()) { }
	}

	public int readStrToInt() throws IOException {
		int val = 0;
		int ch;
		boolean negate = false;
		
		// reads until a non-numeric character
		for(ch = read(); ch < '0' || ch > '9';) {
			if(ch == minus) {
				negate = true;
			}
			ch = read();
			if(ch == -1) {
				throw new IOException("Eof!");
			}
		}

		for(; ch >= '0' && ch <= '9'; ch = read()) {
			val = (val * 10 + ch) - '0';
		}

		if(negate) {
			val = -val;
		}
		return val;
	}

	public int readStrHexToInt() throws IOException {
		int val = 0;
		int ch;

		for(ch = read(); (ch < '0' || ch > '9') && (ch < 'a' || ch > 'f') && (ch < 'A' || ch > 'F');) {
			ch = read();
			if(ch == -1) {
				throw new IOException("Eof!");
			}
		}

		do {
			if(ch >= '0' && ch <= '9') {
				val = (val * 16 + ch) - '0';
			} else
			if(ch >= 'a' && ch <= 'f') {
				val = (val * 16 + ch + 10) - 'a';
			} else {
				if(ch < 'A' || ch > 'F') {
					break;
				}
				val = (val * 16 + ch + 10) - 'A';
			}
			ch = read();
		} while(true);
		return val;
	}
	
	// looks like this function parses a programming language?
	// possibly runescript? it reads typical grammar of a language
	public String readString() throws IOException {
		String text = "";
		int ch;
		boolean quote = false;

		for(ch = read(); ch < space || ch == comma || ch == semicolon || ch == equals;) {
			ch = read();
			if(ch == -1) {
				throw new IOException("Eof!");
			}
		}

		if(ch == quotation) {
			quote = true;
			ch = read();
		}
		for(; ch != -1; ch = read()) {
			if(!quote && (ch == comma || ch == equals || ch == semicolon) || quote && ch == quotation) {
				break;
			}
			text = text + charTable[ch];
		}

		return text;
	}

}
