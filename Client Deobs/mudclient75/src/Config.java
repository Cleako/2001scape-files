import jagex.Stream;
import jagex.Util;

import java.io.IOException;

public class Config {

	public static void loadConfig() {
		try {
			loadProjectiles(new Stream("../gamedata/config/projectile.txt"));
			loadEntities(new Stream("../gamedata/config/entity.txt"));
			loadObjects(new Stream("../gamedata/config/objects.txt"));
			loadNpcs(new Stream("../gamedata/config/npc.txt"));
			loadLocations(new Stream("../gamedata/config/location.txt"));
			loadBoundaries(new Stream("../gamedata/config/boundary.txt"));
			loadRoofs(new Stream("../gamedata/config/roof.txt"));
			loadFloors(new Stream("../gamedata/config/floor.txt"));
			loadSpells(new Stream("../gamedata/config/spells.txt"));
			loadShops(new Stream("../gamedata/config/shop.txt"));
			loadPrayers(new Stream("../gamedata/config/prayers.txt"));
			resolveProjectileObjects();
		} catch(IOException e) {
			System.out.println("Error loading config files");
			e.printStackTrace();
		}
	}

	public static void loadConfig(byte[] archive) {
		try {
			loadProjectiles(new Stream(archive, Util.archiveOffset("projectile.txt", archive)));
			loadEntities(new Stream(archive, Util.archiveOffset("entity.txt", archive)));
			loadObjects(new Stream(archive, Util.archiveOffset("objects.txt", archive)));
			loadNpcs(new Stream(archive, Util.archiveOffset("npc.txt", archive)));
			loadLocations(new Stream(archive, Util.archiveOffset("location.txt", archive)));
			loadBoundaries(new Stream(archive, Util.archiveOffset("boundary.txt", archive)));
			loadRoofs(new Stream(archive, Util.archiveOffset("roof.txt", archive)));
			loadFloors(new Stream(archive, Util.archiveOffset("floor.txt", archive)));
			loadSpells(new Stream(archive, Util.archiveOffset("spells.txt", archive)));
			loadShops(new Stream(archive, Util.archiveOffset("shop.txt", archive)));
			loadPrayers(new Stream(archive, Util.archiveOffset("prayers.txt", archive)));
			resolveProjectileObjects();
		} catch(IOException e) {
			System.out.println("Error loading config files");
			e.printStackTrace();
		}
	}

	public static void loadShops(Stream strean) throws IOException {
		strean.next();
		int shopCount = strean.readStrToInt();
		yo = shopCount;
		System.out.println("Found " + shopCount + " shops");
		shopNames = new String[shopCount];
		ap = new int[shopCount];
		ep = new int[shopCount];
		bp = new int[shopCount];
		cp = new int[shopCount];
		dp = new int[shopCount];
		fp = new int[shopCount][40];
		gp = new int[shopCount][40];
		hp = new int[shopCount][40];
		for (int k = 0; k < shopCount; k++) {
			strean.next();
			shopNames[k] = strean.readString();
			int l = ap[k] = strean.readStrToInt();
			
			bp[k] = strean.readStrToInt();
			cp[k] = strean.readStrToInt();
			dp[k] = strean.readStrToInt();
			ep[k] = strean.readStrToInt();

			//System.out.println(shopNames[k] + ", bp " + bp[k] + ", cp " + cp[k] + ", dp " + dp[k] + ", ep " + ep[k]);
			for (int i1 = 0; i1 < l; i1++) {
				strean.next();
				fp[k][i1] = matchObject(strean.readString());
				gp[k][i1] = strean.readStrToInt();
				hp[k][i1] = strean.readStrToInt();
			}
		}
	}

	public static void loadSpells(Stream m1) throws IOException {
		m1.next();
		int i = m1.readStrToInt();
		qo = i;
		System.out.println("Found " + i + " spells");
		ro = new String[i];
		so = new String[i];
		to = new int[i];
		vo = new int[i];
		uo = new int[i];
		wo = new int[i][];
		xo = new int[i][];
		for(int k = 0; k < i; k++) {
			m1.next();
			ro[k] = m1.readString();
			to[k] = m1.readStrToInt();
			so[k] = m1.readString();
			vo[k] = m1.readStrToInt();
			m1.next();
			int l = uo[k] = m1.readStrToInt();
			wo[k] = new int[l];
			xo[k] = new int[l];
			for(int i1 = 0; i1 < l; i1++) {
				wo[k][i1] = matchObject(m1.readString());
				xo[k][i1] = m1.readStrToInt();
			}

		}

		m1.close();
	}

	public static void loadPrayers(Stream m1) throws IOException {
		m1.next();
		int i = m1.readStrToInt();
		ip = i;
		System.out.println("Found " + i + " prayers");
		jp = new String[i];
		kp = new String[i];
		lp = new int[i];
		mp = new int[i];
		for(int k = 0; k < i; k++) {
			m1.next();
			jp[k] = m1.readString();
			lp[k] = m1.readStrToInt();
			kp[k] = m1.readString();
			mp[k] = m1.readStrToInt();
		}

		m1.close();
	}

	public static void loadProjectiles(Stream m1) throws IOException {
		m1.next();
		int i = m1.readStrToInt();
		projectileCount = i;
		System.out.println("Found " + i + " projectiles");
		io = new String[i];
		projectileObjects = new String[i];
		ko = new int[i];
		lo = new int[i];
		mo = new int[i];
		no = new int[i];
		projectileObjectIds = new int[i];
		po = new int[i];
		for(int k = 0; k < i; k++) {
			m1.next();
			io[k] = m1.readString();
			ko[k] = m1.readStrToInt();
			lo[k] = m1.readStrToInt();
			mo[k] = m1.readStrToInt();
			no[k] = m1.readStrToInt();
			projectileObjects[k] = m1.readString();
			po[k] = m1.readStrToInt();
			if(ko[k] + 1 > ho) {
				ho = ko[k] + 1;
			}
		}

		m1.close();
	}

	public static void resolveProjectileObjects() {
		for(int i = 0; i < projectileCount; i++) {
			projectileObjectIds[i] = matchObject(projectileObjects[i]);
		}
	}

	public static void loadEntities(Stream stream) throws IOException {
		stream.next();
		int i = stream.readStrToInt();
		entityCount = i;
		System.out.println("Found " + i + " entities");
		tm = new String[i];
		um = new String[i];
		wm = new int[i];
		vm = new int[i];
		xm = new int[i];
		ym = new int[i];
		zm = new int[i];
		for(int k = 0; k < i; k++) {
			stream.next();
			tm[k] = stream.readString();
			um[k] = stream.readString();
			vm[k] = stream.readStrHexToInt();
			wm[k] = stream.readStrToInt();
			xm[k] = stream.readStrToInt();
			ym[k] = stream.readStrToInt();
		}

		stream.close();
	}

	public static void loadNpcs(Stream stream) throws IOException {
		stream.next();
		int i = stream.readStrToInt();
		npcCount = i;
		System.out.println("Found " + i + " npcs");
		npcNames = new String[i][];
		npcExamines = new String[i];
		rl = new int[i];
		sl = new int[i];
		tl = new int[i];
		ul = new int[i];
		vl = new int[i];
		wl = new int[i];
		xl = new int[i];
		yl = new int[i];
		zl = new int[i];
		am = new int[i];
		bm = new int[i];
		cm = new int[i];
		dm = new int[i];
		em = new int[i];
		fm = new int[i];
		npcEntityIds = new int[i][12];
		npcColors1 = new int[i];
		npcColors2 = new int[i];
		npcColors3 = new int[i];
		npcColors4 = new int[i];
		lm = new int[i];
		mm = new int[i];
		nm = new int[i];
		om = new int[i];
		pm = new int[i];
		npcDropIds = new int[i][];
		npcDropAmounts = new int[i][];
		for(int k = 0; k < i; k++) {
			stream.next();
			int l = stream.readStrToInt();
			npcNames[k] = new String[l];
			for(int i1 = 0; i1 < l; i1++) {
				npcNames[k][i1] = stream.readString();
			}

			npcExamines[k] = stream.readString();
			stream.next();
			rl[k] = stream.readStrToInt();
			sl[k] = stream.readStrToInt();
			tl[k] = stream.readStrToInt();
			ul[k] = stream.readStrToInt();
			vl[k] = stream.readStrToInt();
			wl[k] = stream.readStrToInt();
			xl[k] = stream.readStrToInt();
			yl[k] = stream.readStrToInt();
			zl[k] = stream.readStrToInt();
			am[k] = stream.readStrToInt();
			bm[k] = stream.readStrToInt();
			cm[k] = stream.readStrToInt();
			dm[k] = stream.readStrToInt();
			em[k] = stream.readStrToInt();
			fm[k] = stream.readStrToInt();
			stream.next();
			for(int j1 = 0; j1 < 12; j1++) {
				npcEntityIds[k][j1] = matchEntity(stream.readString());
			}

			npcColors1[k] = stream.readStrHexToInt();
			npcColors2[k] = stream.readStrHexToInt();
			npcColors3[k] = stream.readStrHexToInt();
			npcColors4[k] = stream.readStrHexToInt();
			stream.next();
			lm[k] = stream.readStrToInt();
			mm[k] = stream.readStrToInt();
			nm[k] = stream.readStrToInt();
			om[k] = stream.readStrToInt();
			pm[k] = stream.readStrToInt();
			stream.next();
			int tableSize = stream.readStrToInt();
			npcDropIds[k] = new int[tableSize];
			npcDropAmounts[k] = new int[tableSize];
			
			for(int l1 = 0; l1 < tableSize; l1++) {
				npcDropIds[k][l1] = matchObject(stream.readString());
				npcDropAmounts[k][l1] = stream.readStrToInt();
			}
		}

		stream.close();
	}

	public static void loadObjects(Stream m1) throws IOException {
		m1.next();
		int i = m1.readStrToInt();
		objectCount = i;
		System.out.println("Found " + i + " objects");
		vk = new String[i][];
		wk = new String[i];
		yk = new String[i];
		zk = new int[i];
		al = new int[i];
		bl = new int[i];
		cl = new int[i];
		dl = new int[i];
		el = new int[i];
		fl = new int[i];
		gl = new int[i];
		hl = new int[i];
		il = new int[i];
		jl = new int[i];
		kl = new int[i];
		ll = new int[i];
		ml = new int[i];
		nl = new int[i];
		for(int k = 0; k < i; k++) {
			m1.next();
			int l = m1.readStrToInt();
			vk[k] = new String[l];
			for(int i1 = 0; i1 < l; i1++) {
				vk[k][i1] = m1.readString();
			}

			wk[k] = m1.readString();
			m1.next();
			zk[k] = m1.readStrToInt();
			if(zk[k] >= uk) {
				uk = zk[k] + 1;
			}
			al[k] = m1.readStrToInt();
			bl[k] = m1.readStrToInt();
			yk[k] = m1.readString();
			m1.next();
			cl[k] = m1.readStrToInt();
			dl[k] = m1.readStrToInt();
			el[k] = m1.readStrToInt();
			fl[k] = m1.readStrToInt();
			gl[k] = m1.readStrToInt();
			hl[k] = m1.readStrToInt();
			il[k] = matchProjectile(m1.readString()) + 1;
			m1.next();
			jl[k] = m1.readStrToInt();
			kl[k] = matchEntity(m1.readString());
			ll[k] = m1.readStrHexToInt();
			ml[k] = m1.readStrToInt();
			nl[k] = m1.readStrToInt();
		}

		m1.close();
	}

	public static void loadLocations(Stream m1) throws IOException {
		m1.next();
		int i = m1.readStrToInt();
		locationCount = i;
		System.out.println("Found " + i + " locations");
		bn = new String[i][];
		cn = new String[i];
		dn = new String[i];
		en = new String[i];
		fn = new int[i];
		gn = new int[i];
		hn = new int[i];
		in = new int[i];
		jn = new int[i];
		kn = new int[i];
		for(int k = 0; k < i; k++) {
			m1.next();
			int l = m1.readStrToInt();
			bn[k] = new String[l];
			for(int i1 = 0; i1 < l; i1++) {
				bn[k][i1] = m1.readString();
			}

			cn[k] = m1.readString();
			m1.next();
			fn[k] = matchModel(m1.readString());
			gn[k] = m1.readStrToInt();
			hn[k] = m1.readStrToInt();
			in[k] = m1.readStrToInt();
			jn[k] = m1.readStrToInt();
			dn[k] = m1.readString();
			if(dn[k].equals("_")) {
				dn[k] = "WalkTo";
			}
			en[k] = m1.readString();
			if(en[k].equals("_")) {
				en[k] = "Examine";
			}
			kn[k] = m1.readStrToInt();
		}

		m1.close();
	}

	public static void loadBoundaries(Stream m1) throws IOException {
		m1.next();
		int i = m1.readStrToInt();
		boundaryCount = i;
		System.out.println("Found " + i + " boundaries");
		mn = new String[i][];
		nn = new String[i];
		on = new String[i];
		pn = new String[i];
		qn = new int[i];
		rn = new int[i];
		sn = new int[i];
		tn = new int[i];
		un = new int[i];
		vn = new int[i];
		for(int k = 0; k < i; k++) {
			m1.next();
			int l = m1.readStrToInt();
			mn[k] = new String[l];
			for(int i1 = 0; i1 < l; i1++) {
				mn[k][i1] = m1.readString();
			}

			nn[k] = m1.readString();
			m1.next();
			qn[k] = m1.readStrToInt();
			rn[k] = m1.readStrToInt();
			sn[k] = m1.readStrToInt();
			tn[k] = m1.readStrToInt();
			un[k] = m1.readStrToInt();
			vn[k] = m1.readStrToInt();
			on[k] = m1.readString();
			if(on[k].equals("_")) {
				on[k] = "WalkTo";
			}
			pn[k] = m1.readString();
			if(pn[k].equals("_")) {
				pn[k] = "Examine";
			}
		}

		m1.close();
	}

	public static void loadRoofs(Stream m1) throws IOException {
		m1.next();
		int i = m1.readStrToInt();
		roofCount = i;
		System.out.println("Found " + i + " roofs");
		xn = new String[i];
		yn = new int[i];
		zn = new int[i];
		ao = new int[i];
		for(int k = 0; k < i; k++) {
			m1.next();
			xn[k] = m1.readString();
			yn[k] = m1.readStrToInt();
			zn[k] = m1.readStrToInt();
			ao[k] = m1.readStrToInt();
		}

		m1.close();
	}

	public static void loadFloors(Stream stream) throws IOException {
		stream.next();
		int i = stream.readStrToInt();
		floorCount = i;
		System.out.println("Found " + i + " floors");
		co = new String[i];
		_flddo = new int[i];
		eo = new int[i];
		fo = new int[i];
		for(int k = 0; k < i; k++) {
			stream.next();
			co[k] = stream.readString();
			_flddo[k] = stream.readStrToInt();
			eo[k] = stream.readStrToInt();
			fo[k] = stream.readStrToInt();
		}

		stream.close();
	}

	public static int matchEntity(String s) {
		if(s.equalsIgnoreCase("na")) {
			return -1;
		}
		for(int i = 0; i < entityCount; i++) {
			if(s.equalsIgnoreCase(tm[i])) {
				return i;
			}
		}

		System.out.println("WARNING: unable to match entity " + s);
		return 0;
	}

	public static int matchModel(String s) {
		if(s.equalsIgnoreCase("na")) {
			return 0;
		}
		for(int i = 0; i < modelCount; i++) {
			if(qp[i].equalsIgnoreCase(s)) {
				return i;
			}
		}

		qp[modelCount++] = s;
		return modelCount - 1;
	}

	public static int matchObject(String s) {
		if(s.equalsIgnoreCase("na")) {
			return 0;
		}
		for(int i = 0; i < objectCount; i++) {
			for(int k = 0; k < vk[i].length; k++) {
				if(vk[i][k].equalsIgnoreCase(s)) {
					return i;
				}
			}

		}

		System.out.println("WARNING: unable to match object: " + s);
		return 0;
	}

	public static int matchProjectile(String s) {
		if(s.equals("_")) {
			return -1;
		}
		for(int i = 0; i < projectileCount; i++) {
			if(io[i].equalsIgnoreCase(s)) {
				return i;
			}
		}

		System.out.println("WARNING: unable to match projectile: " + s);
		return -1;
	}

	public final int sk = 0xbc614e;
	public static int objectCount;
	public static int uk;
	public static String vk[][];
	public static String wk[];
	public static String xk[];
	public static String yk[];
	public static int zk[];
	public static int al[];
	public static int bl[];
	public static int cl[];
	public static int dl[];
	public static int el[];
	public static int fl[];
	public static int gl[];
	public static int hl[];
	public static int il[];
	public static int jl[];
	public static int kl[];
	public static int ll[];
	public static int ml[];
	public static int nl[];
	public static int npcCount;
	public static String npcNames[][];
	public static String npcExamines[];
	public static int rl[];
	public static int sl[];
	public static int tl[];
	public static int ul[];
	public static int vl[];
	public static int wl[];
	public static int xl[];
	public static int yl[];
	public static int zl[];
	public static int am[];
	public static int bm[];
	public static int cm[];
	public static int dm[];
	public static int em[];
	public static int fm[];
	public static int npcEntityIds[][];
	public static int npcColors1[];
	public static int npcColors2[];
	public static int npcColors3[];
	public static int npcColors4[];
	public static int lm[];
	public static int mm[];
	public static int nm[];
	public static int om[];
	public static int pm[];
	public static int npcDropIds[][];
	public static int npcDropAmounts[][];
	public static int entityCount;
	public static String tm[];
	public static String um[];
	public static int vm[];
	public static int wm[];
	public static int xm[];
	public static int ym[];
	public static int zm[];
	public static int locationCount;
	public static String bn[][];
	public static String cn[];
	public static String dn[];
	public static String en[];
	public static int fn[];
	public static int gn[];
	public static int hn[];
	public static int in[];
	public static int jn[];
	public static int kn[];
	public static int boundaryCount;
	public static String mn[][];
	public static String nn[];
	public static String on[];
	public static String pn[];
	public static int qn[];
	public static int rn[];
	public static int sn[];
	public static int tn[];
	public static int un[];
	public static int vn[];
	public static int roofCount;
	public static String xn[];
	public static int yn[];
	public static int zn[];
	public static int ao[];
	public static int floorCount;
	public static String co[];
	public static int _flddo[];
	public static int eo[];
	public static int fo[];
	public static int projectileCount;
	public static int ho;
	public static String io[];
	public static String projectileObjects[];
	public static int ko[];
	public static int lo[];
	public static int mo[];
	public static int no[];
	public static int projectileObjectIds[];
	public static int po[];
	public static int qo;
	public static String ro[];
	public static String so[];
	public static int to[];
	public static int uo[];
	public static int vo[];
	public static int wo[][];
	public static int xo[][];
	public static int yo;
	public static String shopNames[];
	public static int ap[];
	public static int bp[];
	public static int cp[];
	public static int dp[];
	public static int ep[];
	public static int fp[][];
	public static int gp[][];
	public static int hp[][];
	public static int ip;
	public static String jp[];
	public static String kp[];
	public static int lp[];
	public static int mp[];
	static String skills[] = {
		"attack", "defense", "strength", "hits", "ranged", "prayer", "magic", "cooking", "woodcutting", "fletching", 
		"fishing", "firemaking", "crafting", "smithing", "mining", "herblaw"
	};
	public static String op[] = {
		"attack", "defense", "strength", "hits", "aggression", "bravery", "regenerate", "perception"
	};
	public static int modelCount;
	public static String qp[] = new String[200];

}
